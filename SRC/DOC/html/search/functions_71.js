var searchData=
[
  ['q_5fisempty',['q_isEmpty',['../queue_8c.html#a4b64a24bf411c7556fc87e5d3c98ef13',1,'q_isEmpty(Queue l):&#160;queue.c'],['../queue_8h.html#a5e17f8074a3e9f8c00daaf2d3b4a486f',1,'q_isEmpty(Queue):&#160;queue.c']]],
  ['q_5fpeek',['q_peek',['../queue_8c.html#a44e3c19ac18d3aa0f058fcd6ebff334d',1,'q_peek(Queue q, int *size):&#160;queue.c'],['../queue_8h.html#a16107321ae8b80f8dcb7778aa7d85ed2',1,'q_peek(Queue, int *):&#160;queue.c']]],
  ['q_5fpop',['q_pop',['../queue_8c.html#a940644742f24f79aaae96e6787fa62ee',1,'q_pop(Queue q, int *size):&#160;queue.c'],['../queue_8h.html#a46db6fc37633e5c20adeddd410cd32ae',1,'q_pop(Queue, int *):&#160;queue.c']]],
  ['q_5fpush',['q_push',['../queue_8c.html#a7d3ccf030d7cd68a1efcc5f7d6b3dfbd',1,'q_push(Queue q, void *val, int size):&#160;queue.c'],['../queue_8h.html#a3a6ec55045d2948ee6a872e1c915499b',1,'q_push(Queue, void *, int):&#160;queue.c']]]
];
