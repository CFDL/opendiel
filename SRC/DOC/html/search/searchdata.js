var indexSectionsWithContent =
{
  0: "_abcdefghiklmnopqrstuvw",
  1: "acdegimqrst",
  2: "adeilmqrt",
  3: "acdfgimnpqrst",
  4: "abcdefghiklmnopqrstuvw",
  5: "cdegiqrt",
  6: "ce",
  7: "cps",
  8: "_acdefgilmnpqrst"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "enums",
  7: "enumvalues",
  8: "defines"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Enumerations",
  7: "Enumerator",
  8: "Macros"
};

