/*
 * Copyright (c) 2015 University of Tennessee
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>

#include "libconfig.h"

#include "IEL_exec_info.h"

/**
 * Reads the arguments from the config file and puts them in the appropriate structure.
 *
 * @param exec_info -- pointer to the IEL Executive info struct
 * @param dep -- pointer to config setting struct representing the array of arguments
 * @param mod_num -- the current module number
 *
 * @returns 0 on success, -1 on failure.
 */
  static int
init_args(IEL_exec_info_t *exec_info, config_setting_t *dep, int mod_num)
{
  int i,j;

  i = mod_num;

  exec_info->modules[i].mod_argc = config_setting_length(dep);
  if(exec_info->modules[i].mod_argc < 0) {
    fprintf(stderr, "module %d has negative argument = %lld\n", i, 
        (unsigned long long) exec_info->modules[i].mod_argc);
    return -1;
  }

  exec_info->modules[i].mod_argv = (char **)malloc(sizeof(char *) * exec_info->modules[i].mod_argc);

  for(j = 0; j < exec_info->modules[i].mod_argc; j++)
  {
    config_setting_t *e = config_setting_get_elem(dep, j);
    exec_info->modules[i].mod_argv[j] = dupstr(config_setting_get_string(e));
  }

  return 0;
}
/* ZT -- Aug 2017 */
/**
 * Reads the tags from the config file and puts them in the appropriate structure.
 *
 * @param exec_info -- pointer to the IEL Executive info struct
 * @param dep -- pointer to config setting struct representing the array of arguments
 * @param mod_num -- current module number
 *
 * @returns 0 on success, -1 on failure.
 */
int init_tags (IEL_exec_info_t *exec_info, config_setting_t *dep, int mod_num)
{
  int i;
  config_setting_t *e;

  exec_info->modules[mod_num].num_tags= config_setting_length(dep);
  if (exec_info->modules[mod_num].num_tags < 0) {
    fprintf(stderr, "module %d has negative tag = %lld\n", mod_num, 
        (unsigned long long) exec_info->modules[mod_num].num_tags);
    return -1;
  }

  exec_info->modules[mod_num].mod_tags = (char **) malloc(sizeof(char *) * exec_info->modules[mod_num].num_tags);
  for (i = 0; i < exec_info->modules[mod_num].num_tags; i++) {
    e = config_setting_get_elem(dep, i);
    exec_info->modules[mod_num].mod_tags[i] = dupstr(config_setting_get_string(e));
  }

  return 0;
}

/**
 * Prints the args for a certain module to stdout (for debugging purposes).
 *
 * @param exec_info -- pointer to the IEL Executive info struct
 * @param mod_num -- the module's number
 */
  static void
dump_args(IEL_exec_info_t *exec_info, int mod_num)
{
  int i,j;

  i = mod_num;

  //printf("Module %d:\n", i);
  //printf("%d args:\n", exec_info->modules[i].mod_argc);
  for(j = 0; j < exec_info->modules[i].mod_argc; j++)
  {
    //printf("%s ", exec_info->modules[i].mod_argv[j]);
  }
  //printf("\n");
  return;
}

/**
 * initializes a IEL Executive info data structure by reading in the information
 * from the specified configuration file.
 *
 * @param exec_info -- pointer to the IEL Executive info struct to initialize
 * @param cfg_filename -- the name of the configuration file
 *
 * @returns 0 on success, -1 on failure.
 */
  int
IEL_exec_config(IEL_exec_info_t *exec_info, char *cfg_filename)
{
  config_t cfg;
  config_setting_t *setting;
  int i, j, k;
  const char *server_conf;
  char *cwd; // Check the return value of getcwd with this variable

  /* this macro wipes out / cleans up the struct in case of error
   * to avoid memory leaks
   */
#define cleanup_before_err_ret() \
  do {  \
    if(exec_info->module_sizes) free(exec_info->module_sizes); \
    memset(exec_info, 0, sizeof(IEL_exec_info_t)); \
  } while(0);

  /* go ahead and wipe out the struct */
  memset(exec_info, 0, sizeof(IEL_exec_info_t));
  IEL_exec_clear(exec_info);

  //initialize configuration
  config_init(&cfg);

  //read in config file
  if(!config_read_file(&cfg, cfg_filename))
  {
    fprintf(stderr, "%s:%d - %s\n", config_error_file(&cfg),
        config_error_line(&cfg), config_error_text(&cfg));
    cleanup_before_err_ret();
    config_destroy(&cfg);
    return -1;
  }

  /* TC, Nov 2016: No longer requires num_shared_bc to be in the file if shared bc are not used */
  //read in num_shared_bc array
  if((config_lookup_int(&cfg, "num_shared_bc", &exec_info->num_shared_bc)) == CONFIG_FALSE) {
    DBG("num_shared_bc setting not found; setting num_shared_bc to 0");
    exec_info->num_shared_bc = 0;
  }

  //read in tuple size option
  int tupleSize;
  if(!config_lookup_int(&cfg, "tuple_space_size", &tupleSize)) {
    fprintf(stderr, "\"tuple_space_size\" option not set. If not using tuple space, set to 0.\n");
    cleanup_before_err_ret();
    config_destroy(&cfg);
    return -1;
  }

  /* FB, Jul 2018: Read in the number of GPUs available on the system. 
     NOTE: Could get this automatically in the future with 
     cudaGetDeviceCount(), which returns the number of CUDA devices */
  int num_gpu_available, num_gpu_available_found; 
  DBG("Looking up number of GPUs available"); 
  num_gpu_available_found = config_lookup_int(&cfg, "num_gpu_available", &num_gpu_available);
  if ( !num_gpu_available_found ) {
    DBG("Couldn't find num_gpu_available setting, defaulting to 0");
    num_gpu_available = 0;
  }
  DBG("Setting exec_info->num_gpu_available to %d", num_gpu_available);
  exec_info->num_gpu_available = num_gpu_available;

  /* ZT -- Oct 2017 */
  // Read in the server_configuration option
  if (!config_lookup_string(&cfg, "server_configuration", &server_conf)) {
    server_conf = "";
  }
  exec_info->server_config = dupstr(server_conf);
  /* TC, Feb 2016: default tuple_rank to -1 if present, -2 if not present;
   * default tuple_group and tuple_set to -1
   */
  exec_info->tuple_size = tupleSize;
  if (tupleSize == 0)
    exec_info->tuple_rank = -2;
  else
    exec_info->tuple_rank = -1;
  exec_info->tuple_group = -1;
  exec_info->tuple_set = -1;

  //read in modules list
  if((setting = config_lookup(&cfg, "modules"))==NULL) {
    fprintf(stderr, "missing modules in config file\n");
    cleanup_before_err_ret();
    config_destroy(&cfg);
    return -1;
  }

  //set number of modules
  exec_info->num_modules = config_setting_length(setting);

  /* ZT - Feb 2018 - Simplified error checking malloc calls by creating
   * a wrapper function for malloc() that does error checking */

  //allocate for the size of each module
  exec_info->module_sizes = (int *) IEL_alloc(sizeof(int) * exec_info->num_modules);
  
  //allocate for the cores of each module
  exec_info->module_cores = (int *) IEL_alloc(sizeof(int) * exec_info->num_modules);
  
  //allocate for the processes of each module
  exec_info->module_processes = (int *) IEL_alloc(sizeof(int) * exec_info->num_modules);
  
  //allocate for the threads of each module
  exec_info->module_threads = (int *) IEL_alloc(sizeof(int) * exec_info->num_modules);

  //allocate for the number of copies of each module
  exec_info->module_copies = (int *) IEL_alloc(sizeof(int) * exec_info->num_modules);

  //allocate for the number of GPUs for each module 
  exec_info->module_num_gpu = (int *) IEL_alloc(sizeof(int) * exec_info->num_modules);
  
  //allocate for the module dependency structs
  exec_info->modules = (module_depend_t *)IEL_alloc(sizeof(module_depend_t) * exec_info->num_modules);
  
  exec_info->num_proc = 0;

  //this loop populates the library name and type, the function name, the num_procs,
  //and calls functions to populate args
  for(i = 0; i < exec_info->num_modules; ++i)
  {
    config_setting_t *mod = config_setting_get_elem(setting, i);
    config_setting_t *args;
//    config_setting_t *tags;
    config_setting_t *shared_bc_setting, *bc_pair_setting;
    const char *function, *library, *libtype;
    char *splitdir = NULL, *splitdirpath = NULL, *exec_mode = NULL, *stdinFile = NULL;
    // ZT - NOTE: Should these be changed to unsigned values?
    int size, processes, threads, copies, cores, num_gpu;
    int size_set = 0;
    int num_gpu_set = 0;
    int copies_set = 0;
    int procs_set = 0;
    int emode;
    int incomplete = 0;

    if(!config_setting_lookup_string(mod, "function", &function)) incomplete = 1;
    if(!(args=config_setting_get_member(mod, "args"))) incomplete = 1;
    /* ZT -- Aug 2017
     * Added tags to the config file */
//    if(!(tags=config_setting_get_member(mod, "tags"))) incomplete = 1;
    if(!config_setting_lookup_string(mod, "libtype", &libtype)) incomplete = 1;
    if(!config_setting_lookup_string(mod, "library", &library))
    {
      if(strcmp(libtype,"static")) incomplete = 1;
    }
    /* TC, June 2016: Check copies, size, and processes to see which need to be defaulted */
    size_set = config_setting_lookup_int(mod, "size", &size);
    copies_set = config_setting_lookup_int(mod, "copies", &copies);
    procs_set = config_setting_lookup_int(mod, "processes_per_copy", &processes);
    if(!procs_set)
      procs_set = config_setting_lookup_int(mod, "processes", &processes);

    if(!size_set && !copies_set && !procs_set) incomplete = 1;

    /* FB, July 2018: Look up the number of GPUs and error check.
       Default the value to 0 for modules that don't specify num_gpu */ 
    DBG("Looking up num_gpu setting for %s", function);
    num_gpu_set = config_setting_lookup_int(mod, "num_gpu", &num_gpu);
    if (!num_gpu_set) {
      DBG("Couldn't find num_gpu setting, defaulting to 0 for %s", function);
      num_gpu = 0; 
    } else if ( num_gpu_available < num_gpu) {
      fprintf(stderr, "WARNING: number of GPUs assigned to %s exceeds number available \nSetting num_gpu to %d\n", 
              function, num_gpu_available);
      num_gpu = num_gpu_available;
    }
    exec_info->module_num_gpu[i] = num_gpu;

    /* TC, Nov 2016: Set up direct communication settings */
    DBG("Setting up direct communication settings for module %d", i);
    // Initialize reading and writing to 0, then set them to 1 if the module
    // can read from/write to them
    shared_bc_setting = config_setting_get_member(mod, "shared_bc_read");
    // Defaults uses_bc to false; only create the shared_bc array if true
    exec_info->modules[i].uses_bc = 0;

    if(shared_bc_setting != NULL) {
      // Allocate both bc_read and bc_write, even if only setting one is present
      exec_info->modules[i].shared_bc_read = (int*)calloc(exec_info->num_shared_bc, sizeof(int));
      exec_info->modules[i].shared_bc_write =(int*)calloc(exec_info->num_shared_bc, sizeof(int));
      exec_info->modules[i].uses_bc = 1;
      DBG("shared_bc_read present on module %d", exec_info->module_num);
      int num_pairs = config_setting_length(shared_bc_setting);
      // Set shared_bc_read elements
      for(j = 0; j < num_pairs; j++) {
        bc_pair_setting = config_setting_get_elem(shared_bc_setting, j);
        int lower, upper;
        lower = config_setting_get_int_elem(bc_pair_setting, 0);
        upper = config_setting_get_int_elem(bc_pair_setting, 1);

        // Check for validity; output and skip module if problem occurs
        if(lower > upper || upper > exec_info->num_shared_bc || lower < 0) {
          printf("Error with shared_bc_read on module - incorrect pair%d\n", i);
          incomplete = 1;
          break;
        }

        // Set all elements in the interval (inclusive) to readable
        for(k = lower; k <= upper; k++) {
          exec_info->modules[i].shared_bc_read[k] = 1;
        }
      }
    }
    shared_bc_setting = config_setting_get_member(mod, "shared_bc_write");
    if(shared_bc_setting != NULL) {
      // If the read setting is not present, allocate both the read and write settings
      if(exec_info->modules[i].uses_bc != 1) {
        exec_info->modules[i].shared_bc_read = (int*)calloc(exec_info->num_shared_bc, sizeof(int));
        exec_info->modules[i].shared_bc_write = (int*)calloc(exec_info->num_shared_bc, sizeof(int));
        exec_info->modules[i].uses_bc = 1;
      }
      DBG("shared_bc_write present on module %d", exec_info->module_num);
      int num_pairs = config_setting_length(shared_bc_setting);
      // Set shared_bc_read elements
      for(j = 0; j < num_pairs; j++) {
        bc_pair_setting = config_setting_get_elem(shared_bc_setting, j);
        int lower, upper;
        lower = config_setting_get_int_elem(bc_pair_setting, 0);
        upper = config_setting_get_int_elem(bc_pair_setting, 1);

        // Check for validity; output and skip module if problem occurs
        if(lower > upper || upper > exec_info->num_shared_bc || lower < 0) {
          printf("Error with shared_bc_write on module %d - incorrect pair\n", i);
          incomplete = 1;
          break;
        }

        // Set all elements in the interval (inclusive) to writable
        for(k = lower; k <= upper; k++) {
          exec_info->modules[i].shared_bc_write[k] = 1;
        }
      }
    }
    
    /* End direct communication setup */

    if(incomplete == 1)
    {
      printf("Skipping incomplete module %d\n",i);
      continue;
    }

    char freesdp = 0; //Boolean value for if we need to deallocate splitdirpath when we are done
    //Check if this module needs separate working directories
    if(!config_setting_lookup_string(mod, "splitdir", (const char**)&splitdir)) {
      exec_info->modules[i].splitdir = 0;
      splitdirpath = "";
    } else if (!strcmp(splitdir,"true")) {
      exec_info->modules[i].splitdir = 1;
      freesdp = 1;
      splitdirpath = (char*)malloc(4096);
      cwd = getcwd(splitdirpath, 4096);
      if (cwd == NULL) {
        perror("getcwd() error");
        cleanup_before_err_ret();
        config_destroy(&cfg);
        return -1;
      }
    } else if (!strcmp(splitdir,"false")) {
      exec_info->modules[i].splitdir = 0;
      splitdirpath = "";
    } else {
      exec_info->modules[i].splitdir = 1;
      splitdirpath = splitdir;
    }

    if(!config_setting_lookup_string(mod, "exec_mode", (const char**)&exec_mode)) {
      emode = SERIAL;
    } else if(strcmp(exec_mode, "parallel") == 0) {
      emode = PARALLEL;
    } else {
      emode = SERIAL;
    }

    DBG("about to lookup string");
    if(config_setting_lookup_string(mod, "stdin", (const char**)&stdinFile)) {
      exec_info->modules[i].stdinFile = dupstr(stdinFile);
    } else {
      exec_info->modules[i].stdinFile = dupstr("");
      DBG("STRING NOT FOUND");
    }
    DBG("STRING IS %s\n", exec_info->modules[i].stdinFile);

    exec_info->modules[i].emode = emode;

    exec_info->modules[i].splitdirpath = dupstr(splitdirpath);

    if(freesdp) {
      free(splitdirpath);
    }

    /* TC, June 2016: Size is now set later to work with ordering; cores, 
     * processes, threads, copies are now set
     */
    // Set up default assumptions based on what values are given in config file
    if(copies_set && !size_set) {
      size = copies;
      if(emode == PARALLEL && !procs_set) {
        processes = 1;
        fprintf(stderr, "Warning: parallel module %d has no specified processes but has specified copies. Defaulting to 1 process per copy.\n", i);
      }
    } else if(size_set && !copies_set) {
      if(emode == SERIAL) {
        copies = size;
      } else if(emode == PARALLEL && !procs_set) {
        // Have parallel modules assume one copy by default
        fprintf(stderr, "Warning: parallel module %d has no specified number of processes but has specified size. Defaulting to 1 copy of size processes.\n", i);
        copies = 1;
        processes = size;
      } else if(emode == PARALLEL && procs_set) {
        copies = size / processes;
      }
    } else if(!size_set && !copies_set) {
      size = processes;
      copies = 1;
    } else if(emode == PARALLEL && !procs_set) {
      // Roundoff error will be dealt with later, when sanity checking size
      processes = size / copies;
    }

    // Set number of processes per copy; default to 1 if serial.
    // Should only ever be larger than 1 on parallel modules.
    if(emode == SERIAL && !procs_set) {
      processes = 1;
    }
    if(emode == SERIAL && processes > 1) {
      fprintf(stderr, "Warning: more than one process set for serial module %d. Consider using the 'copies' setting instead.\n", i);
    }

    exec_info->module_sizes[i] = size;
    exec_info->module_copies[i] = copies;
    exec_info->module_processes[i] = processes;

    // Set number of threads; default to 1
    threads = 1;
    // Use keyword "threads_per_process" or "threads"
    config_setting_lookup_int(mod, "threads_per_process", &threads);
    config_setting_lookup_int(mod, "threads", &threads);
    exec_info->module_threads[i] = threads;
    if(threads > 1 && exec_info->modules[i].splitdir) {
      fprintf(stderr, "Error: threads used in module with a specified splitdir\n");
      cleanup_before_err_ret();
      config_destroy(&cfg);
      return -1;
    }

    // Set number of copies; default to size if serial, 1 otherwise
    copies = 1;
    if(emode == SERIAL) {
      copies = size;
    }
    config_setting_lookup_int(mod, "copies", &copies);
    exec_info->module_copies[i] = copies;

    // Check to ensure that size value input is correct
    if(processes * copies != size) {
      fprintf(stderr, "Error: incorrect size value for module %d. Size must equal processes_per_copy * copies.\n", i);
      cleanup_before_err_ret();
      config_destroy(&cfg);
      return -1;
    }

    // Set number of cores required for each copy; default to processes times threads
    cores = processes * copies * threads;
    config_setting_lookup_int(mod, "num_cores", &cores);
    exec_info->module_cores[i] = cores;

    // Check to ensure that cores value input is correct
    if(threads * copies * processes != cores) {
      fprintf(stderr, "Error: incorrect cores value for module %d. Cores must equal processes_per_copy * threads_per_process * copies.\n", i);
      cleanup_before_err_ret();
      config_destroy(&cfg);
      return -1;
    }

    DBG("Debug values for module %d: size = %d, processes = %d, threads = %d, cores = %d, copies = %d", i, size,
        processes, threads, cores, copies);
    //exec_info->num_proc += size;

    if(library != NULL && strcmp(libtype,"static")) //If it's not blank, copy
      exec_info->modules[i].libname = dupstr(library);
    else //Otherwise, create an empty string
      exec_info->modules[i].libname = dupstr("");

    exec_info->modules[i].libtype = dupstr(libtype);

    exec_info->modules[i].funcname = dupstr(function);

    if(init_args(exec_info, args, i) < 0) {
      fprintf(stderr, "init_args failed\n");
      cleanup_before_err_ret();
      config_destroy(&cfg);
    }
   
    /* ZT -- Aug 2017 
     * Reads in tags from the config file and stores them in the proper 
     * data structure in the same way that the args are read from the 
     * config file/stored. *
     * NOTE: Tags have been removed for the time being, but all functionality
     * to implement them has been left in with the original comments so that
     * they can easily be re-implemented in the future if desired.
     */
    /*
    if(init_tags(exec_info, tags, i) < 0) {
      fprintf(stderr, "init_tags failed\n");
      cleanup_before_err_ret();
      config_destroy(&cfg);
    }
*/
    /* TC, Nov 2016: Default number of workflow occurrences of the module to 0 */
    exec_info->modules[i].num_head_ranks = 0;

    /*print args for debugging*/
    dump_args(exec_info, i);

    fprintf(stderr, "Initialized module %d with %d core(s) and %d copy/copies.\n", i, exec_info->module_cores[i], exec_info->module_copies[i]);
  }

  /*Read in workflow information*/

  DBG("About to start reading workflow information");

  config_setting_t *groups;
  config_setting_t *group;
  config_setting_t *order;
  config_setting_t *s;
  config_setting_t *depends;
  int iter, group_size; /*persist,*/
  size_t num_sets, num_groups, order_size, num_dep;

  setting = config_lookup(&cfg, "workflow");
  if(setting == NULL) {
    fprintf(stderr, "Malformed workflow configuration. workflow section does not exist\n");
    cleanup_before_err_ret();
    config_destroy(&cfg);
    return -1;
  }

  /* TC, May 2016: Rather than having one set of groups called "groups," allow for as
   * many as are desired by the user.
   */
  num_sets = config_setting_length(setting);
  exec_info->num_work_sets = num_sets;
  exec_info->num_work_groups = (int*)malloc(sizeof(int) * num_sets);
  exec_info->work_groups = (IEL_workflow_group_t **)malloc(sizeof(IEL_workflow_group_t*) * num_sets);
  exec_info->num_runs = (int*)malloc(sizeof(int) * num_sets);
  exec_info->work_set_sizes = (int*)malloc(sizeof(int) * num_sets);
  int total_proc = 0; // TC, June 2016: For finding each group's leader

  for(int set = 0; set < num_sets; set++) {
    groups = config_setting_get_elem(setting, set);
    if(groups == NULL) {
      fprintf(stderr, "Malformed workflow configuration. workflow.groups section does not exist\n");
      cleanup_before_err_ret();
      config_destroy(&cfg);
      return -1;
    }

    int set_size = 0; // Number of ranks in the set

    /* TC, May 2016: Check for number of runs for groups, and remove it so that we don't count it as a group */
    int num_runs = 1; // Default to one run
    if(config_setting_lookup_int(groups, "num_set_runs", &num_runs) == CONFIG_TRUE) {
      config_setting_remove(groups, "num_set_runs");
    }
    exec_info->num_runs[set] = num_runs;
    DBG("Setting num_runs to %d", num_runs);

    num_groups = config_setting_length(groups);
    exec_info->num_work_groups[set] = num_groups;
    exec_info->work_groups[set] =  (IEL_workflow_group_t *)malloc(num_groups * sizeof(IEL_workflow_group_t));

    DBG("Reading in groups");

    for(i = 0; i < num_groups; i++) {

      group = config_setting_get_elem(groups, i);


      order = config_setting_get_member(group, "order");
      if(order == NULL) {
        fprintf(stderr, "Malformed workflow configuration. All groups must specify order property\n");
        cleanup_before_err_ret();
        config_destroy(&cfg);
        return -1;
      }

      order_size = config_setting_length(order);
      exec_info->work_groups[set][i].num_modules = order_size;

      exec_info->work_groups[set][i].order = (char**)malloc(order_size * sizeof(char*));

      for(j = 0; j < order_size; j++) {
        s = config_setting_get_elem(order, j);
        exec_info->work_groups[set][i].order[j] = dupstr(config_setting_get_string(s));
      }

      /* TC, Feb 2016: Set group leader */
      exec_info->work_groups[set][i].leader = total_proc;

      /* TC, Nov 2016: Set the head rank of each module in the group;
       *  used for sending between modules; involves string comparisons,
       *  which could be improved if modules were given numbers upon
       *  first reading into the order, but this should be fine for
       *  our intents
       */
      for(j = 0; j < order_size; j++) {
        for(k = 0; k < exec_info->num_modules; k++) {
          if(strcmp(exec_info->work_groups[set][i].order[j], exec_info->modules[k].funcname) == 0) {
            exec_info->modules[k].head_ranks[exec_info->modules[k].num_head_ranks] = total_proc;
            DBG("Added head rank %d to module %d", 
                exec_info->modules[k].head_ranks[exec_info->modules[k].num_head_ranks], k);
            exec_info->modules[k].num_head_ranks++;
            if(exec_info->modules[k].num_head_ranks > MOD_MAX_WF) {
              printf("Error: maximum number of occurences of module %d in workflow (%d) exceeded\n", k, MOD_MAX_WF);
              cleanup_before_err_ret();
            }
          }
        }
      }

      /* TC, May 2016: Set tuple rank, group and set */
      if(exec_info->tuple_rank == -1 && 
          strcmp(config_setting_get_string_elem(config_setting_get_member(group, "order"), 0), "ielTupleServer") == 0) {
        exec_info->tuple_rank = exec_info->work_groups[set][i].leader;
        exec_info->tuple_group = i;
        exec_info->tuple_set = set;
        DBG("Tuple server set to rank %d, group %d, set %d", exec_info->tuple_rank, i, set);
      }

      if(!config_setting_lookup_int(group, "size", &group_size)) {
        /*If group size is not specified, find the max size of contained modules*/
        group_size = -1;

        for(j = 0; j < order_size; j++) {
          for(k = 0; k < exec_info->num_modules; k++) {
            if(strcmp(exec_info->work_groups[set][i].order[j], exec_info->modules[k].funcname) == 0) {
              if(group_size < exec_info->module_sizes[k]) group_size = exec_info->module_sizes[k];
            }
          }
        }
        if(group_size == -1) {
          fprintf(stderr, "Fatal Error: No group size attribute found. Alternative size calculation failed.\n");
          return -1;
        }
      }
      set_size += group_size;
      /* TC, June 2016: Set num_proc based on actual number of procs determined by workflow */
      exec_info->num_proc += group_size;

      total_proc += group_size;

      exec_info->work_groups[set][i].size = group_size;

      if(!config_setting_lookup_int(group, "iterations", &iter))
        exec_info->work_groups[set][i].iterations = 1;
      else
        exec_info->work_groups[set][i].iterations = iter;

      /* TC, Feb 2016: Set up group dependencies */
      /* Find the number of groups that the current one depends on, if any*/
      depends = config_setting_get_member(group, "depends");

      if(depends == NULL) {
        exec_info->work_groups[set][i].num_dep = 0;
        num_dep = 0;
      } else {
        num_dep = config_setting_length(depends);
        exec_info->work_groups[set][i].num_dep = num_dep;
      }

      /* If this group depends on any groups, set them*/
      exec_info->work_groups[set][i].dep = (int*)malloc(num_dep * sizeof(int));

      for(int j = 0; j < num_dep; j++) {
        if(set == exec_info->tuple_set && i == exec_info->tuple_group) {
          fprintf(stderr, "Error: the tuple server cannot have dependencies\n");
          return -1;
        }
        char *groupname = (char*)(config_setting_get_string_elem(depends, j));

        config_setting_t* check_group;

        /* Check to see if the groups we need are in the file; if not, error */
        int infile = 0;
        for(int k = 0; k < num_groups; k++) {
          check_group = config_setting_get_elem(groups, k);
          if (strcmp(groupname, config_setting_name(check_group)) == 0) {
            infile++;
            /* Check for incorrect dependencies */
            if(set == exec_info->tuple_set && k == exec_info->tuple_group) {
              fprintf(stderr, "Error: nothing can depend on the tuple server\n");
              return -1;
            }
            exec_info->work_groups[set][i].dep[j] = k;
            break;
          }
        }
        if (infile == 0) {
          fprintf(stderr, "Error: required groups not found for group %d", i);
          return -1;
        }
      }
    }
    exec_info->work_set_sizes[set] = set_size;
  }

  DBG("Workflow information read");
  fflush(stdout);

  /*End workflow information*/

  config_destroy(&cfg);

  return 0;
}

void *IEL_alloc(size_t size)
{
  void *rv;
  
  rv = malloc(size);
  Assert(rv != NULL, "In IEL_alloc malloc returned NULL");

  return rv;
}
