/*
 * Copyright (c) 2015 University of Tennessee
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "IEL_exec_info.h"

/**
 * An implementation of the non-standard "strdup" function
 * that was being used in the IEL even though not all relevant
 * compilers support it.
 *
 * @param str -- pointer to string to be duplicated
 *
 * @returns dup, pointer to a duplicate of str
 */
char *dupstr(const char *str)
{
  int n = strlen(str) + 1;
  char *dup = (char*)malloc(n * sizeof(char));
  if(dup)
  {
    strcpy(dup, str);
  }
  return dup;
}

/**
 * Clears a IEL_exec_info_t struct, setting all pointers to NULL.
 *
 * @param exec_info -- pointer to the IEL Executive info struct
 *
 * @returns 0 on success and -1 on failure.
 */
  int
IEL_exec_clear(IEL_exec_info_t *exec_info)
{
  exec_info->module_sizes = NULL;

  exec_info->modules = NULL;

  exec_info->packed = NULL;

  exec_info->shared_bc = NULL;

  return 0;	
}

/**
 * Returns the upper bound on the size of the buffer needed to
 * transmit the IEL Executive data structure via MPI.
 *
 * @param exec_info -- pointer to the IEL Executive info struct 
 * @param comm -- the MPI communicator
 *
 * @returns the size in bytes needed for the buffer.
 *   on failure, returns -1.
 */
  int
pack_size_ub(IEL_exec_info_t *exec_info, MPI_Comm comm)
{

  int i, j, rv, sum = 0, size, slen;

  rv = MPI_Pack_size(1, MPI_INT, comm, &size);
  if(rv != MPI_SUCCESS)
    return -1;
  sum += size;

  rv = MPI_Pack_size(1, MPI_INT, comm, &size);
  if(rv != MPI_SUCCESS)
    return -1;
  sum += size;

  /* Count num_shared_bc*/
  rv = MPI_Pack_size(1, MPI_INT, comm, &size);
  if(rv != MPI_SUCCESS)
    return -1;
  sum += size;

  /* Count size of each module */
  rv = MPI_Pack_size(exec_info->num_modules, MPI_INT, comm, &size);
  if(rv != MPI_SUCCESS)
    return -1;
  sum += size;

  /*TC, June 2016: count number of cores per module*/
  rv = MPI_Pack_size(exec_info->num_modules, MPI_INT, comm, &size);
  if(rv != MPI_SUCCESS)
    return -1;
  sum += size;

  /*TC, June 2016: count number of processs per module*/
  rv = MPI_Pack_size(exec_info->num_modules, MPI_INT, comm, &size);
  if(rv != MPI_SUCCESS)
    return -1;
  sum += size;

  /*TC, June 2016: count number of threads per module*/
  rv = MPI_Pack_size(exec_info->num_modules, MPI_INT, comm, &size);
  if(rv != MPI_SUCCESS)
    return -1;
  sum += size;

  /*TC, June 2016: count number of copies to run of each module*/
  rv = MPI_Pack_size(exec_info->num_modules, MPI_INT, comm, &size);
  if(rv != MPI_SUCCESS)
    return -1;
  sum += size;

  /*FB, July 2018: count number of GPUs to run of each module*/
  rv = MPI_Pack_size(exec_info->num_modules, MPI_INT, comm, &size);
  if(rv != MPI_SUCCESS)
    return -1;
  sum += size;

  /*FB, Feb 2016: count num_gpu_available*/
  rv = MPI_Pack_size(1, MPI_INT, comm, &size);
  if(rv != MPI_SUCCESS)
    return -1;
  sum += size;

  /*TC, Feb 2016: count tuple_rank*/
  rv = MPI_Pack_size(1, MPI_INT, comm, &size);
  if(rv != MPI_SUCCESS)
    return -1;
  sum += size;

  /*TC, May 2016: count tuple group*/
  rv = MPI_Pack_size(1, MPI_INT, comm, &size);
  if(rv != MPI_SUCCESS)
    return -1;
  sum += size;

  /*TC, May 2016: count tuple set*/
  rv = MPI_Pack_size(1, MPI_INT, comm, &size);
  if(rv != MPI_SUCCESS)
    return -1;
  sum += size;

  /* ZT, Sept 2017: include Server_config */
  rv = MPI_Pack_size(1, MPI_INT, comm, &size);
  if(rv != MPI_SUCCESS)
    return -1;
  sum += size;

  slen = strlen(exec_info->server_config) +1;

  rv = MPI_Pack_size(slen, MPI_CHAR, comm, &size);
  if (rv != MPI_SUCCESS) return -1;
  sum += size;

  for(i=0;i<exec_info->num_modules;i++) {
    slen = strlen(exec_info->modules[i].libname) + 1;

    rv = MPI_Pack_size(1, MPI_INT, comm, &size);
    if(rv != MPI_SUCCESS)
      return -1;
    sum += size;

    rv = MPI_Pack_size(slen, MPI_CHAR, comm, &size);
    if(rv != MPI_SUCCESS)
      return -1;
    sum += size;

    slen = strlen(exec_info->modules[i].libtype) + 1;

    rv = MPI_Pack_size(1, MPI_INT, comm, &size);
    if(rv != MPI_SUCCESS)
      return -1;
    sum += size;

    rv = MPI_Pack_size(slen, MPI_CHAR, comm, &size);
    if(rv != MPI_SUCCESS)
      return -1;
    sum += size;

    MPI_Pack_size(1, MPI_CHAR, comm, &size);
    sum += size;

    slen = strlen(exec_info->modules[i].splitdirpath) + 1;

    MPI_Pack_size(1, MPI_INT, comm, &size);
    sum += size;

    MPI_Pack_size(slen, MPI_CHAR,comm, &size);
    sum += size;

    slen = strlen(exec_info->modules[i].funcname) + 1;

    rv = MPI_Pack_size(1, MPI_INT, comm, &size);
    if(rv != MPI_SUCCESS)
      return -1;
    sum += size;

    rv = MPI_Pack_size(slen, MPI_CHAR, comm, &size);
    if(rv != MPI_SUCCESS)
      return -1;
    sum += size;

    rv = MPI_Pack_size(1, MPI_INT, comm, &size);
    if(rv != MPI_SUCCESS)
      return -1;
    sum += size;

    /*include mod_argc*/
    rv = MPI_Pack_size(1, MPI_INT, comm, &size);
    if(rv != MPI_SUCCESS)
      return -1;
    sum += size;

    /*include mod_argv*/
    for(j = 0; j < exec_info->modules[i].mod_argc; j++)
    {
      slen = strlen(exec_info->modules[i].mod_argv[j]) + 1;

      rv = MPI_Pack_size(1, MPI_INT, comm, &size);
      if(rv != MPI_SUCCESS)
        return -1;
      sum += size;

      rv = MPI_Pack_size(slen, MPI_CHAR, comm, &size);
      if(rv != MPI_SUCCESS)
        return -1;
      sum += size;


      rv = MPI_Pack_size(1, MPI_INT, comm, &size);
      if(rv != MPI_SUCCESS)
        return -1;
      sum += size;
    }

    /* ZT -- Aug 2017, Include num_tags */
    /*
    rv = MPI_Pack_size(1, MPI_INT, comm, &size);
    if (rv != MPI_SUCCESS) return -1;
    sum += size;
*/
    /* ZT -- Aug 2017, Include mod_tags*/
    /*
    for (j = 0; j < exec_info->modules[i].num_tags; j++) {
      slen = strlen(exec_info->modules[i].mod_tags[j]) +1;

      rv = MPI_Pack_size(1, MPI_INT, comm, &size);
      if (rv != MPI_SUCCESS) return -1;
      sum += size;

      rv = MPI_Pack_size(slen, MPI_CHAR, comm, &size);
      if (rv != MPI_SUCCESS) return -1;
      sum += size;

      rv = MPI_Pack_size(1, MPI_INT, comm, &size);
      if (rv != MPI_SUCCESS) return -1; 
      sum += size;
    }
*/
    //emode
    rv = MPI_Pack_size(1, MPI_INT, comm, &size);
    if(rv != MPI_SUCCESS)
      return -1;
    sum += size;

    //stdinsize
    slen = strlen(exec_info->modules[i].stdinFile) + 1;
    rv = MPI_Pack_size(1, MPI_INT, comm, &size);
    if(rv != MPI_SUCCESS)
      return -1;
    sum += size;

    rv = MPI_Pack_size(slen, MPI_CHAR, comm, &size);
    if(rv != MPI_SUCCESS)
      return -1;
    sum += size;
  
    /* TC, Nov 2016: Count num_head_ranks */
    rv = MPI_Pack_size(1, MPI_INT, comm, &size);
    if(rv != MPI_SUCCESS)
      return -1;
    sum += size;

    /* TC, Nov 2016: Count head_ranks */
    rv = MPI_Pack_size(exec_info->modules[i].num_head_ranks, MPI_INT, comm, &size);
    if(rv != MPI_SUCCESS)
      return -1;
    sum += size;
  }

  /*Count workflow information*/

  rv = MPI_Pack_size(1, MPI_INT, comm, &size);
  if(rv != MPI_SUCCESS)
    return -1;
  sum += size;

  /*TC, May 2016: Account for sets*/
  /*Count num_work_sets*/
  rv = MPI_Pack_size(1, MPI_INT, comm, &size);
  sum += size;
  if(rv != MPI_SUCCESS)
    return -1;

  /*Count num_work_groups*/
  rv = MPI_Pack_size(exec_info->num_work_sets, MPI_INT, comm, &size);
  sum += size;
  if(rv != MPI_SUCCESS)
    return -1;

  /*Count work_set_sizes*/
  rv = MPI_Pack_size(exec_info->num_work_sets, MPI_INT, comm, &size);
  sum += size;
  if(rv != MPI_SUCCESS)
    return -1;

  for(int set = 0; set < exec_info->num_work_sets; set++) {
    for(i = 0; i < exec_info->num_work_groups[set]; i++) {
      /*Add the 3 integer data fields (currently not including perist)*/
      rv = MPI_Pack_size(3, MPI_INT, comm, &size);
      sum += size;
      if(rv != MPI_SUCCESS)
        return -1;

      for(j = 0; j < exec_info->work_groups[set][i].num_modules; j++) {

        /*Need to pack size of string*/
        rv = MPI_Pack_size(1, MPI_INT, comm, &size);
        if(rv != MPI_SUCCESS)
          return -1;
        sum += size;

        /*Calculate size and account for actual string*/
        slen = strlen(exec_info->work_groups[set][i].order[j]) + 1;
        rv  = MPI_Pack_size(slen, MPI_CHAR, comm, &size);
        if(rv != MPI_SUCCESS)
          return -1;

        sum += size;

        /*number of dependencies and leader*/
        rv = MPI_Pack_size(2, MPI_INT, comm, &size);
        if(rv != MPI_SUCCESS)
          return -1;
        sum += size;

        /*dependencies*/
        rv = MPI_Pack_size(exec_info->work_groups[set][i].num_dep, MPI_INT, comm, &size);
        if(rv != MPI_SUCCESS)
          return -1;
        sum += size;
      }
    }
  }

  /*end workflow information*/

  /*Count direct communication info*/
  for(i = 0; i < exec_info->num_modules; i++) {
    /*uses_bc*/
    rv = MPI_Pack_size(1, MPI_INT, comm, &size);
    if(rv != MPI_SUCCESS)
      return -1;
    sum += size;

    if(exec_info->modules[i].uses_bc == 1) {
      /*shared_bc_read and shared_bc_write*/
      rv = MPI_Pack_size(exec_info->num_shared_bc * 2, MPI_INT, comm, &size);
      if(rv != MPI_SUCCESS)
        return -1;
      sum += size;
    }
  }
  /*end direct communication info*/

  /*Count num_runs*/
  rv = MPI_Pack_size(exec_info->num_work_sets, MPI_INT, comm, &size);
  if(rv != MPI_SUCCESS)
    return -1;
  sum += size;

  return sum;
}

/**
 * Packs the IEL Executive info struct into a buffer to be transmitted via MPI.
 *
 * @param exec_info -- pointer to the IEL Executive info struct 
 * @param comm -- the MPI communicator
 *
 * @returns 0 on success.
 *   on failure, returns -1.
 */
  int
IEL_exec_pack_info(IEL_exec_info_t *exec_info, MPI_Comm comm)
{
  int i, j, rv, size, pos = 0, slen;

  size = pack_size_ub(exec_info, comm);

  exec_info->packsize = size;
  exec_info->packed = (char *)malloc(size);

  rv = MPI_Pack(&(exec_info->num_modules), 1, MPI_INT, (void *)exec_info->packed,
      size, &pos, comm);
  if(rv != MPI_SUCCESS)
    return -1;

  /*Pack num_shared_bc*/
  rv = MPI_Pack(&(exec_info->num_shared_bc), 1, MPI_INT, (void*)exec_info->packed, size, &pos, comm);
  if(rv != MPI_SUCCESS)
    return -1;

  /* Pack size of each module */
  rv = MPI_Pack(exec_info->module_sizes, exec_info->num_modules, MPI_INT,
      (void *)exec_info->packed, size, &pos, comm);
  if(rv != MPI_SUCCESS)
    return -1;

  /*TC, June 2016: Pack number of cores per module*/
  rv = MPI_Pack(exec_info->module_cores, exec_info->num_modules, MPI_INT,
      (void *)exec_info->packed, size, &pos, comm);
  if(rv != MPI_SUCCESS)
    return -1;

  /*TC, June 2016: Pack number of processs per module*/
  rv = MPI_Pack(exec_info->module_processes, exec_info->num_modules, MPI_INT,
      (void *)exec_info->packed, size, &pos, comm);
  if(rv != MPI_SUCCESS)
    return -1;

  /*TC, June 2016: Pack number of threads per module*/
  rv = MPI_Pack(exec_info->module_threads, exec_info->num_modules, MPI_INT,
      (void *)exec_info->packed, size, &pos, comm);
  if(rv != MPI_SUCCESS)
    return -1;

  /*TC, June 2016: Pack number of copies to run of each module*/
  rv = MPI_Pack(exec_info->module_copies, exec_info->num_modules, MPI_INT,
      (void *)exec_info->packed, size, &pos, comm);
  if(rv != MPI_SUCCESS)
    return -1;

  /*TC, June 2016: Pack number of GPUs to use of each module*/
  rv = MPI_Pack(exec_info->module_num_gpu, exec_info->num_modules, MPI_INT,
      (void *)exec_info->packed, size, &pos, comm);
  if(rv != MPI_SUCCESS)
    return -1;

  /*Pack tuple_size*/
  rv = MPI_Pack(&(exec_info->tuple_size), 1, MPI_INT, (void *)exec_info->packed, size, &pos, comm);
  if(rv != MPI_SUCCESS)
    return -1;

  /*Pack num_gpu_available */
  rv = MPI_Pack(&(exec_info->num_gpu_available), 1, MPI_INT, (void *)exec_info->packed, size, &pos, comm);
  if(rv != MPI_SUCCESS)
    return -1;

  /*TC, Feb 2016: Pack tuple_rank*/
  rv = MPI_Pack(&(exec_info->tuple_rank), 1, MPI_INT, (void *)exec_info->packed, size, &pos, comm);
  if(rv != MPI_SUCCESS)
    return -1;

  /*TC, May 2016: Pack tuple_group*/
  rv = MPI_Pack(&(exec_info->tuple_group), 1, MPI_INT, (void *)exec_info->packed, size, &pos, comm);
  if(rv != MPI_SUCCESS)
    return -1;

  /*TC, May 2016: Pack tuple_set*/
  rv = MPI_Pack(&(exec_info->tuple_set), 1, MPI_INT, (void *)exec_info->packed, size, &pos, comm);
  if(rv != MPI_SUCCESS)
    return -1;

  /* ZT -- Oct 2017: Pack server config*/
  slen = strlen(exec_info->server_config);
  rv = MPI_Pack(&slen, 1, MPI_INT, (void *) exec_info->packed, size, &pos, comm);
  if(rv != MPI_SUCCESS)
    return -1;

  rv = MPI_Pack(exec_info->server_config, slen, MPI_CHAR, (void *) exec_info->packed, size, &pos, comm);
  if(rv != MPI_SUCCESS)
    return -1;

  for(i=0;i<exec_info->num_modules;i++) {
    slen = strlen(exec_info->modules[i].libname) + 1;
    rv = MPI_Pack(&slen, 1, MPI_INT, (void *)exec_info->packed, size, &pos, comm);
    if(rv != MPI_SUCCESS)
      return -1;

    rv = MPI_Pack(exec_info->modules[i].libname, slen, MPI_CHAR, (void *)exec_info->packed, size, &pos, comm);
    if(rv != MPI_SUCCESS)
      return -1;

    slen = strlen(exec_info->modules[i].libtype) + 1;
    rv = MPI_Pack(&slen, 1, MPI_INT, (void *)exec_info->packed, size, &pos, comm);
    if(rv != MPI_SUCCESS)
      return -1;

    rv = MPI_Pack(exec_info->modules[i].libtype, slen, MPI_CHAR,
        (void *)exec_info->packed, size, &pos, comm);
    if(rv != MPI_SUCCESS)
      return -1;

    MPI_Pack(&(exec_info->modules[i].splitdir), 1, MPI_CHAR, (void *)exec_info->packed, size, &pos, comm);

    slen = strlen(exec_info->modules[i].splitdirpath) + 1;
    rv = MPI_Pack(&slen, 1, MPI_INT, (void *)exec_info->packed, size, &pos, comm);
    if(rv != MPI_SUCCESS)
      return -1;

    rv = MPI_Pack(exec_info->modules[i].splitdirpath, slen, MPI_CHAR,
        (void *)exec_info->packed, size, &pos, comm);
    if(rv != MPI_SUCCESS)
      return -1;

    slen = strlen(exec_info->modules[i].funcname) + 1;
    rv = MPI_Pack(&slen, 1, MPI_INT, (void *)exec_info->packed, size, &pos, comm);
    if(rv != MPI_SUCCESS)
      return -1;

    rv = MPI_Pack(exec_info->modules[i].funcname, slen, MPI_CHAR,
        (void *)exec_info->packed, size, &pos, comm);
    if(rv != MPI_SUCCESS)
      return -1;

    /*pack mod_argc*/
    rv = MPI_Pack(&(exec_info->modules[i].mod_argc), 1, MPI_INT, 
        (void *)exec_info->packed, size, &pos, comm);
    if(rv != MPI_SUCCESS)
      return -1;

    /*pack mod_argv*/
    for(j = 0; j < exec_info->modules[i].mod_argc; j++)
    {
      slen = strlen(exec_info->modules[i].mod_argv[j]) + 1;
      rv = MPI_Pack(&slen, 1, MPI_INT, (void *)exec_info->packed, size, &pos, comm);
      if(rv != MPI_SUCCESS)
        return -1;

      rv = MPI_Pack(exec_info->modules[i].mod_argv[j], slen, MPI_CHAR, 
          (void *)exec_info->packed, size, &pos, comm);
      if(rv != MPI_SUCCESS)
        return -1;
    }

    /* ZT -- Aug 2017, Pack num_tags */
    /*
    rv = MPI_Pack(&(exec_info->modules[i].num_tags), 1, MPI_INT,
                  (void *) exec_info->packed, size, &pos, comm);
    if (rv != MPI_SUCCESS) return -1;
*/
    /* ZT -- Aug 2017, Pack mod_tags */
    /*
    for (j = 0; j < exec_info->modules[i].num_tags; j++) {
      slen = strlen(exec_info->modules[i].mod_tags[j]) + 1;
      rv = MPI_Pack(&slen, 1, MPI_INT, (void *) exec_info->packed, size, &pos, comm);
      if (rv != MPI_SUCCESS) return -1;

      rv = MPI_Pack(exec_info->modules[i].mod_tags[j], slen, MPI_CHAR, 
                    (void *) exec_info->packed, size, &pos, comm);
      if (rv != MPI_SUCCESS) return -1;
    }
*/
    /*Pack emode*/
    rv = MPI_Pack(&(exec_info->modules[i].emode), 1, MPI_INT, (void *)exec_info->packed, size, &pos, comm);
    if(rv != MPI_SUCCESS)
      return -1;

    /*Pack stdinFile*/
    slen = strlen(exec_info->modules[i].stdinFile) + 1;
    rv = MPI_Pack(&slen, 1, MPI_INT, (void *)exec_info->packed, size, &pos, comm);
    if(rv != MPI_SUCCESS)
      return -1;

    DBG("PACKING %s for %d chars\n", exec_info->modules[i].stdinFile, slen);

    rv = MPI_Pack(exec_info->modules[i].stdinFile, slen, MPI_CHAR,
        (void *)exec_info->packed, size, &pos, comm);
    if(rv != MPI_SUCCESS)
      return -1;

    /*TC, Nov 2016: Pack num_head_ranks*/
    rv = MPI_Pack(&(exec_info->modules[i].num_head_ranks), 1, MPI_INT,
        (void *)exec_info->packed, size, &pos, comm);
    if(rv != MPI_SUCCESS)
      return -1;

    /*TC, Nov 2016: Pack head_ranks*/
    rv = MPI_Pack(exec_info->modules[i].head_ranks, exec_info->modules[i].num_head_ranks, MPI_INT,
        (void *)exec_info->packed, size, &pos, comm);
    if(rv != MPI_SUCCESS)
      return -1;
  } 

  /*Pack workflow information*/

  DBG("Packing workflow info");

  /*TC, May 2016: Account for sets*/
  /*Pack num_work_sets*/
  rv = MPI_Pack(&(exec_info->num_work_sets), 1, MPI_INT, (void*)exec_info->packed,
      size, &pos, comm);

  for(int set = 0; set < exec_info->num_work_sets; set++) {
    /*Pack num_work_groups*/
    rv = MPI_Pack(&(exec_info->num_work_groups[set]), 1, MPI_INT, (void*)exec_info->packed,
        size, &pos, comm);
    if(rv != MPI_SUCCESS)
      return -1;

    /*Pack work_set_sizes*/
    rv = MPI_Pack(&(exec_info->work_set_sizes[set]), 1, MPI_INT, (void*)exec_info->packed,
        size, &pos, comm);
    if(rv != MPI_SUCCESS)
      return -1;

    /*Pack workflow structs*/
    for(i = 0; i < exec_info->num_work_groups[set]; i++) {

      /*group size*/
      rv = MPI_Pack(&(exec_info->work_groups[set][i].size), 1, MPI_INT,
          (void*)exec_info->packed, size, &pos, comm);
      if(rv != MPI_SUCCESS)
        return -1;

      /*group iterations*/
      rv = MPI_Pack(&(exec_info->work_groups[set][i].iterations), 1, MPI_INT,
          (void*)exec_info->packed, size, &pos, comm);
      if(rv != MPI_SUCCESS)
        return -1;

      /*group num_modules*/
      rv = MPI_Pack(&(exec_info->work_groups[set][i].num_modules), 1, MPI_INT,
          (void*)exec_info->packed, size, &pos, comm);
      if(rv != MPI_SUCCESS)
        return -1;

      /*group persist -- unused -- replaced w/ iterations=-1
       *       rv = MPI_Pack(&(exec_info->work), 1, MPI_INT,
       *             (void*)exec_info->packed, size, &pos, comm);
       *                   if(rv != MPI_SUCCESS)
       *                         return -1;
       *                               */

      /*Order information*/
      for(j = 0; j < exec_info->work_groups[set][i].num_modules; j++) {
        slen = strlen(exec_info->work_groups[set][i].order[j]) + 1;

        /*slen for order member*/
        rv = MPI_Pack(&slen, 1, MPI_INT, (void*)exec_info->packed, size,
            &pos, comm);
        if(rv != MPI_SUCCESS)
          return -1;

        /*Pack the string*/
        rv = MPI_Pack(exec_info->work_groups[set][i].order[j], slen, MPI_CHAR,
            (void*)exec_info->packed, size, &pos, comm);
        if(rv != MPI_SUCCESS)
          return -1;
      }

      /*number of group dependency ranks*/
      rv = MPI_Pack(&(exec_info->work_groups[set][i].num_dep), 1, MPI_INT,
          (void*)exec_info->packed, size, &pos, comm);
      if(rv != MPI_SUCCESS)
        return -1;

      /*group dependency ranks*/
      //printf("About to pack all %d dependencies\n", exec_info->work_groups[i].num_dep); fflush(stdout);
      for(j = 0; j < exec_info->work_groups[set][i].num_dep; j++) {
        rv = MPI_Pack(&(exec_info->work_groups[set][i].dep[j]), 1, MPI_INT,
          (void*)exec_info->packed, size, &pos, comm);
        if(rv != MPI_SUCCESS)
          return -1;
      }

      /*group leader*/
      rv = MPI_Pack(&(exec_info->work_groups[set][i].leader), 1, MPI_INT,
          (void*)exec_info->packed, size, &pos, comm);
      if(rv != MPI_SUCCESS)
        return -1;
    }
  }

  DBG("Workflow Packed");

  /*End workflow pack*/

  /*Pack direct communication info*/
  for(i = 0; i < exec_info->num_modules; i++) {
    /*uses_bc*/
    rv = MPI_Pack(&(exec_info->modules[i].uses_bc), 1, MPI_INT, (void*)exec_info->packed, size, &pos, comm);
    if(rv != MPI_SUCCESS)
      return -1;

    if(exec_info->modules[i].uses_bc == 1) {
      for(j = 0; j < exec_info->num_shared_bc; j++) {
        /*shared_bc_read*/
        rv = MPI_Pack(&(exec_info->modules[i].shared_bc_read[j]), 1, MPI_INT,
                      (void*)exec_info->packed, size, &pos, comm);
        if(rv != MPI_SUCCESS)
          return -1;
        /*shared_bc_write*/
        rv = MPI_Pack(&(exec_info->modules[i].shared_bc_write[j]), 1, MPI_INT,
                      (void*)exec_info->packed, size, &pos, comm);
        if(rv != MPI_SUCCESS)
          return -1;
      }
    }
  }
  /*End direct communication pack*/

  /*Pack num_runs*/
  for(i = 0; i < exec_info->num_work_sets; i++) {
    rv = MPI_Pack(&(exec_info->num_runs[i]), 1, MPI_INT, (void*)exec_info->packed, size, &pos, comm);
    if(rv != MPI_SUCCESS)
      return -1;
  }

  return 0;
}

/**
 * Unpacks the buffer received via MPI into the IEL Executive info struct.
 *
 * It is assumed that the buffer data is stored in exec_info->packed
 * and exec_info->packsize is set to the buffer length prior to
 * calling this function.
 *
 * @param exec_info -- pointer to the IEL Executive info struct 
 * @param comm -- the MPI communicator
 *
 * @returns 0 on success.
 *   on failure, returns -1.
 */
  int
IEL_exec_unpack_info(IEL_exec_info_t *exec_info, MPI_Comm comm)
{
  int i, j, rv, pos = 0, size, slen;

  size = exec_info->packsize;

  rv = MPI_Unpack((void *)exec_info->packed, size, &pos, &(exec_info->num_modules),
      1, MPI_INT, comm);
  if(rv != MPI_SUCCESS)
    return -1;

  /*num_shared_bc*/
  rv = MPI_Unpack((void*)exec_info->packed, size, &pos,
      &(exec_info->num_shared_bc), 1, MPI_INT, comm);
  if(rv != MPI_SUCCESS)
    return -1;

  /* Unpack size of each module */
  exec_info->module_sizes = (int *)malloc(sizeof(int) * exec_info->num_modules);
  rv = MPI_Unpack((void *)exec_info->packed, size, &pos, exec_info->module_sizes,
      exec_info->num_modules, MPI_INT, comm);
  if(rv != MPI_SUCCESS)
    return -1;

  /*TC, June 2016: Unpack number of cores per module*/
  exec_info->module_cores = (int *)malloc(sizeof(int) * exec_info->num_modules);
  rv = MPI_Unpack((void *)exec_info->packed, size, &pos, exec_info->module_cores,
      exec_info->num_modules, MPI_INT, comm);
  if(rv != MPI_SUCCESS)
    return -1;

  /*TC, June 2016: Unpack number of processs per module*/
  exec_info->module_processes = (int *)malloc(sizeof(int) * exec_info->num_modules);
  rv = MPI_Unpack((void *)exec_info->packed, size, &pos, exec_info->module_processes,
      exec_info->num_modules, MPI_INT, comm);
  if(rv != MPI_SUCCESS)
    return -1;

  /*TC, June 2016: Unpack number of threads per module*/
  exec_info->module_threads = (int *)malloc(sizeof(int) * exec_info->num_modules);
  rv = MPI_Unpack((void *)exec_info->packed, size, &pos, exec_info->module_threads,
      exec_info->num_modules, MPI_INT, comm);
  if(rv != MPI_SUCCESS)
    return -1;

  /*TC, June 2016: Unpack number of copies to run of each module*/
  exec_info->module_copies = (int *)malloc(sizeof(int) * exec_info->num_modules);
  rv = MPI_Unpack((void *)exec_info->packed, size, &pos, exec_info->module_copies,
      exec_info->num_modules, MPI_INT, comm);
  if(rv != MPI_SUCCESS)
    return -1;

  /* Unpack num_gpu's for each module */
  exec_info->module_num_gpu = (int *)malloc(sizeof(int) * exec_info->num_modules);
  rv = MPI_Unpack((void *)exec_info->packed, size, &pos, exec_info->module_num_gpu,
      exec_info->num_modules, MPI_INT, comm);
  if(rv != MPI_SUCCESS)
    return -1;


  exec_info->modules = (module_depend_t *)malloc(sizeof(module_depend_t) * exec_info->num_modules);
  exec_info->num_proc = 0;

  // //Added by DW, 5/13/14
  // //Unpack tuple_size
  rv = MPI_Unpack((void *)exec_info->packed, size, &pos, &(exec_info->tuple_size),
      1, MPI_INT, comm);
  if(rv != MPI_SUCCESS)
    return -1;
  //end addition

  /* unpack num_gpu_available */
  rv = MPI_Unpack((void *)exec_info->packed, size, &pos, &(exec_info->num_gpu_available),
      1, MPI_INT, comm);
  if(rv != MPI_SUCCESS)
    return -1;

  /*TC, Feb 2016: Unpack tuple_rank*/
  rv = MPI_Unpack((void *)exec_info->packed, size, &pos, &(exec_info->tuple_rank),
      1, MPI_INT, comm);
  if(rv != MPI_SUCCESS)
    return -1;

  /*TC, May 2016: Unpack tuple_group*/
  rv = MPI_Unpack((void *)exec_info->packed, size, &pos, &(exec_info->tuple_group),
      1, MPI_INT, comm);
  if(rv != MPI_SUCCESS)
    return -1;

  /*TC, May 2016: Unpack tuple_set*/
  rv = MPI_Unpack((void *)exec_info->packed, size, &pos, &(exec_info->tuple_set),
      1, MPI_INT, comm);
  if(rv != MPI_SUCCESS)
    return -1;

  /* ZT -- Oct 2017: Unpack server config*/
  rv = MPI_Unpack((void *) exec_info->packed, size, &pos, &slen, 
       1, MPI_INT, comm);
  if(rv != MPI_SUCCESS)
    return -1;

  exec_info->server_config = (char *) malloc(slen); 
  rv = MPI_Unpack((void *) exec_info->packed, size, &pos, exec_info->server_config, 
       slen, MPI_CHAR, comm);
  if(rv != MPI_SUCCESS)
    return -1;

  for(i=0;i<exec_info->num_modules;i++) {
    rv = MPI_Unpack((void *)exec_info->packed, size, &pos, &slen,
        1, MPI_INT, comm);
    if(rv != MPI_SUCCESS)
      return -1;

    exec_info->modules[i].libname = (char *)malloc(slen);

    rv = MPI_Unpack((void *)exec_info->packed, size, &pos, exec_info->modules[i].libname,
        slen, MPI_CHAR, comm);
    if(rv != MPI_SUCCESS)
      return -1;

    rv = MPI_Unpack((void *)exec_info->packed, size, &pos, &slen,
        1, MPI_INT, comm);
    if(rv != MPI_SUCCESS)
      return -1;

    exec_info->modules[i].libtype = (char *)malloc(slen);

    rv = MPI_Unpack((void *)exec_info->packed, size, &pos, exec_info->modules[i].libtype,
        slen, MPI_CHAR, comm);
    if(rv != MPI_SUCCESS)
      return -1;

    MPI_Unpack((void*)exec_info->packed, size, &pos, &(exec_info->modules[i].splitdir), 1, MPI_CHAR, comm);
    MPI_Unpack((void *)exec_info->packed, size, &pos, &slen, 1, MPI_INT, comm);
    exec_info->modules[i].splitdirpath =(char*) malloc(slen);
    MPI_Unpack((void *)exec_info->packed, size, &pos, exec_info->modules[i].splitdirpath, slen, MPI_CHAR, comm);

    rv = MPI_Unpack((void *)exec_info->packed, size, &pos, &slen,
        1, MPI_INT, comm);
    if(rv != MPI_SUCCESS)
      return -1;

    exec_info->modules[i].funcname = (char *)malloc(slen);

    rv = MPI_Unpack((void *)exec_info->packed, size, &pos, exec_info->modules[i].funcname,
        slen, MPI_CHAR, comm);
    if(rv != MPI_SUCCESS)
      return -1;

    exec_info->num_proc += exec_info->module_sizes[i];

    /*unpack mod_argc*/
    rv = MPI_Unpack((void *)exec_info->packed, size, &pos, &(exec_info->modules[i].mod_argc),
        1, MPI_INT, comm);
    if(rv != MPI_SUCCESS)
      return -1;

    exec_info->modules[i].mod_argv = (char **)malloc(sizeof(char *) * exec_info->modules[i].mod_argc+1);

    /*unpack mod_argv*/
    for(j = 0; j < exec_info->modules[i].mod_argc; j++)
    {
      rv = MPI_Unpack((void *)exec_info->packed, size, &pos, &slen,
          1, MPI_INT, comm);
      if(rv != MPI_SUCCESS)
        return -1;

      exec_info->modules[i].mod_argv[j] = (char *)malloc(slen);


      rv = MPI_Unpack((void *)exec_info->packed, size, &pos, exec_info->modules[i].mod_argv[j],
          slen, MPI_CHAR, comm);
      if(rv != MPI_SUCCESS)
        return -1;

      DBG("module: [%s] argc [%d] val [%s]",exec_info->modules[i].funcname, j, exec_info->modules[i].mod_argv[j]);
    }

    /* ZT -- Aug 2017, Unpack num_tags */
    /*
    rv = MPI_Unpack((void *) exec_info->packed, size, &pos, &(exec_info->modules[i].num_tags),
                    1, MPI_INT, comm);
    if (rv != MPI_SUCCESS) return -1;

    exec_info->modules[i].mod_tags = (char **) malloc(sizeof(char *) * exec_info->modules[i].num_tags + 1);
*/
    /* ZT -- Aug 2017, Unpack mod_tags */
    /*
    for (j = 0; j < exec_info->modules[i].num_tags; j++) {
      rv = MPI_Unpack((void *) exec_info->packed, size, &pos, &slen, 
                      1, MPI_INT, comm);
      if (rv != MPI_SUCCESS) return -1;

      exec_info->modules[i].mod_tags[j] = (char *) malloc(slen);

      rv = MPI_Unpack((void *) exec_info->packed, size, &pos, exec_info->modules[i].mod_tags[j],
                      slen, MPI_CHAR, comm);
      if (rv != MPI_SUCCESS) return -1;
    }
*/
    /*Unpack emode*/
    rv = MPI_Unpack((void *)exec_info->packed, size, &pos, &(exec_info->modules[i].emode),
        1, MPI_INT, comm);
    if(rv != MPI_SUCCESS)
      return -1;

    /*stdinFile*/
    rv = MPI_Unpack((void *)exec_info->packed, size, &pos, &slen,
        1, MPI_INT, comm);
    if(rv != MPI_SUCCESS)
      return -1;

    exec_info->modules[i].stdinFile = (char*)malloc(slen);

    rv = MPI_Unpack((void *)exec_info->packed, size, &pos, 
        exec_info->modules[i].stdinFile, slen, MPI_CHAR, comm);
    if(rv != MPI_SUCCESS)
      return -1;
    DBG("STDINFILE: %s SLEN: %d", exec_info->modules[i].stdinFile, slen);

    /*TC, Nov 2016: Unpack num_head_ranks*/
    rv = MPI_Unpack((void *)exec_info->packed, size, &pos, 
        &(exec_info->modules[i].num_head_ranks), 1, MPI_INT, comm);
    if(rv != MPI_SUCCESS)
      return -1;

    /*TC, Nov 2016: Unpack head_ranks*/
    rv = MPI_Unpack((void *)exec_info->packed, size, &pos, 
        exec_info->modules[i].head_ranks, exec_info->modules[i].num_head_ranks, MPI_INT, comm);
    if(rv != MPI_SUCCESS)
      return -1;
  }

  /*Unpack workflow information*/
  DBG("Unpacking Workflow...");

  /*TC, May 2016: Account for sets*/
  /*Unpack num_work_sets*/
  rv = MPI_Unpack((void*)exec_info->packed, size, &pos, &(exec_info->num_work_sets),
      1, MPI_INT, comm);

  exec_info->num_work_groups = (int*)malloc(sizeof(int) * exec_info->num_work_sets);
  exec_info->work_set_sizes = (int*)malloc(sizeof(int) * exec_info->num_work_sets);
  exec_info->work_groups = (IEL_workflow_group_t **)malloc(sizeof(IEL_workflow_group_t*) * exec_info->num_work_sets);
  exec_info->num_runs = (int*)malloc(sizeof(int) * exec_info->num_work_sets);

  for(int set = 0; set < exec_info->num_work_sets; set++) {
    /*Unpack num_work_groups*/
    rv = MPI_Unpack((void*)exec_info->packed, size, &pos, &(exec_info->num_work_groups[set]),
        1, MPI_INT, comm);
    if(rv != MPI_SUCCESS)
      return -1;

    /*Unpack work_set_sizes*/
    rv = MPI_Unpack((void*)exec_info->packed, size, &pos, &(exec_info->work_set_sizes[set]),
        1, MPI_INT, comm);
    if(rv != MPI_SUCCESS)
      return -1;

    exec_info->work_groups[set] = (IEL_workflow_group_t *)malloc(exec_info->num_work_groups[set] * sizeof(IEL_workflow_group_t));

    for(i = 0; i < exec_info->num_work_groups[set]; i++) {

      rv = MPI_Unpack((void*)exec_info->packed, size, &pos,
          &(exec_info->work_groups[set][i].size), 1, MPI_INT, comm);
      if(rv != MPI_SUCCESS)
        return -1;

      rv = MPI_Unpack((void*)exec_info->packed, size, &pos,
          &(exec_info->work_groups[set][i].iterations), 1, MPI_INT, comm);
      if(rv != MPI_SUCCESS)
        return -1;

      rv = MPI_Unpack((void*)exec_info->packed, size, &pos,
          &(exec_info->work_groups[set][i].num_modules), 1, MPI_INT, comm);
      if(rv != MPI_SUCCESS)
        return -1;

      exec_info->work_groups[set][i].order = (char**)malloc(exec_info->work_groups[set][i].num_modules * sizeof(char*));

      for(j = 0; j < exec_info->work_groups[set][i].num_modules; j++) {
        /*unpack slen*/
        rv = MPI_Unpack((void*)exec_info->packed, size, &pos,
            &slen, 1, MPI_INT, comm);
        if(rv != MPI_SUCCESS)
          return -1;

        exec_info->work_groups[set][i].order[j] = (char*)malloc(slen * sizeof(char));
        DBG("SLEN=%d", slen);

        rv = MPI_Unpack((void*)exec_info->packed, size, &pos,
            exec_info->work_groups[set][i].order[j], slen, MPI_CHAR, comm);
        if(rv != MPI_SUCCESS)
          return -1;

        DBG("exec_info->work_groups[set][i].order[j]=%s", exec_info->work_groups[set][i].order[j]);

      }

      rv = MPI_Unpack((void*)exec_info->packed, size, &pos,
          &(exec_info->work_groups[set][i].num_dep), 1, MPI_INT, comm);
      if(rv != MPI_SUCCESS)
        return -1;

      exec_info->work_groups[set][i].dep = (int*)malloc(sizeof(int) * exec_info->work_groups[set][i].num_dep);
      for(j = 0; j < exec_info->work_groups[set][i].num_dep; j++) {
        rv = MPI_Unpack((void*)exec_info->packed, size, &pos,
            &(exec_info->work_groups[set][i].dep[j]), 1, MPI_INT, comm);
        if(rv != MPI_SUCCESS)
          return -1;
      }

      rv = MPI_Unpack((void*)exec_info->packed, size, &pos,
          &(exec_info->work_groups[set][i].leader), 1, MPI_INT, comm);
      if(rv != MPI_SUCCESS)
        return -1;
    }
  }
  DBG("Workflow unpacked");

  /*End unpack workflow information*/

  /*Unpack direct communication info*/
  for(i = 0; i < exec_info->num_modules; i++) {
    /*uses_bc*/
    rv = MPI_Unpack((void*)exec_info->packed, size, &pos,
        &(exec_info->modules[i].uses_bc), 1, MPI_INT, comm);
    if(rv != MPI_SUCCESS)
      return -1;

    if(exec_info->modules[i].uses_bc == 1) {
      exec_info->modules[i].shared_bc_read = (int*)malloc(sizeof(int) * exec_info->num_shared_bc);
      exec_info->modules[i].shared_bc_write = (int*)malloc(sizeof(int) * exec_info->num_shared_bc);
      for(j = 0; j < exec_info->num_shared_bc; j++) {
        /*shared_bc_read*/
        rv = MPI_Unpack((void*)exec_info->packed, size, &pos,
            &(exec_info->modules[i].shared_bc_read[j]), 1, MPI_INT, comm);
        if(rv != MPI_SUCCESS)
          return -1;

        /*bc_write_lower*/
        rv = MPI_Unpack((void*)exec_info->packed, size, &pos,
            &(exec_info->modules[i].shared_bc_write[j]), 1, MPI_INT, comm);
        if(rv != MPI_SUCCESS)
          return -1;
      }
    }
  }
  /*End unpack direct communication info*/
 
  /*Unpack num_runs*/
  for(i = 0; i < exec_info->num_work_sets; i++) {
    rv = MPI_Unpack((void *)exec_info->packed, size, &pos, &exec_info->num_runs[i], 1, MPI_INT, comm);
    if(rv != MPI_SUCCESS)
      return -1;
  }

  return 0;
}

/**
 * Prints the IEL Executive struct to stdout.  This is mainly for
 * debug/diagnostic purposes.
 *
 * @param exec_info -- pointer to the IEL Executive info struct 
 *
 * @returns 0 on success, -1 on failure.
 */
  int
IEL_exec_dump_info(IEL_exec_info_t *exec_info)
{
  /* int i,j,k;

     printf("number of shared BCs: %d\n", exec_info->num_shared_bc);
     for(i=0;i<exec_info->num_shared_bc;i++)
     printf("  size of BC %d: %d\n", i, exec_info->shared_bc_sizes[i]);


     printf("number of modules: %d\n", exec_info->num_modules);
     for(i=0;i<exec_info->num_modules;i++) {
     printf("  size of module %d: %d\n", i, exec_info->module_sizes[i]);
     printf("  module library name: %s\n", exec_info->modules[i].libname);
     printf("  module function name: %s\n", exec_info->modules[i].funcname);
     }

     for(i=0;i<exec_info->num_shared_bc;i++) {
     printf("BC %d:\n", i);
     for(j=0;j<exec_info->shared_bc_sizes[i];j++) {
     printf("  Point %d: ", j);
     for(k=0;k<exec_info->num_proc;k++) {
     printf("  %d ", exec_info->commtab[i][j][k]);
     }
     printf("\n");
     }
     }*/

  return 0;
}

/**
 * Places data from executive info into a struct
 * containing only information that would be useful
 * to a fortran module
 *
 * @param exec_info -- pointer to the IEL Executive info struct
 * @return f -- pointer to the module info struct
 */
IEL_fort_info_t reduceFortInfo(IEL_exec_info_t* ei) {
  IEL_fort_info_t finfo;

  finfo.num_modules = ei->num_modules;
  finfo.IEL_comm = MPI_Comm_c2f(ei->IEL_comm);
  finfo.module_comm = MPI_Comm_c2f(ei->module_comm);
  finfo.module_copy_comm = MPI_Comm_c2f(ei->module_copy_comm);
  finfo.IEL_rank = ei->IEL_rank;
  finfo.module_rank = ei->module_rank;
  finfo.module_copy_rank = ei->module_copy_rank;
  finfo.IEL_num_ranks = ei->IEL_num_ranks;
  finfo.module_num_ranks = ei->module_num_ranks;
  finfo.module_copy_num_ranks = ei->module_processes[ei->module_rank];
  finfo.module_num = ei->module_num;
  finfo.module_copy_num = ei->module_copy_num;
  finfo.tuple_size = ei->tuple_size;
  // TODO: Add in direct communication fields
  //finfo.accessible_bc_size = ei->accessible_bc_size;
  //finfo.accessible_bc = malloc(sizeof(double) * ei->accessible_bc_size);
  //for(int i = 0; i < ei->accessible_bc_size; i++) {
  //  finfo.accessible_bc[i] = ei->accessible_bc[i];
  //}
  finfo.mod_argc = ei->modules[ei->module_num].mod_argc;
  finfo.mod_argv_len = (int*)malloc(sizeof(int) * ei->modules[ei->module_num].mod_argc);
  for(int i = 0; i < finfo.mod_argc; i++) {
    finfo.mod_argv_len[i] = strlen(ei->modules[ei->module_num].mod_argv[i]);
  }
  finfo.mod_argv = (char**)malloc(sizeof(char*) * finfo.mod_argc);
  for(int i = 0; i < finfo.mod_argc; i++) {
    finfo.mod_argv[i] = dupstr(ei->modules[ei->module_num].mod_argv[i]);
  }

  return finfo;
}

/* TC, Nov 2016: Implemented new direct communication functions IEL_bc_get and
 * IEL_bc_put.
 *
 * TB, Jun 2017: Removed 'cpy' variable as it's already in exec_info passed in.
 */
/**
 * Packs and sends (nonblocking) the write-partitioned data in this process's
 * shared_bc array to the head process of copy number cpy of the specified module;
 * should only be called on this copy's head process.
 * Returns IEL_SUCCESS on success and -1 on failure.
 * In the event that the module specified by the argument is used multiple times
 * in the workflow, only gets from the first instance.
 *
 * @param exec_info -- pointer to the IEL Executive info struct
 * @param module    -- name of the module to put data on
 * @param request   -- request status for the send
 */
  int
IEL_bc_put(IEL_exec_info_t *exec_info, const char* module, MPI_Request *request)
{
  // Check that this module is valid
  if(exec_info->modules[exec_info->module_num].uses_bc != 1) {
    printf("Error: module that does not use direct communication attempting to call IEL_bc_put\n");
    return -1;
  }

  // Find which module the argument is referring to
  int mod_num = -1;
  for(int i = 0; i < exec_info->num_modules; i++) {
    if(strcmp(module, exec_info->modules[i].funcname) == 0) {
      mod_num = i;
    }
  }
  if(mod_num == -1) {
    printf("Error: module %s not found\n", module);
    return -1;
  }
  // Check that the model is valid
  if(exec_info->modules[mod_num].uses_bc != 1) {
    printf("Error: attempted IEL_bc_put to module not using direct communication\n");
    return -1;
  }

  int count = 0;
  // Count number of elements of shared_bc to be sent based on this
  // module's write permissions and the recipient's read permissions
  for(int i = 0; i < exec_info->num_shared_bc; i++) {
    if(exec_info->modules[exec_info->module_num].shared_bc_write[i] == 1 &&
       exec_info->modules[mod_num].shared_bc_read[i] == 1) {
      count++;
    }
  }
  double *buffer = (double*)malloc(sizeof(double) * count);

  int pos = 0;
  // Place all necessary elements into the buffer
  for(int i = 0; i < exec_info->num_shared_bc; i++) {
    if(exec_info->modules[exec_info->module_num].shared_bc_write[i] == 1 &&
       exec_info->modules[mod_num].shared_bc_read[i] == 1) {
      buffer[pos] = exec_info->shared_bc[i];
      pos++;
      DBG("Packing shared_bc[%d] into buffer[%d] (%f) on module %d to module %d",
          i, pos, buffer[pos], exec_info->module_num, mod_num);
    }
  }
  
  // Send to the head of the other module's specified copy
  int ret;
  if(exec_info->modules[mod_num].num_head_ranks > 1) {
    printf("Warning: IEL_bc_put occurring to module %s, where module %s has multiple occurrences in workflow.\nOnly putting to first instance of module %s.\n", module, module, module);
  }
  // Account for the copy number
  int recipient = exec_info->modules[mod_num].head_ranks[0]
                  + exec_info->module_copy_num * exec_info->module_processes[mod_num];
  DBG("recipient in IEL_bc_get on process %d: %d", exec_info->IEL_rank, recipient);
  // Nonblocking send
  ret = MPI_Isend(buffer, count, MPI_DOUBLE, recipient, IEL_DIR_COMM_TAG,
                  exec_info->IEL_comm, request);

  if(count != 0) free(buffer);

  if(ret != MPI_SUCCESS) {
    printf("Error: failure in IEL_put on module %d, process %d\n",
           exec_info->module_num, exec_info->module_rank);
    return -1;
  }

  return IEL_SUCCESS;
}

 /*
 * TB, Jun 2017: Removed 'cpy' module as it's already in exec_info passed in.
 */
/**
 * Receives (nonblocking with wait) and unpacks the read-partitioned data 
 * into this process's shared_bc array from the head process of copy number
 * cpy of the specified module; should only be called on this copy's head 
 * process, with a corresponding bc_put.
 * Returns IEL_SUCCESS on success and -1 on failure.
 * In the event that the module specified by the argument is used multiple times
 * in the workflow, only gets from the first instance.
 *
 * @param exec_info -- pointer to the IEL Executive info struct 
 * @param module    -- name of the module to get data from
 * @param request   -- request status for the receive and wait
 */
  int
IEL_bc_get(IEL_exec_info_t *exec_info, const char* module, MPI_Request *request)
{
  // Check that this module is valid
  if(exec_info->modules[exec_info->module_num].uses_bc != 1) {
    printf("Error: module that does not use direct communication attempting to call IEL_bc_get\n");
    return -1;
  }

  // Find which module the argument is referring to
  int mod_num = -1;
  for(int i = 0; i < exec_info->num_modules; i++) {
    if(strcmp(module, exec_info->modules[i].funcname) == 0) {
      mod_num = i;
    }
  }
  if(mod_num == -1) {
    printf("Error: module %s not found\n", module);
    return -1;
  }
  // Check that the model is valid
  if(exec_info->modules[mod_num].uses_bc != 1) {
    printf("Error: attempted IEL_bc_get from module not using direct communication\n");
    return -1;
  }

  int count = 0;
  // Count number of elements of shared_bc to be received based on this
  // module's read permissions and the sender's write permissions
  for(int i = 0; i < exec_info->num_shared_bc; i++) {
    if(exec_info->modules[mod_num].shared_bc_write[i] == 1 &&
       exec_info->modules[exec_info->module_num].shared_bc_read[i] == 1) {
      count++;
    }
  }
  double *buffer = (double*)malloc(sizeof(double) * count);

  // Receive the message
  int ret;
  if(exec_info->modules[mod_num].num_head_ranks > 1) {
    printf("Warning: IEL_get occurring from module %s, where module %s has multiple occurrences in workflow. Only getting from first instance of module %s.\n", module, module, module);
  }
  // Account for the copy number
  int sender = exec_info->modules[mod_num].head_ranks[0]
               + exec_info->module_copy_num * exec_info->module_processes[mod_num];
  DBG("sender in IEL_get on process %d: %d", exec_info->IEL_rank, sender);
  // Nonblocking receive and a wait to ensure data has transferred
  ret = MPI_Irecv(buffer, count, MPI_DOUBLE, sender, IEL_DIR_COMM_TAG,
                  exec_info->IEL_comm, request);
  if(ret != MPI_SUCCESS) {
    printf("Error: failure in IEL_put on module %d, process %d\n",
           exec_info->module_num, exec_info->module_rank);
    return -1;
  }
  ret = MPI_Wait(request, MPI_STATUS_IGNORE);
  if(ret != MPI_SUCCESS) {
    printf("Error: failure in IEL_put on module %d, process %d\n",
           exec_info->module_num, exec_info->module_rank);
    return -1;
  }

  // Unpack the buffer into the shared_bc array
  int pos = 0;
  for(int i = 0; i < exec_info->num_shared_bc; i++) {
    if(exec_info->modules[mod_num].shared_bc_write[i] == 1 &&
       exec_info->modules[exec_info->module_num].shared_bc_read[i] == 1) {
      DBG("Unpacking shared_bc[%d] as buffer[%d] (%f) on module %d from module %d",
          i, pos, buffer[pos], exec_info->module_num, mod_num);
      exec_info->shared_bc[i] = buffer[pos];
      pos++;
    }
  }
  free(buffer);  

  return IEL_SUCCESS;
}

/* Wrapper for IEL_bc_put and IEL_bc_get *
 * TB, 2017                              */  
int IEL_bc_exchange(IEL_exec_info_t *exec_info, const char* module, MPI_Request *request) {
    IEL_bc_put(exec_info, module, request);
    IEL_bc_get(exec_info, module, request);
    
    return 0; // ZT -- Aug 2017, Shut compiler warnings up
}

/*
 * A wrapper around MPI_Barrier that allows synchronization of all
 * processes within the openDIEL
 *
 * @param exec_info -- pointer to the IEL Executive info struct 
 */
  int
IEL_barrier(IEL_exec_info_t *exec_info)
{
  int rv;
  rv = MPI_Barrier(exec_info->IEL_comm);
  if (rv != MPI_SUCCESS)
    return IEL_SYNCH_ERR;
  return IEL_SUCCESS;
}

/*
 * A wrapper around MPI_Barrier that allows synchronization of only the
 * calling module
 *
 * @param exec_info -- pointer to the IEL Executive info struct 
 */
  int
IEL_module_barrier(IEL_exec_info_t *exec_info)
{
  int rv;
  rv = MPI_Barrier(exec_info->module_comm);
  if (rv != MPI_SUCCESS)
    return IEL_SYNCH_ERR;
  return IEL_SUCCESS;
}

/*
 * A wrapper around MPI_Barrier that allows synchronization of only the
 * calling workflow group
 *
 * @param exec_info -- pointer to the IEL Executive info struct 
 */
  int
IEL_group_barrier(IEL_exec_info_t *exec_info)
{
  int rv;
  rv = MPI_Barrier(exec_info->group_comm);
  if (rv != MPI_SUCCESS)
    return IEL_SYNCH_ERR;
  return IEL_SUCCESS;
}

/*
 * A wrapper around MPI_Barrier that allows synchronization of only the
 * calling module copy
 *
 * @param exec_info -- pointer to the IEL Executive info struct 
 */
  int
IEL_module_copy_barrier(IEL_exec_info_t *exec_info)
{
  int rv;
  rv = MPI_Barrier(exec_info->module_copy_comm);
  if (rv != MPI_SUCCESS)
    return IEL_SYNCH_ERR;
  return IEL_SUCCESS;
}

