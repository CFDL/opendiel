/*
 * Copyright (c) 2015 University of Tennessee
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include "IEL_group_graph.h"

/* TC, June 2016: Function created */
/*
 * Run a depth first search on the graph starting at the node specified
 * by group, checking if the graph is cyclic or not. Return IEL_SUCCESS
 * if no cycles are found (meaning the graph is a Directed Acyclic Graph),
 * and -1 if a cycle is found.
 */
int dfs(group_node *group) {
  // Mark the node gray; we are currently traversing its branch
  group->color = 'g';
  // Traverse each forward edge
  for(int i = 0; i < group->num_successors; i++) {
    DBG("traversing [%d]->[%d]", group->groupno, group->successors[i]->groupno);
    if(group->successors[i]->color == 'g') {
      // We have found a back edge to a node in the current branch;
      // the graph is cyclic
      return -1;
    }
    // traverse any non-traversed successors
    if(group->successors[i]->color == 'w') {
      if(dfs(group->successors[i]) == -1) {
        return -1;
      }
    }
  }
  // Mark the node black so we do not re-traverse it
  group->color = 'b';
  return IEL_SUCCESS;
}
