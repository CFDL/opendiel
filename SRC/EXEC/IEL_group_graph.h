/*
 * Copyright (c) 2015 University of Tennessee
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#ifndef IEL_GROUP_GRAPH_H
#define IEL_GROUP_GRAPH_H
#include "tuple_server.h"

#ifdef __cplusplus
extern "C" {
#endif

/* TC, Feb 2016: Structure created */
/* Represents all of the info for a single group node
 * in the dependency graph. Used in the tuple server
 * for calculating dependencies for workflow.
 */
typedef struct group_node{
  int setno;                           /**< Set number of the group in the executive */
  int groupno;                         /**< Group number in the executive */
  int size;                            /**< Number of processes that the group requires */
  int num_pred;                        /**< Size of pred array; number of predecessors */
  int cur_pred;                        /**< Number of predecessors currently in pred; for decrementation */
  int num_successors;                  /**< Size of successors array */
  //int is_head;                         /**< If the node is has no predecessors, set to 1 so it is run first */
  char color;                          /**< Used for depth-first traversal; w=white, g=gray, b=black */
  struct group_node **pred;            /**< Predecessor list for the group; groups it depends on; arr of pointers */
  struct group_node **successors;      /**< Successor list for the group; groups that depend on it; arr of pointers*/
  struct group_node **pred_remaining;  /**< Remaining predecessors; will be cleared as dependencies are met */
} group_node;

int dfs(group_node *group);

#ifdef __cplusplus
}
#endif

#endif
