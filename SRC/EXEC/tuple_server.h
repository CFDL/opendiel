/*
 * Copyright (c) 2015 University of Tennessee
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#ifndef _TUPLE_SERVER_H
#define _TUPLE_SERVER_H
#include "IEL_exec_info.h"

#ifdef __cplusplus
extern "C" {
#endif

int IEL_group_waiting(int tupleRank, IEL_exec_info_t *exec_info);
int IEL_run_waiting(int tupleRank, IEL_exec_info_t *exec_info);
int IEL_group_done(int tupleRank, int work_group_num, int work_set_num, IEL_exec_info_t *exec_info);
int ielTupleServer(IEL_exec_info_t *exec_info);

#ifdef __cplusplus
}
#endif

#endif
