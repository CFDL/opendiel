/*
 * Copyright (c) 2015 University of Tennessee
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include <stdio.h>
#include <string.h>
#include <dlfcn.h>
#include <unistd.h>
#include <sys/types.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>
#include <omp.h>
#include <stdlib.h>

#include "IEL_exec_info.h"
#include "IEL_err.h"
#include "tuple_server.h"
#include "tuple_comm.h"

#ifndef FALSE
#define FALSE 0
#endif
#ifndef TRUE
#define TRUE 1
#endif

//int IELExecutive(MPI_Comm, char *);

/**
 * Module definitions
 */
typedef struct {
	char *name;
	int (*func)(IEL_exec_info_t *);
} map;

struct time_info{
	int level_change;
	double time;
	char *description;
	struct time_info *next;
};

typedef struct time_info time_info;

static map *function_map;

int fmap_count = 0;
int fmap_length = 0;
int tuple_exit;

time_info *start = NULL;
time_info *curr = NULL;
double maxTime;
void timestamp (char *s1, char *s2, int change) {

	if (start == NULL) {
		start =(time_info*) malloc (sizeof (time_info));
		curr = start;
	}

	char *temp = (char*)malloc (8 + strlen (s2));
	int i = 0;

	strcpy (temp, "[");
	if (strlen (s1) >= 5)
		strncat (temp, s1, 5); 
	else {
		int buff = 5 - strlen (s1);
		for (i = 0; i < buff / 2; i++)
			strcat (temp, " ");
		strcat (temp, s1);
		for (i = 0; i < buff - buff / 2; i++)
			strcat (temp, " ");
	}

	strcat (temp, "] ");
	strcat (temp, s2);

	curr -> level_change = change;
	curr -> description = temp;
	curr -> time = MPI_Wtime ();
	curr -> next = (time_info*) malloc (sizeof (time_info));
	maxTime = curr -> time;

	curr = curr -> next;
	curr -> next = NULL;
	return;
}

void timer_finalize (int rank) {
	struct time_diff_info {
		double time;
		struct time_diff_info *prev;
	};
	typedef struct time_diff_info time_diff_info;
	
	time_diff_info *curr = (time_diff_info*) malloc (sizeof (time_diff_info));
	curr -> prev = NULL;
	curr -> time = start -> time;
	int level = 1;

	time_info* iter = start;
	time_info* temp;
	FILE *fp;
	char *sep;
	char timeString [9];
	char timeDiffString [23];

	int i = 0;
	double timeSpent;
	double last_ended_time = 0;
	double begin_time = 0;
	char* filename;

	//-------------- Final Stat Variables ---------------//
	double idleTime = 0;
	double endTime = 0;
	int stopUpdating = 0;
	//-------------- Final Stat Variables ---------------//
	
	//-------------- Creating Directory ----------------//
	struct stat st = {0};
	if (stat("Timers", &st) == -1)
		mkdir ("Timers", 0700);
	int tempsize = (sizeof("Timers") + 14);
	filename = (char*)malloc (tempsize);
	snprintf (filename, tempsize, "Timers/timer_%d.txt", rank);
	fp = fopen (filename, "w");
	free (filename);
	//-------------- Creating Directory ----------------//
	

	if (iter != NULL) {
		begin_time = iter -> time;
		last_ended_time = begin_time;
		timeSpent = maxTime - begin_time;
	}

	if (iter == NULL){
		fclose (fp);
		return;
	}

	while (iter -> next != NULL) {

		double time_diff = 0;
		int change = 0;

		if (!stopUpdating)
		{
			if (strcmp ("[Begin] IEL Finalize", iter -> description) == 0)
				stopUpdating = 1;
			else
				endTime = iter -> time - begin_time;	
		}
		//-------------- Calculating Time Difference --------------
		if (iter -> level_change > 0) {
			time_diff_info *temp = (time_diff_info*) malloc (sizeof (time_diff_info));
			temp -> time = iter -> time;
			temp -> prev = curr;
			curr = temp;
			change = 1;
			time_diff = iter -> time - last_ended_time;

		} else if (iter -> level_change  < 0) {
			time_diff_info *temp = curr -> prev;
			time_diff = iter -> time - curr -> time;
			free (curr);
			curr = temp;
			change = -1;
			level += iter -> level_change;
		}
		
		last_ended_time = iter -> time;
		//-------------- Calculating Time Difference ---------------//


		sep = (char*)malloc (sizeof ("\t") * level);
		strcpy (sep, "\t");

		for(i=0; i < level - 1; i++)
			strcat (sep, "\t");

		if (change < 0) {
			snprintf (timeDiffString, 11, "( %f", time_diff);
			strcat (timeDiffString, ") ");

			//------------------- Calculating Percentage ---------------//
		//	char percentage [11];
		//	double p = time_diff / timeSpent * 100;
		//	snprintf (percentage, 9, " +%f", p);
		//	strcat (percentage, "% ");
		//	strcat (timeDiffString, percentage);
			//------------------- Calculating Percentage ---------------//
			strcat (timeDiffString, "\t\t");
		}
		else if (change > 0) {
			snprintf (timeDiffString, 11, "(-%f", time_diff);
			strcat (timeDiffString, ") ");

			//------------------- Calculating Percentage ---------------//
			char percentage [11];
			double p = time_diff / timeSpent * 100;
			if (p > 10) {
				snprintf (percentage, 9, "[-%f", p);
				strcat (percentage, "%]");
				strcat (timeDiffString, percentage);
			}
			else {
				//snprintf (percentage, 9, " -%f", p);
				//strcat (percentage, "% ");
				strcat (timeDiffString, "\t\t");
			}
			idleTime += time_diff;
		}
		else {
			strcpy (timeDiffString, "\t\t");
		}
		snprintf (timeString, 9, "%f", (iter -> time - begin_time));
		fprintf (fp, "%s %s %s%s\n", timeString, timeDiffString, sep, iter -> description);

		free (sep);
		if (iter -> level_change > 0)
			level += iter -> level_change;

		temp = iter;
		iter = iter -> next;
		free (temp);
	}

	fprintf (fp, "\n");
	fprintf (fp, "Total Idle Time = %f seconds. (%f%%)\n", idleTime, idleTime / timeSpent * 100);
 	fprintf (fp, "End Time = %f.", endTime);

	
	//--------------------Calculating Double-------------------//
	struct {
		double time;
		int rank;
	} in[2], min[2], max[2];

	in[0].time = idleTime;
	in[0].rank = rank;
	in[1].time = endTime;
	in[1].rank = rank;

	

	MPI_Reduce (in, min, 2, MPI_DOUBLE_INT, MPI_MINLOC, 0, MPI_COMM_WORLD);
	MPI_Reduce (in, max, 2, MPI_DOUBLE_INT, MPI_MAXLOC, 0, MPI_COMM_WORLD);
	
	if (rank == 0){
		printf ("----------------------------------------------------------\n");
		printf ("Most Idle Time:   \tProcess %d\t%f seconds (%f%%).\n", max[0].rank, max[0].time, max[0].time / timeSpent * 100); 
		//printf ("Least Idle Time:  \tProcess %d\t%f seconds (%f%%).\n", min[0].rank, min[0].time, min[0].time / timeSpent * 100);
		printf ("Earliest End Time:\tProcess %d\ttime = %f seconds.\n", min[1].rank, min[1].time);
		printf ("Latest End Time:  \tProcess %d\ttime = %f seconds.\n", max[1].rank, max[1].time);
		printf ("----------------------------------------------------------\n");
	}
	//--------------------Calculating Double-------------------//
	free (iter);
	fclose (fp);
	return;
}

/**
 * @brief Adds a module to the function map.
 * 
 * @param *func A pointer to the function in the static library
 * @param *func_name A char* string that contains the name or identifying string for the function
 */
void IELAddModule(int (*func)(IEL_exec_info_t *), char *func_name) {
  timestamp ("Add", func_name, 0); 
  if (fmap_count == 0) {
    //Initialize allocation
    function_map = (map*)malloc(sizeof(map) * 3);
    fmap_length = 3;
  }

  if (fmap_count == fmap_length) {
    //Reallocate function map
    function_map = (map*)realloc(function_map, 2 * fmap_length * sizeof(map));
    fmap_length *= 2;
  }

  function_map[fmap_count].name = dupstr(func_name);
  function_map[fmap_count].func = func;
  fmap_count++;
}

/**
 * @brief The main function. Calls IELExecutive().
 *
 * @param argc Typical command line argc.
 * @param argv[] Typical command line argv.
 *
 * @return Returns 0 if IELExecutive() is successful. Otherwise, returns a negative value.
 */
int IELExecInit(int argc, char* argv[]) {
  if (argc != 2) {
    printf("usage: %s config_file\n", argv[0]);
    exit(1);
  }

  int rc;
  MPI_Init(&argc, &argv);
  rc = IELExecutive(MPI_COMM_WORLD, argv[1]);
  MPI_Finalize();
  return rc;
}

/**
 * Initializes the IEL Executive.
 *
 * @param IELComm -- the MPI communicator for the IEL system
 * @param exec_info -- pointer to IEL executive structure
 * @param inFileName -- name of the input configuration file
 *
 * @returns 0 on success, -1 on failure.
 */
int IEL_executive_init(MPI_Comm IELComm, IEL_exec_info_t *exec_info, char *inFileName) {
  int rank, size, modRank, /*maxRank,*/ serverRank, rankCount, rv, i;
  MPI_Comm groupComm;
  MPI_Comm_rank(IELComm, &rank);
  MPI_Comm_size(IELComm, &size);
  serverRank = 0; //Server is always rank 0 in the IELComm group.

  if (rank == serverRank) {
    if (IEL_exec_config(exec_info, inFileName) < 0) {
      fprintf(stderr, "Failed to initialize Executive\n");
      MPI_Abort(IELComm, 1234);
      return -1;
    }

	DBG("Starting info pack");
	timestamp ("Begin", "Info Pack", 1);
	IEL_exec_pack_info(exec_info, IELComm);
	timestamp ("End", "Info Pack", -1);
	DBG("Info pack complete");

    rv = MPI_Bcast(&(exec_info->packsize), 1, MPI_INT, 0, IELComm);
    if (rv != MPI_SUCCESS)
      return -1;
    rv = MPI_Bcast(exec_info->packed, exec_info->packsize, MPI_PACKED, 0, IELComm);
    if (rv != MPI_SUCCESS)
      return -1;
  } else {
    rv = MPI_Bcast(&(exec_info->packsize), 1, MPI_INT, 0, IELComm);
    if (rv != MPI_SUCCESS)
      return -1;
    exec_info->packed = (char *) malloc(exec_info->packsize);
    rv = MPI_Bcast(exec_info->packed, exec_info->packsize, MPI_PACKED, 0, IELComm);
    if (rv != MPI_SUCCESS)
      return -1;

	DBG("Starting unpack");
	timestamp ("Begin", "Info Unpack", 1); 
	IEL_exec_unpack_info(exec_info, IELComm);
	timestamp ("End", "Info Unpack", -1);
	DBG("Unpack Complete");
  }

  //timestamp ("Begin", "Direct Communication Setup", 1);

  //timestamp ("End", "Direct Communication Setup", -1);

  /****************************** Workflow Setup ******************************/
  timestamp ("Begin", "Workflow Setup", 1);
  rankCount = 0;
  /*
   * Split IELComm processes into groups. Need x process for each module.
   * each rank of each module will have the same color
   */

  exec_info->module_num = -1;

  /* TC, May 2016: Determine the set ID. */
  exec_info->work_set_num = MPI_UNDEFINED;
  for(i = 0; i < exec_info->num_work_sets; i++) {
    if(rank >= rankCount && rank < rankCount + exec_info->work_set_sizes[i]) {
      exec_info->work_set_num = i;
    }
    rankCount += exec_info->work_set_sizes[i];
  }
  if(exec_info->work_set_num == MPI_UNDEFINED) {
    // We still do not have a set, meaning this is an extra module
    timestamp ("End", "Workflow Setup", -1);
    return IEL_UNUSED_RANK;
  }

  /* *
   * Workflow Change: determine the group ID. Module ID will be determined
   * moduleComm is now called groupComm
   * */
  //maxRank = 0;

  exec_info->work_group_num = MPI_UNDEFINED;
  /* TC, June 2016: Set work_group_num in a way that is compatible with worksets */
  // Set rankCount to the number of ranks we have passed to reach this set
  rankCount = 0;
  for(i = 0; i < exec_info->work_set_num; i++) {
    rankCount += exec_info->work_set_sizes[i];
  }

  for(i = 0; i < exec_info->num_work_groups[exec_info->work_set_num]; i++) {
    if(rank >= rankCount && rank < rankCount + exec_info->work_groups[exec_info->work_set_num][i].size) {
      exec_info->work_group_num = i;
      DBG("Setting work_group_num of proc %d to %d", rank, i);
    }
    rankCount += exec_info->work_groups[exec_info->work_set_num][i].size;
  }

  if(size < rankCount) {
    fprintf(stderr,"Fatal error: not enough IEL Processes to  handle all groups\n");
    timestamp ("Error", "Workflow Setup", -1);
    return -1;
  }

  DBG("Rank %d in workgroup %d in set %d", rank, exec_info->work_group_num, exec_info->work_set_num);

  /* TC, June 2016: Split based on both set and group rather than just group */
  // Use Cantor pairing function to find unique number for each set-group combination
  // Calculate differently if set and group have an even or odd sum to prevent integer truncation
  int color;
  if(exec_info->work_group_num + exec_info->work_set_num % 2 == 0) { // Even sum
    color = (exec_info->work_group_num + exec_info->work_set_num)/2 * 
      (exec_info->work_group_num + exec_info->work_set_num + 1) + exec_info->work_set_num;
  } else { // Odd sum
    color = (exec_info->work_group_num + exec_info->work_set_num + 1)/2 *
      (exec_info->work_group_num + exec_info->work_set_num) + exec_info->work_set_num;
  }
  MPI_Comm_split(IELComm, color, 0, &groupComm);

  if(exec_info->work_group_num == MPI_UNDEFINED) {
    modRank = -1;
  } else {
    MPI_Comm_rank(groupComm, &modRank);
  }

  exec_info->IEL_comm = IELComm;
  exec_info->IEL_rank = rank;
  exec_info->IEL_num_ranks = size;

  exec_info->group_comm = groupComm;
  exec_info->module_rank = modRank;
  exec_info->module_num_ranks = -1;
  //printf("I am process %d and i am rank %d of module %d.\n", rank, modRank, exec_info->module_num);

  timestamp ("End", "Workflow Setup", -1);
  return 0;
}

/* TC, June 2016: Updated with new data fields; removed check based on module_comm, 
 * since it is deallocated in workflow loop. All memory is now accessible on program finish.
 *
 * Nov 2016: Updated with new direct communication data fields; cleaned out old code
 */
/**
 * Finalizes the IEL Executive.  This should correctly free everything related
 *  to the IEL_executive.
 *
 * @param exec_info -- pointer to IEL executive structure
 *
 * @returns 0 on success, -1 on failure.
 */
int IEL_exec_finalize(IEL_exec_info_t *exec_info) {
  DBG("Beginning finalize on %d", exec_info->IEL_rank);
  int i, j, k;

  //free packed//
  if (exec_info->packed != NULL) {
    free(exec_info->packed);
    exec_info->packed = NULL;
  }

  //free fields in the modules struct//
  if (exec_info->modules != NULL) {
    for (i = 0; i < exec_info->num_modules; i++) {
      //free mod_argv//
      if (exec_info->modules[i].mod_argv != NULL) {
        for (j = 0; j < exec_info->modules[i].mod_argc; j++) {
          if (exec_info->modules[i].mod_argv[j] != NULL) {
            free(exec_info->modules[i].mod_argv[j]);
            exec_info->modules[i].mod_argv[j] = NULL;
          }
        }
        free(exec_info->modules[i].mod_argv);
        exec_info->modules[i].mod_argv = NULL;
      }

      //free libname//
      if (exec_info->modules[i].libname != NULL) {
        free(exec_info->modules[i].libname);
        exec_info->modules[i].libname = NULL;
      }

      //free funcname//
      if (exec_info->modules[i].funcname != NULL) {
        free(exec_info->modules[i].funcname);
        exec_info->modules[i].funcname = NULL;
      }

      //free libtype//
      if(exec_info->modules[i].libtype != NULL) {
        free(exec_info->modules[i].libtype);
        exec_info->modules[i].libtype = NULL;
      }

      //free splitdirpath//
      if(exec_info->modules[i].splitdirpath != NULL) {
        free(exec_info->modules[i].splitdirpath);
        exec_info->modules[i].splitdirpath = NULL;
      }

      //free stdinFile//
      if(exec_info->modules[i].stdinFile != NULL) {
        free(exec_info->modules[i].stdinFile);
        exec_info->modules[i].stdinFile = NULL;
      }

      if(exec_info->modules[i].uses_bc == 1) {
        //free shared_bc_read//
        if(exec_info->modules[i].shared_bc_read != NULL) {
          free(exec_info->modules[i].shared_bc_read);
          exec_info->modules[i].shared_bc_read = NULL;
        }

        //free shared_bc_write//
        if(exec_info->modules[i].shared_bc_write != NULL) {
          free(exec_info->modules[i].shared_bc_write);
          exec_info->modules[i].shared_bc_write = NULL;
        }
      }

    }

    //free modules//
    free(exec_info->modules);
    exec_info->modules = NULL;
  }

  //free module_sizes//
  if (exec_info->module_sizes != NULL) {
    free(exec_info->module_sizes);
    exec_info->module_sizes = NULL;
  }

  //free module_num_gpu
  if (exec_info->module_num_gpu != NULL) {
    free(exec_info->module_num_gpu); 
    exec_info->module_num_gpu = NULL;
  }

  //free module_threads//
  if (exec_info->module_threads != NULL) {
    free(exec_info->module_threads);
    exec_info->module_threads = NULL;
  }

  //free work group info//
  if(exec_info->work_groups != NULL){
    for(i = 0; i < exec_info->num_work_sets; i++) {
      for(j = 0; j < exec_info->num_work_groups[i]; j++) {
        //free order//
        if(exec_info->work_groups[i][j].order != NULL) {
          for(k = 0; k < exec_info->work_groups[i][j].num_modules; k++) {
            if(exec_info->work_groups[i][j].order[k] != NULL) {
              free(exec_info->work_groups[i][j].order[k]);
              exec_info->work_groups[i][j].order[k] = NULL;
            }
          }
          free(exec_info->work_groups[i][j].order);
          exec_info->work_groups[i][j].order = NULL;
        }

        //free dep//
        if(exec_info->work_groups[i][j].dep != NULL/* && exec_info->work_groups[i][j].num_dep > 0*/) {
          free(exec_info->work_groups[i][j].dep);
          exec_info->work_groups[i][j].dep = NULL;
        }
      }
      free(exec_info->work_groups[i]);
      exec_info->work_groups[i] = NULL;
    }
    //free work_groups//
    free(exec_info->work_groups);
    exec_info->work_groups = NULL;
  }

  //free num_work_groups//
  if(exec_info->num_work_groups != NULL) {
    free(exec_info->num_work_groups);
    exec_info->work_groups = NULL;
  }

  //free work_set_sizes//
  if(exec_info->work_set_sizes != NULL) {
    free(exec_info->work_set_sizes);
    exec_info->work_set_sizes = NULL;
  }

  //free num_runs//
  if(exec_info->num_runs != NULL) {
    free(exec_info->num_runs);
    exec_info->num_runs = NULL;
  }

  DBG("Completed finalize on %d", exec_info->IEL_rank);

  return 0;
}

/**
 * Finalizes a reduced Fortran info structure. Frees all related pointers.
 *
 * @param finfo -- pointer to reduced Fortran info structure
 *
 * @returns 0 on success, -1 on failure.
 */
int IEL_fort_info_finalize(IEL_fort_info_t *finfo) {
//TODO: Update this

  for(int i = 0; i < finfo->mod_argc; i++) {
    if(finfo->mod_argv[i] != NULL)
      free(finfo->mod_argv[i]);
  }
  if(finfo->mod_argv != NULL)
    free(finfo->mod_argv);
  if(finfo->mod_argv_len != NULL)
    free(finfo->mod_argv_len);
  if(finfo != NULL)
    free(finfo);

  return 0;
}

int IEL_workflow_reset(IEL_exec_info_t *exec_info) {

  if(exec_info->module_num != -1) {
    MPI_Comm_free(&(exec_info->module_comm));
  }

  return IEL_SUCCESS;

}

/**
 * This function prepares exec_info for the next iteration of the workflow. In the
 * future this function will receive information from the tupleserver about which
 * module to run next. For now, this is based on the groups specified by the user.
 * This function also sets the flag that determines when IEL executive is finished.
 *
 * @param  exec_info -- A pointer to exec_info, which will be passed along to other
 * functions
 * @param idx -- Stores what the next module in the group to run is.
 * @param all_done -- A pointer to the flag that will be changed to  true when this
 * process is all executing workflow tasks
 * 
 * @return Returns 0 on sucess, negative value otherwise
 */
int IEL_workflow_init(IEL_exec_info_t *exec_info, int idx, char* all_done) 
{

  /* Information stored in idx and used later in this function
   *
   * idx/work_group->num_modules is the current iteration of a group
   * idx%work_group->num_modules is the current module to be executed
   *
   * */

  int i, /*j,*/ mod_id = -1;
  int rank;
  int mod_size/*, iter*/;
  MPI_Comm moduleComm; //Allocates a communicator for the next module
  MPI_Comm copyComm; // TC, June 2016: Allocates a comm for the copy
  char* cur_module; //Points to the name of the next module to run
  IEL_workflow_group_t *workinfo = NULL;

  rank = exec_info->IEL_rank;

  if(exec_info->work_group_num == MPI_UNDEFINED) {
    fprintf(stderr, "IEL Warning: rank %d is unused\n", rank);
    return IEL_UNUSED_RANK;
  }


  workinfo = &(exec_info->work_groups[exec_info->work_set_num][exec_info->work_group_num]);
  cur_module = workinfo->order[idx%workinfo->num_modules];
  DBG("Rank %d configuring module \"%s\"", rank, cur_module);

  for(i = 0; i < exec_info->num_modules; i++) {
    //if modules names match up we've found the module, break and set info
    if(strcmp(exec_info->modules[i].funcname, cur_module) == 0) {
      mod_id = i;
      break;
    }
  }

  if(mod_id == -1) {
    fprintf(stderr,"IEL Error: %s not found in config file\n", cur_module);
    return -1;
  }

  for(i = 0; i < fmap_count; i++) {
    if(strcmp(function_map[i].name, cur_module) == 0) break;
  }

  /*If the module was not found in the function map*/
  if(i == fmap_count) {
    fprintf(stderr,"IEL Error: %s not found in function map\n", cur_module);
    return -1;
  }

  // Create the module communicator
  mod_size = exec_info->module_sizes[mod_id];
  exec_info->module_num_ranks = mod_size;
  if(exec_info->module_rank >= mod_size) mod_id = MPI_UNDEFINED;
  exec_info->module_num = mod_id;

  DBG("Mod num ranks[%d] mod rank [%d] mod num[%d] rank[%d]", exec_info->module_num_ranks, exec_info->module_rank, mod_id, rank);

  DBG("About to split module comm for group %d, module %d", exec_info->work_group_num, exec_info->module_num);
  MPI_Comm_split(exec_info->group_comm, exec_info->module_num, 0, &moduleComm);
  DBG("Split successful for module %d", mod_id);
  exec_info->module_comm = moduleComm;

  /* TC, Nov 2016: Split module comm into copy comm */
  // If the mod_id is MPI_UNDEFINED, the process is unneeded for this module
  if(mod_id != MPI_UNDEFINED) {
    // Determine the copy number and rank of this process in the module and create module_copy_comm
    exec_info->module_copy_num = exec_info->module_rank / exec_info->module_processes[mod_id];
    DBG("About to split copy comm for group %d, module %d, copy %d",
        exec_info->work_group_num, exec_info->module_num, exec_info->module_copy_num);
    MPI_Comm_split(exec_info->module_comm, exec_info->module_copy_num, 0, &copyComm);

    exec_info->module_copy_comm = copyComm;
    exec_info->module_copy_rank = exec_info->module_rank % exec_info->module_processes[mod_id];

    DBG("Copy comm split successful for group %d, module %d, copy %d, copy rank %d",
        exec_info->work_group_num, exec_info->module_num, exec_info->module_copy_num, exec_info->module_copy_rank);
  } else {
    exec_info->module_copy_num = MPI_UNDEFINED;
    exec_info->module_copy_rank = MPI_UNDEFINED;
  }

  DBG("Iteration: %d num_modules %d\n",idx/workinfo->num_modules, idx%workinfo->num_modules );

  if(workinfo->iterations == idx/workinfo->num_modules + 1 \
      && workinfo->num_modules == idx%workinfo->num_modules + 1) {
    *all_done = 1;
  }

  if(exec_info->module_num == MPI_UNDEFINED) exec_info->module_num = -1;

  return IEL_SUCCESS;
}

/**
 * The workhorse function of the IEL Executive.  This initializes
 * the system, loads the shared objects, and calls the modules. 
 * Called by user driver code.
 *
 * @param IELComm -- Communicator the executive will use.
 * @param inFileName -- The name of the config file.
 *
 * @return Returns 0 on success. A negative value otherwise.
 */
int IELExecutive(MPI_Comm IELComm, char *inFileName) {
  IEL_exec_info_t exec_info;
  /* TC, June 2016: error, curdir, and lib_handle declared later, inside thread */
  //char *error, *curdir;
  //void *lib_handle;
  void (*func)(IEL_exec_info_t *);
  int num_runs;
  char *cwd;
  int ret;

  fprintf(stderr, "Initializing the executive...\n");
  int init = IEL_executive_init(IELComm, &exec_info, inFileName);
  if(init == IEL_UNUSED_RANK) {
    fprintf(stderr, "Warning: Unused rank\n");
    // Split the unused rank to a null comm to match up with group comm split
    MPI_Comm_split(IELComm, MPI_UNDEFINED, 0, &exec_info.group_comm);
    // If we have a tuple server running, notify it that we are done to avoid deadlocking
    if (exec_info.tuple_size > 0) {
      IEL_tupleImDone(exec_info.tuple_size);
    }
    return 0;
  } else if(init < 0) {
    fprintf(stderr, "initialization failed\n");
    return -1;
  }

  /* TC, May 2016: Set up multiple runs for groups */
  num_runs = exec_info.num_runs[exec_info.work_set_num];
  // Only run tuple server once; only use multiple runs if tuple server is present
  /* ZT 7/6/2017 WANT TO RUN MULTIPLE TUPLE SERVERS! */
  if((exec_info.work_group_num == exec_info.tuple_group && exec_info.work_set_num == exec_info.tuple_set)
      || exec_info.tuple_size < 1)
    num_runs = 1;

  /* TC, June 2016: Check for dependency deadlocking */
  MPI_Request request;
  MPI_Status status;
  int set_deadlock = 0; // Default 0 for tuple server
  // Requires that we receive a message from the tuple on every process besides the tuple notifying us that
  // this process's set does not deadlock; if no tuple, do not run this check.
  if(exec_info.tuple_size > 0 && (exec_info.IEL_rank < exec_info.tuple_rank 
        || exec_info.IEL_rank >= exec_info.tuple_rank + exec_info.tuple_size)) {
    MPI_Irecv(&set_deadlock, 1, MPI_INT, exec_info.tuple_rank, RUN_READY, exec_info.IEL_comm, &request);
    MPI_Wait(&request, &status);
  }

  /* FB, July 2018: Set the number of GPUs that are available */ 
  /* NOTE: This value needs error checking, can do so with 
     cudaGetDeviceCount() */
  int next_gpu = 0; /* the next gpu to be assigned */
  int num_gpu_available = exec_info.num_gpu_available; 

  // To avoid unexpected behavior while still allowing other sets to run,
  // skip running this set if there is a deadlock. Go straight to finalize.
  if(!set_deadlock) {
    for(int run = 0; run < num_runs; run++) {
      char all_done = 0;
      int rv;
      int idx = 0; // The current iteration we are on

      if(run > 0) {
        // We must wait for the rest of the set to finish before we can run again
        IEL_run_waiting(exec_info.tuple_rank, &exec_info);
        DBG("No longer waiting on group %d proc %d run %d", exec_info.work_group_num, exec_info.IEL_rank, run);
      }

      //Check if we must wait on any groups that need to be finished (only when tuple server is used)
      if(exec_info.work_groups[exec_info.work_set_num][exec_info.work_group_num].num_dep > 0 && exec_info.tuple_size > 0) {
        IEL_group_waiting(exec_info.tuple_rank, &exec_info);
      }

      /*Added for workflow purposes*/
      while(!all_done) {

        /*Inialize "module id" and all other fields based on what part of the 
         * workflow we are at */

        DBG("about to init wf on %d", exec_info.IEL_rank);
        if( (rv = IEL_workflow_init(&exec_info, idx, &all_done)) < 0) {
           /* returning to prevent the tuple server from deadlocking
           */
          if(rv == IEL_UNUSED_RANK) {
            //return IEL_SUCCESS;
            printf("Warning: skipping unused rank...\n");
            run = num_runs;
            break;
          } else return rv;
        }
          // TC, July 2016: if rank is unused, skip the rest of the loop without
        // TC, Nov. 2016: If this copy is unused, go ahead to the next assigned task
        if(exec_info.module_copy_rank == MPI_UNDEFINED) {
          idx++;
          continue;
        }

        int threads = exec_info.module_threads[exec_info.module_num];
          // Set these now for consistency
          int stdinredirect = 0;
          int realSTDIN = 0;
          int inFD = 0;
          int is_err = 0;

          if (exec_info.module_num != -1) {
            //Handle dedicated working directories if necessary
            char curdir[4096];
            if(exec_info.modules[exec_info.module_num].splitdir) {
              struct stat st = {0};

              //Build  path string
              /* ZT - Feb. 2018 - Error checking getcwd() to shut compiler up */
              cwd = getcwd(curdir, 4096);
              if (cwd == NULL) {
                perror("getcwd() error");
                is_err = -1;
              }
              char newdir[720];
              char rstr[80];
              strcpy(newdir,exec_info.modules[exec_info.module_num].splitdirpath);
              /*Remove these for now to emulate SEQUENCE library functionality*/
              //    strcat(newdir,"/");
              //    strcat(newdir,exec_info.modules[exec_info.module_num].funcname);
              strcat(newdir, "-");
              /* TC, July 2016: Split output based on which copy the process is in */
              sprintf(rstr,"%d",exec_info.module_copy_num);

              strcat(newdir,rstr);

              // Only have the head process of the copy make a new directory
              if (stat(newdir, &st) == -1 && exec_info.module_copy_rank == 0) {
                if(mkdir(newdir,0700)) { //If something went wrong
                  if(errno == EACCES)
                    fprintf(stderr,"Fatal Error: Received \"Permission Denied\" when attempting to create directory %s\n",newdir);
                  else if(errno == ENOSPC)
                    fprintf(stderr,"Fatal Error: Filesystem full\n");
                  else if(errno == EMLINK)
                    fprintf(stderr,"Fatal Error: Parent directory has too many links\n");
                  else if(errno == EROFS)
                    fprintf(stderr,"Fatal Error: %s is on a read-only file system\n",exec_info.modules[exec_info.module_num].splitdirpath);
                  else if(errno == ENOENT)
                    fprintf(stderr,"Fatal Error: A component in path %s does not exist or is a symbolic link whose target file does not exist\n",newdir);
                  else if(errno == ENOTDIR)
                    fprintf(stderr,"Fatal Error: A component in path %s exists, but is not a directory\n", exec_info.modules[exec_info.module_num].splitdirpath);
                  else if(errno == ELOOP)
                    fprintf(stderr,"Fatal Error: Too many symbolic links when trying to look up %s\n",exec_info.modules[exec_info.module_num].splitdirpath);
                  else
                    fprintf(stderr,"Fatal Error: unknown error when trying to look up %s\n",exec_info.modules[exec_info.module_num].splitdirpath);
                  //return -1;
                  is_err = -1;
                }
              }
              // Ensure that the copy's directory is made before changing to it
              MPI_Barrier(exec_info.module_copy_comm);

              if(chdir(newdir)) { //If something went wrong
                if(errno == EACCES)
                  fprintf(stderr,"Fatal Error: Received \"Permission Denied\" when attempting to change directory to %s\n",newdir);
                else if(errno == EFAULT)
                  fprintf(stderr,"Fatal Error: %s is outside accessible address space\n",newdir);
                else if(errno == EIO)
                  fprintf(stderr,"Fatal Error: I/O error when attempting to change directory to %s\n",newdir);
                else if(errno == ENOENT)
                  fprintf(stderr,"Fatal Error: %s does not exist\n",newdir);
                else if(errno == ENOTDIR)
                  fprintf(stderr,"Fatal Error: A component in path %s exists, but is not a directory\n",newdir);
                else if(errno == ELOOP)
                  fprintf(stderr,"Fatal Error: Too many symbolic links when trying to look up %s\n",newdir);
                else
                  fprintf(stderr,"Fatal Error: unknown error when trying to change directory %s\n",newdir);
                //return -1;
                is_err = -1;
              }
            }

            // Only continue if there is no error
            if(!is_err) {
              /*Redirect stdin from another file*/

              //stdinredirect = 0;
              if(strcmp(exec_info.modules[exec_info.module_num].stdinFile, "") != 0) {
                stdinredirect = 1;
                printf("Redirecting %s to STDIN\n", exec_info.modules[exec_info.module_num].stdinFile);
                inFD = open(exec_info.modules[exec_info.module_num].stdinFile, O_RDONLY);
                if(inFD != -1) {
                  realSTDIN = dup(0);
                  if(dup2(inFD, 0) == -1) {
                    perror("Problem redirecting stdin");
                    close(inFD);
                    stdinredirect = 0;
                  }
                } else {
                  perror("open");
                  stdinredirect = 0;
                }
              }

              DBG("VAL OF INFD IS %d",inFD);


              DBG("About to run modules");

              // Set the number of threads required for this module
              char *dynamic, *num_threads;
              dynamic = (char *) malloc(sizeof(char) * 5); 
              num_threads= (char *) malloc(sizeof(char) * 5); 
              if(threads > 1) {
                /* Set environment variables for automatic modules */
                sprintf(dynamic, "%d", 0); 
                sprintf(num_threads, "%d", threads); 
                setenv("OMP_SET_DYNAMIC", dynamic, 1);
                setenv("OMP_NUM_THREADS", num_threads, 1);

                /* Set environment variables for managed modules */
                omp_set_dynamic(0);
                omp_set_num_threads(threads);
              } 
              DBG("OMP_SET_DYNAMIC=%s", getenv("OMP_SET_DYNAMIC"));
              DBG("OMP_NUM_THREADS=%s", getenv("OMP_NUM_THREADS"));
              free(dynamic); 
              free(num_threads);

              /* Set the number of GPU's to be used by the module. 
                 Environment variable 'CUDA_VISIBLE_DEVICES' is
                 comma separated list of devices numbers. i.e. if
                 you want 3 devices, CUDA_VISIBLE_DEVICES=0,1,2 */
              /* NOTE: Should this be separated into a function? */
              DBG("Getting num GPU for module");
              int num_gpu = exec_info.module_num_gpu[exec_info.module_num];
              DBG("Got num_gpu = %d for module %s", num_gpu, exec_info.modules[exec_info.module_num].funcname);
              if (num_gpu > 0) {
                char *visible_devices; 
                char *visible_devices_env_var; 
                visible_devices = (char*)malloc(sizeof(char) * (num_gpu * 2) + 1); /* 2 characters for each device */
                int i;
                visible_devices[0] = '\0';
                /* Build string for list of devices */
                for (i = 0; i < num_gpu; i++) {
                  DBG("Giving module gpu %d", next_gpu);
                  sprintf(&visible_devices[i * 2], "%d,", next_gpu);
                  DBG("setting next_gpu to: %d", next_gpu);
                  next_gpu = (next_gpu + 1) % num_gpu_available;
                }
                /* Get rid of trailing comma */
                visible_devices[strlen(visible_devices) - 1] = '\0';
                /* Create the final string */
                visible_devices_env_var = (char*)malloc(sizeof(char) * strlen("CUDA_VISIBLE_DEVICES") * strlen("visible_devices"));
                visible_devices_env_var[0] = '\0';
                strcat(visible_devices_env_var, "CUDA_VISIBLE_DEVICES="); 
                strcat(visible_devices_env_var, visible_devices);
                DBG("Setting environment variable for %s to %s", 
                  exec_info.modules[exec_info.module_num].funcname, visible_devices_env_var);
                putenv(visible_devices_env_var);
                free(visible_devices); 
                free(visible_devices_env_var);
              }

              //Loop through functions to check for existence of Module
              //If static library:
              if (!strcmp(exec_info.modules[exec_info.module_num].libtype, "static")) {
                // Find the module in the function map
                for (int i = 0; i < fmap_count; i++) {
                  // If the module's name is in the function map
                  if (!strcmp(function_map[i].name, exec_info.modules[exec_info.module_num].funcname)) {
                    printf("IEL-Module-Start : Rank[%d] Name[%s]\n", exec_info.IEL_rank, exec_info.modules[exec_info.module_num].funcname);
                    // Run the module
                    
                    /* TC, Nov 2016: Allocate shared_bc for the module */
                    if(exec_info.modules[exec_info.module_num].uses_bc == 1) {
                      exec_info.shared_bc = (double*)calloc(exec_info.num_shared_bc, sizeof(double));
                    }

                    char temp [20];
                    sprintf (temp, "Function_%d", i);
                    timestamp ("Begin", temp, 1);
                    // Why would anyone be such a sloppy programmer as to not
                    // declare your variables properly at the beginning of the
                    // function? Please have some pride and produce halfway
                    // decent code or don't bother.
//                    int ret = function_map[i].func(&exec_info);
                    ret = function_map[i].func(&exec_info);
                    timestamp ("End", temp, -1);
                    printf("IEL-Module-End : Rank[%d] Name[%s] Status[%d]\n", exec_info.IEL_rank, exec_info.modules[exec_info.module_num].funcname, ret);
                    IEL_print_error(ret);

                    // Deallocate the shared_bc
                    if(exec_info.modules[exec_info.module_num].uses_bc == 1 &&
                       exec_info.num_shared_bc > 0 &&
                       exec_info.shared_bc != NULL) {
                      free(exec_info.shared_bc);
                    }

                    break;
                  }
                }
              } else if (!strcmp(exec_info.modules[exec_info.module_num].libtype, "shared")
                  || !strcmp(exec_info.modules[exec_info.module_num].libtype, "dynamic")) {
                // If shared library:
                printf("rank %d module %d going to dlopen '%s'\n", exec_info.IEL_rank,
                    exec_info.module_num, exec_info.modules[exec_info.module_num].libname);

                void *lib_handle = dlopen(exec_info.modules[exec_info.module_num].libname, RTLD_LAZY);
                if (!lib_handle) {
                  fprintf(stderr, "dlopen failed: %s\n", dlerror());
                  //return -1;
                  is_err = -1;
                }

                /* Clear any existing error before calling dlsym */
                dlerror();

                // Only call dlsym if no error
                if(!is_err) {
                  func = (void (*)(IEL_exec_info_t *))dlsym(lib_handle, exec_info.modules[exec_info.module_num].funcname);

                  char* error;
                  if ((error = dlerror()) != NULL) {
                    fprintf(stderr, "%s\n", error);
                    //return -1;
                    is_err = -1;
                  }

                  (*func)(&exec_info);
                }

                dlclose(lib_handle);
              }
              if(exec_info.modules[exec_info.module_num].splitdir) {
                //TODO: save original directory earlier and switch back to it later on
                // I had to waste my time to go back through and see where ret
                // is used so I can properly re-declare it at the top. Anyone
                // who doesn't declare variables at the top of the function
                // should be ashamed of their unreadable code. 
                ret = chdir(curdir);
                if (ret != 0) {
                  perror("chdir() error");
                  is_err = -1;
                }
                //free(curdir);
              }
            }
          }
          if(stdinredirect) {
            //ret = 0;
            if(dup2(realSTDIN, 0) == -1) {
              perror("Failed to restore stdin");
            }
            if(close(realSTDIN)) {
              perror("Failed to close file");
            }
            if(close(inFD)) {
              perror("Failed to close file");
            }
          }
        //if(!all_done)
        IEL_workflow_reset(&exec_info);

        // Reset number of threads to 1 so we do not occupy too many at once
        // (May not need this?)
        if(threads >= 1) {
          omp_set_num_threads(threads);
        }
        
        idx++;
      }
      //DBG("Out of main workflow loop on rank %d run %d", exec_info.IEL_rank, run);
      DBG("Leader is %d, current rank is %d", exec_info.work_groups[exec_info.work_set_num][exec_info.work_group_num].leader,
          exec_info.IEL_rank);
      if (exec_info.tuple_size > 0 && exec_info.work_groups[exec_info.work_set_num][exec_info.work_group_num].leader 
          == exec_info.IEL_rank && (exec_info.tuple_group != exec_info.work_group_num || exec_info.tuple_set != exec_info.work_set_num)) {
        // The group has finished; send a message to the tuple server saying so
        IEL_group_done(exec_info.tuple_rank, exec_info.work_group_num, exec_info.work_set_num, &exec_info);
      }

      // Catch remaining RUN_DONE calls; should help with repurposing processes later
      if(exec_info.num_runs[exec_info.work_set_num] > 1 && run == num_runs-1 
          && (exec_info.tuple_group != exec_info.work_group_num || exec_info.tuple_set != exec_info.work_set_num)) {
        IEL_run_waiting(exec_info.tuple_rank, &exec_info);
      }
    }
  }

  // If we have a tuple server running, notify it that we are done
  if (exec_info.tuple_size > 0 
      && (exec_info.IEL_rank >= exec_info.tuple_rank + exec_info.tuple_size || exec_info.IEL_rank < exec_info.tuple_rank)) {
    IEL_tupleImDone(exec_info.tuple_size);
  }

  DBG("All done, returning...");
  //MPI_Barrier(exec_info.IEL_comm);
  timestamp ("Begin", "IEL Finalize", 1);
  IEL_exec_finalize(&exec_info);
  timestamp ("End", "IEL Finalize", -1);

  return 0;
}

