#!/bin/sh

export LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:$PWD"

# mpirun -n 3 xterm -e gdb ../IEL_executive
mpirun -n 3 valgrind ../IEL_executive simple.cfg
