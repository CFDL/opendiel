#include <mpi.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#include "IEL.h"
#include "IEL_exec_info.h"

/**
 * @brief A dummy module function for the sake of simulation.
 *
 * @param IELComm The global communicator of the executive.
 * @param moduleComm The communicator for this module.
 * @param my_cinfo Pointer to this process's component info.
 * @param gets 2d array of gets.
 * @param puts 2d array of puts.
 * @param numSharedBC The number of shared BCs.
 * @param sharedBCSize Array of shared BC sizes.
 * @param modRank Rank within the module. 
 * @param rank Global rank.
 */
void Module0(IEL_exec_info_t *exec_info)
{
  int i, j, k, my_bc, iterations, seg_start, seg_len;
  shared_bc_t *my_segment, init_val;

  // pick some initial value for the shared bc elements
  init_val = (shared_bc_t) exec_info->module_rank + 10;

  printf("Module 0 proc %d provides: ", exec_info->module_rank);
  for(k = 0; k < exec_info->cinfo->nump; k++)
    printf("%d ", exec_info->cinfo->provides[k]);
  printf("\n");
  
  printf("Module 0 proc %d requires: ", exec_info->module_rank);
  for(k = 0; k < exec_info->cinfo->numr; k++)
    printf("%d ", exec_info->cinfo->requires[k]);
  printf("\n");

  for(i = 0; i < exec_info->num_shared_bc; i++) {
    if(exec_info->bc_start[i] >= 0) {
      my_segment = exec_info->shared_bc[i];
      seg_start = exec_info->bc_start[i];
      seg_len = exec_info->bc_len[i];
      my_bc = i;
    }
    for(j=0;j<exec_info->shared_bc_sizes[i];j++)
      exec_info->shared_bc[i][j] = 0.0;
  }

  printf("Module 0 proc %d, BC %d:  start = %d, len = %d\n", 
     exec_info->module_rank, my_bc, seg_start, seg_len);

  iterations = 3;

  for(k = 0; k < iterations; k++) {
    if(k == 0) {
      for(j = seg_start; j < seg_start+seg_len; j++)
        exec_info->shared_bc[my_bc][j] = init_val;

      printf("iter %d, module 0 proc %d after INIT, BC:\n", k, exec_info->module_rank);
      IEL_exec_print_sbc(exec_info);
    }
    else {
      if(IEL_get_all(exec_info->cinfo, exec_info->num_handles, exec_info->handles) != IEL_SUCCESS) {
        fprintf(stderr, "get failed\n");
      }

      printf("iter %d, module 0 proc %d after GET, BC:\n", k, exec_info->module_rank);
      IEL_exec_print_sbc(exec_info);
    }

    // do a pointless calculation.. in this case increment by 10
    
    for(j = seg_start; j < seg_start+seg_len; j++)
      exec_info->shared_bc[my_bc][j] += 10;
    sleep(1);

    printf("iter %d, module 0 proc %d before PUT, BC:\n", k, exec_info->module_rank);
    IEL_exec_print_sbc(exec_info);

    if(IEL_put_all(exec_info->cinfo, exec_info->num_handles, exec_info->handles) != IEL_SUCCESS) {
      fprintf(stderr, "put failed\n");
    }
  }

//<<<<<<< .mine
/*  ret = free_resources(exec_info->shared_bc, ghs, phs, exec_info->num_shared_bc);
if(ret < 0) {
    fprintf(stderr, "Module 0, modRank %d: free_resources failed\n", exec_info->module_rank);
    exit(EXIT_FAILURE);
  }
*/
//=======
//>>>>>>> .r217
  printf("finished module 0\n");

  return;
}
