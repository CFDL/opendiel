#!/bin/sh
#PBS -N 4_4_test
#PBS -j oe
#PBS -A UT-TENN0036
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=4

### End of PBS options ###

date

if [ "$PBS_JOBID" != "" ]; then
  cd $PBS_O_WORKDIR

  echo "nodefile="
  cat $PBS_NODEFILE
  echo "=end nodefile"
fi

# run the program

which mpirun

export LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:$PWD"

mpirun -n 4 ../IEL_executive 4proc_4node.cfg
