#!/bin/sh
#PBS -N 6_4_test
#PBS -j oe
#PBS -A UT-TENN0036
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=8

### End of PBS options ###

date

if [ "$PBS_JOBID" != "" ]; then
  cd $PBS_O_WORKDIR

  echo "nodefile="
  cat $PBS_NODEFILE
  echo "=end nodefile"
fi

# run the program

which mpirun

export LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:$PWD"

mpirun -n 6 ../IEL_executive 6proc_4node.cfg
