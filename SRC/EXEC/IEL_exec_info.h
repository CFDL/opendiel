/*
 * Copyright (c) 2015 University of Tennessee
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/**
 * file IEL_exec_info.h
 */

#ifndef _IEL_EXEC_INFO_H
#define _IEL_EXEC_INFO_H

#ifndef DEBUG 
#define DEBUG 0
#endif

#if defined(DEBUG) && DEBUG > 0
 #define DBG(fmt, args...) fprintf(stdout, "IEL_DEBUG: %s:%d:%s(): " fmt "\n", \
         __FILE__, __LINE__, __func__, ##args); fflush(stdout)
#define LINE DBG()
#else
 #define DBG(fmt, args...) /* Don't do anything in release builds */
 #define LINE
#endif

/* Macros to access the current module's argc and argv from confic file */
#define IEL_ARGC(a) a->modules[a->module_num].mod_argc
#define IEL_ARGV(a) a->modules[a->module_num].mod_argv

/* Set the maximum number of times a module can occurr in the workflow */
#define MOD_MAX_WF 10

/* Put and get tags; should not interfere with tuple */
#define IEL_DIR_COMM_TAG 10

#include "IEL.h"
#include <string.h>

#ifdef __cplusplus
extern "C" {
#endif


////////////////////////////////////////////
//
// DATA STRUCTURES 
//
///////////////////////////////////////////
/* TC, Mar 2016: Added num_dep, dep, and leader for calculating dependencies */
/**
 * This structure represents information required to describe workflow groups,
 * specified in the congiguration file.
 */

typedef struct {
  int size;
  int iterations;
  int persist;
  int num_modules;
  char **order;
  int num_dep;
  int *dep;
  int leader;
} IEL_workflow_group_t;

/**
 * This type represents whether a module is serial or parallel
 */

typedef enum  exec_mode_t { SERIAL, PARALLEL } exec_mode_t;

/* TC, Nov 2016: Added num_head_ranks, head_rank, and bc fields for direct communication */
/**
 * This structure represents the module information from the configuration
 * file.
 */
typedef struct {
  char*  libname;                /**< Name of the shared library to load */
  char*  libtype;                /**< Is the lib static or shared? */
  char   splitdir;               /**< Boolean, do we need separate working directories? */
  char*  splitdirpath;           /**< Path to separate directories */
  char*  funcname;               /**< Function name of the module to be called */
  char*  stdinFile;              /**< File to redirect stdin from */
  int    emode;                  /**< Is this module SERIAL or PARALLEL? (execution mode)*/
  int mod_argc;	             /**< The number of char * module arguments */
  char   **mod_argv;	           /**< Array of char * module arguments */
  int num_tags;               /**< Number of tags for this module */
  char   **mod_tags;             /**< Array of user-defined tags for each module*/
  int    num_head_ranks;         /**< The number of times the module is used in the workflow */
  int    head_ranks[MOD_MAX_WF]; /**< The ranks in IEL_comm that the head of the module will run on (rank 0 of module_comm) */
  /* TC, Nov 2016: Info for direct communication */
  int*  shared_bc_read;          /**< The elements of the shared_bc array the module can read from other modules */
  int*  shared_bc_write;         /**< The elements of the shared_bc array the module can write to other modules */
  int   uses_bc;                 /**< 1 if the module uses direct communication; 0 if not */
} module_depend_t;

/* TC, May 2016: Added num_work_sets, work_set_sizes, work_set_num,
 * tuple_set, and num_runs to structure, and added extra pointer to
 * num_work_groups and work_groups for implementation of workflow sets
 *
 * June 2016: Changed sizes to specify number of cores, added
 * module_processes, module_threads, module_copies,
 * module_device, copy_num, copy_comm
 *
 * Nov 2016: Revisions to direct communication
 */
/**
 * This structure represents everything the IEL Executive needs to know
 * to operate.
 */
typedef struct {
  int  num_shared_bc;     /**< Number of shared BCs */
  double* shared_bc;  /**< Array of shared boundary conditions */

  int num_modules;       /**< Number of modules */
  int* module_sizes;      /**< Array of processes for all copies of each module */
  int* module_cores;      /**< Array of cores for all copies of each module (threads * copies * processes) */
  int* module_processes;  /**< Array of number of processes of an individual copy of each module */
  int* module_threads;    /**< Array of number of threads of each module */
  int* module_copies;     /**< Array of number of copies of each module's processes to run */
  int* module_num_gpu;    /**< Array of number of GPUs to allocate for each module */
  //char* device;           /**< Array of if each module will run on a unique device (GPU, MIC, etc.) */
  int  num_work_sets;     /**< Number of workflow sets */
  int* num_work_groups;   /**< Number of workflow groups */
  int* work_set_sizes;    /**< Size of each workflow set */

  module_depend_t* modules;  /**< Module info struct for each module */
  IEL_workflow_group_t** work_groups; /**< Information about each workflow group in each set */

  int   packsize;  /**< size of packed buffer needed to xmit IEL exec struct */
  char* packed;    /**< packed buffer for xmitting IEL exec struct vi MPI */

  MPI_Comm IEL_comm;    /**< MPI communicator for the whole IEL system */
  MPI_Comm module_comm; /**< MPI communicator for just the modules */
  MPI_Comm group_comm;  /**< MPI communicator for just the group */
  MPI_Comm module_copy_comm;   /**< MPI communicator for just the copy */
  
  int IEL_rank;         /**< MPI rank within the IEL_comm communicator */
  int module_rank;      /**< MPI rank within the module_comm communicator */
  int module_copy_rank; /**< MPI rank within the module_copy_comm communicator */
  int IEL_num_ranks;    /**< Number of MPI ranks in IEL_comm communicator */
  int module_num_ranks; /**< Number of MPI ranks in module_comm communicator */
  int module_num;       /**< Current module number */
  int module_copy_num;  /**< Current copy number within the module */
  int work_group_num;   /**< Current number of work group */
  int work_set_num;     /**< Current number of work set */
  int* num_runs;        /**< Number of total runs */
  int num_proc;         /**< Total number of processes for all modules */

  IEL_component_info_t* cinfo;   /**< Pointer to the IELCOMM structure */

  int tuple_size;                /**< Number of tuple space servers */
  int tuple_rank;		 /**< Rank of the first tuple server; used in workflow group dependencies. -1 if no tuple server */
  int tuple_group;               /**< Tuple server's group number in work_groups. -1 if no tuple server */
  int tuple_set;                 /**< Tuple server's set number in work_groups. -1 if no tuple server */

  int num_gpu_available;         /**< Number of GPUs available */

  char *server_config;    /**<The configuration of the servers if there are multiple tuple servers */
  //char* cfg_filename;          /** Name of the config file **/
  
} IEL_exec_info_t;


//Just the essential information from the struct above
//for a module using tuple space communication.
typedef struct {
	int      num_modules;
	int*     module_sizes;
	int      mod_argc;
	char**   mod_argv;
	MPI_Comm IEL_comm;
	MPI_Comm module_comm;
	int      IEL_rank;
	int      module_rank;
	int      IEL_num_ranks;
	int      module_num_ranks;
	int      module_num;
	int      num_proc;
	int      tuple_size;
} IEL_reduced_info_t;

//For a Fortran module we need to separately pass
//a converted argv array, so we don't include it here.
typedef struct {
  int       num_modules;
  MPI_Fint  IEL_comm;
  MPI_Fint  module_comm;
  MPI_Fint  module_copy_comm;
  int       IEL_rank;
  int       module_rank;
  int       module_copy_rank;
  int       IEL_num_ranks;
  int       module_num_ranks;
  int       module_copy_num_ranks;
  int       module_num;
  int       module_copy_num;
  int       tuple_size;
  int       mod_argc;
  int*      mod_argv_len;
  char**    mod_argv;
  // TODO: Add direct communication fields
} IEL_fort_info_t;

////////////////////////////////////////////
//
// FUNCTIONS
//
///////////////////////////////////////////

/* strdup is not supported by every compiler (source?) */
char* dupstr(const char* str);

/* Calls MPI_Init, IELExecutive, and MPI_Finalize -- Skeleton Driver*/
int IELExecInit(int, char**);

/* Called by the user driver code, loads shared objects and calls the modules*/
int IELExecutive(MPI_Comm, char*);

/* Adds a module to the function map */
/* TODO: Asked Tanner about the map struct's (and map *) purpose */
void IELAddModule(int (*func)(IEL_exec_info_t*), char* func_name);

/* Populates the exec_info_t struct. This data is obtained from the config file */
int IEL_exec_config(IEL_exec_info_t*, char*);

/* Clears the exec_info_t struct (Only shared_bc, module_sizes, modules, and
 * packed are set to NULL. Nothing else is touched. TODO: ask about this*/
int IEL_exec_clear(IEL_exec_info_t*);

/* Populates the exec_info_t struct. This data is obtained from an hdf5 file */
int IEL_exec_init_from_hdf5(IEL_exec_info_t*, char*);

/* Programmer assist function to print out the IEL Executive struct to stdout */
int IEL_exec_dump_info(IEL_exec_info_t*);

/* Unpacks the buffer received via MPI ino the IEL Executive info struct. This
 * buffer is stored in exec_info->packed and the length is in exec_info->packsize. 
 * Returns 0 on success and -1 on failure. */
int IEL_exec_unpack_info(IEL_exec_info_t*, MPI_Comm);

/* Packs the exec_info_t struct into a buffer to be transmitted via MPI. */
int IEL_exec_pack_info(IEL_exec_info_t*, MPI_Comm);

/* TODO: Not implemented (No decleration) */
int IEL_exec_get_idx(IEL_exec_info_t*, int, int*, int*);

/* Stores data from executive info into a struct containing only information
 * useful to a Fortran module. */
IEL_fort_info_t reduceFortInfo(IEL_exec_info_t*);

/* */
int IEL_workflow_init(IEL_exec_info_t *, int, char *);
int IEL_bc_put(IEL_exec_info_t*, const char*, MPI_Request*);
int IEL_bc_get(IEL_exec_info_t*, const char*, MPI_Request*);
int IEL_bc_exchange(IEL_exec_info_t *exec_info, const char* module, MPI_Request *request);
int IEL_barrier(IEL_exec_info_t*);
int IEL_module_barrier(IEL_exec_info_t*);
int IEL_group_barrier(IEL_exec_info_t*);
int IEL_module_copy_barrier(IEL_exec_info_t*);
void timestamp(char*, char*, int);
void timer_finalize(int); 

/* Checks the return value of malloc to ensure that the malloc call succeeded.
 * Call as if calling malloc() */
void *IEL_alloc(size_t size);
void IEL_exec_print_sbc(IEL_exec_info_t *);

#ifdef __cplusplus
}
#endif

#endif
