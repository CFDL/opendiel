/*
 * Copyright (c) 2015 University of Tennessee
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include "IEL_exec_info.h"
#include "tuple_comm.h"
#include <mpi.h>
#include "IEL.h"
#include "tspace.h"
#include "IEL_group_graph.h"
//#include "ts.h"

typedef struct request_ {
  unsigned char del;
  int src;
} *request;

/* TC, Feb 2016: Function created */
/**
 * Wrapper around a receive; called so that the executive can access the
 * server to know when the calling group is ready to run
 */
int IEL_group_waiting(int tuple_rank, IEL_exec_info_t* exec_info) {
  int recvBuf;
  MPI_Request request;
  MPI_Status status;
  //printf("About to receive on waiting group!\n");
  // When we receive a message back, the groups we need to finish are done; block until then
  MPI_Irecv(&recvBuf, 1, MPI_INT, tuple_rank, GROUP_READY, exec_info->IEL_comm, &request);
  MPI_Wait(&request, &status);
  //printf("Receive on waiting group successful!\n");
  return IEL_SUCCESS;
}

/* TC, Feb 2016: Function created */
/**
 * Wrapper around a receive; called so that the executive can access the
 * server to know when the calling group is ready to run
 */
int IEL_run_waiting(int tuple_rank, IEL_exec_info_t *exec_info) {
  int recvBuf;
  MPI_Request request;
  MPI_Status status;
  //printf("About to receive on waiting group!\n");
  // When we receive a message back, the groups we need to finish are done; block until then
  MPI_Irecv(&recvBuf, 1, MPI_INT, tuple_rank, RUN_READY, exec_info->IEL_comm, &request);
  MPI_Wait(&request, &status);
  //printf("Receive on waiting group successful!\n");
  return IEL_SUCCESS;
}

/**
 * Wrapper around a send; called so that the executive can access the
 * server to let it know that the group has finished
 */
int IEL_group_done(int tuple_rank, int work_group_num, int work_set_num, IEL_exec_info_t* exec_info) {
  //printf("Group %d done; about to send to tuple on %d", work_group_num, tuple_rank);
  // Place group and set number in a buffer and send to tuple server
  int data[2];
  data[0] = work_group_num;
  data[1] = work_set_num;
  DBG("About to do a group_done send from group %d set %d to tuple server on %d", data[0], data[1], tuple_rank);
  MPI_Send(&data, 2, MPI_INT, tuple_rank, GROUP_DONE, exec_info->IEL_comm);
  return IEL_SUCCESS;
}

int ielTupleServer(IEL_exec_info_t *exec_info)
{
  int rv, server_number;
  MPI_Status status;
  TS ts = make_ts();
  TS requests = make_ts();
  void* buf;
  int numRequests = 0;
  int tag;
  int src;
  int packsize;
  int pos;
  void* data;
  int datasize;
  int datatag;
  request curreq;
  int trash;
  MPI_Request request;

  int nothing = 0; // Used as placeholder for sends/receives that serve as signals
//  server_number++; // Number of the tuple server
  server_number = exec_info->module_num;
//  fprintf(stderr, "This server has been initialized by module num %d\n", exec_info->module_num);
//  fprintf(stderr, "Number of servers: %d\n", exec_info->tuple_size);
//  fprintf(stderr, "Server_number: %d\n", server_number); // ZT DEBUG
//  fprintf(stderr, "Module_num from tuple server: %d\n", exec_info->module_num);

  // Store a list of the set and group of each group that is ready to run
  int* ready_list_set;
  int* ready_list_group;
  // Initialize to list of -1s that is the size of all groups (most possible ready)
  int ready_list_size = 0;
  for(int i = 0; i < exec_info->num_work_sets; i++) {
    ready_list_size += exec_info->num_work_groups[i];
  }
  ready_list_set = (int*)malloc(sizeof(int) * ready_list_size);
  ready_list_group = (int*)malloc(sizeof(int) * ready_list_size);
  for(int i = 0; i < ready_list_size; i++) {
    ready_list_set[i] = -1;
    ready_list_group[i] = -1;
  }
  //int ready_list_pos = 0; // Current index in the ready list; not currently used
  int ready_list_elem = 0; // Current number of elements in the ready list
  
  // Set up the dependency graph
  group_node** dep_nodes;
  dep_nodes = (group_node **)malloc(sizeof(group_node*) * exec_info->num_work_sets);
  for(int i = 0; i < exec_info->num_work_sets; i++) {
    dep_nodes[i] = (group_node *)malloc(sizeof(group_node) * exec_info->num_work_groups[i]);
    for(int j = 0; j < exec_info->num_work_groups[i]; j++) {
      dep_nodes[i][j].setno = i;
      dep_nodes[i][j].groupno = j;
      dep_nodes[i][j].size = exec_info->work_groups[i][j].size;
      dep_nodes[i][j].color = 'w';
      dep_nodes[i][j].num_pred = exec_info->work_groups[i][j].num_dep;
      dep_nodes[i][j].num_successors = 0; // Default 0; incremented later
      /*if(dep_nodes[i][j].num_pred == 0) {
	dep_nodes[i][j].is_head = 1;
      } else {
	dep_nodes[i][j].is_head = 0;
      }*/
      dep_nodes[i][j].cur_pred = dep_nodes[i][j].num_pred; // Preserve num_pred
    }
  }
  // Set up predecessors
  for(int i = 0; i < exec_info->num_work_sets; i++) {
    for(int j = 0; j < exec_info->num_work_groups[i]; j++) {
      dep_nodes[i][j].pred = (group_node**)malloc(sizeof(group_node**) * dep_nodes[i][j].num_pred);
      for(int k = 0; k < exec_info->work_groups[i][j].num_dep; k++) {
	// Set each predecessor of the node to each of the node's dependency numbers;
	// use pointers to avoid having to reallocate the struct and for consistency
	dep_nodes[i][j].pred[k] = &(dep_nodes[i][exec_info->work_groups[i][j].dep[k]]);
	DBG("dep_nodes[%d][%d].pred[%d] = &(dep_nodes[%d][%d])", i, j, k, i, exec_info->work_groups[i][j].dep[k]);
      }
    }
  }

  // Set up successors
  for(int i = 0; i < exec_info->num_work_sets; i++) {
    // Find the number of successors
    for(int j = 0; j < exec_info->num_work_groups[i]; j++) {
      for(int k = 0; k < dep_nodes[i][j].num_pred; k++) {
	// Update the successor count of each predecessor
	dep_nodes[i][dep_nodes[i][j].pred[k]->groupno].num_successors++;
      }
    }
    // Allocate the successors array
    for(int j = 0; j < exec_info->num_work_groups[i]; j++) {
      dep_nodes[i][j].successors = (group_node**)malloc(sizeof(group_node**) * dep_nodes[i][j].num_successors);
    }

    // Keep track of our position in the successors array for each group
    int *successor_idx = (int*)calloc(exec_info->num_work_groups[i], sizeof(int));
    // Set up the successors array
    for(int j = 0; j < exec_info->num_work_groups[i]; j++) {
      for(int k = 0; k < dep_nodes[i][j].num_pred; k++) {
	// Set the successor of the current group's predecessor to be the current group
	int pred_group = dep_nodes[i][j].pred[k]->groupno;
	dep_nodes[i][pred_group].successors[successor_idx[pred_group]] = &(dep_nodes[i][j]);
	DBG("dep_nodes[%i][%d].successors[%d] = dep_nodes[%d][%d]", i, pred_group, successor_idx[pred_group], i, j);
	successor_idx[pred_group]++;
      }
    }
    free(successor_idx);

    // Set up a copy of the predecessors array for tracking completion of dependencies
    for(int j = 0; j < exec_info->num_work_groups[i]; j++) {
      dep_nodes[i][j].pred_remaining = (group_node**)malloc(sizeof(group_node**) * dep_nodes[i][j].num_pred);
      for(int k = 0; k < dep_nodes[i][j].num_pred; k++) {
	dep_nodes[i][j].pred_remaining[k] = dep_nodes[i][j].pred[k];
      }
    }
  }

  // Perform DFS to check if the dependencies will cause a deadlock
  int *deadlocked_sets = (int*)calloc(exec_info->num_work_sets, sizeof(int));
  for(int i = 0; i < exec_info->num_work_sets; i++) {
    // TODO: Keep track of if there are sections of nodes without heads
    for(int j = 0; j < exec_info->num_work_groups[i]; j++) {
      if(dep_nodes[i][j].color == 'w') {
	if(dfs(&dep_nodes[i][j]) != IEL_SUCCESS) {
	  // The DFS has detected a cycle; ignore dependencies for these groups
	  fprintf(stderr, "Error: IEL dependencies form a deadlock in set %d\n", i);
	  // TODO: Make this do something
	  deadlocked_sets[i] = 1;
	  break;
	}
      }
    }
  }

  // Notify all processes whether their set deadlocks
  for(int i = 0; i < exec_info->num_work_sets; i++) {
    for(int j = 0; j < exec_info->num_work_groups[i]; j++) {
      for(int proc = exec_info->work_groups[i][j].leader;
	  proc < exec_info->work_groups[i][j].leader + exec_info->work_groups[i][j].size; proc++) {
	// Do not send to tuple server
	// ZT DEBUG:
//	fprintf(stderr, "proc: %d, exec_info->tuple_rank: %d, exec_info->tuple_size: %d\n", 
//		proc, exec_info->tuple_rank, exec_info->tuple_size);
//fprintf(stderr, "Checking if proc (%d) is < %d or <= %d\n", proc, exec_info->tuple_rank, 
//	exec_info->tuple_rank + exec_info->tuple_size);
	// ZT DEBUG CHANGING THIS IF STATEMENT TO WORK WITH MULTIPLE TUPLE SERVERS
	if(proc < exec_info->tuple_rank || proc >= exec_info->tuple_rank + exec_info->tuple_size) {
	  MPI_Isend(&(deadlocked_sets[i]), 1, MPI_INT, proc, RUN_READY, exec_info->IEL_comm, &request);
	}
      }
    }
  }
  DBG("Successful formation of dependency graph");


  // ZT 7/13/2017
  if (exec_info->module_num == 0 && exec_info->tuple_size > 1) manager_init(exec_info->tuple_size, exec_info);
  // Create a list of how many groups each a set are done; if all of them finish,
  // the next run can begin
  int* num_groups_done = (int*)calloc(exec_info->num_work_sets, sizeof(int));
  num_groups_done[exec_info->tuple_set] = 1;


  //Overall exit condition for main loop
  //Set to true when all modules have reported being done
  bool done = false;
  //Keep track of the status of all non-tuple-space processes
  int numProcsDone = 0;
  const int numProcs = exec_info->num_proc - exec_info->tuple_size;

  //for safety reasons, we need to keep track of each individual process
  //and whether it is done.
  bool procsDone[exec_info->num_proc];
  //Everyone starts out not done
  for(int proc = exec_info->tuple_size; proc < exec_info->num_proc; proc++)
    procsDone[proc] = false;
    
  while(!done) 
  { 
    rv = MPI_Probe(MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
// ZT DEBUG
//    fprintf(stderr, "Request being fulfilled by server %d\n", server_number);
    //Assert(!rv, "TSERV: error in MPI_Probe");

    tag = status.MPI_TAG;
    src = status.MPI_SOURCE;

    if(tag == TUPLE_PUT) {
      numRequests++;
// ZT
//fprintf(stderr, "Putting data on server (%d) from source %d\n", server_number, src);

      rv = MPI_Get_count(&status, MPI_PACKED, &packsize);
      //Assert(!rv, "TSERVP: error in MPI_Get_count");

      buf = malloc(packsize);

      rv = MPI_Recv(buf, packsize, MPI_PACKED, src, tag, MPI_COMM_WORLD, &status);
      //Assert(!rv, "TSERVP: error in MPI_Recv");

      pos = 0;
      rv = MPI_Unpack(buf, packsize, &pos, &datatag, 1, MPI_INT, MPI_COMM_WORLD);
      //Assert(!rv, "TSERVP: error in MPI_Unpack 1");

      rv = MPI_Unpack(buf, packsize, &pos, &datasize, 1, MPI_INT, MPI_COMM_WORLD);
      //Assert(!rv, "TSERVP: error in MPI_Unpack 2");

      data = malloc(datasize);

      rv = MPI_Unpack(buf, packsize, &pos, data, datasize, MPI_BYTE, MPI_COMM_WORLD);
      //Assert(!rv, "TSERVP: error in MPI_Unpack 3");
// ZT DEBUG: 
if (0) {
  int z;
  fprintf(stderr, "RECEIVED THE FOLLOWING DATA ON SERVER # %d (tagged: %d, size: %d):\n", server_number, datatag, datasize);
  fprintf(stderr, "----------------------------------------\n");
  for (z = 130; z < 145; z++) fprintf(stderr, "SERVER %d (data): (%d) %.4lf\n", server_number, z, ((double *) data)[z]);
  fprintf(stderr, "****************************************\n");
  for (z = 131; z < 146; z++) fprintf(stderr, "SERVER %d  (buf): (%d) %.4lf\n", server_number, z-1, ((double *) buf)[z]);
  fprintf(stderr, "----------------------------------------\n");
}



      ts_put(ts, datatag, data, datasize);

      free(buf);
      free(data);

      //Check if someone is waiting for this tuple
      while(1) {
        curreq = (struct request_*)ts_read(requests, datatag, &trash);
        //If not, stop
        if(curreq == NULL) break;

        //If so, get the data and optionally delete it
        if(curreq->del) data = ts_take(ts, datatag, &datasize);
        else data = ts_read(ts, datatag, &datasize);

        //If the data is now gone, stop
        //If the data is there, take the request and fulfill it
        if(data == NULL) {
          break;
        } else {
            curreq = (struct request_*)ts_take(requests, datatag, &trash);
        }

        rv = MPI_Send(data, datasize, MPI_BYTE, curreq->src, SERVER_SEND, MPI_COMM_WORLD);
        //Assert(!rv, "TSERVP: error in MPI_Send");

        /* ZT - Jan. 2018: Only free the data if it is being removed from the
         * ts, otherwise data that is expected to be on the ts is freed. 
         */
        if (curreq->del) free(data);
        free(curreq);
      }
    } else if (tag == TUPLE_TAKE || tag == TUPLE_READ) {
      numRequests++;
//ZT
//fprintf(stderr, "Getting data from server (%d) requested by src: %d\n", server_number, src);

      rv = MPI_Recv(&datatag, 1, MPI_INT, src, tag, MPI_COMM_WORLD, &status);
      //Assert(!rv, "TSERVG: error in MPI_Recv");

      if(tag == TUPLE_TAKE)
        data = ts_take(ts, datatag, &datasize);
      else
        data = ts_read(ts, datatag, &datasize);

      if(data == NULL) {
        rv = MPI_Send(&nothing, 1, MPI_INT, src, NOT_FOUND, MPI_COMM_WORLD);
        //Assert(!rv, "TSERVG: error in MPI_Send 1");

        curreq = (struct request_*)malloc(sizeof(struct request_));
        curreq->src = src;
        if(tag == TUPLE_TAKE) {
          curreq->del = 1;
        } else {
          curreq->del = 0;
        }

        ts_put(requests, datatag, curreq, sizeof(struct request_));
        free(curreq);
      } else {
        rv = MPI_Send(data, datasize, MPI_BYTE, src, SERVER_SEND, MPI_COMM_WORLD);
        //Assert(!rv, "TSERVG: error in MPI_Send 2");

	// ZT, 6/30/2017: Found a 'double free/memory corruption' bug with the tget 
	// function when using the keep (TUPLE_READ) flag option. Traced the bug to 
	// this free statement. I am removing this statement until I can determine 
	// when to correctly free. 
	// END
//        free(data);
      }

    } else if (tag == PROC_DONE) {
      rv = MPI_Recv(&nothing, 1, MPI_INT, src, tag, MPI_COMM_WORLD, &status);
      //Assert(!rv, "TSERVG: error in MPI_Recv");
      if(!procsDone[src]) {
        procsDone[src] = true;
        numProcsDone++;
      }
    } else if (tag == GROUP_DONE) {
      /**
       * TC, June 2016:
       * Receives a message that a group has finished, and in return, removes the
       * finished group from the list of all groups that depend on it. If any
       * groups no longer have dependencies afterward, sends them a message that
       * their dependencies have finished. Updated to utilize a DAG.
       */
      int groupNum, setNum/*, source*/;
      // Source is for later use
      MPI_Status status;

      int data[2];
      // Receive the numbers of the completed group and set
      rv = MPI_Recv(&data, 2, MPI_INT, MPI_ANY_SOURCE,
          GROUP_DONE, exec_info->IEL_comm, &status);
      groupNum = data[0];
      setNum = data[1]; // All group dependency logic stays in-set

      // Mark this group as done
      num_groups_done[setNum]++;
      // Mark the groups that have finished as ready to run; check all successors of the finished node
      for(int i = 0; i < dep_nodes[setNum][groupNum].num_successors; i++) {
        // Current successor that we are dealing with
        int suc_num = dep_nodes[setNum][groupNum].successors[i]->groupno;
        // Track if the group is ready to run
        int is_ready = 1;
        // Remove the predecessor that contains this node
        for(int j = 0; j < dep_nodes[setNum][suc_num].num_pred; j++) {
          // Examine each node that has not been removed
          if(dep_nodes[setNum][suc_num].pred_remaining[j] != NULL) {
            // If this predecessor is the node that just finished, remove it
            if(dep_nodes[setNum][suc_num].pred_remaining[j]->groupno == groupNum) {
              dep_nodes[setNum][suc_num].pred_remaining[j] = NULL;
            } else {
              // If the node has any predecessors left that have not finished, do not run yet
              is_ready = 0;
            }
          }
        }
        // Place the ready successor node in the ready list
        if(is_ready) {
          ready_list_set[ready_list_elem] = setNum;
          ready_list_group[ready_list_elem] = suc_num;
          ready_list_elem++;
        }
      }

      // TODO: Eventually replace this method with a more complex queue system based
      //       on how many processes are available, once process recycling is available
      // Send a message to all nodes ready to run after dependencies are managed
      for(int i = 0; i < ready_list_elem; i++) {
        // Send to all processes in the group
        for(int j = 0; j < exec_info->work_groups[ready_list_set[i]][ready_list_group[i]].size; j++) {
          MPI_Isend(&nothing, 1, MPI_INT, exec_info->work_groups[ready_list_set[i]][ready_list_group[i]].leader + j, GROUP_READY,
              exec_info->IEL_comm, &request);
        }
        // Clear out the ready list
        ready_list_set[i] = -1;
        ready_list_group[i] = -1;
      }
      ready_list_elem = 0;

      // Check if the run is done, and if so, send a message to all processes in the set and then reset dependencies
      if(num_groups_done[setNum] == exec_info->num_work_groups[setNum]) {
        // Send to every group in the set
        for(int i = 0; i < exec_info->num_work_groups[setNum]; i++) {
          // Send to every process in the group
          int lead_proc = exec_info->work_groups[setNum][i].leader;
          for(int j = lead_proc; j < lead_proc + exec_info->work_groups[setNum][i].size; j++) {
            // Make sure not to send to tuple server
            if(j < exec_info->tuple_rank || j >= exec_info->tuple_rank + exec_info->tuple_size) {
              MPI_Isend(&nothing, 1, MPI_INT, j, RUN_READY, exec_info->IEL_comm, &request);
            }
          }
        }

        // Reset the dependency tracking; tuple is always done
        num_groups_done[setNum] = 0;
        if(exec_info->tuple_set == setNum) {
          num_groups_done[setNum] = 1;
        } else {
          num_groups_done[setNum] = 0;
        }
        for(int i = 0; i < exec_info->num_work_groups[setNum]; i++) {
          for(int j = 0; j < dep_nodes[setNum][i].num_pred; j++) {
            dep_nodes[setNum][i].pred_remaining[j] = dep_nodes[setNum][i].pred[j];
          }
        }
      }
    } else {
      Assert(0, "TSERV: received invalid message type");
    }

    if(numProcsDone == numProcs)
      done = true;
  }

  printf("%d: Server fulfilled %d requests\n",exec_info->IEL_rank,numRequests);

  /* TC, June 2016: Free memory from tuple server */
  for(int i = 0; i < exec_info->num_work_sets; i++) {
    for(int j = 0; j < exec_info->num_work_groups[i]; j++) {
      free(dep_nodes[i][j].pred);
      free(dep_nodes[i][j].pred_remaining);
      free(dep_nodes[i][j].successors);
    }
    free(dep_nodes[i]);
  }
  free(dep_nodes);
  free(ready_list_set);
  free(ready_list_group);
  free(num_groups_done);

  ts_freeAll(ts);
  ts_freeAll(requests);
  DBG("Tuple server has freed all memory");

  return IEL_SUCCESS;
}
