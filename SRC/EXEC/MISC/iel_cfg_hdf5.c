/*
 * Copyright (c) 2015 University of Tennessee
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "hdf5.h"

#include "IEL_exec_info.h"

/**
 * @brief Initiates the values in the array sharedBCSize. Should be called in H5Literate.
 *
 * @param loc_id Id of the group that is being iterated through.
 * @param name Name of the current subobject.
 * @param linfo Info about the current link.
 * @param opdata Parameters that are passed to the function. Should be two (int *)s.
 *
 * @return 0 on success, -1 on failure
 */
static herr_t
sharedBCSize_init(hid_t loc_id, const char *name, const H5L_info_t *linfo, void *opdata)
{
  int *i;
  int size;
  herr_t status;
  hid_t oid, atid;
  H5O_info_t statbuf;
  int *sharedBCSize;

  i = ((int **)opdata)[0];
  sharedBCSize = ((int **)opdata)[1];

  oid = H5Oopen(loc_id, name, H5P_DEFAULT);
  if(oid < 0) {
    fprintf(stderr, "Error opening object /%s\nExiting...\n", name);
    return -1;
  }

  H5Oget_info(oid, &statbuf);
  if(statbuf.type == H5G_GROUP) {
    atid = H5Aopen(oid, "size", H5P_DEFAULT);
    if(atid < 0) {
      fprintf(stderr, "Error opening attribute /%s/size\nExiting...\n", name);
      return -1;
    }

    status = H5Aread(atid, H5T_NATIVE_INT, &size);
    if(status < 0) {
      fprintf(stderr, "Error reading attribute /%s/size\nExiting...\n", name);
      return -1;
    }

    status = H5Aclose(atid);
    if(status < 0) {
      fprintf(stderr, "Error closing attribute /%s/size\nExiting...\n", name);
      return -1;
    }

    sharedBCSize[(*i)] = size;
    (*i)++;
  }

  H5Oclose(oid);
  return 0;
}

/**
 * @brief Initiates the values in the array moduleSize. Should be called in H5Literate.
 *
 * @param loc_id Id of the group that is being iterated through.
 * @param name Name of the current subobject.
 * @param linfo Info about the current link.
 * @param opdata Parameters that are passed to the function. Should be two (int *)s.
 *
 * @return 0 on success
 */
static herr_t
moduleSize_init(hid_t loc_id, const char *name, const H5L_info_t *linfo, void *opdata)
{
  int *i;
  int size;
  herr_t status;
  hid_t oid, atid;
  H5O_info_t statbuf;
  int *moduleSize;

  i =  ((int **)opdata)[0];
  moduleSize = ((int **)opdata)[1];

  oid = H5Oopen(loc_id, name, H5P_DEFAULT);
  if(oid < 0) {
    fprintf(stderr, "Error opening object /%s\nExiting...\n", name);
    return -1;
  }

  H5Oget_info(oid, &statbuf);
  if(statbuf.type == H5G_GROUP) {
    atid = H5Aopen(oid, "size", H5P_DEFAULT);
    if(atid < 0) {
      fprintf(stderr, "Error opening attribute /%s/size\nExiting...\n", name);
      return -1;
    }

    status = H5Aread(atid, H5T_NATIVE_INT, &size);
    if(status < 0) {
      fprintf(stderr, "Error reading attribute /%s/size\nExiting...\n", name);
      return -1;
    }

    status = H5Aclose(atid);
    if(status < 0) {
      fprintf(stderr, "Error closing attribute /%s/size\nExiting...\n", name);
      return -1;
    }

    moduleSize[(*i)] = size;
    (*i)++;
  }

  H5Oclose(oid);
  return 0;
}

/**
 * @brief An hdf5 iteration function.
 *
 * @param loc_id The id of the current hdf5 parent object.
 * @param name Name of the current hdf5 object.
 * @param linfo Object that holds info about the current hdf5 link.
 * @param opdata Used to pass user defined data into the function.
 *
 * @return Returns a herr_t object.
 */
static herr_t
get_control(hid_t loc_id, const char *name, const H5L_info_t *linfo, void *opdata)
{
  int *sharedBCSize;
  int **g_array;
  int temp, size;
  herr_t status;
  hid_t dataSetID;
  H5O_info_t statbuf;

  sharedBCSize = (int *)((void **)opdata)[0];
  g_array = (int **)((void **)opdata)[1];

  dataSetID = H5Dopen(loc_id, name, H5P_DEFAULT);
  if(dataSetID < 0) {
    fprintf(stderr, "Error opening dataset /%s\nExiting...\n", name);
    return -1;
  }

  temp = atoi(name);
  g_array[temp] = (int *)malloc(sizeof(int) * sharedBCSize[temp]);

  status = H5Dread(dataSetID, H5T_NATIVE_INT, H5S_ALL, H5S_ALL, H5P_DEFAULT, g_array[temp]);
  if(status < 0) {
    fprintf(stderr, "Error reading dataset /%s\nExiting...\n", name);
    return -1;
  }

  status = H5Dclose(dataSetID);
  if(status < 0) {
    fprintf(stderr, "Error closing dataset /%s\nExiting...\n", name);
    return -1;
  }

  return 0;
}

/**
 * @brief An hdf5 iteration function.
 *
 * @param loc_id The id of the current hdf5 parent object.
 * @param name Name of the current hdf5 object.
 * @param linfo Object that holds info about the current hdf5 link.
 * @param opdata Used to pass user defined data into the function.
 *
 * @return Returns a herr_t object.
 */
static herr_t
read_input(hid_t loc_id, const char *name, const H5L_info_t *linfo, void *opdata)
{
  int *j;
  double **in_array;
  herr_t status;
  hid_t dataSetID;
  H5O_info_t statbuf;

  j = (int *)((void **)opdata)[0];
  in_array = (double **)((void **)opdata)[1];

  dataSetID = H5Dopen(loc_id, name, H5P_DEFAULT);
  if(dataSetID < 0) {
    fprintf(stderr, "Error opening dataset input/%s\nExiting...\n", name);
    return -1;
  }

  status = H5Dread(dataSetID, H5T_NATIVE_DOUBLE, H5S_ALL, H5S_ALL, H5P_DEFAULT, in_array);
  if(status < 0) {
    fprintf(stderr, "Error reading dataset /%s\nExiting...\n", name);
    return -1;
  }

  status = H5Dclose(dataSetID);
  if(status < 0) {
    fprintf(stderr, "Error closing dataset input/%s\nExiting...\n", name);
    return -1;
  }

  return 0;
}

/**
 * @brief An hdf5 iteration function.
 *
 * @param loc_id The id of the current hdf5 parent object.
 * @param name Name of the current hdf5 object.
 * @param linfo Object that holds info about the current hdf5 link.
 * @param opdata Used to pass user defined data into the function.
 *
 * @return Returns a herr_t object.
 */
static herr_t
old_read_input(hid_t loc_id, const char *name, const H5L_info_t *linfo, void *opdata)
{
  int *j;
  double **in_array;
  herr_t status;
  hid_t dataSetID, dataSpaceID;
  H5O_info_t statbuf;

  j = (int *)((void **)opdata)[0];
  in_array = (double **)((void **)opdata)[1];

  dataSetID = H5Dopen(loc_id, name, H5P_DEFAULT);
  if(dataSetID < 0) {
    fprintf(stderr, "Error opening dataset input/%s\nExiting...\n", name);
    return -1;
  }

  dataSpaceID = H5Dget_space(dataSetID);
  if(dataSpaceID < 0) {
    fprintf(stderr, "Error getting dataspace from dataset input/%s\nExiting...\n", name);
    return -1;
  }

  printf("input: number of dimensions = %d\n", H5Sget_simple_extent_ndims(dataSpaceID));

  status = H5Sclose(dataSpaceID);
  if(status < 0) {
    fprintf(stderr, "Error closing dataspace from dataset input/%s\nExiting...\n", name);
    return -1;
  }

  status = H5Dclose(dataSetID);
  if(status < 0) {
    fprintf(stderr, "Error closing dataset input/%s\nExiting...\n", name);
    return -1;
  }

  return 0;
}

char *
trim(char *s)
{
  char *p = s;

  if(p && *p) {
    while(*p && !isspace(*p)) p++;
    *p = '\0';
  }

  return s;
}

char *
read_str_attr(hid_t groupID, char *attr_name)
{
  hid_t attributeID, type, ftype, status;
  H5T_class_t type_class;
  char *newstr=NULL;
  size_t size;

  /* open attribute */
  attributeID = H5Aopen_name(groupID, attr_name);
  if(attributeID < 0) {
    fprintf(stderr, "Error opening attribute %s\nExiting...\n", attr_name);
    return NULL;
  }

  ftype = H5Aget_type(attributeID);
  if(ftype < 0) {
    fprintf(stderr, "Error getting attribute type for %s\nExiting...\n", attr_name);
    return NULL;
  }

  type_class = H5Tget_class(ftype);
  if(type_class < 0) {
    fprintf(stderr, "Error getting type class for %s\nExiting...\n", attr_name);
    return NULL;
  }
  else if(type_class != H5T_STRING) {
    fprintf(stderr, "Error: expected attribute %s to be H5T_STRING\nExiting...\n", attr_name);
    return NULL;
  }

  size = H5Tget_size(ftype);
  if(size < 0) {
    fprintf(stderr, "Error getting size of %s\nExiting...\n", attr_name);
    return NULL;
  }

  newstr = (char *)malloc(size);
  if(!newstr) {
    fprintf(stderr, "malloc failed in read_str_attr()\n");
    return NULL;
  }

  status = H5Aread(attributeID, ftype, newstr);
  if(status < 0) {
    fprintf(stderr, "Error reading attribute %s\nExiting...\n", attr_name);
    return NULL;
  }

  status = H5Tclose(ftype);
  if(status < 0) {
    fprintf(stderr, "Error closing attribute type %s\nExiting...\n", attr_name);
    return NULL;
  }

  /* close attribute */
  status = H5Aclose(attributeID);
  if(status < 0) {
    fprintf(stderr, "Error closing attribute %s\nExiting...\n", attr_name);
    return NULL;
  }

  return trim(newstr);
}

/**
 * initializes a IEL Executive info data structure by reading in the information
 * from the specified HDF5 file.
 *
 * @param exec_info -- pointer to the IEL Executive info struct to initialize
 * @param hdf5_filename -- the name of the input file
 *
 * @returns 0 on success, -1 on failure.
 */
int
IEL_exec_init_from_hdf5(IEL_exec_info_t *exec_info, char *hdf5_filename)
{
  hid_t fileID, groupID1, groupID2, groupID3, attributeID;
  herr_t status;
  int i, j, q, *data[2];
  char gname[64];
  int **g_array, **p_array;

  /* this macro wipes out / cleans up the struct in case of error
   * to avoid memory leaks
   */
#define cleanup_before_err_ret() \
  do {  \
    if(exec_info->shared_bc_sizes) free(exec_info->shared_bc_sizes); \
    if(exec_info->module_sizes) free(exec_info->module_sizes); \
    memset(exec_info, 0, sizeof(IEL_exec_info_t)); \
  } while(0);

  /* go ahead and wipe out the struct */
  memset(exec_info, 0, sizeof(IEL_exec_info_t));

  /* open hdf5 input file */
  fileID = H5Fopen(hdf5_filename, H5F_ACC_RDWR, H5P_DEFAULT);
  if(fileID < 0) {
    fprintf(stderr, "Error opening %s\nExiting...\n", hdf5_filename);
    cleanup_before_err_ret();
    return -1;
  }

  /* open group /General */
  groupID1 = H5Gopen(fileID, "General", H5P_DEFAULT);
  if(groupID1 < 0) {
    fprintf(stderr, "Error opening group /General\nExiting...\n");
    cleanup_before_err_ret();
    return -1;
  }

  /* open group /General/Shared BC */
  groupID2 = H5Gopen(groupID1, "Shared BC", H5P_DEFAULT);
  if(groupID2 < 0) {
    fprintf(stderr, "Error opening group /General/Shared BC\nExiting...\n");
    cleanup_before_err_ret();
    return -1;
  }

  /* open attribute /General/Shared BC/number */
  attributeID = H5Aopen(groupID2, "number", H5P_DEFAULT);
  if(attributeID < 0) {
    fprintf(stderr, "Error opening attribute /General/Shared BC/number\nExiting...\n");
    cleanup_before_err_ret();
    return -1;
  }

  /* read attribute /General/Shared BC/number into numSharedBC */
  status = H5Aread(attributeID, H5T_NATIVE_INT, &(exec_info->num_shared_bc));
  if(status < 0) {
    fprintf(stderr, "Error reading attribute /General/Shared BC/number\nExiting...\n");
    cleanup_before_err_ret();
    return -1;
  }

  /* close attribute /General/Shared BC/number */
  status = H5Aclose(attributeID);
  if(status < 0) {
    fprintf(stderr, "Error closing attribute /General/Shared BC/number\nExiting...\n");
    cleanup_before_err_ret();
    return -1;
  }

  exec_info->shared_bc_sizes = (int *)malloc(sizeof(int) * exec_info->num_shared_bc);

  if(!exec_info->shared_bc_sizes) {
    fprintf(stderr, "malloc failed\n");
    cleanup_before_err_ret();
    return -1;
  }

  i = 0;
  data[0] = &i;
  data[1] = exec_info->shared_bc_sizes;

  /* iterate through /General/Shared BC/ */
  H5Literate(groupID2, H5_INDEX_NAME, H5_ITER_INC, NULL, sharedBCSize_init, data);

  /* close group /General/Shared BC */
  status = H5Gclose(groupID2);
  if(status < 0) {
    fprintf(stderr, "Error closing group /General/Shared BC\nExiting...\n");
    cleanup_before_err_ret();
    return -1;
  }

  /* close group /General */
  status = H5Gclose(groupID1);
  if(status < 0) {
    fprintf(stderr, "Error closing group /General\nExiting...\n");
    cleanup_before_err_ret();
    return -1;
  }

  /* open group /Modules */
  groupID1 = H5Gopen(fileID, "Modules", H5P_DEFAULT);
  if(groupID1 < 0) {
    fprintf(stderr, "Error opening group /Modules\nExiting...\n");
    cleanup_before_err_ret();
    return -1;
  }

  /* open attribute /Modules/number */
  attributeID = H5Aopen(groupID1, "number", H5P_DEFAULT);
  if(attributeID < 0) {
    fprintf(stderr, "Error opening attribute /Modules/number\nExiting...\n");
    cleanup_before_err_ret();
    return -1;
  }

  /* read attribute /Modules/number */
  status = H5Aread(attributeID, H5T_NATIVE_INT, &(exec_info->num_modules));
  if(status < 0) {
    fprintf(stderr, "Error reading attribute /Modules/number\nExiting...\n");
    cleanup_before_err_ret();
    return -1;
  }

  /* close attribute /Modules/number */
  status = H5Aclose(attributeID);
  if(status < 0) {
    fprintf(stderr, "Error closing attribute /Modules/number\nExiting...\n");
    cleanup_before_err_ret();
    return -1;
  }

  /* create array for module sizes */
  exec_info->module_sizes = (int *)malloc(sizeof(int) * exec_info->num_modules);

  if(!exec_info->module_sizes) {
    fprintf(stderr, "malloc failed\n");
    cleanup_before_err_ret();
    return -1;
  }

  i = 0;
  data[0] = &i;
  data[1] = exec_info->module_sizes;

  H5Literate(groupID1, H5_INDEX_NAME, H5_ITER_INC, NULL, moduleSize_init, data);

  /* begin reading module gets/puts */

  exec_info->modules = (module_depend_t *)malloc(sizeof(module_depend_t) * exec_info->num_modules);

  if(!exec_info->modules) {
    fprintf(stderr, "malloc failed\n");
    cleanup_before_err_ret();
    return -1;
  }

  for(i=0;i<exec_info->num_modules;i++) {
    exec_info->modules[i].gets = (int **) malloc(sizeof(int *) * exec_info->num_shared_bc);
    exec_info->modules[i].puts = (int **) malloc(sizeof(int *) * exec_info->num_shared_bc);
    exec_info->modules[i].input = (double *) malloc(sizeof(double) * exec_info->num_shared_bc);

    sprintf(gname, "%d", i);

    /* open group /Modules/i */

    groupID2 = H5Gopen(groupID1, gname, H5P_DEFAULT);
    if(groupID2 < 0) {
      fprintf(stderr, "Error opening group /Modules/%s\nExiting...\n", gname);
      return -1;
    }

    exec_info->modules[i].libname = read_str_attr(groupID2, "library name");
    exec_info->modules[i].funcname = read_str_attr(groupID2, "function name");

//      /* open group /Modules/i/input if it exists */
//      if(H5Lexists(groupID2, "in", H5P_DEFAULT) > 0) {
//        groupID3 = H5Dopen(groupID2, "in", H5P_DEFAULT);
//        if(groupID3 >= 0) {
//          j = 0;
//          data[0] = &j;
//          data[1] = in_array;
//
//          /* read input datasets */
//          H5Literate(groupID3, H5_INDEX_NAME, H5_ITER_INC, NULL, read_input, data);
//
//          /* close group /Modules/i/input */
//          status = H5Dclose(groupID3);
//        }
//      }



      /* open group /Modules/i/gets */
      groupID3 = H5Gopen(groupID2, "gets", H5P_DEFAULT);
      if(groupID3 < 0) {
        fprintf(stderr, "Error opening group /Modules/%s/gets\nExiting...\n", gname);
        return -1;
      }

      g_array = (int **)malloc(sizeof(int *) * exec_info->num_shared_bc);
      for(j = 0; j < exec_info->num_shared_bc; j++)
        g_array[j] = NULL;

      data[0] = exec_info->shared_bc_sizes;
      //data[1] = (int *) &g;
      data[1] = (int *) g_array;

      /* read gets */
      H5Literate(groupID3, H5_INDEX_NAME, H5_ITER_INC, NULL, get_control, data);

      /* close group /Modules/i/gets */
      status = H5Gclose(groupID3);
      if(status < 0) {
        fprintf(stderr, "Error closing group /Modules/%s/gets\nExiting...\n", gname);
        return -1;
      }

      for(q = 0; q < exec_info->num_shared_bc; q++) {
        if(g_array[q] == NULL)
          exec_info->modules[i].gets[q] = NULL;
        else if(g_array[q] != NULL) {
          exec_info->modules[i].gets[q] = (int *)malloc(sizeof(int) * exec_info->shared_bc_sizes[q]);
          memcpy(exec_info->modules[i].gets[q], g_array[q], sizeof(int) * exec_info->shared_bc_sizes[q]);
          free(g_array[q]);
        }
      }

      free(g_array);

      /* open group /Modules/i/puts */
      groupID3 = H5Gopen(groupID2, "puts", H5P_DEFAULT);
      if(groupID3 < 0) {
        fprintf(stderr, "Error opening group /Modules/%s/puts\nExiting...\n", gname);
        return -1;
      }

      p_array = (int **)malloc(sizeof(int *) * exec_info->num_shared_bc);
      for(j = 0; j < exec_info->num_shared_bc; j++)
        p_array[j] = NULL;

      data[0] = exec_info->shared_bc_sizes;
      //data[1] = (int *) &p;
      data[1] = (int *) p_array;

      /* read puts */
      H5Literate(groupID3, H5_INDEX_NAME, H5_ITER_INC, NULL, get_control, data);

      /* close group /Modules/i/puts */
      status = H5Gclose(groupID3);
      if(status < 0) {
        fprintf(stderr, "Error closing group /Modules/%s/puts\nExiting...\n", gname);
        return -1;
      }

      for(q = 0; q < exec_info->num_shared_bc; q++) {
        if(p_array[q] == NULL)
          exec_info->modules[i].puts[q] = NULL;
        else if(p_array[q] != NULL) {
          exec_info->modules[i].puts[q] = (int *)malloc(sizeof(int) * exec_info->shared_bc_sizes[q]);
          memcpy(exec_info->modules[i].puts[q], p_array[q], sizeof(int) * exec_info->shared_bc_sizes[q]);
          free(p_array[q]);
        }
      }

      free(p_array);

      status = H5Gclose(groupID2);                                        //close group /Modules/c
      if(status < 0) {
        fprintf(stderr, "Error closing group /Modules\nExiting...\n");
        return -1;
      }

  }

  /* finished reading module gets/puts */

  /* close group /Modules */
  status = H5Gclose(groupID1);
  if(status < 0) {
    fprintf(stderr, "Error closing group /Modules\nExiting...\n");
    cleanup_before_err_ret();
    return -1;
  }

  status = H5Fclose(fileID);
  if(status < 0) {
    fprintf(stderr, "Error closing file %s\nExiting", hdf5_filename);
    cleanup_before_err_ret();
    return -1;
  }

  return 0;
}
