/*
 * Copyright (c) 2015 University of Tennessee
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#include "mpi.h"

#include "IEL_exec_info.h"

int
main(int argc, char *argv[])
{
  int rank, nprocs;
  IEL_exec_info_t exec_info;

  if(argc < 2) {
    fprintf(stderr, "usage: %s config_file\n", argv[0]);
    exit(1);
  }

  MPI_Init(&argc,&argv);
  MPI_Comm_size(MPI_COMM_WORLD,&nprocs);
  MPI_Comm_rank(MPI_COMM_WORLD,&rank);

  if(nprocs < 2) {
    printf("requires at least 2 ranks\n");
    MPI_Finalize();
    exit(1);
  }

  if(rank == 0) {
    IEL_exec_init(&exec_info, argv[1]);

    IEL_exec_dump_info(&exec_info);
    IEL_exec_pack_info(&exec_info, MPI_COMM_WORLD);

    MPI_Bcast(&(exec_info.packsize), 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(exec_info.packed, exec_info.packsize, MPI_PACKED, 0, MPI_COMM_WORLD);
  }
  else {
    MPI_Bcast(&(exec_info.packsize), 1, MPI_INT, 0, MPI_COMM_WORLD);
    exec_info.packed = (char *)malloc(exec_info.packsize);
    MPI_Bcast(exec_info.packed, exec_info.packsize, MPI_PACKED, 0, MPI_COMM_WORLD);
    IEL_exec_unpack_info(&exec_info, MPI_COMM_WORLD);
    sleep(rank);
    IEL_exec_dump_info(&exec_info);
  }

  MPI_Finalize();
  exit(0);
}
