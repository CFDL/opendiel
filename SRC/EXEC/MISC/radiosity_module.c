/*
 * Copyright (c) 2015 University of Tennessee
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include <mpi.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <cuda.h>
#include <unistd.h>
#include <sys/wait.h>
#include <cublas.h>
#include <mkl.h>
#include <mkl_lapack.h>

#include "testings.h"
#include "magmautils.h"
#include "cublas_mv.h"
#include "IEL.h"
#include "IEL_exec_info.h"

/**
 *	This struct represents an element of the mesh.
 *	It contains information about itself to utilize a semi-object oriented style. When elements are passed between processes it is useful to have each element know its global scope.
 */

struct element{
	int id;					/**< the global id of the element */
	int nodeIndices[4];		/**< the indices into the matrix of the local nodes */
	int *processMapping;	/**< mapping of the global id to the local ids */
};


/**
 * This is the core function of the Radiation Module. All of the radiation calculations take place through this function. This is the function that will be called by the IEL_executive.
 *
 * @param exec_info -- The IEL_executive information struct passed to all modules. This struct contains all information necessary for IEL communication. See the IEL reference manual for more details.
 *
 * @return Upon success, Radiosity_Module will return EXIT_SUCCESS. Upon failure, it will return EXIT_FAILURE.
 */

int Module2(IEL_exec_info_t *exec_info)
{
	int NUM_NODES_PER_ELEMENT = 4; /* This will need to be changed for triangle meshes */
	int l,m,n,i,j,k,ii,jj,kk;      /* looping variables */
	int iterations;				   /* The number of times it will communicate with thermal */
	int my_bc;					   /* The bc id# that this process contains */
	int seg_start;				   /* Where my segment begins in the SBC array */
	int seg_len;				   /* How long my segment is */
	int module_size;			   /* This is the number of processes for this module */
	double *h_A;				   /* The local inverse Radiosity matrix */ 
	double *d_A;				   /* The whole inverse matrix which gets split up among processes */
	double consth;				   /* Used in Radiosity calculation */
	double *emis;				   /* Array of emissivities -- allows elements to have varying values */
	double *area;				   /* Array of areas -- allows elements to have varying areas */
	double det;					   /* Area divided by the number of nodes per element */
	int localid;				   /* Used to read in the local element id */
	int globalid;				   /* Used to read in the global element id */
	int rank;					   /* Used to read process rank */
	int radnelem;				   /* The total number of radiating elements */
	int numradnodes;			   /* Used to read the number of radiating nodes */
	int totNumElems;			   /* The total number of elements */
	double B200[NUM_NODES_PER_ELEMENT][NUM_NODES_PER_ELEMENT]; /* FEM B200 matrix */
	double B[NUM_NODES_PER_ELEMENT*NUM_NODES_PER_ELEMENT];	   /* Modified B200 matrix used in Radiosity calculation */
	double Temp[NUM_NODES_PER_ELEMENT];						   /* A temporary array for holding radiosities */
	int TOTAL_T_SIZE;										   /* The number of temperatures (with multiplicities) */
	char *VECTOR_FILE;						/* Optional filename to write out inverse matrix  */
	char *GEOMETRY_FILE;				    /* This is the view3d input geometry file  */
	char *VIEW3D_OUT = strdup("view3dout"); /* This is the hardcoded output file view3d will generate that contains the VF info */
	int d = 1;								/* Make sure an event happens once */
	int *elementMapping;					/* Maps the global element id to the index into the radiating elements array */
	int *nodeMapping;						/* Maps the local node ids to the global node ids */
	int *nodeDistribution;					/* Contains the order of the nodes in order of how they appear in the matrix */
	int *nodeDistribution2;					/* Contains the order of the nodes in order of how they appear in the matrix */
	int *distributedMapping;				/* This is how the Elements are arranged on each process */
	int *localNumElements;					/* This is an array whose indices correspond to process ids and have the # of Elements */
	int *localtoGlobal;						/* This is the local Element id to global element id mapping for each process */
	struct element *Elements;				/* The array of Elements */
    const double sigma = 0.0000000567;		/* Stephen-Boltzmann constant */
	FILE *innodes;							/* Opens innodes.indat */
	FILE *nodes;							/* Opens nodes.dat */
	FILE *inmapping;						/* Opens inmapping.indat */
	char line[256];
	shared_bc_t *my_segment;
	matrix_t mat;
	MPI_Status status;
	MPI_Request request;
	CUcontext context;                                                        
	cublasHandle_t handle;
	cublasStatus_t stat;

	// find out the number of module processes from the executive
	module_size = exec_info->module_num_ranks; 

	fprintf(stderr,"Opening Radiation module %d\n",exec_info->module_rank);

	// loop through the shared bcs
	for(m = 0; m < exec_info->num_shared_bc; m++) {
		// find the start of the shared bc
		if(exec_info->bc_start[m] >= 0) {

			// find out information about my rank's shared_bc
			my_segment = exec_info->shared_bc[m]; 
			seg_start = exec_info->bc_start[m];
			seg_len = exec_info->bc_len[m];
			// determine the number of nodes that my rank has
			TOTAL_T_SIZE = exec_info->shared_bc_sizes[m]*module_size;
			my_bc = m;
		}
		for(l = 0; l < exec_info->shared_bc_sizes[m]; l++)
			exec_info->shared_bc[m][l] = 0.0;
	}
	shared_bc_t totalT[TOTAL_T_SIZE];
	localNumElements = (int *)malloc(sizeof(int) * module_size);

	//Grab the filenames for the files that view3d needs
	GEOMETRY_FILE = strdup(exec_info->modules[exec_info->module_num].mod_argv[0]);
	VECTOR_FILE = strdup(exec_info->modules[exec_info->module_num].mod_argv[1]);

	nodes = fopen("nodes.dat","r");
	if (nodes == NULL){
		perror("nodes.dat");
		return EXIT_FAILURE;
	}
	/* Begin reading nodes.dat to learn how the nodes are delegated
	 * throughout the matrix
	 * */
	nodeDistribution = (int *)malloc(TOTAL_T_SIZE *sizeof(int));
	nodeDistribution2 = (int *)malloc(TOTAL_T_SIZE *sizeof(int));
	for(i = 0; i < TOTAL_T_SIZE; i++){
		fscanf(nodes,"%d",&localid);
		nodeDistribution[i] = localid;
		nodeDistribution2[i] = localid;
	}
	fclose(nodes);
	/***************** end reading nodes.dat ********************/
	fprintf(stderr, "Before inmapping\n");
	//begin to read inmapping.indat
	inmapping = fopen("inmapping.indat","r");
	if(inmapping == NULL){
		perror("inmapping.indat");
		return EXIT_FAILURE;
	}
	fscanf(inmapping,"%d",&numradnodes);
	nodeMapping = (int *)malloc(numradnodes * sizeof(int) + 1);

	//loop through and generate an array that has mapping[localid] = globalid;
	for(i = 0; i < numradnodes; i++){
		fscanf(inmapping,"%d", &localid);
		fscanf(inmapping,"%d", &globalid);
		nodeMapping[localid] = globalid;
	}
	fclose(inmapping);
	//end read inmapping.indat

	/*
	 *	Create an array of global nodal id's corresponding to the rows
	 *  of the radiosity matrix
	 * */
	for(i = 0; i < TOTAL_T_SIZE; i++){
		nodeDistribution[i] = nodeMapping[nodeDistribution[i]];
	}

	fprintf(stderr, "Before innodes\n");
	innodes = fopen("innodes.indat", "r");
	if (innodes == NULL){
		perror("innodes.indat");
		return EXIT_FAILURE;
	}

	//begin reading innodes.indat
	fscanf(innodes,"%s",line);     //read  "TOTAL_#_ELEMENTS"
	fscanf(innodes,"%d", &totNumElems);    //read  TOTAL # ELEMENTS
	elementMapping = (int *)malloc(sizeof(int) * totNumElems);
	localtoGlobal = (int *)malloc(sizeof(int) * totNumElems);
	fscanf(innodes,"%s",line);			//read "Total_Radiating_Elements"
	fscanf(innodes,"%d",&radnelem);		//read radnelem #
	Elements = (struct element *)malloc(sizeof(struct element) * radnelem);
	for(i = 0; i < radnelem; i++){
		fscanf(innodes,"%s",line);       // read Element
		fscanf(innodes,"%d",&globalid);  // read global element id
		fscanf(innodes,"%d",&j);		 // read local node 1
		fscanf(innodes,"%d",&k);		 // read local node 2
		fscanf(innodes,"%d",&l);		 // read local node 3
		fscanf(innodes,"%d",&m);		 // read local node 4

		//determine where this element's nodes lie in the matrix
		for(ii=0; ii < TOTAL_T_SIZE; ii++){
			if(j == nodeDistribution[ii])
				if(k == nodeDistribution[ii+1])
					if(l == nodeDistribution[ii+2])
						if(m == nodeDistribution[ii+3]){
							elementMapping[globalid] = i;
							Elements[i].id = globalid;
							Elements[i].nodeIndices[0] = ii;
							Elements[i].nodeIndices[1] = ii+1;
							Elements[i].nodeIndices[2] = ii+2;
							Elements[i].nodeIndices[3] = ii+3;
							Elements[i].processMapping = (int *) malloc(sizeof(int) * module_size);
							break;
						}
		}
	}

	fscanf(innodes,"%s",line);         //read "process"
	for(i = 0; i < module_size; i++){
		fscanf(innodes,"%d",&rank);    //read  process #
		fscanf(innodes,"%s",line);     //read "Element"
		localNumElements[rank] = 0;
		while(strcmp(line,"process") != 0){
			fscanf(innodes,"%d", &localid);
			fscanf(innodes,"%d", &globalid);
			Elements[elementMapping[globalid]].processMapping[rank] = localid;
			if(rank == exec_info->module_rank){
				localtoGlobal[localid] = globalid;
			}
			localNumElements[rank]++;
			if(fscanf(innodes,"%s", line) == EOF){
				break;
			}
		}
	}
	fclose(innodes);
	//end reading innodes.indat

	distributedMapping = (int *)malloc(sizeof(int) * localNumElements[exec_info->module_rank]);


	h_A = (double *)malloc(sizeof(double) *localNumElements[exec_info->module_rank]*TOTAL_T_SIZE*NUM_NODES_PER_ELEMENT);
	/*go around the loop 5 times*/
	iterations = RAND_MAX;
	fprintf(stderr, "value :: %d\nBefore B200\n",localNumElements[exec_info->module_rank]);

	//initialize B200
	MAKEB200(B200);
	
	fprintf(stderr, "After B200\n");

	for(n = 0; n < iterations; n++) {

		if(n == 0) {

			/*determine the inverse matrix*/
			if(exec_info->module_rank == 0) {
				fprintf(stderr, "BEFORE radinv\n");
				mat = radinv(GEOMETRY_FILE, VECTOR_FILE);
				fprintf(stderr, "AFTER radinv\n");
				d_A = mat->mat;
				emis = mat->emis;
				area = mat->area;
			//	fprintf(stderr,"AAAAAAAAAAAAAAAAAAAAAAAAAAA\n");

				//Send the portions of the matrix to the processes
				k = 0;
				for(j = 0; j < module_size; j++){
					fprintf(stderr, "BEFORE MPI_SENDS\n");
					if(j > 0){
						MPI_Send(emis, radnelem, MPI_DOUBLE_PRECISION,j,1,exec_info->module_comm);
						MPI_Send(area, radnelem, MPI_DOUBLE_PRECISION,j,1,exec_info->module_comm);
					}
					fprintf(stderr, "BEFORE OTHER MPI_SENDS j = %d\n",j);

					for(i = 0; i < radnelem; i++){
						if(Elements[i].processMapping[j] != 0){
							//if it is not process 0
							if(j != 0){
							//send matrix parts to other processes	
								//MPI_Send(&d_A[TOTAL_T_SIZE*Elements[i].nodeIndices[0]],
								//		NUM_NODES_PER_ELEMENT*TOTAL_T_SIZE, MPI_DOUBLE_PRECISION,j,
								//	1,exec_info->module_comm);
								MPI_Send(&d_A[seg_len*Elements[i].nodeIndices[0]],
										NUM_NODES_PER_ELEMENT*seg_len, MPI_DOUBLE_PRECISION,j,
									1,exec_info->module_comm);
							}
							else {
								//fprintf(stderr,"distrib = %x\n",distributedMapping);
								distributedMapping[k] = Elements[i].id;
								int ui = 0;
								//for (ui = 0; ui < localNumElements[exec_info->module_rank]; ui++) 
								//	fprintf(stderr, " %d", distributedMapping[ui]);
								
								//fprintf(stderr,"\n");
								
								//send matrix parts to myself
				/*
								MPI_Isend(&d_A[TOTAL_T_SIZE*Elements[i].nodeIndices[0]],
										NUM_NODES_PER_ELEMENT*TOTAL_T_SIZE, MPI_DOUBLE_PRECISION,j,
										1,exec_info->module_comm, &request);
								MPI_Irecv(&h_A[TOTAL_T_SIZE*k*NUM_NODES_PER_ELEMENT],
										NUM_NODES_PER_ELEMENT*TOTAL_T_SIZE, MPI_DOUBLE_PRECISION, 0,
										1, exec_info->module_comm,&request);
				*/
								MPI_Isend(&d_A[seg_len*Elements[i].nodeIndices[0]],
										NUM_NODES_PER_ELEMENT*seg_len, MPI_DOUBLE_PRECISION,j,
										1,exec_info->module_comm, &request);
								MPI_Irecv(&h_A[seg_len*k*NUM_NODES_PER_ELEMENT],
										NUM_NODES_PER_ELEMENT*seg_len, MPI_DOUBLE_PRECISION, 0,
										1, exec_info->module_comm,&request);
								MPI_Wait(&request,&status);

								k++;
							}
						}
					}
				}
			}  
			else {
				fprintf(stderr, "BEFORE ELSE MPI_RECV\n");
				emis = (double *)malloc(sizeof(double)*radnelem);
				area = (double *)malloc(sizeof(double)*radnelem);
				MPI_Recv(emis, radnelem, MPI_DOUBLE_PRECISION, 0, 1, exec_info->module_comm, &status);
				MPI_Recv(area, radnelem, MPI_DOUBLE_PRECISION, 0, 1, exec_info->module_comm, &status);

				k = 0;

				/*receive part of inverse matrix*/
				for(j = 1; j < module_size; j++){
					for(i = 0; i < radnelem; i++){
						if(Elements[i].processMapping[j] != 0){
							distributedMapping[k] = Elements[i].id;
							//MPI_Recv(&h_A[TOTAL_T_SIZE*k*NUM_NODES_PER_ELEMENT],
							//	NUM_NODES_PER_ELEMENT*TOTAL_T_SIZE, MPI_DOUBLE_PRECISION, 0,
							//	1, exec_info->module_comm, &status);
							MPI_Recv(&h_A[seg_len*k*NUM_NODES_PER_ELEMENT],
								NUM_NODES_PER_ELEMENT*seg_len, MPI_DOUBLE_PRECISION, 0,
								1, exec_info->module_comm, &status);
							k++;
						}
					}
				}
			}
		}
		fprintf(stderr, "Before IEL stuff\n");

		if(d){
			TESTING_CUDA_INIT2();
			d = 0;
			stat = cublasCreate(&handle);
			cudaSetDevice(0);	
			if(stat != CUBLAS_STATUS_SUCCESS) {
				printf("failure to create cublas handle\n");
				return EXIT_FAILURE;
			}
		}



		/*inverse has been computed and each process should have a piece*/
		/*get T from sbc*/
		if(IEL_get_all(exec_info->cinfo, exec_info->num_handles, exec_info->handles) != IEL_SUCCESS) {
			fprintf(stderr, "get failed\n");
		}

		//early exit potentially
		if(exec_info->shared_bc[exec_info->module_rank][0] < 0){
			printf("process %d exiting radiosity module!\n", exec_info->module_rank);
			exit(1);
		}

		//rearrange totalT so that we can avoid swapping columns in the matri
		for(ii=0;ii<seg_len;ii++){
			totalT[ii] = totalT[nodeDistribution2[nodeDistribution2[ii]]];
		}

		/*gather the pieces of totalT from each process*/
		MPI_Allgather(my_segment+seg_start, seg_len, MPI_DOUBLE_PRECISION, totalT, seg_len, MPI_DOUBLE_PRECISION, exec_info->module_comm);

		for(i = 0; i < TOTAL_T_SIZE; i++){
			totalT[i] = totalT[i]*totalT[i]*totalT[i]*totalT[i];
		}
		//loop through the elements and multiply by B200*sigma*emis*det
		for(i = 0; i < radnelem; i++){
			for(ii=0;ii<4;ii++)for(jj=0;jj<4;jj++){B[ii*4+jj]=(1.0/9.0)*B200[ii][jj];Temp[ii] = 0;}

			
			
			kk = elementMapping[localtoGlobal[distributedMapping[i]]];
			det = area[kk]/4.0;
			consth = sigma*emis[kk]*det/(1.0 - emis[kk]);

			//radiosity calculation on a per element basis
			for(j = 0; j < 4; j++)
				for(k = 0; k < 4; k++){
						B[j*4+k] *= consth;
						Temp[j] += B[j*4+k]*totalT[i*4+k];
					}

			totalT[i*4] = Temp[0];
			totalT[i*4+1] = Temp[1];
			totalT[i*4+2] = Temp[2];
			totalT[i*4+3] = Temp[3];


			
		}


		/*do matrix vector multiply with gpu*/
		CublasMV(handle, h_A, totalT, my_segment+seg_start, seg_len, seg_len);

		if(n != iterations-1) {
			/*put R to sbc*/
			if(IEL_put_all(exec_info->cinfo, exec_info->num_handles, exec_info->handles) != IEL_SUCCESS) {
				fprintf(stderr, "put failed\n");
			}
		}
	}

	cublasDestroy(handle);
	TESTING_CUDA_FINALIZE();


	return EXIT_SUCCESS;
}
