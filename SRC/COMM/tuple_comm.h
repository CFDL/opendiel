/*
 * Copyright (c) 2015 University of Tennessee
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#ifndef _TUPLE_COMM_H
#define _TUPLE_COMM_H

#ifdef __cplusplus
extern "C" {
#endif

#define RUN_READY 8
#define GROUP_READY 7
#define GROUP_DONE 6
#define SERVER_SEND 5
#define NOT_FOUND 4
#define PROC_DONE 3
#define TUPLE_READ 2
#define TUPLE_TAKE 1
#define TUPLE_PUT 0

#define MetadataTag 1
#define NumTSTag -1
#define NumModulesTag -2
#define DistServerArray -3

#include "IEL_exec_info.h"
#include <stddef.h>

typedef enum { INT, DOUBLE } TYPE;
// ZT
typedef struct server_info {
  int number_of_servers; /* Total number of tuple servers. The first will be the
                            meta-data server */

  int last_used_server; /* The last server used. Will be used to determine which
                         * server to distribute the data to next. */
} Info;

int IEL_tput(size_t size, int tag, int serverRank, void *data);
int IEL_tget(size_t *size, int tag, int serverRank, unsigned char del,
             void **data);
int IEL_tget_malloced(int *size, int tag, int serverRank, unsigned char del,
                      void *data);
int IEL_tput_simplified(size_t size, char *mod_name, char *tag, void *data);
int IEL_tupleImDone(int size);

int manager_init(int num_servers, IEL_exec_info_t *exec_info);

void user_init(IEL_exec_info_t *exec_info, const char *mod_name);
int *add_metadata_entry(int *metadata, int server_rank);
int *remove_metadata_value(int *metadata);

int IEL_tget_simplified(size_t *size, char *mod_name, char *str_tag,
                        void **data);
int IEL_tremove_simplified(size_t *size, char *mod_name, char *str_tag,
                           void **data);

int IEL_static_tput(size_t size, char *recving_mod, char *str_tag, void *data);
int IEL_static_tget(size_t *size, char *recving_mod, char *str_tag,
                    void **data);
int IEL_static_tremove(size_t *size, char *recving_mod, char *str_tag,
                       void **data);

int IEL_multi_tput(size_t size, char *mod_name, char *tag, void *data);
int IEL_multi_tremove(size_t *size, char *mod_name, char *str_tag, void **data);

/* ZT -- Get the index of the module in the metadata array */
int get_index(int *metadata_array, int mod_name_tag);

/* ZT -- Returns to the user the size that should be passed to any of the
 * IEL put functions for an array with the given number of elements of the
 * specified type. Hopfully this function will reduce ambiguity.
 */
size_t get_size(TYPE T, int numElements);

/* ZT -- tput function when there are multiple tuple servers. The sender sends
 * their ID so that the meta data server can be updated. If the user elects to
 * chop the data, it will be distributed among the tuple servers.
 */
int IEL_dist_tput(size_t size, const char *string_id, void *data);

/* ZT -- tget function for use with distributed tuple servers. The sender
 * simply needs the same tag used when calling tget. This function will
 * reassemble the data that was distributed across the tuple servers by using
 * the location data stored on the meta data server
 */
int IEL_dist_tget(size_t *size, const char *string_id, unsigned char del,
                  void **data);

/* ZT -- This is, in effect, a scheduling function. It will return the rank of
 * the next server to use. Currently, it operates in a round-robin fassion.
 */
int get_server_rank();

/* ZT -- The djb hash function for strings. Chosen because:
 * 1. Need to hash strings
 * 2. Simplicity of function
 * 3. Quickness/computational efficiency of function.
 */
unsigned long get_hash(const char *string_to_hash);

#ifdef __cplusplus
}
#endif

#endif
