/*
 * Copyright (c) 2015 University of Tennessee
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include <stdio.h>
#include <stdlib.h>

#include "IEL.h"

/**
 * Creates a new message queue.
 *
 * @returns pointer to new queue or NULL on error.
 */

IEL_msg_queue_t *
IEL_msg_queue_create()
{
  IEL_msg_queue_t *newq;

  newq = (IEL_msg_queue_t *)malloc(sizeof(IEL_msg_queue_t));

  if(!newq) {
    ERRPRINTF("failed to alloc new queue\n");
    return NULL;
  }

  newq->q = make_dl();

  if(!newq->q) {
    ERRPRINTF("failed to alloc new dlist\n");
    free(newq);
    return NULL;
  }

  return newq;
}

/**
 * Inserts a message entry into the queue.
 *
 * @param queue -- the message queue
 * @param item -- pointer to the item to insert
 *
 * @returns
 *    \li \c IEL_SUCCESS -- no errors encountered
 */
int
IEL_msg_queue_insert(IEL_msg_queue_t *queue, IEL_q_item_t *item)
{
  dl_insert_b(queue->q, item);

  return IEL_SUCCESS;
}

/**
 * Finds (and removes) the first message found with the specified source and
 * tag.
 *
 * @param queue -- the message queue
 * @param src -- the source rank
 * @param tag -- the message tag (currently same as the handle ID)
 *
 * @returns pointer to the entry or NULL if not found
 */
IEL_q_item_t *
IEL_msg_queue_find_remove(IEL_msg_queue_t *queue, int src, int tag)
{
  Dlist tmp;
  IEL_q_item_t *entry;

  dl_traverse(tmp, queue->q) {
    entry = (IEL_q_item_t *)dl_val(tmp);
    if((entry->source == src) && (entry->tag == tag)) {
      dl_delete_node(tmp);
      return entry;
    }
  }

  return IEL_SUCCESS;
}
