/*
 * Copyright (c) 2015 University of Tennessee
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

//Credit to Jim Plank for the basic code which was heavily expanded from
#ifndef _DLIST_H
#define _DLIST_H

#include <stdio.h>    /* Basic includes and definitions */
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "IEL.h"

#ifdef __cplusplus
extern "C" {
#endif

#define IEL_REGISTER          1
#define IEL_QUERY             2
#define IEL_QUERY_RESPONSE    3
#define IEL_DATA_XFER         4
#define IEL_EXIT            666

#ifndef TRUE
#define TRUE 1
#endif
#ifndef FALSE
#define FALSE 0
#endif

#ifdef IEL_DEBUG
#define DBGPRINTF(...) do {fprintf(stderr,"%s:%d [%s] DEBUG: ",__FILE__,__LINE__,__FUNCTION__);fprintf(stderr, __VA_ARGS__); fflush(stderr); } while (0)
#else
#define DBGPRINTF(...)
#endif

#ifdef IEL_QUIET
static void ERRPRINTF(const char *format, ...) {}
#else
#define ERRPRINTF(...) do {fprintf(stderr,"%s:%d [%s] ERROR: ",__FILE__,__LINE__,__FUNCTION__);fprintf(stderr, __VA_ARGS__); fflush(stderr); } while (0)
#endif

#define IEL_CHECK_NULL(p, desc) \
    if((p) == NULL) { \
      ERRPRINTF("Bad args: %s\n", (desc)); \
      return IEL_ERR_ARG; \
    }

typedef struct dlist {
  struct dlist *flink;
  struct dlist *blink;
  void *val;
  int *tag;
  size_t *size;
  bool *lock;
} *Dlist;

/* Nil, first, next, and prev are macro expansions for list traversal 
 * primitives. */

#define dl_nil(l) (l)
#define dl_first(l) (l->flink)
#define dl_last(l) (l->blink)
#define dl_next(n) (n->flink)
#define dl_prev(n) (n->blink)

/* These are the routines for manipulating lists */

extern Dlist make_dl(void); /** Makes an empty dlist, returns pointer to said list */
extern void dl_insert_b(Dlist, void *, int tag, size_t size); /** Inserts \c list_to_insert behind 
                                                               \c node. If \c node is the head of the
                                                               list, the new node is inserted at the 
                                                               end of the list*/
#define dl_insert_a(n, val, tag, size) dl_insert_b(n->flink, val, tag, size)

extern int dl_delete_node(Dlist);    /* Deletes and free's a node */
extern int dl_delete_list(Dlist);  /** Deletes and frees and entire list */
extern void *dl_val(Dlist);   /** Returns node->val (used to shut lint up) */
extern void *dl_tag(Dlist);
extern void *dl_pop(Dlist);  /** returns the first node and removes it from the list */
extern int dl_insert_list_b(Dlist, Dlist);
extern int dl_insert_n(Dlist, int, void *, int, size_t);
extern void * dl_val_n(Dlist, int);
extern int * dl_tag_n(Dlist, int);
extern void * dl_node_n(Dlist, int);
extern int dl_find(Dlist, int);
extern int dl_lock_node(Dlist);
extern int dl_unlock_node(Dlist);
extern int dl_lock_n(Dlist, int);
extern int dl_unlock_n(Dlist, int);
extern int dl_lock_tag(Dlist, int);
extern int dl_unlock_tag(Dlist, int);
extern int sizeof_node(Dlist);

#define dl_traverse(ptr, list) \
  for (ptr = dl_first(list); ptr != dl_nil(list); ptr = dl_next(ptr))
#define dl_traverse_b(ptr, list) \
  for (ptr = dl_last(list); ptr != dl_nil(list); ptr = dl_prev(ptr))
#define dl_empty(list) (list->flink == list)
	  
#ifdef __cplusplus
}
#endif

#endif
