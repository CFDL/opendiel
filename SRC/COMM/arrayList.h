/*
 * Copyright (c) 2015 University of Tennessee
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#ifndef _ARRAYLIST_H
#define _ARRAYLIST_H

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "IEL.h"

#ifdef __cplusplus
extern "C" {
#endif

/* This is a simple dynamic array that stores integers. It is meant to
 * be easy to nest inside a second dimension. It is an amortized cost
 * array, and thus elements can only be added to/removed from
 * the end. No function to remove has been implemented because it has
 * not been needed. Always call freeArrayList when you are done!
 */

typedef struct {
    int* array;
    int used;
    int size;
    int tag;
} arrayList;

//You must call this function on all new arrayLists. If you don't
//need a tag, just set it to anything and forget about it
void initArrayList(arrayList *a, int initialSize, int tag);
//This function exclusively adds the new element to the index after the
//current last element. If the array is currently full, it first doubles
//the size.
void arrayListInsert(arrayList *a, int element);
//Releases memory
void freeArrayList(arrayList *a);
bool alElemExists(arrayList a, int elem);

#ifdef __cplusplus
}
#endif

#endif
