/*
 * Copyright (c) 2015 University of Tennessee
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#ifndef _IEL_MSG_QUEUE_H
#define _IEL_MSG_QUEUE_H

#include "dlist.h"

/**
 * This structure represents an entry in the message queue.
 */
typedef struct {
  int source;   /**< Source rank of message */
  int tag;      /**< Message tag (currently same as handle ID) */
  int len;      /**< Length in bytes of the message */
  void *data;   /**< Pointer to data buffer */
} IEL_q_item_t;

/**
 * This structure represents a message queue for storing incoming
 * messages to be retrieved later.
 */
typedef struct {
} IEL_msg_queue_t;

IEL_msg_queue_t * IEL_msg_queue_create();

int IEL_msg_queue_insert(IEL_msg_queue_t *, IEL_q_item_t *);

IEL_q_item_t * IEL_msg_queue_find_remove(IEL_msg_queue_t *, int, int);

#endif
