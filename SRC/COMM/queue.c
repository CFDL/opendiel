/*
 * Copyright (c) 2015 University of Tennessee
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "queue.h"

static void q_insert_b(Queue, void*, int);
static void q_delete_node(Queue);

Queue new_queue()
{
  Queue q;

  q = malloc (sizeof(struct queue));
  q->flink = q;
  q->blink = q;
  return q;
}
 
static void q_insert_b(Queue node, void* val, int size)       /* Inserts before a given node */
{
  Queue newnode;

  newnode = malloc (sizeof(struct queue));
  newnode->val = malloc((size_t)size);
  memcpy(newnode->val,val,(size_t)size);
  newnode->size = size;

  newnode->flink = node;
  newnode->blink = node->blink;
  newnode->flink->blink = newnode;
  newnode->blink->flink = newnode;
}

void q_push(Queue q, void* val, int size)     /* Inserts at the end of the list */
{
  q_insert_b(q, val, size);
}

void* q_pop(Queue q, int* size)     /* Reads and removes first element */
{
	if(q_isEmpty(q)) return NULL;
	*size = q_first(q)->size;
	void* rv = malloc(*size);
	memcpy(rv,q_first(q)->val,*size);
	q_delete_node(q_first(q));
	return rv;
}

void* q_peek(Queue q, int* size)     /* Reads first element */
{
	if(q_isEmpty(q)) return NULL;
	*size = q_first(q)->size;
	return q_first(q)->val;
}

static void q_delete_node(Queue node)		/* Deletes an arbitrary item */
{
  node->flink->blink = node->blink;
  node->blink->flink = node->flink;
  free(node->val);
  free(node);
}

int q_isEmpty(Queue l)
{
  return (l->flink == l);
}
 
void free_queue(Queue l)
{
  while (!q_isEmpty(l)) {
    q_delete_node(q_first(l));
  }
  free(l);
}
