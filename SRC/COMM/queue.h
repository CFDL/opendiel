/*
 * Copyright (c) 2015 University of Tennessee
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#ifndef _QUEUE_H_
#define _QUEUE_H_

typedef struct queue {
  struct queue *flink;
  struct queue *blink;
  void* val;
  int size;
} *Queue;

Queue new_queue();
void free_queue(Queue);

void q_push(Queue, void*, int);
void* q_pop(Queue, int*);
void* q_peek(Queue, int*);

int q_isEmpty(Queue);

#define q_first(d) ((d)->flink)
#define q_next(d) ((d)->flink)
#define q_last(d) ((d)->blink)
#define q_prev(d) ((d)->blink)
#define q_nil(d) (d)

#endif
