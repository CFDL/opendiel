/*
 * Copyright (c) 2015 University of Tennessee
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/**
 * \file IEL_data_handle.h
 */

#ifndef _IEL_DATA_HANDLE_H
#define _IEL_DATA_HANDLE_H

#include <mpi.h>
#include <stdio.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * IEL_MAX_DEPEND is the maximum number of dependencies allowed.  That is,
 * if IEL_MAX_DEPEND is set to 20, a module cannot have more than 20 incoming
 * or outgoing data handles.
 */
#define IEL_MAX_DEPEND 100

/**
 * This structure describes a data handle, which is a named "connection"
 * between two modules.  Modules communicate via IEL_put() and IEL_get()
 * to and from data handles.  For example, if a module calls IEL_put() to
 * handle 10, then IELComm will initiate data transfer from this module
 * to all modules that have called IEL_require() specifying data handle 10.
 */
typedef struct {
  int id;        /**< The handle's ID */
  size_t size;   /**< The size (in bytes) of the handle's data buffer */
  int num_bufs;  /**< Unused.  This may be used later for multiple buffering */
  void *buf;     /**< Pointer to copy of the module's data */
  void *addr;    /**< Optional pointer to data location */
  int dest[IEL_MAX_DEPEND];       /**< List of destinations for this handle */
  int complete[IEL_MAX_DEPEND];   /**< List of completion status indicators */
  MPI_Request mpi_req[IEL_MAX_DEPEND];    /**< List of MPI requests */
} IEL_data_handle_t;
  
IEL_data_handle_t * IEL_new_data_handle(int, unsigned long, int);
IEL_data_handle_t * IEL_new_data_handle_addr(int, size_t, int, void *);

int IEL_data_handle_destruct(IEL_data_handle_t *);

#ifdef __cplusplus
}
#endif


#endif
