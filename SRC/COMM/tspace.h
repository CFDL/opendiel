/*
 * Copyright (c) 2015 University of Tennessee
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#ifndef	_TSPACE_H_
#define	_TSPACE_H_

#include "queue.h"

typedef struct ts_node {
  unsigned char red;
  unsigned char internal;
  unsigned char left;
  unsigned char roothead;  /* (bit 1 is root, bit 2 is head) */
  struct ts_node *flink;
  struct ts_node *blink;
  struct ts_node *parent;
  int key;
  Queue q;
} *TS;

TS make_ts();
void ts_freeAll(TS ts);

void* ts_read(TS ts, int key, int* size);
void* ts_take(TS ts, int key, int* size);
void  ts_put (TS ts, int key, void* val, int size);

#define ts_first(n) (n->flink)
#define ts_last(n) (n->blink)
#define ts_next(n) (n->flink)
#define ts_prev(n) (n->blink)
#define ts_empty(t) (t->flink == t)
#define ts_nil(t) (t)
 
#define ts_traverse(ptr, lst) \
  for(ptr = ts_first(lst); ptr != ts_nil(lst); ptr = ts_next(ptr))
 
#define ts_rtraverse(ptr, lst) \
  for(ptr = ts_last(lst); ptr != ts_nil(lst); ptr = ts_prev(ptr))
 
#endif
