/*
 * Copyright (c) 2015 University of Tennessee
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/**
 * \file IEL_data_handle.c
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "IEL_comm.h"
#include "IEL_err.h"

/**
 * Creates a new data handle.
 *
 * @param id -- the identifier for this handle (should be non-negative)
 * @param size -- the data size in bytes
 * @param num_bufs -- currently unused.  may be used later to specify
 *    amount of multiple buffering
 *
 * @returns pointer to the newly allocated data handle.  NULL on error.
 */

IEL_data_handle_t *
IEL_new_data_handle(int id, size_t size, int num_bufs)
{
  return IEL_new_data_handle_addr(id, size, num_bufs, NULL);
}

/**
 * Creates a new data handle.
 *
 * @param id -- the identifier for this handle (should be non-negative)
 * @param size -- the data size in bytes
 * @param num_bufs -- currently unused.  may be used later to specify
 *    amount of multiple buffering
 * @param addr -- optional pointer to the handle's local data.  this is
 *    used when IEL_get_impl() and IEL_put_impl() are called.
 *
 * @returns pointer to the newly allocated data handle.  NULL on error.
 */

IEL_data_handle_t *
IEL_new_data_handle_addr(int id, size_t size, int num_bufs, void *addr)
{
  IEL_data_handle_t *newh;
  int i;

  /* sanity checks */

  if(id < 0) {
    ERRPRINTF("handle id must be non-negative\n");
    return NULL;
  }

  if(num_bufs < 0) {
    ERRPRINTF("num_bufs must be non-negative\n");
    return NULL;
  }

  newh = (IEL_data_handle_t *) malloc(sizeof(IEL_data_handle_t));

  if(!newh) {
    ERRPRINTF("malloc failure creating data handle\n");
    return NULL;
  }

  newh->id = id;
  newh->size = size;
  newh->num_bufs = num_bufs;
  newh->buf = NULL;
  newh->addr = addr;

  for(i=0;i<IEL_MAX_DEPEND;i++) {
    newh->dest[i] = -1;
    newh->complete[i] = TRUE;
  }

  return newh;
}

/**
 * Frees resources associated with the specified handle.  Does not
 * free the handle itself.
 *
 * @param handle -- the handle to be destroyed
 *
 * @returns
 *    \li \c IEL_SUCCESS -- no errors encountered
 *    \li \c IEL_ERR_ARG -- Invalid/NULL argument(s)
 */

int
IEL_data_handle_destruct(IEL_data_handle_t *handle)
{
  IEL_CHECK_NULL(handle, "handle");

  if(handle->buf)
    free(handle->buf);

  return IEL_SUCCESS;
}
