/*
 * Copyright (c) 2015 University of Tennessee
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/**
 * \file IEL_err.c
 */

#include <string.h>

#include "IEL_err.h"
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>

char *IEL_errors[] = {
  "No error",
  "Failed to gather requirements",
  "Maximum dependency count reached",
  "Cannot open file",
  "Cannot find handle in dependency graph",
  "Invalid number of ranks specified",
  "Invalid rank number for this node",
  "Invalid rank number for server node",
  "Failed to allocate memory",
  "Failed to receive message",
  "Failed to send message",
  "Could not determine source for this data",
  "Failed to wait for previous outstanding request(s)",
  "Invalid/NULL argument",
  "Could not create new communicator/group",
  "Could not create threads",
  "Last error message (placeholder)",
  "Tag not found in tuple space", 
  "Cannot delete null node/list",
  "Cannot insert null node/list",
  "Assert failed",
  "IEL Sych Error",
  "Problem with DIRCOMM send",
  "Problem with DIRCOMM receive",
  "Unused IEL rank"
};

/**
 * Given the specified error return code, copies a string description of
 * the error into the buffer.
 *
 * @param errorcode -- the error code
 * @param string -- string buffer into which the error string is placed
 * @param resultlen -- the length of the result string
 *
 * @returns
 *   \li \c IEL_SUCCESS -- no error encountered
 *   \li \c IEL_ERR_ARG -- the errorcode is invalid or the buffer is NULL
 */

int
IEL_error_string(int errorcode, char *string, int *resultlen)
{
  if(errorcode > 0 || errorcode < IEL_LAST_ERROR || !string || !resultlen)
    return IEL_ERR_ARG;

  *resultlen = strlen(IEL_errors[-errorcode]);
  strncpy(string, IEL_errors[-errorcode], *resultlen+1);

  return IEL_SUCCESS;
}

void IEL_print_error(int errID) {
  if(errID < IEL_MAX_CODE || errID > 0) {
    printf("Module Exit Status: IEL Error Code %d\n", errID);
  } else {
    printf("Module Exit Status: %s\n", IEL_errors[-errID]);
  }
}

void Assert(int assertion, char* errormsg)
{
	if(!assertion){
		fprintf(stderr,"FATAL ERROR: %s\n",errormsg);
		MPI_Abort(MPI_COMM_WORLD,ASSERT_FAILED);
	}
}

void* SafeMalloc(int size)
//void* SafeMalloc(size_t size)
{
	Assert(size > 0, "SafeMalloc: trying to SafeMalloc size <= 0 is not allowed");
	
	void* rv = malloc(size);
	Assert(rv != NULL, "SafeMalloc: malloc returned NULL");
	
	return rv;
}

