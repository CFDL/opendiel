/*
 * Copyright (c) 2015 University of Tennessee
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/* 
* \file IEL_comm.c
 */

//Enable POSIX definitions so we can use time.h
#if __STDC_VERSION__ >= 199901L
#define _XOPEN_SOURCE 600
#else
#define _XOPEN_SOURCE 500
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <signal.h>
#include <stdarg.h>
#include <pthread.h>

#include "IEL_comm.h"
#include "IEL_err.h"

/**
 * For the given handle, finds the source (sender).

 * @param component_info -- pointer to structure describing this
 *    node (rank number, data dependencies, etc.) 
 * @param handle -- pointer to the handle describing the data
 *    to be transferred
 *
 * @returns
 *    \li non-zero source rank, if found
 *    \li -1 if not found
 */

static int
get_source_idx(IEL_component_info_t *component_info, IEL_data_handle_t *handle)
{
  IEL_data_flow_t *dflow;
  int i, j, src;

  if(!component_info || !handle || !component_info->data_flow 
    || !component_info->data_flow->provides)
    return -1;

  dflow = component_info->data_flow;

  src = -1;

  for(i=0;i<component_info->size;i++) {
    for(j=0;j<IEL_MAX_DEPEND;j++) {
      if(dflow->provides[i*IEL_MAX_DEPEND+j] == handle->id) {
        DBGPRINTF("rank %d get(): need to recv handle %d from %d\n",
             component_info->my_rank, handle->id, i);
        src = i;
        break;
      }
    }
  }

  return src;
}

/**
 * Read data from the specified data handle.  This will initiate
 * communication with the owner of the handle, possibly on a
 * remote machine.  This differs from IEL_get() in that it
 * always writes to the address stored in the data handle.
 *
 * @param component_info -- pointer to structure describing this
 *    node (rank number, data dependencies, etc.)
 * @param handle -- pointer to the handle describing the data
 *    to be transferred
 *
 * @returns
 *    \li \c IEL_SUCCESS -- no errors encountered
 *    \li \c IEL_ERR_ARG -- Invalid/NULL argument(s)
 *    \li \c IEL_UNKNNOWN_SRC -- Could not determine source rank for handle
 *    \li \c IEL_RECV_ERROR -- Error receiving
 */

int
IEL_get_impl(IEL_component_info_t *component_info, IEL_data_handle_t *handle)
{
  if(!handle->addr) {
    ERRPRINTF("NULL handle addr\n");
    return IEL_ERR_ARG;
  }

  return IEL_get(component_info, handle, handle->addr);
}

/**
 * Reads from all the specified handles.  Note that only handles which
 * are registered as 'required' will be read.
 *
 * @param component_info -- pointer to structure describing this
 *    node (rank number, data dependencies, etc.)
 * @param numh -- number of handles to put
 * @param handle -- array of handles describing the data
 *    to be transferred
 *
 * @returns
 *    \li \c IEL_SUCCESS -- no errors encountered
 *    \li \c IEL_ERR_ARG -- Invalid/NULL argument(s)
 *    \li \c IEL_UNKNNOWN_SRC -- Could not determine source rank for handle
 *    \li \c IEL_RECV_ERROR -- Error receiving
 */

int
IEL_get_all(IEL_component_info_t *component_info, int numh, IEL_data_handle_t **handle)
{
  int i, rv;

  for(i=0;i<numh;i++) {
    if(IEL_is_required(component_info, handle[i]->id)) {
      rv = IEL_get(component_info, handle[i], handle[i]->addr);

      if(rv != IEL_SUCCESS)
        return rv;
    }
  }

  return IEL_SUCCESS;
}

/**
 * Read data from the specified data handle.  This will initiate
 * communication with the owner of the handle, possibly on a
 * remote machine.
 *
 * @param component_info -- pointer to structure describing this
 *    node (rank number, data dependencies, etc.) 
 * @param handle -- pointer to the handle describing the data
 *    to be transferred
 * @param data -- buffer into which the data will be placed
 *
 * @returns
 *    \li \c IEL_SUCCESS -- no errors encountered
 *    \li \c IEL_ERR_ARG -- Invalid/NULL argument(s)
 *    \li \c IEL_UNKNNOWN_SRC -- Could not determine source rank for handle
 *    \li \c IEL_RECV_ERROR -- Error receiving
 */

int
IEL_get(IEL_component_info_t *component_info, IEL_data_handle_t *handle, 
  void *data)
{
#ifdef IEL_THREADS
  IEL_q_item_t *item;
#else
  MPI_Status status;
  int rv;
#endif
  int src;

  IEL_CHECK_NULL(component_info, "component info");
  IEL_CHECK_NULL(handle, "handle");
  IEL_CHECK_NULL(data, "data pointer");

  src = get_source_idx(component_info, handle);

  if(src < 0) {
    ERRPRINTF("Could not determine source rank for handle %d\n", handle->id);
    return IEL_UNKNNOWN_SRC;
  }

#ifdef IEL_THREADS
  DBGPRINTF("rank %d checking queue for message\n", component_info->my_rank);
  do {
    pthread_mutex_lock(&(component_info->q_mutex));

    item = IEL_msg_queue_find_remove(component_info->msg_queue, src, handle->id);

    pthread_mutex_unlock(&(component_info->q_mutex));

    if(item == NULL) {
      DBGPRINTF("item not available yet.. sleeping....\n");
      usleep(500000);
    }
  } while(item == NULL);

  DBGPRINTF("rank %d got item from queue.\n", component_info->my_rank);

  memcpy(data, item->data, item->len);
  free(item->data);
  free(item);

#else
  DBGPRINTF("rank %d waiting to recv %lu bytes from %d\n",
     component_info->my_rank, handle->size, src);

  rv = MPI_Recv(data, handle->size, MPI_CHAR, src, handle->id,
     component_info->comm, &status);

  DBGPRINTF("  rank %d recv complete (from %d)\n",
     component_info->my_rank, src);

  if(rv != MPI_SUCCESS) {
    ERRPRINTF("error receiving\n");
    return IEL_RECV_ERROR;
  }
#endif

  return IEL_SUCCESS;
}

/**
 * This is similar to IEL_get(), but reads data from all of the specified data
 * handles.  This can be advantageous when there are several handles to receive
 * from, but you don't know which will be ready first.  Using individual calls
 * to IEL_get() could be less effecient if you have to block for a message
 * while others are ready for reading.
 *
 *
 * @param component_info -- pointer to structure describing this
 *    node (rank number, data dependencies, etc.)
 * @param num_handles -- number of handles in the handle array
 * @param handle -- array of pointers to the handles
 * @param data -- array of buffers into which the data will be placed
 *
 * @returns
 *    \li \c IEL_SUCCESS -- no errors encountered
 *    \li \c IEL_ERR_ARG -- Invalid/NULL argument(s)
 *    \li \c IEL_NOMEM -- failed to allocate memory
 *    \li \c IEL_UNKNNOWN_SRC -- Could not determine source rank for handle
 *    \li \c IEL_RECV_ERROR -- Error receiving
 */

int
IEL_get_many(IEL_component_info_t *component_info, int num_handles,
  IEL_data_handle_t *handle[], void **data)
{
  struct timespec time, time2;
  int i, src, remain, *complete, num_recv;
#ifdef IEL_THREADS
  IEL_q_item_t *item;
#else
  MPI_Status status;
  int flag, rv;
#endif

  IEL_CHECK_NULL(component_info, "component info");
  for(i=0; i<num_handles; i++) {
    IEL_CHECK_NULL(handle[i], "handle");
    IEL_CHECK_NULL(data[i], "data pointer");
  }

  if(num_handles < 0) {
    ERRPRINTF("negative num_handles\n");
    return IEL_ERR_ARG;
  }

  complete = (int *)malloc(num_handles * sizeof(int));

  if(!complete) {
    DBGPRINTF("malloc failed\n");
    return IEL_NOMEM;
  }

  memset(complete, 0, num_handles * sizeof(int));

  remain = num_handles;

  while(remain > 0) {
    num_recv = 0;
    for(i=0; i<num_handles; i++) {
      if(!complete[i]) {
        src = get_source_idx(component_info, handle[i]);

        if(src < 0) {
          ERRPRINTF("Could not determine source rank for handle %d\n",
             handle[i]->id);
          free(complete);
          return IEL_UNKNNOWN_SRC;
        }

#ifdef IEL_THREADS
        DBGPRINTF("check if message from %d is available in queue\n", src);

        pthread_mutex_lock(&(component_info->q_mutex));
        item = IEL_msg_queue_find_remove(component_info->msg_queue, src, handle[i]->id);
        pthread_mutex_unlock(&(component_info->q_mutex));

        if(item) {
          DBGPRINTF("got item from queue\n");
          memcpy(data[i], item->data, item->len);
          free(item->data);
          free(item);

          complete[i] = 1;
          num_recv++;
          remain--;
        }
#else
        DBGPRINTF("probe for message from %d\n", src);

        rv = MPI_Iprobe(src, IEL_DATA_XFER, component_info->comm, &flag,
                 &status);

        if(rv != MPI_SUCCESS) {
          ERRPRINTF("MPI_Iprobe failed\n");
          free(complete);
          return IEL_RECV_ERROR;
        }

        if(flag) {
          DBGPRINTF("message from %d ready, calling MPI_Recv\n", src);

          rv = MPI_Recv(data[i], handle[i]->size, MPI_CHAR, src, handle[i]->id,
            component_info->comm, &status);

          if(rv != MPI_SUCCESS) {
            ERRPRINTF("MPI_Recv failed\n");
            free(complete);
            return IEL_RECV_ERROR;
          }

          complete[i] = 1;
          num_recv++;
          remain--;
        }
#endif
      }
    }

    time.tv_sec = 0;
    time.tv_nsec = 100000000L;

    if(num_recv == 0) {
      DBGPRINTF("nothing ready this iteration... sleeping\n");
      nanosleep(&time , &time2);
    }
  }

  free(complete);

  return IEL_SUCCESS;
}

/**
 * Returns the index of the specified handle.  The index is the location
 * in the array of data handles provided by this node.  This is typically
 * not called by user code.
 *
 * @param component_info -- pointer to structure describing this
 *    node (rank number, data dependencies, etc.)
 * @param handle -- pointer to the handle describing the data
 *    to be transferred
 *
 * @returns the index or -1 if it cannot be found.
 */

static int
get_handle_idx(IEL_component_info_t *component_info,
  IEL_data_handle_t *handle) 
{
  int idx, i;

  IEL_CHECK_NULL(component_info, "component info");
  IEL_CHECK_NULL(handle, "handle");

  idx = -1;
  for(i=0;i<IEL_MAX_DEPEND;i++)
    if(handle->id == component_info->provides[i]) {
      idx = i;
      break;
    }

  return idx;
}

/**
 * Waits for all previous 'put' operations to complete.  This is typically
 * called by IEL_put() to avoid overwriting the send buffer of the
 * previous call.
 *
 * @param component_info -- pointer to structure describing this
 *    node (rank number, data dependencies, etc.)
 * @param handle -- pointer to the handle describing the data
 *    to be transferred
 *
 * @returns
 *    \li \c IEL_SUCCESS -- no errors encountered
 *    \li \c IEL_ERR_ARG -- Invalid/NULL argument(s)
 *    \li \c IEL_HANDLE_NO_MATCH -- couldn't find matching data handle
 *    \li \c IEL_WAIT_ERROR -- failed to wait for previous requests to complete
 */

int
IEL_wait_prev_puts(IEL_component_info_t *component_info,
  IEL_data_handle_t *handle)
{
  int i, idx, rv, err;
  MPI_Status stat;

  IEL_CHECK_NULL(component_info, "component info");
  IEL_CHECK_NULL(handle, "handle");

  err = 0;

  idx = get_handle_idx(component_info, handle);

  if(idx < 0) {
    ERRPRINTF("error: couldn't match handle\n");
    return IEL_HANDLE_NO_MATCH;
  }

  for(i=0;i<component_info->pcount[idx];i++) {
    if(!handle->complete[i]) {
      DBGPRINTF("waiting on previous request..\n");
      rv = MPI_Wait(&(handle->mpi_req[i]), &stat);

      if(rv != MPI_SUCCESS) {
        ERRPRINTF("MPI_Wait failed\n");
        err++;
      }

      handle->complete[i] = TRUE;
    }
  }

  if(err > 0) {
    ERRPRINTF("%d errors waiting for previous requests\n", err);
    return IEL_WAIT_ERROR;
  }

  return IEL_SUCCESS;
}

/**
 * This should be called by all IEL components prior to exit
 * to ensure that outstanding transfers have completed.
 *
 * @param component_info -- pointer to structure describing this
 *    node (rank number, data dependencies, etc.)
 *
 * @returns
 *    \li \c IEL_SUCCESS -- no errors encountered
 *    \li \c IEL_ERR_ARG -- Invalid/NULL argument(s)
 *    \li \c IEL_WAIT_ERROR -- failed to wait for previous requests to complete
 */

int
IEL_finalize(IEL_component_info_t *component_info)
{
  int i, err;

  IEL_CHECK_NULL(component_info, "component info");

  err = 0;

  DBGPRINTF("rank %d finalizing\n", component_info->my_rank);

#ifdef IEL_THREADS
  if(component_info->use_threads) {
    DBGPRINTF("rank %d: sending signal to recv thread\n",
       component_info->my_rank);

    pthread_kill(component_info->recv_thread, SIGHUP);

    DBGPRINTF("rank %d: now waiting for thread to exit\n",
       component_info->my_rank);

    pthread_join(component_info->recv_thread, NULL);

    DBGPRINTF("rank %d: thread exited\n", component_info->my_rank);
  }
#endif

  for(i=0;i<IEL_MAX_DEPEND;i++)
    if(component_info->phandle[i]) {
      
      DBGPRINTF("rank %d finalizing handle %d\n", component_info->my_rank, i);
      if(IEL_wait_prev_puts(component_info, component_info->phandle[i]) != 
           IEL_SUCCESS)
        err++;
    }

  if(err > 0) {
    ERRPRINTF("%d handles could not be finalized\n", err);
    return IEL_WAIT_ERROR;
  }
  /*
  err = IEL_kill_space();
  if(err != IEL_SUCCESS) {
      ERRPRINTF("rank %d: server loop not closed",component_info->my_rank);
      return err;
  }
  DBGPRINTF("rank %d: server loop closed",component_info->my_rank);
  */
      


  return IEL_SUCCESS;
}

/**
 * Write data to the specified data handle.  This will initiate
 * communication with the owner of the handle, possibly on a
 * remote machine.  This differs from IEL_put() in that it
 * always reads from the address stored in the data handle.
 *
 * @param component_info -- pointer to structure describing this
 *    node (rank number, data dependencies, etc.)
 * @param handle -- pointer to the handle describing the data
 *    to be transferred
 *
 * @returns
 *    \li \c IEL_SUCCESS -- no errors encountered
 *    \li \c IEL_ERR_ARG -- Invalid/NULL argument(s)
 *    \li \c IEL_SEND_ERROR -- failed to send message
 *    \li \c IEL_HANDLE_NO_MATCH -- couldn't find matching data handle
 *    \li \c IEL_NOMEM -- failed to allocate memory
 *    \li \c IEL_WAIT_ERROR -- failed to wait for previous requests to complete
 */

int
IEL_put_impl(IEL_component_info_t *component_info, IEL_data_handle_t *handle)
{
  if(!handle->addr) {
    ERRPRINTF("NULL handle addr\n");
    return IEL_ERR_ARG;
  }

  return IEL_put(component_info, handle, handle->addr);
}

/**
 * Checks if the specified handle ID is provided by this component.
 *
 * @param cinfo -- pointer to structure describing this
 *    node (rank number, data dependencies, etc.)
 * @param key -- handle ID to look for
 *
 * @returns TRUE if this component provides this ID, FALSE otherwise.
 */
int
IEL_is_provided(IEL_component_info_t *cinfo, int key)
{
  int i;

  for(i=0;i<IEL_MAX_DEPEND;i++)
    if((cinfo->provides[i] >= 0) && (cinfo->provides[i] == key))
      return TRUE;

  return FALSE;
}

/**
 * Checks if the specified handle ID is required by this component.
 *
 * @param cinfo -- pointer to structure describing this
 *    node (rank number, data dependencies, etc.)
 * @param key -- handle ID to look for
 *
 * @returns TRUE if this component requires this ID, FALSE otherwise.
 */
int
IEL_is_required(IEL_component_info_t *cinfo, int key)
{
  int i;

  for(i=0;i<IEL_MAX_DEPEND;i++)
    if((cinfo->requires[i] >= 0) && (cinfo->requires[i] == key))
      return TRUE;

  return FALSE;
}

/**
 * Writes to all the specified handles.  Note that only handles which
 * are registered as 'provided' will be sent.
 *
 * @param component_info -- pointer to structure describing this
 *    node (rank number, data dependencies, etc.)
 * @param numh -- number of handles to put
 * @param handle -- array of handles describing the data
 *    to be transferred
 *
 * @returns
 *    \li \c IEL_SUCCESS -- no errors encountered
 *    \li \c IEL_ERR_ARG -- Invalid/NULL argument(s)
 *    \li \c IEL_SEND_ERROR -- failed to send message
 *    \li \c IEL_HANDLE_NO_MATCH -- couldn't find matching data handle
 *    \li \c IEL_NOMEM -- failed to allocate memory
 *    \li \c IEL_WAIT_ERROR -- failed to wait for previous requests to complete
 */

int
IEL_put_all(IEL_component_info_t *component_info, int numh, IEL_data_handle_t **handle)
{
  int i, rv;

  for(i=0;i<numh;i++) {
    if(IEL_is_provided(component_info, handle[i]->id)) {
      rv = IEL_put(component_info, handle[i], handle[i]->addr);

      if(rv != IEL_SUCCESS)
        return rv;
    }
  }

  return IEL_SUCCESS;
}

/**
 * Write data to the specified data handle.  This will initiate
 * communication with the owner of the handle, possibly on a
 * remote machine.
 *
 * @param component_info -- pointer to structure describing this
 *    node (rank number, data dependencies, etc.)
 * @param handle -- pointer to the handle describing the data
 *    to be transferred
 * @param data -- buffer from which the data will be sent
 *
 * @returns
 *    \li \c IEL_SUCCESS -- no errors encountered
 *    \li \c IEL_ERR_ARG -- Invalid/NULL argument(s)
 *    \li \c IEL_SEND_ERROR -- failed to send message
 *    \li \c IEL_HANDLE_NO_MATCH -- couldn't find matching data handle
 *    \li \c IEL_NOMEM -- failed to allocate memory
 *    \li \c IEL_WAIT_ERROR -- failed to wait for previous requests to complete
 */

int
IEL_put(IEL_component_info_t *component_info, IEL_data_handle_t *handle, 
  void *data)
{
  int rv, idx, i, j, k;
  IEL_data_flow_t *dflow;

  IEL_CHECK_NULL(component_info, "component info");
  IEL_CHECK_NULL(handle, "handle");
  IEL_CHECK_NULL(data, "data pointer");

  dflow = component_info->data_flow;
  IEL_CHECK_NULL(dflow, "data flow");

  idx = get_handle_idx(component_info, handle);

  if(idx < 0) {
    ERRPRINTF("couldn't match handle\n");
    return IEL_HANDLE_NO_MATCH;
  }

  DBGPRINTF("count for id %d is %d\n", handle->id, component_info->pcount[idx]);

  if(IEL_wait_prev_puts(component_info, handle) != IEL_SUCCESS) {
    ERRPRINTF("before sending data, error waiting for previous requests\n");
    return IEL_WAIT_ERROR;
  }

  if(handle->buf == NULL) {
    handle->buf = (void *)malloc(handle->size * handle->num_bufs);

    if(!handle->buf) {
      ERRPRINTF("malloc failed in put()\n");
      return IEL_NOMEM;
    }

    k = 0;
    for(i=0;i<component_info->size;i++) {
      for(j=0;j<IEL_MAX_DEPEND;j++) {
        if(dflow->requires[i*IEL_MAX_DEPEND+j] == handle->id) {
          DBGPRINTF("rank %d put(): need to send handle %d to %d\n",
             component_info->my_rank, handle->id, i);
          handle->dest[k] = i;
          k++;
        }
      }
    }
  }

  /* assumes only one buf, fix later */
  memcpy(handle->buf, data, handle->size);

  for(i=0;i<component_info->pcount[idx];i++) {
    DBGPRINTF("calling MPI_Isend for handle %d (rank %d -> %d)\n", handle->id,
      component_info->my_rank, handle->dest[i]);

    rv = MPI_Isend(handle->buf, handle->size, MPI_CHAR, 
      handle->dest[i], handle->id, component_info->comm,
      &(handle->mpi_req[i]));

    if(rv != MPI_SUCCESS) {
      ERRPRINTF("error sending\n");
      return IEL_SEND_ERROR;
    }

    handle->complete[i] = FALSE;
  }

  return IEL_SUCCESS;
}

/**
 * Create a new MPI communicator for the specified subset of ranks.
 * This function does not need to be called if you have already created
 * a communicator.  If you do need to call this function, then ALL ranks
 * from the communicator "comm" must call it too.
 *
 * @param comm -- the communicator containing the subset of ranks to be put
 *    in the new communicator
 * @param ranks -- array of ranks to put in the new communicator
 * @param new_size -- number of elements in the ranks array
 * @param new_comm -- pointer to the new communicator
 *
 * @returns
 *    \li \c IEL_SUCCESS -- no errors encountered
 *    \li \c IEL_ERR_NEW_COMM -- Failed to create communicator
 */

int
IEL_comm_create(MPI_Comm comm, int *ranks, int new_size, MPI_Comm *new_comm)
{
  MPI_Group orig_group, new_group;
  int rv;

#ifdef IEL_DEBUG
  printf("IEL_comm_create: creating new communicator ");
  printf("of size %d using ranks: ", new_size);
  for(rv=0;rv<new_size;rv++)
    printf("%d, ", ranks[rv]);
  printf("\n");
#endif

  rv = MPI_Comm_group(comm, &orig_group);
  if(rv != MPI_SUCCESS) {
    ERRPRINTF("MPI_Comm_group failed\n");
    return IEL_ERR_NEW_COMM;
  }

  rv = MPI_Group_incl(orig_group, new_size, ranks, &new_group);
  if(rv != MPI_SUCCESS) {
    ERRPRINTF("MPI_Group_incl failed\n");
    return IEL_ERR_NEW_COMM;
  }

  rv = MPI_Comm_create(comm, new_group, new_comm); 
  if(rv != MPI_SUCCESS) {
    ERRPRINTF("MPI_Comm_create failed\n");
    return IEL_ERR_NEW_COMM;
  }

  return IEL_SUCCESS;
}

/**
 * Initializes the component_info struct with information about
 * this node.
 *
 * @param component_info -- pointer to structure describing this
 *    node (rank number, data dependencies, etc.)
 * @param my_rank -- the MPI rank of this node
 * @param server_rank -- the rank of the root node
 * @param size -- the total number of MPI ranks in this run
 * @param comm -- the MPI communicator
 *
 * @returns
 *    \li \c IEL_SUCCESS -- no errors encountered
 *    \li \c IEL_ERR_ARG -- Invalid/NULL argument(s)
 *    \li \c IEL_INVALID_RANK -- invalid rank specified
 *    \li \c IEL_INVALID_SERVER_RANK -- invalid server rank specified
 *    \li \c IEL_INVALID_NUM_RANKS -- invalid number of ranks specified
 *    \li \c IEL_ERR_THREAD -- threads enabled, but MPI not initialized in
 *                multiple thread mode
 */

int
IEL_init_component(IEL_component_info_t *component_info, int my_rank,
  int server_rank, int size, MPI_Comm comm)
{
  int i;

  IEL_CHECK_NULL(component_info, "component info");

#ifdef IEL_THREADS
/* Might want to add this check later if needed:
 *
 *  MPI_Query_thread(&i);
 *
 *  if(i != MPI_THREAD_MULTIPLE) {
 *    ERRPRINTF("Threads enabled, but MPI not in multiple thread mode\n");
 *    return IEL_ERR_THREAD;
 *  }
 */
#endif

  component_info->my_rank = my_rank;
  component_info->server_rank = server_rank;
  component_info->comm = comm;
  component_info->size = size;
  component_info->nump = component_info->numr = 0;
  component_info->use_threads = 0;

  for(i=0;i<IEL_MAX_DEPEND;i++) {
    component_info->provides[i] = component_info->requires[i] = -1;
    component_info->pcount[i] = 0;
    component_info->phandle[i] = NULL;
  }

  /* sanity checks */

  if(component_info->my_rank < 0) {
    ERRPRINTF("my_rank must be non-negative\n");
    return IEL_INVALID_RANK;
  }

  if(component_info->server_rank < 0) {
    ERRPRINTF("server_rank must be non-negative\n");
    return IEL_INVALID_SERVER_RANK;
  }

  if(component_info->size < 0) {
    ERRPRINTF("size must be non-negative\n");
    return IEL_INVALID_NUM_RANKS;
  }

  return IEL_SUCCESS;
}

/**
 * Declare that this node provides the specified data handle.
 * This information is used for constructing the global data flow.
 *
 * @param component_info -- pointer to structure describing this
 *    node (rank number, data dependencies, etc.)
 * @param handle -- pointer to the handle describing the data
 *    that is provided
 *
 * @returns
 *    \li \c IEL_SUCCESS -- no errors encountered
 *    \li \c IEL_ERR_ARG -- Invalid/NULL argument(s)
 *    \li \c IEL_DEPS_FULL -- maximum dependency limit reached
 */

int
IEL_provide(IEL_component_info_t *component_info, IEL_data_handle_t *handle)
{
  IEL_CHECK_NULL(component_info, "component info");
  IEL_CHECK_NULL(handle, "handle");

  DBGPRINTF("rank %d provides handle %d\n", component_info->my_rank,
    handle->id);

  if(component_info->nump >= IEL_MAX_DEPEND) {
    ERRPRINTF("maximum dependency count reached\n");
    return IEL_DEPS_FULL;
  }

  component_info->provides[component_info->nump] = handle->id;
  component_info->phandle[component_info->nump] = handle;
  component_info->nump++;

  return IEL_SUCCESS;
}

/**
 * Declare that this node requires the specified data handle.
 * This information is used for constructing the global data flow.
 *
 * @param component_info -- pointer to structure describing this
 *    node (rank number, data dependencies, etc.)
 * @param handle -- pointer to the handle describing the data
 *    that is required
 *
 * @returns
 *    \li \c IEL_SUCCESS -- no errors encountered
 *    \li \c IEL_ERR_ARG -- Invalid/NULL argument(s)
 *    \li \c IEL_DEPS_FULL -- maximum dependency limit reached
 */

int
IEL_require(IEL_component_info_t *component_info, IEL_data_handle_t *handle)
{
  IEL_CHECK_NULL(component_info, "component info");
  IEL_CHECK_NULL(handle, "handle");

  DBGPRINTF("rank %d requires handle %d\n", component_info->my_rank,
    handle->id);

  if(component_info->numr >= IEL_MAX_DEPEND) {
    ERRPRINTF("maximum dependency count reached\n");
    return IEL_DEPS_FULL;
  }

  component_info->requires[component_info->numr] = handle->id;
  component_info->numr++;

  return IEL_SUCCESS;
}

/**
 * Declare that this node requires the specified data handle.
 * This information is used for constructing the global data flow.
 * Same as IEL_require(), but accepts a variable number of
 * data handles.  Note that the last argument must be NULL, e.g.:
 *
 *   IEL_require_many(cinfo, h1, h2, h3, NULL);
 *
 * @param component_info -- pointer to structure describing this
 *    node (rank number, data dependencies, etc.)
 * @param ... -- variable argument list of pointers to the
 *    handles describing the data that is required
 *
 * @returns
 *    \li \c IEL_SUCCESS -- no errors encountered
 *    \li \c IEL_ERR_ARG -- Invalid/NULL argument(s)
 *    \li \c IEL_DEPS_FULL -- maximum dependency limit reached
 */

int
IEL_require_many(IEL_component_info_t *component_info, ...)
{
  va_list argp;
  IEL_data_handle_t *ph;
  int err, rv;

  err = 0;

  va_start(argp, component_info);

  for(;;) {
    ph = va_arg(argp, IEL_data_handle_t *);

    if(ph == NULL)
      break;

    rv = IEL_require(component_info, ph);

    if(rv != IEL_SUCCESS) {
      err = 1;
      break;
    }
  }
  
  va_end(argp);

  if(err)
    return rv;

  return IEL_SUCCESS;
}

/**
 * Declare that this node requires the specified data handle.
 * This information is used for constructing the global data flow.
 * Same as IEL_require(), but accepts an array of data handles.
 *
 * @param component_info -- pointer to structure describing this
 *    node (rank number, data dependencies, etc.)
 * @param num_handles -- the number of handles in the array
 * @param handles -- array of pointers to the
 *    handles describing the data that is required
 *
 * @returns
 *    \li \c IEL_SUCCESS -- no errors encountered
 *    \li \c IEL_ERR_ARG -- Invalid/NULL argument(s)
 *    \li \c IEL_DEPS_FULL -- maximum dependency limit reached
 */

int
IEL_require_many_arr(IEL_component_info_t *component_info, int num_handles,
  IEL_data_handle_t **handles)
{
  int i, err, rv;

  if(num_handles < 0) {
    ERRPRINTF("num_handles is negative\n");
    return IEL_ERR_ARG;
  }

  for(i=0;i<num_handles;i++) {
    if(handles[i]) {
      rv = IEL_require(component_info, handles[i]);

      if(rv != IEL_SUCCESS) {
        err = 1;
        break;
      }
    }
  }

  if(err)
    return rv;

  return IEL_SUCCESS;
}

/**
 * Declare that this node provides the specified data handle.
 * This information is used for constructing the global data flow.
 * Same as IEL_provide(), but accepts a variable number of
 * data handles.  Note that the last argument must be NULL, e.g.:
 *
 *   IEL_provide_many(cinfo, h1, h2, h3, NULL);
 *
 * @param component_info -- pointer to structure describing this
 *    node (rank number, data dependencies, etc.)
 * @param ... -- variable argument list of pointers to the
 *    handles describing the data that is provided
 *
 * @returns
 *    \li \c IEL_SUCCESS -- no errors encountered
 *    \li \c IEL_ERR_ARG -- Invalid/NULL argument(s)
 *    \li \c IEL_DEPS_FULL -- maximum dependency limit reached
 */

int
IEL_provide_many(IEL_component_info_t *component_info, ...)
{
  va_list argp;
  IEL_data_handle_t *ph;
  int err, rv;

  err = 0;

  va_start(argp, component_info);

  for(;;) {
    ph = va_arg(argp, IEL_data_handle_t *);

    if(ph == NULL)
      break;

    rv = IEL_provide(component_info, ph);

    if(rv != IEL_SUCCESS) {
      err = 1;
      break;
    }
  }

  va_end(argp);

  if(err)
    return rv;

  return IEL_SUCCESS;
}

/**
 * Declare that this node provides the specified data handle.
 * This information is used for constructing the global data flow.
 * Same as IEL_provide(), but accepts an array of data handles.
 *
 * @param component_info -- pointer to structure describing this
 *    node (rank number, data dependencies, etc.)
 * @param num_handles -- the number of handles in the array
 * @param handles -- array of pointers to the
 *    handles describing the data that is provided
 *
 * @returns
 *    \li \c IEL_SUCCESS -- no errors encountered
 *    \li \c IEL_ERR_ARG -- Invalid/NULL argument(s)
 *    \li \c IEL_DEPS_FULL -- maximum dependency limit reached
 */

int
IEL_provide_many_arr(IEL_component_info_t *component_info, int num_handles,
  IEL_data_handle_t **handles)
{
  int i, err, rv;

  if(num_handles < 0) {
    ERRPRINTF("num_handles is negative\n");
    return IEL_ERR_ARG;
  }

  for(i=0;i<num_handles;i++) {
    if(handles[i]) {
      rv = IEL_provide(component_info, handles[i]);

      if(rv != IEL_SUCCESS) {
        err = 1;
        break;
      }
    }
  }

  if(err)
    return rv;

  return IEL_SUCCESS;
}

/**
 * Writes the global data flow to a 'dot' file.  This is mainly for testing
 * purposes.  See dot(1), dotty(1), circo(1), etc.
 *
 * @param fname -- the file name for the dot file
 * @param component_info -- pointer to structure describing this
 *    node (rank number, data dependencies, etc.)
 *
 * @returns
 *    \li \c IEL_SUCCESS -- no errors encountered
 *    \li \c IEL_ERR_ARG -- Invalid/NULL argument(s)
 *    \li \c IEL_FILE_OPEN -- cannot open the specified file for writing
 */

int
IEL_write_dot_file(char *fname, IEL_component_info_t *component_info)
{
  IEL_data_flow_t *dflow;
  FILE *ofile;
  int i, j, k, l, p_idx;

  IEL_CHECK_NULL(component_info, "component info");
  IEL_CHECK_NULL(fname, "filename");

  dflow = component_info->data_flow;
  IEL_CHECK_NULL(dflow, "data flow");

  ofile = fopen(fname, "w");

  if(!ofile) {
    ERRPRINTF("cant open file '%s'\n", fname);
    return IEL_FILE_OPEN;
  }

  fprintf(ofile, "digraph foo {\n");

  for(i=0;i<component_info->size;i++) {
    for(j=0;j<IEL_MAX_DEPEND;j++) {
      p_idx = i*IEL_MAX_DEPEND+j;
      if(dflow->provides[p_idx] >= 0) {
        for(k=0;k<component_info->size;k++) {
          for(l=0;l<IEL_MAX_DEPEND;l++) {
            if(dflow->requires[k*IEL_MAX_DEPEND+l] == dflow->provides[p_idx]) {
              fprintf(ofile, "  \"rank %d\" -> \"rank %d\" [label = \"%d\"];\n",
                 i, k, dflow->provides[p_idx]);
            }
          }
        }
      }
    }
  }

  fprintf(ofile, "}\n");

  fclose(ofile);

  return IEL_SUCCESS;
}

/**
 * Initializes the global data flow structure.
 *
 * @param data_flow -- pointer to the structure to be initialized
 * @param size -- the total number of MPI ranks in this run
 *
 * @returns
 *    \li \c IEL_SUCCESS -- no errors encountered
 *    \li \c IEL_ERR_ARG -- Invalid/NULL argument(s)
 *    \li \c IEL_NOMEM -- failed to allocate memory
 */

int
IEL_data_flow_init(IEL_data_flow_t *data_flow, size_t size)
{
  int i;

  IEL_CHECK_NULL(data_flow, "data flow");

  data_flow->requires = (int *) malloc(IEL_MAX_DEPEND * sizeof(int) * size);
  data_flow->provides = (int *) malloc(IEL_MAX_DEPEND * sizeof(int) * size);

  if(!data_flow->requires || !data_flow->provides) {
    ERRPRINTF("malloc error creating data flow\n");
    return IEL_NOMEM;
  }

  for(i=0;i<IEL_MAX_DEPEND;i++) 
    data_flow->requires[i] = data_flow->provides[i] = -1;

  return IEL_SUCCESS;
}

#ifdef IEL_THREADS

/**
 * signal handler for recv thread.
 *
 * @param -- signal number received.
 */
void SIGHUP_handler(int signum)
{
  pthread_exit(NULL);
}

/**
 * This is a receiver thread used when threads are enabled.
 * It blocks until a message is available, then receives the
 * message and places it into a queue to be retrieved later
 * by calling IEL_get().
 *
 * @param arg -- pointer to structure describing this
 *    node (rank number, data dependencies, etc.)
 */

void *
IEL_mpi_recv_thread(void *arg)
{
  IEL_component_info_t *component_info = (IEL_component_info_t *)arg;
  MPI_Status status;
  int rv, msg_size;
  IEL_q_item_t *new_msg;

  struct sigaction sa;
  sigemptyset (&sa.sa_mask);
  sa.sa_flags = 0;
  sa.sa_handler = SIGHUP_handler;
  sigaction(SIGHUP, &sa, 0);

  for(;;) {
    DBGPRINTF("rank %d: recv thread waiting for message\n",
      component_info->my_rank);

    rv = MPI_Probe(MPI_ANY_SOURCE, MPI_ANY_TAG, component_info->comm,
              &status);
    if(rv != MPI_SUCCESS) {
      ERRPRINTF("MPI_Probe failed\n");
      continue;
    }

    rv = MPI_Get_count(&status, MPI_BYTE, &msg_size);
    if(rv != MPI_SUCCESS) {
      ERRPRINTF("MPI_Get_count failed\n");
      continue;
    }

    DBGPRINTF("going to recv %d byte message from %d, with tag %d\n", 
        msg_size, status.MPI_SOURCE, status.MPI_TAG);

    new_msg = (IEL_q_item_t *)malloc(sizeof(IEL_q_item_t));
    if(!new_msg) {
      ERRPRINTF("couldn't alloc new queue item\n");
      continue;
    }

    new_msg->source = status.MPI_SOURCE;
    new_msg->tag = status.MPI_TAG;
    new_msg->len = msg_size;
    new_msg->data = (void *)malloc(msg_size);

    if(!new_msg->data) {
      ERRPRINTF("couldn't alloc buffer for recv: %d bytes\n", msg_size);
      free(new_msg);
      continue;
    }

    rv = MPI_Recv(new_msg->data, new_msg->len, MPI_CHAR, new_msg->source,
           new_msg->tag, component_info->comm, &status);

    if(rv != MPI_SUCCESS) {
      ERRPRINTF("rank %d: recv thread, error receiving\n",
        component_info->my_rank);
      free(new_msg->data);
      free(new_msg);
      continue;
    }

    DBGPRINTF("rank %d: recv thread completed MPI_Recv\n",
      component_info->my_rank);

    pthread_mutex_lock(&(component_info->q_mutex));

    IEL_msg_queue_insert(component_info->msg_queue, new_msg);

    pthread_mutex_unlock(&(component_info->q_mutex));
  }
}
#endif

/**
 * This builds the global data flow by gathering the dependency
 * information from all nodes.
 *
 * @param component_info -- pointer to structure describing this
 *    node (rank number, data dependencies, etc.)
 *
 * @returns
 *    \li \c IEL_SUCCESS -- no errors encountered
 *    \li \c IEL_ERR_ARG -- Invalid/NULL argument(s)
 *    \li \c IEL_NOMEM -- failed to allocate memory
 *    \li \c IEL_ALLGATHER_ERROR -- gather of requirements failed
 *    \li \c IEL_ERR_THREAD -- could not create comm threads
 */

int
IEL_gather_requirements(IEL_component_info_t *component_info)
{
  int rv, i, j, k;

  IEL_CHECK_NULL(component_info, "component info");

  component_info->data_flow = (IEL_data_flow_t *)
     malloc(sizeof(IEL_data_flow_t));

  if(!component_info->data_flow) {
    ERRPRINTF("failed to malloc data flow\n");
    return IEL_NOMEM;
  }

  rv = IEL_data_flow_init(component_info->data_flow, component_info->size);

  if(rv != IEL_SUCCESS) {
    ERRPRINTF("failed to init dflow\n");
    return rv;
  }

  rv = MPI_Allgather(component_info->provides, IEL_MAX_DEPEND, MPI_INT, 
          component_info->data_flow->provides, IEL_MAX_DEPEND, MPI_INT,
          component_info->comm);

  if(rv != MPI_SUCCESS) {
    ERRPRINTF("alltoall failed\n");
    return IEL_ALLGATHER_ERROR;
  }

  rv = MPI_Allgather(component_info->requires, IEL_MAX_DEPEND, MPI_INT, 
          component_info->data_flow->requires, IEL_MAX_DEPEND, MPI_INT,
          component_info->comm);

  if(rv != MPI_SUCCESS) {
    ERRPRINTF("alltoall failed\n");
    return IEL_ALLGATHER_ERROR;
  }

  for(i=0;i<IEL_MAX_DEPEND;i++) {
    if(component_info->provides[i] >= 0) {
      for(j=0;j<component_info->size;j++) {
        for(k=0;k<IEL_MAX_DEPEND;k++) {
          if(component_info->data_flow->requires[j*IEL_MAX_DEPEND+k] == 
             component_info->provides[i])
          {
            component_info->pcount[i]++;
            DBGPRINTF("[%d] ----%d---> [%d]\n", component_info->my_rank,
               component_info->provides[i], j);
          }
        }
      }
    }
  }

#ifdef IEL_THREADS
  if((component_info->nump == 0) && (component_info->numr == 0)) {
    DBGPRINTF("this module appears to have no dependencies, so not creating recv thread\n");
    component_info->use_threads = 0;
  }
  else {
    component_info->use_threads = 1;
    pthread_mutex_init(&(component_info->q_mutex), NULL);
    component_info->msg_queue = IEL_msg_queue_create();
    if(!component_info->msg_queue) {
      ERRPRINTF("couldn't create message queue\n");
      return IEL_NOMEM;
    }

    rv = pthread_create(&(component_info->recv_thread), NULL, 
            IEL_mpi_recv_thread, (void*) component_info);

    if(rv != 0) {
      ERRPRINTF("couldn't create recv thread, retval = %d\n", rv);
      return IEL_ERR_THREAD;
    }
  }
#endif

  return IEL_SUCCESS;
}

/**
 * Frees resources associated with the specified component.  Does not
 * free the component itself.
 *
 * @param component_info -- the component to be destroyed
 *
 * @returns
 *    \li \c IEL_SUCCESS -- no errors encountered
 *    \li \c IEL_ERR_ARG -- Invalid/NULL argument(s)
 */

int
IEL_component_destruct(IEL_component_info_t *component_info)
{
  IEL_CHECK_NULL(component_info, "component info");

  if(component_info->data_flow) {
    if(component_info->data_flow->requires)
      free(component_info->data_flow->requires);
    if(component_info->data_flow->provides)
      free(component_info->data_flow->provides);
    free(component_info->data_flow);
  }

  return IEL_SUCCESS;
}

