/*
 * Copyright (c) 2015 University of Tennessee
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/**
 * \file IEL_err.h
 */
#include <stdlib.h>

#ifndef _IEL_ERR_H
#define _IEL_ERR_H

#define IEL_MAX_CODE -24

#define IEL_SUCCESS                    0
#define IEL_ALLGATHER_ERROR           -1
#define IEL_DEPS_FULL                 -2
#define IEL_FILE_OPEN                 -3
#define IEL_HANDLE_NO_MATCH           -4
#define IEL_INVALID_NUM_RANKS         -5
#define IEL_INVALID_RANK              -6
#define IEL_INVALID_SERVER_RANK       -7
#define IEL_NOMEM                     -8
#define IEL_RECV_ERROR                -9
#define IEL_SEND_ERROR                -10
#define IEL_UNKNNOWN_SRC              -11
#define IEL_WAIT_ERROR                -12
#define IEL_ERR_ARG                   -13
#define IEL_ERR_NEW_COMM              -14
#define IEL_ERR_THREAD                -15
#define IEL_LAST_ERROR                -16
#define IEL_TUPLETAG_NOTFOUND         -17
#define DLIST_DELETE_NULL             -18
#define DLIST_INSERT_NULL             -19
#define ASSERT_FAILED                 -20
#define IEL_SYNCH_ERR		      -21
#define IEL_DIR_SEND_ERR              -22
#define IEL_DIR_RECV_ERR              -23
#define IEL_UNUSED_RANK		      -24

#define IEL_MAX_ERROR_STRING  128

#ifdef __cplusplus
extern "C" {
#endif

int IEL_error_string(int errorcode, char *string, int *resultlen);
void IEL_print_error(int);
void* SafeMalloc(int size);
//void* SafeMalloc(size_t size);
void Assert(int assertion, char* errormsg);

#ifdef __cplusplus
}
#endif

#endif
