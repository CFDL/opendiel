/*
 * Copyright (c) 2015 University of Tennessee
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/**
 * \file IEL_comm.h
 */

#ifndef _IEL_COMM_H
#define _IEL_COMM_H

#include "IEL_data_handle.h"

#ifdef __cplusplus
extern "C" {
#endif

#ifdef IEL_THREADS
#include <pthread.h>
#endif

#define IEL_REGISTER          1
#define IEL_QUERY             2
#define IEL_QUERY_RESPONSE    3
#define IEL_DATA_XFER         4
#define IEL_EXIT            666

#ifndef TRUE
#define TRUE 1
#endif
#ifndef FALSE
#define FALSE 0
#endif

#ifdef IEL_DEBUG
#define DBGPRINTF(...) do {fprintf(stderr,"%s:%d [%s] DEBUG: ",__FILE__,__LINE__,__FUNCTION__);fprintf(stderr, __VA_ARGS__); fflush(stderr); } while (0)
#else
#define DBGPRINTF(...)
#endif

#ifdef IEL_QUIET
static void ERRPRINTF(const char *format, ...) {}
#else
#define ERRPRINTF(...) do {fprintf(stderr,"%s:%d [%s] ERROR: ",__FILE__,__LINE__,__FUNCTION__);fprintf(stderr, __VA_ARGS__); fflush(stderr); } while (0)
#endif

#define IEL_CHECK_NULL(p, desc) \
    if((p) == NULL) { \
      ERRPRINTF("Bad args: %s\n", (desc)); \
      return IEL_ERR_ARG; \
    }

/**
 * This structure represents the data flow for the IELComm system.  There are
 * two arrays: one array of the required data handles and another array of the
 * provided data handles.  Each array will be allocated #IEL_MAX_DEPEND entries
 * per rank.  So, assuming #IEL_MAX_DEPEND is 20, entries 0-19 will contain the
 * dependencies for rank 0, entries 20-39 will contain the dependencies for
 * rank 1, and so on for each rank.
 */
typedef struct {
  int *requires;   /**< List of required data handle IDs */
  int *provides;   /**< List of provided data handle IDs */
} IEL_data_flow_t;

/**
 * This structure contains the information pertaining to a IELComm module.
 */
typedef struct {
  MPI_Comm comm;     /**< The MPI communicator containing this rank */
  int server_rank;   /**< The server rank in communicator 'comm' */
  int my_rank;       /**< This component's MPI rank */
  int size;          /**< The number of ranks in the IELComm system */
  int provides[IEL_MAX_DEPEND];  /**< The data handle IDs required by this module */
  int requires[IEL_MAX_DEPEND];  /**< The data handle IDs provided by this module */
  int pcount[IEL_MAX_DEPEND];    /**< The number of receivers for each handle */
  int nump;  /**< The number of entries in the IEL_component_info_t::provides array */
  int numr;  /**< The number of entries in the IEL_component_info_t::requires array */
  IEL_data_handle_t *phandle[IEL_MAX_DEPEND];   /**< Pointers to sent handles */
  IEL_data_flow_t *data_flow;                    /**< Pointer to global data flow */
  int use_threads;
#ifdef IEL_THREADS
  pthread_t recv_thread;
  IEL_msg_queue_t *msg_queue;
  pthread_mutex_t q_mutex;
#endif
} IEL_component_info_t;

int
  IEL_is_provided(IEL_component_info_t *, int),
  IEL_is_required(IEL_component_info_t *, int),
  IEL_put(IEL_component_info_t *, IEL_data_handle_t *, void *),
  IEL_put_impl(IEL_component_info_t *, IEL_data_handle_t *),
  IEL_put_all(IEL_component_info_t *, int, IEL_data_handle_t **),
  IEL_get(IEL_component_info_t *, IEL_data_handle_t *, void *),
  IEL_get_impl(IEL_component_info_t *, IEL_data_handle_t *),
  IEL_get_many(IEL_component_info_t *, int, IEL_data_handle_t **, void **),
  IEL_get_all(IEL_component_info_t *, int, IEL_data_handle_t **),
  IEL_data_flow_init(IEL_data_flow_t *, size_t),
  IEL_write_dot_file(char *, IEL_component_info_t *),
  IEL_provide(IEL_component_info_t *, IEL_data_handle_t *),
  IEL_provide_many(IEL_component_info_t *component_info, ...),
  IEL_provide_many_arr(IEL_component_info_t *component_info, int, IEL_data_handle_t **),
  IEL_require(IEL_component_info_t *, IEL_data_handle_t *),
  IEL_require_many(IEL_component_info_t *component_info, ...),
  IEL_require_many_arr(IEL_component_info_t *component_info, int, IEL_data_handle_t **),
  IEL_comm_create(MPI_Comm, int *, int, MPI_Comm *),
  IEL_init_component(IEL_component_info_t *, int, int, int, MPI_Comm),
  IEL_finalize(IEL_component_info_t *),
  IEL_gather_requirements(IEL_component_info_t *),
  IEL_component_destruct(IEL_component_info_t *);

#ifdef __cplusplus
}
#endif

#endif
