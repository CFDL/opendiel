/*
 * Copyright (c) 2015 University of Tennessee
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include "dlist.h" 

/*
 * Creates an empty dlist
 *
 * @returns
 *  \li \c new_list -- a pointer to the head of the new dlist
 *
 */

Dlist make_dl()
{
  Dlist new_list;

  new_list = (Dlist) malloc (sizeof(struct dlist));
  new_list->flink = new_list;
  new_list->blink = new_list;
  new_list->val = (void *) NULL;
  new_list->tag = (int *) NULL;
  new_list->size = (size_t *) NULL;
  new_list->lock = (bool *) true;
  return new_list;
}

int
sizeof_node(Dlist node)
{
    int size = 0;
    size += sizeof(node->flink);
    size += sizeof(node->blink);
    size += sizeof(node->val);
    size += sizeof(node->tag);
    size += sizeof(node->size);
    size += sizeof(node->lock);
    return size;
}
 
/*
 * Inserts \c list_to_insert behind \c node. If \c node is the head of the list, the new node is
 * inserted at the end of the list
 *
 * @param[in] node -- node to be inserted behind
 * @param[in] list_to_insert -- list of nodes to be inserted
 *
 * @returns
 *    \li \c IEL_SUCCESS -- no errors occurred
 *    \li \c IEL_ERR_ARG -- invalid \c list_to_insert pointer
 */

int
dl_insert_list_b(Dlist node, Dlist list_to_insert)
{
  Dlist last_node, f, l;

  //if list_to_insert is empty, return
  if (dl_empty(list_to_insert)) {
    free(list_to_insert);
    return IEL_ERR_ARG;
  }
  f = list_to_insert->flink;
  l = list_to_insert->blink;
  last_node = node->blink;

  node->blink = l;
  last_node->flink = f;
  f->blink = last_node;
  l->flink = node;
  free(list_to_insert);
  return IEL_SUCCESS;
}

/*
 * Deletes and frees an arbitrary node
 *
 * @param[in] item -- node to be deleted
 *
 * @returns IEL_SUCCESS
 * @returns
 *    \li \c IEL_SUCCESS -- no errors occurred
 */

int
dl_delete_node(Dlist node)
{
  if(node==NULL) {
    ERRPRINTF("DLIST:Delete Error, null pointer");
    return IEL_ERR_ARG;
  }
  //DBGPRINTF("DLIST:Deleting node with tag %d\n", *(node->tag));
  node->flink->blink = node->blink;
  node->blink->flink = node->flink;
  free(node);
  return IEL_SUCCESS;
}

/*
 * Deletes and frees an entire list.
 *
 * @param[in] list -- list of nodes to be deleted
 * @returns
 *    \li \c IEL_SUCCESS -- no errors occurred
 */

int
dl_delete_list(list)
Dlist list;
{
  Dlist d, next_node;
 
  if(list == NULL)
    return DLIST_DELETE_NULL;

  d = list->flink;
  while(d != list) {
    next_node = d->flink;
    free(d);
    d = next_node;
  }
  free(d);
  return IEL_SUCCESS;
}

/*
 * Inserts \c list_to_insert behind \c node
 *
 * @param[in] node -- node to be inserted behind
 * @param[in] list_to_insert -- list of nodes to be inserted
 * @returns
 *    \li \c IEL_SUCCESS -- no errors occurred
 */

void *
dl_val(l)
Dlist l;
{
  return l->val;
}

/*
 * Inserts \c list_to_insert behind \c node
 *
 * @param[in] node -- node to be inserted behind
 * @param[in] list_to_insert -- list of nodes to be inserted
 * @returns
 *    \li \c IEL_SUCCESS -- no errors occurred
 */

void *
dl_tag(l)
Dlist l;
{
    return l->tag;
}

/*
 * Inserts \c list_to_insert behind \c node
 *
 * @param[in] node -- node to be inserted behind
 * @param[in] list_to_insert -- list of nodes to be inserted
 * @returns
 *    \li \c IEL_SUCCESS -- no errors occurred
 */

void*
dl_pop(li)
Dlist li;
{
  Dlist item = dl_last(li);

  if(item == NULL)
    return NULL;

  item->flink->blink = item->blink;
  item->blink->flink = item->flink;

  return item;
}

/*
 * Inserts \c list_to_insert behind \c node
 *
 * @param[in] node -- node to be inserted behind
 * @param[in] list_to_insert -- list of nodes to be inserted
 * @returns
 *    \li \c IEL_SUCCESS -- no errors occurred
 */

//inserts node with value 'val' BEFORE index 'n'
void
dl_insert_b(node, val, tag, size)	/* Inserts to the end of a list */
Dlist node;
void *val;
int tag;
size_t size;
{
  Dlist last_node, new;
  int *ptag = malloc(sizeof(int));
  *ptag = tag;
  void *pval = malloc(size);
  memcpy(pval, val, size);
  size_t *psize = malloc (sizeof(size_t));
  *psize = size;
  bool *plock = malloc (sizeof(bool *));
  *plock = true;
  new = malloc (sizeof(struct dlist));
  new->val = pval;
  new->tag = ptag;
  new->size = psize;
  new->lock = plock;

  last_node = node->blink;

  node->blink = new;
  last_node->flink = new;
  new->blink = last_node;
  new->flink = node;
}
/*
 * Inserts \c list_to_insert behind \c node
 *
 * @param[in] node -- node to be inserted behind
 * @param[in] list_to_insert -- list of nodes to be inserted
 * @returns
 *    \li \c IEL_SUCCESS -- no errors occurred
 */

int
dl_insert_n(Dlist list, int n, void *val, int tag, size_t size) {
    Dlist next_node;
    next_node = dl_node_n(list, n);
    if(next_node == NULL) {
        ERRPRINTF("DL_NODE_N ERR\n");
        return -1;
    }
    else
        dl_insert_b(dl_node_n(list, n), val, tag, size);
    return 0;
}
/*
 * Inserts \c list_to_insert behind \c node
 *
 * @param[in] node -- node to be inserted behind
 * @param[in] list_to_insert -- list of nodes to be inserted
 * @returns
 *    \li \c IEL_SUCCESS -- no errors occurred
 */

void *
dl_node_n(Dlist list, int n)
{
    if(n < 0) return NULL;
	Dlist ptr = dl_first(list);
    //iterate to given index in list
    if(list->flink == list) {
        return NULL;
        //empty list
    }
    for(int i = 0; i < n && ptr != dl_nil(list); i++)
    {
        ptr = dl_next(ptr);
    }
    if(ptr == dl_nil(list)) {
        //printf("ptr == dl_nil(list) \n");
        return NULL;
        //not found
    }
    else{
        return ptr;
        //found
    }
}
/*
 * Inserts \c list_to_insert behind \c node
 *
 * @param[in] node -- node to be inserted behind
 * @param[in] list_to_insert -- list of nodes to be inserted
 * @returns
 *    \li \c IEL_SUCCESS -- no errors occurred
 */

void *
dl_val_n(Dlist list, int n)
{
    Dlist ptr = dl_first(list);
    //iterate to given index in list
    for(int i = 0; i < n && ptr != dl_nil(list); i++)
    {
        ptr = dl_next(ptr);
    }
    if(ptr == dl_nil(list))
        return NULL;
    else
        return ptr->val;
}
/*
 * Inserts \c list_to_insert behind \c node
 *
 * @param[in] node -- node to be inserted behind
 * @param[in] list_to_insert -- list of nodes to be inserted
 * @returns
 *    \li \c IEL_SUCCESS -- no errors occurred
 */

int *
dl_tag_n(Dlist list, int n)
{
    Dlist ptr = dl_first(list);
    //iterate to given index in list
    for(int i = 0; i < n && ptr != dl_nil(list); i++)
    {
        ptr = dl_next(ptr);
    }
    if(ptr == dl_nil(list))
        return NULL;
    else
        return ptr->tag;
}
/*
 * Inserts \c list_to_insert behind \c node
 *
 * @param[in] node -- node to be inserted behind
 * @param[in] list_to_insert -- list of nodes to be inserted
 * @returns
 *    \li \c IEL_SUCCESS -- no errors occurred
 */

int
dl_find(Dlist list, int tag) {
    Dlist ptr = dl_first(list);
    int i;
    for(i = 0; ptr != dl_nil(list); i++)
    {
        //DBGPRINTF("DLIST:Looking for tag %d...\n", tag);
        if(*(ptr->tag) == tag) {
            //printf("DLIST:Found matching node with tag %d index %d\n", *(ptr->tag), i);
            return i;
        }
        ptr = dl_next(ptr);
    }
    //printf("HIT\n");
    return -1;
}
/*
 * Inserts \c list_to_insert behind \c node
 *
 * @param[in] node -- node to be inserted behind
 * @param[in] list_to_insert -- list of nodes to be inserted
 * @returns
 *    \li \c IEL_SUCCESS -- no errors occurred
 */

int
dl_lock_node(Dlist node) {
    if((*(node->lock) = true))
        return true;
    else
        return false;
}
/*
 * Inserts \c list_to_insert behind \c node
 *
 * @param[in] node -- node to be inserted behind
 * @param[in] list_to_insert -- list of nodes to be inserted
 * @returns
 *    \li \c IEL_SUCCESS -- no errors occurred
 */

int
dl_unlock_node(Dlist node) {
    if(!(*(node->lock) = false))
        return true;
    else {
        return false;
    }
}
/*
 * Inserts \c list_to_insert behind \c node
 *
 * @param[in] node -- node to be inserted behind
 * @param[in] list_to_insert -- list of nodes to be inserted
 * @returns
 *    \li \c IEL_SUCCESS -- no errors occurred
 */

int
dl_lock_n(Dlist list, int index) {
    Dlist temp = dl_node_n(list, index);
    return dl_lock_node(temp);
}
/*
 * Inserts \c list_to_insert behind \c node
 *
 * @param[in] node -- node to be inserted behind
 * @param[in] list_to_insert -- list of nodes to be inserted
 * @returns
 *    \li \c IEL_SUCCESS -- no errors occurred
 */

int
dl_unlock_n(Dlist list, int index) {
    Dlist temp = dl_node_n(list, index);
    return dl_unlock_node(temp);
}
/*
 * Inserts \c list_to_insert behind \c node
 *
 * @param[in] node -- node to be inserted behind
 * @param[in] list_to_insert -- list of nodes to be inserted
 * @returns
 *    \li \c IEL_SUCCESS -- no errors occurred
 */

int
dl_lock_tag(Dlist list, int tag) {
    return dl_lock_n(list, dl_find(list, tag));
}
/*
 * Inserts \c list_to_insert behind \c node
 *
 * @param[in] node -- node to be inserted behind
 * @param[in] list_to_insert -- list of nodes to be inserted
 * @returns
 *    \li \c IEL_SUCCESS -- no errors occurred
 */

int
dl_unlock_tag(Dlist list, int tag) {
    return dl_unlock_n(list, dl_find(list, tag));
}
