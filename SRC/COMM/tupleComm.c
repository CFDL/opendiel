/*
 * Copyright (c) 2015 University of Tennessee
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "IEL_comm.h"
#include "IEL_err.h"
#include "tuple_comm.h"

/**
 * Sends a tuple to the server.
 *
 * @param[in] size -- size of the data sent
 * @param[in] tag -- the identification tag to be stored in the tuple space
 * @param[in] serverRank -- the rank of the tuple server to use
 * @param[in] data -- pointer to the data to be stored in the tuple space
 *
 * @returns
 *    \li \c IEL_SUCCESS -- no errors encountered
 */
int IEL_tput(size_t size, int tag, int serverRank, void *data) {
  int rv, pos = 0;

  int packsize = size + (2 * sizeof(int));
  void *sendbuf = SafeMalloc(packsize);

  rv = MPI_Pack(&tag, 1, MPI_INT, sendbuf, packsize, &pos, MPI_COMM_WORLD);
  // ZT -- Shut compiler warnings up
  if (rv != MPI_SUCCESS) {
    fprintf(stderr, "MPI communication failed! Returned error code: %d\n", rv);
  }
  // Assert(rv == MPI_SUCCESS, "tput: error in MPI_Pack 1");

  //  fprintf(stderr, "Packing the following address 0x%x, with the size %d\n",
  //  &size, size);
  rv = MPI_Pack(&size, 1, MPI_INT, sendbuf, packsize, &pos, MPI_COMM_WORLD);
  // ZT -- Shut compiler warnings up
  if (rv != MPI_SUCCESS) {
    fprintf(stderr, "MPI communication failed! Returned error code: %d\n", rv);
  }
  // Assert(rv == MPI_SUCCESS, "tput: error in MPI_Pack 2");

  rv = MPI_Pack(data, size, MPI_BYTE, sendbuf, packsize, &pos, MPI_COMM_WORLD);
  // ZT -- Shut compiler warnings up
  if (rv != MPI_SUCCESS) {
    fprintf(stderr, "MPI communication failed! Returned error code: %d\n", rv);
  }
  // Assert(rv == MPI_SUCCESS, "tput: error in MPI_Pack 3");

  rv =
      MPI_Send(sendbuf, pos, MPI_PACKED, serverRank, TUPLE_PUT, MPI_COMM_WORLD);
  // ZT -- Shut compiler warnings up
  if (rv != MPI_SUCCESS) {
    fprintf(stderr, "MPI communication failed! Returned error code: %d\n", rv);
  }
  // Assert(rv == MPI_SUCCESS, "tput: error in MPI_Send");

  free(sendbuf);

  return IEL_SUCCESS;
}

/**
 * Requests a tuple from the server.
 *
 * @param[out] size -- size of the data received
 * @param[in] tag -- the identification tag requested from the tuple space
 * @param[in] serverRank -- the rank of the tuple server to use
 * @param[in] del -- truth value representing whether to delete the tuple from
 * the space (true) or leave it there for another client to get (false)
 * @param[out] data -- unallocated address where data will be copied6 from tuple
 * space
 *
 * @returns
 *    \li \c IEL_SUCCESS -- no errors encountered
 */
int IEL_tget(size_t *size, int tag, int serverRank, unsigned char del,
             void **data) {
  int rv;
  MPI_Status status;

  if (del)
    rv = MPI_Send(&tag, 1, MPI_INT, serverRank, TUPLE_TAKE, MPI_COMM_WORLD);
  else
    rv = MPI_Send(&tag, 1, MPI_INT, serverRank, TUPLE_READ, MPI_COMM_WORLD);
  // Assert(rv == MPI_SUCCESS, "tget: error in MPI_Send");
  // ZT -- Shut compiler warnings up
  if (rv != MPI_SUCCESS) {
    fprintf(stderr, "MPI communication failed! Returned error code: %d\n", rv);
  }
  // ZT 7/7/2017 When attempting to run multiple tuple servers, calling tget
  // would result in undefined behavior in regards to the size of the data to be
  // received. In my test case, the needed size of the data was 80 (10 doubles),
  // but the second tget call would always return 4. Likely, using the
  // MPI_ANY_TAG was picking up garbage. This change currently appears to work
  // with limited testing. Needs to be further vetted!
  rv = MPI_Probe(serverRank, SERVER_SEND, MPI_COMM_WORLD, &status);
  //    rv = MPI_Probe(serverRank, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
  // Assert(rv == MPI_SUCCESS, "tget: error in MPI_Probe 1");

  // ZT 7/7/2017 NOTE: Using the above tag may make this if statement useless.
  // Need to determine!
  // If the server says tag not found, wait until it does have the data
  if (status.MPI_TAG == NOT_FOUND) {
    int trash;
    rv = MPI_Recv(&trash, 1, MPI_INT, serverRank, NOT_FOUND, MPI_COMM_WORLD,
                  &status);
    // Assert(rv == MPI_SUCCESS, "tget: error in MPI_Recv 1");
    rv = MPI_Probe(serverRank, SERVER_SEND, MPI_COMM_WORLD, &status);
    // Assert(rv == MPI_SUCCESS, "tget: error in MPI_Probe 2");
    // ZT -- Shut compiler warnings up
    if (rv != MPI_SUCCESS) {
      fprintf(stderr, "MPI communication failed! Returned error code: %d\n",
              rv);
    }
  }

  // extract size of incoming message
  rv = MPI_Get_count(&status, MPI_BYTE, (int *)size);
  /* MPI_Get_count returns MPI_UNDEFINED iff the message size is not a multiple
   * of the data type given to the function */
  if (rv == MPI_UNDEFINED) {
    fprintf(stderr, "MPI_Get_count encountered a size that is not a multiple "
                    "of the data type requested!\n");
    return -1;
  }
  // ZT -- Shut compiler warnings up
  if (rv != MPI_SUCCESS) {
    fprintf(stderr, "MPI communication failed! Returned error code: %d\n", rv);
  }
  // Assert(rv == MPI_SUCCESS, "tget: error in MPI_Get_count");

  //      fprintf(stderr, "Trying to malloc with size: %lu, to server: %d\n",
  //      *size, serverRank);
  // allocate buffer to size found in Get_count
  *data = SafeMalloc(*size);
  // receive data from server of size received in Get_count
  rv = MPI_Recv(*data, *size, MPI_BYTE, serverRank, SERVER_SEND, MPI_COMM_WORLD,
                &status);
  // ZT -- Shut compiler warnings up
  if (rv != MPI_SUCCESS) {
    fprintf(stderr, "MPI communication failed! Returned error code: %d\n", rv);
  }
  // Assert(rv == MPI_SUCCESS, "tget: error in MPI_Recv 2");

  return IEL_SUCCESS;
}

/**
 * Requests a tuple from the server.
 *
 * @param[out] size -- size of the data received
 * @param[in] tag -- the identification tag requested from the tuple space
 * @param[in] serverRank -- the rank of the tuple server to use
 * @param[in] del -- truth value representing whether to delete the tuple from
 * the space (true) or leave it there for another client to get (false)
 * @param[out] data -- allocated address where data will be copied from tuple
 * space
 *
 * @returns
 *    \li \c IEL_SUCCESS -- no errors encountered
 */
int IEL_tget_malloced(int *size, int tag, int serverRank, unsigned char del,
                      void *data) {
  int rv;
  MPI_Status status;

  if (del)
    rv = MPI_Send(&tag, 1, MPI_INT, serverRank, TUPLE_TAKE, MPI_COMM_WORLD);
  else
    rv = MPI_Send(&tag, 1, MPI_INT, serverRank, TUPLE_READ, MPI_COMM_WORLD);
  // Assert(rv == MPI_SUCCESS, "tget: error in MPI_Send");
  // ZT -- Shut compiler warnings up
  if (rv != MPI_SUCCESS) {
    fprintf(stderr, "MPI communication failed! Returned error code: %d\n", rv);
  }
  // ZT 7/7/2017 When attempting to run multiple tuple servers, calling tget
  // would result in undefined behavior in regards to the size of the data to be
  // received. In my test case, the needed size of the data was 80 (10 doubles),
  // but the second tget call would always return 4. Likely, using the
  // MPI_ANY_TAG was picking up garbage. This change currently appears to work
  // with limited testing. Needs to be further vetted!
  rv = MPI_Probe(serverRank, SERVER_SEND, MPI_COMM_WORLD, &status);
  //    rv = MPI_Probe(serverRank, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
  // Assert(rv == MPI_SUCCESS, "tget: error in MPI_Probe 1");

  // ZT 7/7/2017 NOTE: Using the above tag may make this if statement useless.
  // Need to determine!
  // If the server says tag not found, wait until it does have the data
  if (status.MPI_TAG == NOT_FOUND) {
    int trash;
    rv = MPI_Recv(&trash, 1, MPI_INT, serverRank, NOT_FOUND, MPI_COMM_WORLD,
                  &status);
    // Assert(rv == MPI_SUCCESS, "tget: error in MPI_Recv 1");
    rv = MPI_Probe(serverRank, SERVER_SEND, MPI_COMM_WORLD, &status);
    // Assert(rv == MPI_SUCCESS, "tget: error in MPI_Probe 2");
    // ZT -- Shut compiler warnings up
    if (rv != MPI_SUCCESS) {
      fprintf(stderr, "MPI communication failed! Returned error code: %d\n",
              rv);
    }
  }

  // extract size of incoming message
  rv = MPI_Get_count(&status, MPI_BYTE, (int *)size);
  /* MPI_Get_count returns MPI_UNDEFINED iff the message size is not a multiple
   * of the data type given to the function */
  if (rv == MPI_UNDEFINED) {
    fprintf(stderr, "MPI_Get_count encountered a size that is not a multiple "
                    "of the data type requested!\n");
    return -1;
  }
  if (rv != MPI_SUCCESS) {
    fprintf(stderr, "MPI communication failed! Returned error code: %d\n", rv);
  }

  // receive data from server of size received in Get_count
  rv = MPI_Recv(data, *size, MPI_BYTE, serverRank, SERVER_SEND, MPI_COMM_WORLD,
                &status);
  // ZT -- Shut compiler warnings up
  if (rv != MPI_SUCCESS) {
    fprintf(stderr, "MPI communication failed! Returned error code: %d\n", rv);
  }
  // Assert(rv == MPI_SUCCESS, "tget: error in MPI_Recv 2");

  return IEL_SUCCESS;
}

/* ZT - Aug 2017 */
int IEL_tput_simplified(size_t size, char *mod_name, char *str_tag,
                        void *data) {
  int mod_tag, tag, srank, rv;
  unsigned long hash;
  char *stored_tag;
  size_t sz;

  srank = 0;
  mod_tag = get_hash(mod_name) & INT_MAX;
  /* Get the tag for the calling module */
  rv = IEL_tget(&sz, mod_tag, srank, 1, (void **)&stored_tag);
  if (rv != IEL_SUCCESS) {
    fprintf(stderr, "Failed to retrieve tag from tuple server\n");
    exit(1);
  }

  if (strcmp(stored_tag, str_tag) != 0) {
    fprintf(stderr, "Inconsistancy between the tags in the config file and "
                    "function calls! Please ensure that the tags in the "
                    "workflow file match the tags and the order of the "
                    "function calls that they're used in\n");
    exit(1);
  }
  // Turn the string tag into an integer tag
  hash = get_hash(str_tag);
  /* Since this hash returns an unsigned long and the tag accepted by the tuple
   * server is an int, put the hash into an int. Lost info doesn't matter as
   * it's merely an identifier. */
  tag = hash & INT_MAX;

  /* Put the data on the server */
  rv = IEL_tput(size, tag, 0, data);
  if (rv != IEL_SUCCESS) {
    fprintf(stderr, "Error in communicating with the tuple server!\n");
    exit(1);
  }
  /* Put the tag back on the server at the end of the list */
  rv = IEL_tput(strlen(str_tag) + 1 * sizeof(char), mod_tag, srank,
                (void *)str_tag);
  if (rv != IEL_SUCCESS) {
    fprintf(stderr, "Error in communicating with the tuple server!\n");
    exit(1);
  }

  return IEL_SUCCESS;
}

/* ZT - Sept 2017 */
/**
 * A simplified version of IEL_tget, the user simply specifies the size of the
 * data, the module that is making the function call, the data tag, and the
 * data itself. The user does not need to determine the server rank or request
 * for the data to be kept on the server.
 *
 * @params[out] size -- the size of the data pulled from the server(s)
 * @params[in] mod_name -- the name of the callling function
 * @params[in] str_tag -- the tag used to retrieve the data
 * @params[out] data -- memory location to store the retrieved data
 *
 * @returns
 *    \li \c IEL_SUCCESS -- no errors encountered
 */

int IEL_tget_simplified(size_t *size, char *mod_name, char *str_tag,
                        void **data) {
  int mod_tag, tag, srank;
  char *stored_tag;
  size_t sz;

  srank = 0;
  mod_tag = get_hash(mod_name) & INT_MAX;
  IEL_tget(&sz, mod_tag, srank, 1, (void **)&stored_tag);

  /* If the next tag does not match the tag the user specified, alert of the
   * inconsistancy and exit */
  // TODO: Should this return an exit code instead of exiting the program?
  if (strcmp(stored_tag, str_tag) != 0) {
    fprintf(stderr, "Comparing %s to %s\n", stored_tag, str_tag);
    fprintf(stderr, "Inconsistancy between the tags in the config file and "
                    "function calls! Please ensure that the tags in the "
                    "workflow file match the tags and the order of the "
                    "function calls that they're used in\n");
    exit(1);
  }

  /* Get the hash of the tag and call IEL_tget */
  tag = get_hash(str_tag) & INT_MAX;
  IEL_tget(size, tag, srank, 0, data);
  IEL_tput(strlen(str_tag) + 1 * sizeof(char), mod_tag, srank, (void *)str_tag);

  return IEL_SUCCESS;
}

/* ZT - Sept 2017 */
/**
 * A simplified version of IEL_tget, the user simply specifies the size of the
 * data, the module that is making the function call, the data tag, and the
 * data itself. The user does not need to determine the server rank or request
 * deletion.
 *
 * @params[out] size -- the size of the data pulled from the server(s)
 * @params[in] mod_name -- the name of the callling function
 * @params[in] str_tag -- the tag used to retrieve the data
 * @params[out] data -- memory location to store the retrieved data
 *
 * @returns
 *    \li \c IEL_SUCCESS -- no errors encountered
 */
int IEL_tremove_simplified(size_t *size, char *mod_name, char *str_tag,
                           void **data) {
  int tag, mod_tag, srank, rv;
  char *stored_tag;
  size_t sz;

  srank = 0;
  mod_tag = get_hash(mod_name) & INT_MAX;

  /* Get the next tag for the calling module */
  // TODO: Should these checks return an exit code instead of exiting the
  // program?
  rv = IEL_tget(&sz, mod_tag, srank, 1, (void *)&stored_tag);
  if (rv != IEL_SUCCESS) {
    fprintf(stderr, "Failed to retrieve tag from tuple server\n");
    exit(1);
  }

  if (strcmp(stored_tag, str_tag) != 0) {
    fprintf(stderr, "Inconsistancy between the tags in the config file and "
                    "function calls! Please ensure that the tags in the "
                    "workflow file match the tags and the order of the "
                    "function calls that they're used in\n");
    exit(1);
  }

  tag = get_hash(str_tag) & INT_MAX;
  /* Get the data at the specified tag from the tuple server */
  rv = IEL_tget(size, tag, srank, 1, data);
  if (rv != IEL_SUCCESS) {
    fprintf(stderr, "Error in communicating with the tuple server!\n");
    exit(1);
  }

  /* Replace the tag on the tuple server. It will now be at the end of the tags
   */
  rv = IEL_tput(strlen(str_tag) + 1 * sizeof(char), mod_tag, srank,
                (void *)str_tag);
  if (rv != IEL_SUCCESS) {
    fprintf(stderr, "Error in communicating with the tuple server!\n");
    exit(1);
  }

  return IEL_SUCCESS;
}

/* ZT - Nov. 2017 */
/**
 * A wrapper for IEL_tput when multiple tuple servers are being used. This
 * function is for use when a static mapping up tuple servers is desired - that
 * is each module receives messages from only a single tuple server.
 * @params[in] size -- the size of the data placed on the server
 * @params[in] recving_mod -- the name of the module the data is being sent to
 * @params[in] str_tag -- the tag used to store the data
 * @params[in] data -- memory location of the data to be stored
 *
 * @returns
 *    \li \c IEL_SUCCESS -- no errors encountered
 *    \li \c -1 -- Could not find the receiving module
 */
int IEL_static_tput(size_t size, char *recving_mod, char *str_tag, void *data) {
  int mod_name_tag, tag, i, srank;
  int *metadata, *numModules;
  size_t sz;

  fprintf(stderr, "START STATIC TPUT\n");
  /* Grab the metadata (mapping of modules to servers) from the metadata server
   * and search for the receiving module */
  IEL_tget(&sz, MetadataTag, 1, 0, (void **)&metadata);
  IEL_tget(&sz, NumModulesTag, 1, 0, (void **)&numModules);
  mod_name_tag = (get_hash(recving_mod) & INT_MAX);

  /* Get the index of the module in the metadata array */
  i = get_index(metadata, mod_name_tag);

  /* If the module was not found, return an error. Otherwise, calculate server
   * rank */
  if (i == *numModules)
    return -1;
  srank = i + 2;
  tag = (get_hash(str_tag) & INT_MAX);

  /* Place data with the given tag on the proper server and manage memory*/
  IEL_tput(size, tag, srank, data);
  free(metadata);
  fprintf(stderr, "END STATIC TPUT\n");

  return IEL_SUCCESS;
}

/* ZT - Nov. 2017 */
/**
 * A wrapper for IEL_tget when multiple tuple servers are being used. This
 * function is for use when a static mapping up tuple servers is desired - that
 * is each module receives messages from only a single tuple server. This
 * function leaves the data on the tuple server (makes a copy).
 * @params[out] size -- the size of the data to retrieve from the server
 * @params[in] requesting_mod -- the name of the module requesting the data
 * @params[in] str_tag -- the tag used to store/retrieve the data
 * @params[out] data -- memory location to store the retrieved data
 *
 * @returns
 *    \li \c IEL_SUCCESS -- no errors encountered
 *    \li \c -1 -- Could not find the requesting module
 */
int IEL_static_tget(size_t *size, char *requesting_mod, char *str_tag,
                    void **data) {
  int mod_name_tag, tag, i, srank;
  int *metadata, *numModules;
  size_t sz;

  fprintf(stderr, "START STATIC TGET\n");
  /* Grab the metadata (mapping of modules to servers) from the metadata server
   * and search for the receiving module */
  IEL_tget(&sz, MetadataTag, 1, 0, (void **)&metadata);
  mod_name_tag = (get_hash(requesting_mod) & INT_MAX);

  /* Get the total number of modules */
  IEL_tget(&sz, NumModulesTag, 1, 0, (void **)&numModules);
  /* Get the index of the module in the metadata array */
  i = get_index(metadata, mod_name_tag);

  /* If the module was not found, return an error, otherwise, calculate server
   * rank*/
  if (i == *numModules)
    return -1;
  srank = i + 2;
  tag = (get_hash(str_tag) & INT_MAX);

  /* Pull data with the given tag from the proper server and manage memory*/
  IEL_tget(size, tag, srank, 0, data);
  free(metadata);

  fprintf(stderr, "END STATIC TGET\n");
  return IEL_SUCCESS;
}

/* ZT - Nov. 2017 */
/**
 * A wrapper for IEL_tget when multiple tuple servers are being used. This
 * function is for use when a static mapping up tuple servers is desired - that
 * is each module receives messages from only a single tuple server. This
 * function deletes the data from the tuple server.
 * @params[out] size -- the size of the data to retrieve from the server
 * @params[in] requesting_mod -- the name of the module requesting the data
 * @params[in] str_tag -- the tag used to store/retrieve the data
 * @params[out] data -- memory location to store the retrieved data
 *
 * @returns
 *    \li \c IEL_SUCCESS -- no errors encountered
 *    \li \c -1 -- Could not find the requesting module
 */
int IEL_static_tremove(size_t *size, char *requesting_mod, char *str_tag,
                       void **data) {
  int mod_name_tag, tag, i, srank;
  int *metadata, *numModules;
  size_t sz;

  fprintf(stderr, "START STATIC TREMOVE\n");
  /* Grab the metadata (mapping of modules to servers) from the metadata server
   * and search for the receiving module */
  IEL_tget(&sz, MetadataTag, 1, 0, (void **)&metadata);
  mod_name_tag = (get_hash(requesting_mod) & INT_MAX);

  /* Get the total number of modules */
  IEL_tget(&sz, NumModulesTag, 1, 0, (void **)&numModules);
  /* Get the index of the module in the metadata array */
  i = get_index(metadata, mod_name_tag);

  /* If the module was not found, return an error. Otherwise, calculate server
   * rank */
  if (i == *numModules)
    return -1;
  srank = i + 2;
  tag = (get_hash(str_tag) & INT_MAX);

  /* Remove data with the given tag from the proper server and manage memory*/
  IEL_tget(size, tag, srank, 1, data);
  free(metadata);
  fprintf(stderr, "END STATIC TREMOVE\n");

  return IEL_SUCCESS;
}

/**
 * Tells the tuple space that this process is done. Is called at the end of a
 * process's runtime in the executive. This is not needed if not using the tuple
 * space, and conditions are applied in the executive to reflect this.
 *
 * @param[in] size -- exec_info->tuple_size
 *
 * @returns
 *    \li \c IEL_SUCCESS -- no errors encountered
 */
int IEL_tupleImDone(int size) {
  int rv;
  int nothing = 0;
  for (int i = 0; i < size; i++) {
    rv = MPI_Send(&nothing, 1, MPI_INT, i, PROC_DONE, MPI_COMM_WORLD);
    Assert(rv == MPI_SUCCESS, "tupleImDone: error in MPI_Send");
  }

  return IEL_SUCCESS;
}

/**
 * Initialize the meta-data server's meta data and for each process, send the
 * meta data server a message with the process rank as the tag and -1 as the
 * message to default all [server used] info to -1. This will keep processes
 * from waiting forever if they querry but have nothing sent.
 *
 * @param[in] num_servers -- The number of tuple servers being used
 *
 * @returns
 *  \li \c IEL_SUCCESS -- no errors encountered
 */
int manager_init(int num_servers, IEL_exec_info_t *exec_info) {
  int i;
  int *sm;
  size_t sz;
  int *numServ, *numModules;

  // TODO: Once other configs are added, all of this (except for the return
  // statement and placing the numModules data) will have to go in an if
  // statement checking if the config is "Static"
  sz = exec_info->num_modules - exec_info->tuple_size;
  sm = (int *)malloc(sizeof(int) * sz);

  /* Set the first indicies of the array to the hash of the module names */
  for (i = exec_info->tuple_size; i < exec_info->num_modules; i++) {
    sm[i - exec_info->tuple_size] =
        (get_hash(exec_info->modules[i].funcname) & INT_MAX);
  }

  /* Set the rest of the indicies to 0 */
  for (; i < exec_info->num_modules; i++) {
    sm[i - exec_info->tuple_size] = 0;
  }

  /* DEBUG */
  int j;
  for (j = 0; j < i; j++)
    fprintf(stderr, "sm[%d] = %d\n", j, sm[j]);
  /* END DEBUG */

  /* Place the array on the metadata server */
  IEL_tput(sizeof(int) * sz, MetadataTag, 1, sm);

  /* Place the number of tuple servers on the metadata server */
  if (num_servers > 1) {
    numServ = (int *)malloc(sizeof(int));
    *numServ = num_servers;

    IEL_tput(sizeof(int), NumTSTag, 1, numServ);
  }

  /* Place the number of modules that are not tuple servers on the metadata
   * server */
  numModules = (int *)malloc(sizeof(int));
  *numModules = exec_info->num_modules - exec_info->tuple_size;

  IEL_tput(sizeof(int), NumModulesTag, 1, numModules);

  return IEL_SUCCESS;
}
/*
int IEL_put(size_t size, char *recving_mod, char *str_tag, void *data)
{

  return IEL_SUCCESS;
}
*/

/* ZT Sept. 2017 */
/**
 * This function is called by each module. It ensures that the module was
 * declared in the workflow file, then it adds all tags for that module on the
 * tuple server in order.
 *
 * @param[in] exec_info -- A copy of the exec_info struct
 * @param[in] mod_name -- The name of the module that called init
 *
 */
void user_init(IEL_exec_info_t *exec_info, const char *mod_name) {
  int i, j, tag, srank;
  //  size_t sz;
  //  Info *I;
  //  const char * end_tag = "End of tags for current module";

  for (i = 0; i < exec_info->num_modules; i++) {
    if (strcmp(mod_name, exec_info->modules[i].funcname) == 0) {
      break;
    }
  }

  // Any other cleanup needed?
  if (i == exec_info->num_modules) {
    fprintf(stderr,
            "Could not find module %s in the module list.\nPlease ensure that \
the config function names and the string used to call user_init match!\n",
            mod_name);
    exit(1);
  }

  tag = get_hash(mod_name) & INT_MAX;
  /* If we have multiple tuple servers, store the tags on the metadata server,
   * otherwise store them on the only existing tuple server: server 0 */
  if (exec_info->tuple_size == 1) {
    srank = 0;
  } else {
    srank = 1;
    //    IEL_tget(&sz, 0, 1, 0, (void **) &I);
  }

  // Is this check necessary?
  for (j = 0; j < exec_info->modules[i].num_tags; j++) {
    if (strcmp(mod_name, exec_info->modules[i].mod_tags[j]) == 0) {
      fprintf(stderr, "Please do not use a tag that exactly matches a module "
                      "name! This will cause conflicts with "
                      "data storage locations on the tuple server!\n");
      exit(1);
    }

    /* Put the tag on the server */
    IEL_tput(strlen(exec_info->modules[i].mod_tags[j]) + 1 * sizeof(char), tag,
             srank, (void *)exec_info->modules[i].mod_tags[j]);
  }
  // put the ending tag on the tuple server
  //  IEL_tput(strlen(end_tag) * sizeof(char), tag, srank, (void **) end_tag);
}

/* ZT Nov 2017 */
/**
 * This function searches the metadata array passed to it for the given tag and
 * returns the index that the tag was found out (number of modules + 1 if the
 * tag is not found).
 * @param[in] metadata -- An array of integers that is the hash of each module
 * name
 * @param[in] mod_name_tag -- The hash of a module name to be searched for
 *
 * @returns
 *  \li \c i -- The index the tag was found at (number of modules + 1 if tag not
 * in array)
 */
int get_index(int *metadata, int mod_name_tag) {
  int i;
  int *numModules;
  size_t sz;

  IEL_tget(&sz, NumModulesTag, 1, 0, (void **)&numModules);

  for (i = 0; i < *numModules; i++) {
    if (metadata[i] == mod_name_tag)
      break;
  }

  free(numModules);

  return i;
}

/* ZT 7/13/2017 */
/**
 * Manager function to do the work of deciding which server to use when
 * distributed (multiple) tuple servers are being used. If the tag given is '0',
 * the manager function will also create a unique hash for the data and return
 * that to the user. Otherwise, the user can tag the data however they like.
 * data_class is a rough identifier of which third of the tuple servers the data
 * belongs to. 1, 2, or 3 for now.
 *
 * @param[in] size -- The size of the data being stored on the tuple servers
 * @param[in] str_tag -- User-defined string that is the unique tag for the
 * data
 * @param[in] data -- The array of data to be stored
 *
 * @returns
 *  \li \c IEL_SUCCESS -- no errors encountered
 */
int IEL_dist_tput(size_t size, const char *str_tag, void *data) {
  int rv, pos, packsize, i, j;
  int serverRank, tag;
  int *metaData, *metaData2, *numServ;
  size_t sz;
  void *sendbuf;
  double *chopped_data, *last;
  unsigned long hash;

  if (DEBUG)
    fprintf(stderr, "HELLO FROM DIST_TPUT !\n\n\n"); // DEBUG

  // Turn the string tag into an integer tag
  hash = get_hash(str_tag);
  // Since this hash returns an unsigned long and the tag accepted by the tuple
  // server is an int, put the hash into an int. Lost info doesn't matter as
  // it's merely an identifier.
  tag = hash & INT_MAX;

  // TODO: put a check here or elsewhere for the number of tuple servers and
  // config type
  IEL_tget(&sz, NumTSTag, 1, 0, (void **)&numServ);
  metaData = (int *)malloc(sizeof(int) * (*numServ) - 2);
  metaData2 = (int *)malloc(sizeof(int) * (*numServ) - 2);

  for (i = 0; i < (*numServ) - 2; i++) {
    metaData[i] = -1;
    metaData2[i] = -1;
  }

  pos = 0;
  sendbuf = NULL;
  // For now we will distribute evenly among tuple servers.
  if (size / sizeof(double) <=
      (*numServ) - 2) { // If there's more servers than data points
    chopped_data = (double *)malloc(sz * sizeof(double));
    sz = sizeof(double);
    packsize = sz + 2 * sizeof(int);
    sendbuf = SafeMalloc(packsize);

    for (i = 0; i < size / sizeof(double); i++) {
      pos = 0;
      //      *chopped_data = ((double *) data)[i];
      //      chopped_data = ((double *) data) + i;
      serverRank = get_server_rank();

      // Send the data to the correct server
      rv = MPI_Pack(&tag, 1, MPI_INT, sendbuf, packsize, &pos, MPI_COMM_WORLD);
      rv = MPI_Pack(&sz, 1, MPI_INT, sendbuf, packsize, &pos, MPI_COMM_WORLD);
      rv = MPI_Pack((void *)chopped_data, sz, MPI_BYTE, sendbuf, packsize, &pos,
                    MPI_COMM_WORLD);
      rv = MPI_Send(sendbuf, pos, MPI_PACKED, serverRank, TUPLE_PUT,
                    MPI_COMM_WORLD);
      // ZT -- Shut compiler warnings up
      if (rv != MPI_SUCCESS) {
        fprintf(stderr, "MPI communication failed! Returned error code: %d\n",
                rv);
      }

      metaData[i] = serverRank;
      metaData2[i] = sz;
    }
    free(sendbuf);
  } else { // There's more data points than servers (the typical scenario)
    // Take the total data size (number of doubles) and divide it among the
    // tuple servers. Tuple servers are 0-indexed, we do not have access to
    // tuple server 0, and tuple server 1 is the management server. This
    // necessitates subtracting 3 from the number of tuple servers for the
    // divisor.
    sz = (size / sizeof(double)) / ((*numServ) - 3);
    chopped_data = (double *)malloc(sz * sizeof(double));
    packsize = sz * sizeof(double) + 2 * sizeof(int);
    sendbuf = SafeMalloc(packsize);
    // This is the number of tuple servers we have access to,
    // it's 0-indexed to make the logic for storing the meta-data easier.
    for (i = 0; i < (*numServ) - 3; i++) {
      // Iterate over the number of tuple servers we have access to..
      pos = 0;
      // Pull each double from the data. Since we're breaking it up into parts,
      // the index 'sz*i + j" will keep track of which data point we need to
      // pull
      for (j = 0; j < sz; j++)
        chopped_data[j] = ((double *)data)[sz * i + j];
      serverRank =
          get_server_rank(); // Get the rank of the next available server

      // Send our data to the next available server
      rv = MPI_Pack(&tag, 1, MPI_INT, sendbuf, packsize, &pos, MPI_COMM_WORLD);
      sz *= sizeof(double); // Ensure we have the correct size of the data!
      rv = MPI_Pack(&sz, 1, MPI_INT, sendbuf, packsize, &pos, MPI_COMM_WORLD);
      sz /= sizeof(double);
      rv = MPI_Pack((void *)chopped_data, sz * sizeof(double), MPI_BYTE,
                    sendbuf, packsize, &pos, MPI_COMM_WORLD);
      rv = MPI_Send(sendbuf, pos, MPI_PACKED, serverRank, TUPLE_PUT,
                    MPI_COMM_WORLD);
      // ZT -- Shut compiler warnings up
      if (rv != MPI_SUCCESS) {
        fprintf(stderr, "MPI communication failed! Returned error code: %d\n",
                rv);
      }
      // Update the meta data
      metaData[i] = serverRank;
      metaData2[i] = sz;
    }
    // Free our memory
    free(sendbuf);
    free(chopped_data);
    // Logic to get the last partial piece! The size is the remainder of the
    // data
    sz = (size / sizeof(double)) - (sz * (i));
    last = (double *)malloc(sz * sizeof(double));
    // Iterate over the last of the data points and collect all of the doubles
    for (j = 0; j < sz; j++)
      last[j] = ((double *)data)[size / sizeof(double) - sz + j];
    pos = 0;
    packsize = sz * sizeof(double) + 2 * sizeof(int);
    sendbuf = SafeMalloc(packsize);

    serverRank = get_server_rank(); // Get the next available server
    // Send the data to the server
    rv = MPI_Pack(&tag, 1, MPI_INT, sendbuf, packsize, &pos, MPI_COMM_WORLD);
    sz *= sizeof(double);
    rv = MPI_Pack(&sz, 1, MPI_INT, sendbuf, packsize, &pos, MPI_COMM_WORLD);
    sz /= sizeof(double);
    rv = MPI_Pack((void *)last, sz * sizeof(double), MPI_BYTE, sendbuf,
                  packsize, &pos, MPI_COMM_WORLD);
    rv = MPI_Send(sendbuf, pos, MPI_PACKED, serverRank, TUPLE_PUT,
                  MPI_COMM_WORLD);

    // ZT -- Shut compiler warnings up
    if (rv != MPI_SUCCESS) {
      fprintf(stderr, "MPI communication failed! Returned error code: %d\n",
              rv);
    }

    // Update the meta data and manage memory
    metaData[i] = serverRank;
    metaData2[i] = sz;
    free(sendbuf);
    free(last);
  }

  // Update the meta-data server with the latest changes
  sz = sizeof(int) * (*numServ) - 2;
  IEL_tput(sz, tag, 1, (void **)metaData);
  IEL_tput(sz, -tag, 1, (void **)metaData2);

  if (DEBUG)
    fprintf(stderr, "GOODBYE FROM DIST_TPUT !\n");
  return IEL_SUCCESS;
}

// ZT --
/* Wrapper for the tget function for use when multiple tuple servers are
 * present. This function querries the meta-data server (server rank 0) to
 * determine if any data has been deposited in any of the tuple servers that is
 * meant for the caller of this function. This function is written with the
 * assumption that the caller knows the tag of the sender.
 */
int IEL_dist_tget(size_t *size, const char *str_id, unsigned char del,
                  void **data) {
  int rv, i, serverRank, total_size, tag;
  int *metaData, *metaData2, *numServ;
  size_t sz, sz1, total;
  MPI_Status status;
  unsigned long hash;

  if (DEBUG)
    fprintf(stderr, "HELLO FROM DIST_TGET !\n");

  // Turn the string tag into an integer tag
  hash = get_hash(str_id);
  // Since this hash returns an unsigned long and the tag accepted by the tuple
  // server is an int, put the hash into an int. Lost info doesn't matter as
  // it's merely an identifier.
  tag = hash & INT_MAX;

  IEL_tget(&sz, NumTSTag, 1, 0, (void **)&numServ);
  // Grab the meta data from the meta data server
  IEL_tget(&sz, tag, 1, 1, (void **)&metaData);
  IEL_tget(&sz1, -tag, 1, 1, (void **)&metaData2);

  sz = (*numServ) - 2;
  sz1 = (*numServ) - 2;
  if (metaData[0] == -1)
    return -1;
  serverRank = metaData[0];

  if (DEBUG)
    for (i = 0; i < 10; i++)
      fprintf(stderr, "meta_data[%d] = %d\n", i, metaData[i]);
  if (DEBUG)
    fprintf(stderr, "sz: %lu, sz1:  %lu\n", sz, sz1);

  total_size = 0;
  total = 0;
  for (i = 0; i < sz1; i++) {
    if (metaData2[i] == -1)
      break;
    total_size += metaData2[i];
    total += metaData2[i];
  }
  // Allocate the total size of the data to receive.
  *data = SafeMalloc(total_size * sizeof(double));

  i = 0;
  total_size = 0;
  while (serverRank != -1) {
    if (DEBUG)
      fprintf(stderr, "Requesting to pull data from server number %d\n",
              serverRank);
    if (del) {
      rv = MPI_Send(&tag, 1, MPI_INT, serverRank, TUPLE_TAKE, MPI_COMM_WORLD);
    } else {
      rv = MPI_Send(&tag, 1, MPI_INT, serverRank, TUPLE_READ, MPI_COMM_WORLD);
    }

    // ZT -- Shut compiler warnings up
    if (rv != MPI_SUCCESS) {
      fprintf(stderr, "MPI communication failed! Returned error code: %d\n",
              rv);
    }

    rv = MPI_Probe(serverRank, SERVER_SEND, MPI_COMM_WORLD, &status);

    // ZT -- Shut compiler warnings up
    if (rv != MPI_SUCCESS) {
      fprintf(stderr, "MPI communication failed! Returned error code: %d\n",
              rv);
    }
    // extract size of incoming message
    rv = MPI_Get_count(&status, MPI_BYTE, (int *)size);
    // ZT -- Shut compiler warnings up
    if (rv != MPI_SUCCESS) {
      fprintf(stderr, "MPI communication failed! Returned error code: %d\n",
              rv);
    }
    if (i == 0) { // We don't have to index into the buffer if this is the first
                  // piece
      // receive data from server of size received in Get_count
      rv = MPI_Recv(*data, *size, MPI_BYTE, serverRank, SERVER_SEND,
                    MPI_COMM_WORLD, &status);
    } else { // After receiving the first chunk of data, we must index into the
             // buffer so that we have a contiguous array of doubles.
      // receive data from server of size received in Get_count and index into
      // the buffer by the total size of all previous data
      rv = MPI_Recv(((double *)*data) + total_size, *size, MPI_BYTE, serverRank,
                    SERVER_SEND, MPI_COMM_WORLD, &status);
    }
    // ZT -- Shut compiler warnings up
    if (rv != MPI_SUCCESS) {
      fprintf(stderr, "MPI communication failed! Returned error code: %d\n",
              rv);
    }
    // Update total_size so we can continue to properly index into the array
    total_size += metaData2[i];
    // Keep track of the number of chunks we've received. End once all of them
    // have been received.
    i++;
    if (i >= sz)
      break;
    // If the meta_data array is not full (all of the servers were not used)
    // exit after receiving from the last used server
    serverRank = metaData[i];
  }

  // If we're deleting this data from the server, reset the meta data
  if (del) {
    for (i = 0; i < sz; i++)
      metaData[i] = -1;
    for (i = 0; i < sz; i++)
      metaData2[i] = -1;
  }

  // Put the meta data back on the manager server
  IEL_tput(sz, tag, 1, (void **)metaData);
  IEL_tput(sz, -tag, 1, (void **)metaData2);

  if (DEBUG)
    fprintf(stderr, "GOODBYE FROM DIST_TGET !\n");
  return IEL_SUCCESS;
}

/* ZT --
 * A simple round-robin scheduling algorithm for server selection */
int get_server_rank() {
  Info *I;
  size_t size;

  // Delete the data from the meta-data server because it will have to be
  // updated
  IEL_tget(&size, 0, 1, 1, (void **)&I);

  if (DEBUG)
    fprintf(stderr, "last_used server: %d\n", I->last_used_server);
  if (I->last_used_server < I->number_of_servers - 1) {
    I->last_used_server++;
  } else {
    I->last_used_server = 2;
  }
  // Update the meta-data server and return the server to use.
  IEL_tput(sizeof(Info), 0, 1, (void *)I);
  return I->last_used_server;
}

/* ZT July 2017 */
/**
 * Simple djb hash function for strings. Takes the user-given tag and hashes it.
 * NOTE: This function assumes that the user-given tag is NULL terminated! If
 * the user elects to pass the tag in the function call using a char * variable,
 * the user must NULL terminate the tag (i.e IEL_dist_tget(..., "MY_TAG\0",
 * ...). It is suggested that the user pass the string to the function using  ""
 * to avoid errors.
 *
 * @param[in] const char *str -- The string to be hashed
 *
 * @returns
 *  \li \c hash -- Unsigned long that is the result of the hash function
 */
unsigned long get_hash(const char *str) {
  unsigned long hash;
  int i, c;

  if (DEBUG)
    fprintf(stderr, "Hashing: %s\n", str);
  hash = 5381;

  c = strlen(str);
  for (i = 0; i < c; i++) {
    hash = (hash << 5) + hash + str[i];
  }
  if (DEBUG)
    fprintf(stderr, "Got a hash of %lu\n", hash);
  return hash;
}

/* ZT July 2017 */
/**
 * Returns to the user the size that should be passed to any of the IEL put
 * functions for an array with the given number of elements of the specified
 * type. Hopefully this function will reduce ambiguity/errors in passing the
 * correct size parameter to the relevant functions.
 *
 * @param[in] TYPE T -- One of the TYPE enum values; currenty INT or DOUBLE
 * @param[in] int numElements -- The number of elements in the array
 *
 * @returns \li \c size -- Element of type size_t that is the total memory size
 */
size_t get_size(TYPE T, int numElements) {
  size_t sz;

  sz = 0;
  switch (T) {
  case INT:
    sz = sizeof(int) * numElements;
    break;

  case DOUBLE:
    sz = sizeof(double) * numElements;
    break;

  default:
    fprintf(stderr, "Unsupported data type.\n");
    sz = -1;
  }

  return sz;
}
/* TODO: The next to functions are likely unused with the current revision.
 * Ensure that this is the case then delete them! (add_metadata_entry and
 * remove_metadata_value)*/
int *add_metadata_entry(int *metadata, int server_rank) {
  unsigned int i, sz;
  int *new_metadata_array;

  sz = metadata[0];

  fprintf(stderr, "size of the current metadata array: %d\n", sz);
  /* If there are unused spaces in the current metadata array, use one */
  for (i = 1; i < sz; i++) {
    if (metadata[i] == -1) {
      metadata[i] = server_rank;
      return metadata;
    }
  }

  /* Create the new array and set the new size (double the previous array size)
   */
  new_metadata_array = malloc(sizeof(int) * sz * 2);
  new_metadata_array[0] = sz * 2;
  for (i = 1; i < sz; i++)
    new_metadata_array[i] = metadata[i];
  for (; i < sz * 2; i++)
    new_metadata_array[i] = -1;

  // DEBUG
  for (i = 0; i < sz; i++)
    fprintf(stderr, "!%d!\n", new_metadata_array[i]);

  free(metadata);
  return new_metadata_array;
}

/* The first value of the metadata has been removed. Shift everything to the
 * left. */
int *remove_metadata_value(int *metadata) {
  int i, flag;

  flag = 1;
  metadata[1] = -1;
  for (i = 2; i < metadata[0]; i++) {
    if (metadata[i] != -1) {
      flag = 0;
      break;
    }
    metadata[i - 1] = metadata[i];
  }

  if (flag)
    metadata[metadata[0] - 1] = -1;

  return metadata;
}
