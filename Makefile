prefix     ?= /usr/local/openDIEL

default:
	@echo
	@echo "Choose or configure a Makefile.inc from MAKE.INC before compiling"
	@echo
	@echo "make all               Build openDIEL libraries, it's dependencies, and basic examples"
	@echo "make install           Install openDIEL include, libraries, and executable to prefix dir"
	@echo "make exe               Build openDIEL executable, it's dependencies, and basic examples"
	@echo "make source            Build openDIEL libraries"
	@echo "make extlib            Build external libraries"
	@echo "make examples          Build basic examples found in EXAMPLES"
	@echo "make applications      Build applications found in APPLICATIONS"
	@echo
	@echo "make cleanall          Remove openDIEL libraries and object files, external libraries and object files"
	@echo "make cleansource       Remove openDIEL libraries and object files"
	@echo "make cleanallextlib    Remove external libraries and object files"
	@echo "make cleanapplications Remove libraries and object files found in APPLICATIONS"
	@echo "make cleanexamples     Remove libraries and object files found in EXAMPLES"
	@echo

clean:
	@echo
	@echo "make cleanall          Clean openDIEL and external libraries"
	@echo "make cleansource       Clean openDIEL libraries"
	@echo "make cleanallextlib    Clean external libraries"
	@echo "make cleanapplications Clean examples found in APPLICATION"
	@echo "make cleanexamples     Clean libraries and object files found in EXAMPLES"
	@echo

IEL_HOME=$(PWD)

ifeq (,$(wildcard ./Makefile.inc))
$(error Makefile.inc not found. Please create one or use a template\
found in MAKE.INC, and place in the directory $(IEL_HOME))
endif

include $(IEL_HOME)/Makefile.inc

all: extlib source examples applications driver

install: source
	mkdir -p $(prefix)
	mkdir -p $(prefix)/include
	mkdir -p $(prefix)/lib
	mkdir -p $(prefix)/bin
	cp $(INCLUDE_DIR)/$(MACH)/* $(prefix)/include
	cp $(LIB_DIR)/$(MACH)/* $(prefix)/lib
	cp $(IEL_HOME)/BIN/* $(prefix)/bin
	@echo "FINISHED INSTALLING TO $(prefix)"

exe: driver

source: libdir libconfig comm exec driver

libdir:
	mkdir -p LIB
	mkdir -p INC

comm:
	cd $(SOURCE_DIR)/COMM; make
	@echo "FINISHED BUILDING COMMUNICATION (SOURCE) LIBRARY"
exec:
	cd $(SOURCE_DIR)/EXEC; make
	@echo "FINISHED BUILDING EXECUTIVE (SOURCE) LIBRARY"

driver:
	cd $(SOURCE_DIR)/DRIVER; make;
	@echo "FINISHED BUILDING DRIVER EXECUTABLE"

extlib: libconfig

examples: source
	cd EXAMPLES/HELLOWORLD; make
	cd EXAMPLES/DEPEND-BASIC; make
	cd EXAMPLES/DIR-COMM-TEST; make
	cd EXAMPLES/HELLOWORLD; make
	cd EXAMPLES/DEPEND-BASIC; make
	cd EXAMPLES/FANTEST; make
	cd EXAMPLES/MULTI-SET-FANTEST; make
	cd EXAMPLES/PARALLELTEST; make
	cd EXAMPLES/OMP-TEST; make
	cd EXAMPLES/DIR-COMM-TEST; make
	cd EXAMPLES/FORTRAN_MM; make
	cd EXAMPLES/CPP_WORKFLOW; make

applications: source 
	cd APPLICATIONS/LAPLACE-TUPLE-COMM; make
	cd APPLICATIONS/MULTI-TUPLE-SCALE; make
	cd APPLICATIONS/GPU-KEYWORDS; make
	cd APPLICATIONS/LAPLACE-DIR-COMM; make
	cd APPLICATIONS/LIGGGHTS; make
	cd APPLICATIONS/MULTI-TUPLE; make
	cd APPLICATIONS/OMP_AND_MPI-TEST; make
	cd APPLICATIONS/LAMMPS; make
	cd APPLICATIONS/MAGMA; make

uninstall:
	@echo "openDIEL is installed in $(prefix) by default. REMOVE MANUALLY"

cleanall: cleansource cleanallextlib cleanapplications cleanexamples

cleansource:
	cd $(SOURCE_DIR)/DRIVER; make clean
	cd $(SOURCE_DIR)/COMM; $(MAKE) clean
	cd $(SOURCE_DIR)/EXEC; $(MAKE) clean
	rm -f $(LIB_DIR)/COMM/*
	rm -f $(LIB_DIR)/EXEC/*
	rm -rf $(IEL_HOME)/BIN
	rm -rf $(IEL_HOME)/INC
	@echo "FINISHED CLEANING COMM & EXEC (SOURCE)"

cleanallextlib: cleanextlibsymlink
	cd $(IEL_HOME)/EXTLIB/mpich; $(MAKE) cleanall
	cd $(IEL_HOME)/EXTLIB/trilinos; $(MAKE) cleanall
	cd $(IEL_HOME)/EXTLIB/libconfig; $(MAKE) cleanall
	cd $(IEL_HOME)/EXTLIB/R; $(MAKE) cleanall
	cd $(IEL_HOME)/EXTLIB/perl; $(MAKE) cleanall
	@echo "FINISHED CLEANING EXTLIB & EXTLIB SYMBOLIC LINKS"

cleanexamples:
	cd EXAMPLES/HELLOWORLD; make cleanall
	cd EXAMPLES/DEPEND-BASIC; make cleanall
	cd EXAMPLES/DIR-COMM-TEST; make cleanall
	cd EXAMPLES/HELLOWORLD; make cleanall
	cd EXAMPLES/DEPEND-BASIC; make cleanall
	cd EXAMPLES/FANTEST; make cleanall
	cd EXAMPLES/MULTI-SET-FANTEST; make cleanall
	cd EXAMPLES/PARALLELTEST; make cleanall
	cd EXAMPLES/OMP-TEST; make cleanall
	cd EXAMPLES/DIR-COMM-TEST; make cleanall
	cd EXAMPLES/FORTRAN_MM; make cleanall
	cd EXAMPLES/CPP_WORKFLOW; make cleanall

cleanapplications:
	cd APPLICATIONS/LAMMPS; make cleanall
	cd APPLICATIONS/LAPLACE-TUPLE-COMM; make cleanall
	cd APPLICATIONS/MULTI-TUPLE-SCALE; make cleanall
	cd APPLICATIONS/FORT-TEST; make cleanall
	cd APPLICATIONS/GPU-KEYWORDS; make cleanall
	cd APPLICATIONS/LAPLACE-DIR-COMM; make cleanall
	cd APPLICATIONS/LIGGGHTS; make cleanall
	cd APPLICATIONS/MULTI-TUPLE; make cleanall
	cd APPLICATIONS/OMP_AND_MPI-TEST; make cleanall

cleanextlibsymlink:
	cd $(IEL_HOME)/EXTLIB/mpich; make cleansymlink
	cd $(IEL_HOME)/EXTLIB/trilinos; make cleansymlink
	cd $(IEL_HOME)/EXTLIB/libconfig; make cleansymlink
	cd $(IEL_HOME)/EXTLIB/R; make cleansymlink
	cd $(IEL_HOME)/EXTLIB/perl; make cleansymlink

cleanallmodule: cleandrivers
	cd EXAMPLES/HELLOWORLD; $(MAKE) clean
	@echo "FINISHED CLEANING ALL MODULES"

mpich:
	cd $(IEL_HOME)/EXTLIB/mpich; make
	@echo "FINISHED BUILDING MPICH EXTLIB"

trilinos: 
	cd $(IEL_HOME)/EXTLIB/trilinos; make
	@echo "FINISHED BUILDING TRILINOS EXTLIB"

libconfig:
	cd $(IEL_HOME)/EXTLIB/libconfig; make
	@echo "FINISHED BUILDING LIBCONFIG EXTLIB"

R:
	cd $(IEL_HOME)/EXTLIB/R; make
	@echo "FINISHED BUILDING R EXTLIB"

perl:
	cd $(IEL_HOME)/EXTLIB/perl; make
	@echo "FINISHED BUILDING PERL EXTLIB"
