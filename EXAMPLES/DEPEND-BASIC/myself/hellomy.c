#include "hellomy.h"
#include "unistd.h"

int hellomyself(IEL_exec_info_t *exec_info) {
  sleep(1);
  printf("myself\n");
  fflush(stdout);
  return IEL_SUCCESS;
}
