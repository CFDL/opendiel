Welcome to the DEPEND-BASIC example for using the openDIEL workflow system.
This example features an introduction to using the openDIEL workflow
group dependencies, which allow groups of modules to wait until other
groups have finished running before running themselves.
For a more detailed description of configuration files, see the HELLOWORLD
simulation's annotated configuration files. 

To build the example, simply use "make" in this directory
(openDIEL/APPLICATION/DEPEND-BASIC), and to rebuild the entire example,
use "make cleanall" in this directory and then type "make" again.
This compiles all modules involved in the example and then compiles the
driver, which is used to run the example in the openDIEL.

This simulation contains both an automatic module loading example and a
manual module loading example, differentiated by their configuration
files (workflowMM.cfg for the managed module and workflowAM.cfg for the
automatic module). Both of these examples use the same driver in this 
case, since adding the modules for use by the workflowMM does not interfere
with automatically loading the modules at runtime.

Note that when making your own simulations, it is important to modify the
driver.c file to include any parallel modules, and to modify the Makefile
in the DRIVER directory to include the libraries of the parallel modules.

To run the example, enter the DRIVER directory relative to this one, and
then you can utilize your machine's runscript if it exists, and if not,
you can simply run the driver under your machine's MPI environment with 12
processes.
