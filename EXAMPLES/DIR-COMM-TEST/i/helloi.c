/*
 * Copyright (c) 2015 University of Tennessee
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include "helloi.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int helloi(IEL_exec_info_t *exec_info) {
  int i;
  MPI_Request request;

  for(i = 0; i < exec_info->num_shared_bc; i++) {
    exec_info->shared_bc[i] = exec_info->IEL_rank;
  }

  // Assume there are an equal number of copies of all modules
  IEL_bc_put(exec_info, "hellome", &request);
  IEL_bc_put(exec_info, "hellomyself", &request);
  IEL_bc_get(exec_info, "hellome", &request);
  IEL_bc_get(exec_info, "hellomyself", &request);

  for(i = 0; i < exec_info->num_shared_bc; i++) {
    printf("Shared bc element %d on copy %d of helloi contents: %f\n",
           i, exec_info->module_copy_num, exec_info->shared_bc[i]);
  }

  return IEL_SUCCESS;
}
