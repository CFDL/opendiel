Welcome to the DIR-COMM-TEST example for using the openDIEL direct
communication functionality. This example explains how to use the
openDIEL to transfer data directly between modules during runtime
based on utilizing library functions provided. These direct
communication features are useful for transferring data between
different simulations that are "coupled," and can be a more
efficient solution than communication via the Tuple server because
direct communication sends a contiguous chunk of memory directly to
the desired location, rather than placing the data in a queue for
the recipient to pull from.

The openDIEL direct communication system uses an array of data
called the shared boundary conditions (the shared_bc data field
of the IEL_exec_info object passed into a module as an argument).
Modules can modify this array, and then call the IEL_bc_put and
IEL_bc_get functions to move data in the array between modules.
Note that the shared_bc array will only exist if the currently
running module is using direct communication.

The shared_bc array is partitioned to each module according to the
configuration file into readable and writable sections, and when
moving data via direct communication between modules, only writable
sections of the array will be sent, and only readable sections of
the array will be received. See the annotated workflow.cfg file
for more information about the setup of the shared_bc array.

As previously stated, the direct communication system features
functions provided by the openDIEL that can be inserted into
the source code of the modules. These functions are called
IEL_bc_put and IEL_bc_get, and serve as a nonblocking send (IEL_bc_put)
and a nonblocking receive with a wait (IEL_bc_get). Thus, the process
that calls IEL_bc_put will continue running after communication, while
the process that calls IEL_bc_get will wait until the communication
is complete. These functions are named as such because they use the
shared boundary condition array to simulate a partitioned memory window,
somewhat similar to the MPI functions MPI_Put and MPI_Get.
Documentation regarding these functions are as follows:

 *********************
 *
 * IEL_bc_put:
 *
 * Packs and sends (nonblocking) the write-partitioned data in this process's
 * shared_bc array to the head process of copy number cpy of the specified module;
 * should only be called on this copy's head process. Returns IEL_SUCCESS
 * on success and -1 on failure.
 * In the event that the module specified by the argument is used multiple times
 * in the workflow, only gets from the first instance.
 *
 * @param exec_info -- pointer to the IEL Executive info struct
 * @param module    -- name of the module to put data on
 * @param cpy       -- copy number of the module to put data on
 * @param request   -- request status for the send
 *
 * function:
 * int IEL_bc_put(IEL_exec_info_t *exec_info, const char* module, int cpy, MPI_Request *request)
 *
 *********************
 *
 * IEL_bc_get:
 *
 * Receives (nonblocking with wait) and unpacks the read-partitioned data 
 * into this process's shared_bc array from the head process of copy number
 * cpy of the specified module; should only be called on this copy's head 
 * process, with a corresponding bc_put.
 * Returns IEL_SUCCESS on success and -1 on failure.
 * In the event that the module specified by the argument is used multiple times
 * in the workflow, only gets from the first instance.
 *
 * @param exec_info -- pointer to the IEL Executive info struct 
 * @param module    -- name of the module to get data from
 * @param cpy       -- copy number of the module to get data from
 * @param request   -- request status for the receive and wait
 *
 * function:
 * int IEL_bc_get(IEL_exec_info_t *exec_info, const char* module, int cpy, MPI_Request *request)
 *
 *********************

The openDIEL's direct communication library also features a few functions
to help with synchronization within certain workflow elements, to aid in
keeping  modules running in the intended order. These functions are effectively
wrappers around MPI_Barrier calls based on different communicators, which
prevent the user from needing to access data fiels of the executive info
structure beyond the shared_bc array. These functions all return 
IEL_SUCCESS on success and IEL_SYNCH_ERR on failure. The documentation
for these functions is as follows:

 *********************
 *
 * IEL_barrier:
 *
 * A wrapper around MPI_Barrier that allows synchronization of all
 * processes within the openDIEL
 *
 * @param exec_info -- pointer to the IEL Executive info struct 
 *
 * function:
 * int IEL_barrier(IEL_exec_info_t *exec_info)
 *
 *********************
 *
 * IEL_module_barrier:
 *
 * A wrapper around MPI_Barrier that allows synchronization of only the
 * calling module
 *
 * @param exec_info -- pointer to the IEL Executive info struct 
 *
 * function:
 * int IEL_module_barrier(IEL_exec_info_t *exec_info)
 *
 *********************
 *
 * IEL_group_barrier:
 *
 * A wrapper around MPI_Barrier that allows synchronization of only the
 * calling workflow group
 *
 * @param exec_info -- pointer to the IEL Executive info struct 
 *
 * function:
 * int IEL_group_barrier(IEL_exec_info_t *exec_info)
 *
 *********************
 *
 * IEL_copy_barrier:
 *
 * A wrapper around MPI_Barrier that allows synchronization of only the
 * calling module copy
 *
 * @param exec_info -- pointer to the IEL Executive info struct 
 *
 * function:
 * int IEL_module_copy_barrier(IEL_exec_info_t *exec_info)
 *
 *********************

This example contains three modules, called helloi, hellome, and hellomyself,
which are modified versions of modules in previous examples. These modules
in this example will fill their shared boundary condition array with their
process number, then perform an IEL_bc_put to both other modules and then
an IEL_bc_get from both other modules. This causes the modules to exchange
their partioned portion of the shared boundary condition array with the
other modules in the example; each module is partitioned a section of five
elements of the shared boundary conditions to write, and a the rest of the
elements to read from the other modules. The modules then print the contents
of their respective shared_bc array. For more information on this
partitioning, see the workflow.cfg file contained in the driver.

The example is set up to use a single copy of each module, but multiple
copies can also be tested by setting the "size" or "copies" setting
inside the workflow.cfg file; these test modules are set up to perform
communication with the matching copy number of the other modules, as
long as the number of copies of all modules is the same. For more
information on running multiple copies of a module, see the PARALLELTEST
module. See the actual module files (i/helloi.c, me/hellome.c,
myself/hellomy.c) for an example of how the communication functions
are called.

To build the example, simply use "make" in this directory
(openDIEL/APPLICATION/DIR-COMM-TEST), and to rebuild the entire
example, use "make cleanall" in this directory and then type "make" again.
This compiles all non-automatically loaded modules involved in the example,
and then compiles the driver, which is used to run the example in the
openDIEL.

To run the example, enter the DRIVER directory relative to this one, and
then you can utilize your machine's runscript if it exists, and if not,
you can simply run the driver under your machine's MPI environment with 4
processes.
