/*
 * Copyright (c) 2015 University of Tennessee
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include "IEL.h"
#include "helloi.h"
#include "mpi.h"
#include "hellome.h"
#include "hellomy.h"
#include "tuple_server.h"


int main(int argc, char** argv) {

  MPI_Init(&argc, &argv);

  int rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  // ----------------- Timer ---------------------
  timestamp ("Begin", "driver.c", 1);
  // ----------------- Timer ---------------------

  IELAddModule(ielTupleServer, "ielTupleServer");
  IELAddModule(helloi, "helloi");
  IELAddModule(hellome, "hellome");

  MPI_Barrier(MPI_COMM_WORLD);
  IELExecutive(MPI_COMM_WORLD, argv[1]);
  MPI_Barrier(MPI_COMM_WORLD);

  // ----------------- Timer ---------------------
  timestamp ("End", "driver.c", -1);
  timer_finalize (rank);
  // ----------------- Timer ---------------------
  
  MPI_Finalize();

}
