#########################
Nicholas Moran, Kwai Wong
Joint Institute for Computational Sciences

Notes
------------------------------------------------------------------------------------

AM - Automatic mode - for serial codes. The driver will take an arbitrary amount of 
     executables and will not need to be compiled again if a module is created, 
     modified or deleted. This is designed to run a sequence of multiple copies of serial 
     codes. The module configuration file is the input for the path of execitable and others
     information.  It can run typically any executables including perl, python, and R scripts.

MM - Managed mode - for parallel codes. The driver will need to be recompiled if the 
     modules are created, modified or deleted. The modules are user specifc modules that
     are written to run in parallel. The user modules will need to be modified
     to run under a sub-MPI communicator designated by the openDIEL executive.
     A script called modMaker will modify the code but user attention will be needed to
     compile the code because in general it is hard to automate the generation
     of the Makefile and the compiling process of a user's code.

----------------------------------------------------------------------------------
----------------------------------------------------------------------------------

HELLOWORLD 

An example module to showcase the functionality of the openDIEL's workflow engine.

Overall : --
type " make " in the top directory

----------------------------------------------------------------------------------
----------------------------------------------------------------------------------

Automatic Mode : i-serial directory -------------------------->

Simple serial codes are used.

1) cd to i-serial
2) type "make"
 
Managed Mode : i, me, myself directories ---------------------->

Each module (me, i, myself) simply prints the name of the module. This allows
the user to play with different workflows to see how they execute. 
Each code accepts a sub communicator passes down from the IEL and compiles as
a library (module). They will be loaded by the executive identified
in the configuration (input) file.


Compiling Modules
-----------------
cd to i and type make
cd to me and type make me
cd to myself and type make myself

MODULE-FILE directory --------------------------------------->

ALl the needed libraries and module files, primarily modexec.c and modrscript.c, are put in 
this directory for access. The automatic code for running serial code is also compiled as
a module, which will be loaded. The R script module is special now on darter (path).
You may read the modexec.c code to get an idea how things are organized.

1) cd MODULE-FILE
2) type make
3) copy all the libxxx.a and *.h from i, me, and myself to here

DRIVER Directory ------------------------------------------->

The DRIVER directory contains the main program (driverxx.c), to run the code. The openDIEL
workflow engine contains three main parts, the driver (main) program, the module 
libraries (users' codes), and the configuration file (input and workflow instructions). 
The driver code loads the specific modules to be used, the modexec module is the
automatic module for any serial codes. 

The configuration files (workflow.cfg, workflowAM.cfg, and workflowMM.cfg) in the DRIVER
directory are all documented in a way that explains the basics of configuration files
and are meant to serve as basic guides for understanding how configuration files are set up.

There are three drivers: the simple driver (driverSM.c), the automatic mode driver (driverAM.c),
and the managed mode driver (driverMM.c). The managed mode driver in fact is a combination of
the simple driver and the automatic mode driver.

1) cd DRIVER
2) type make

USECASE Directory: Running WORKFLOW-AM examples --------------------->

1) cd USECASE/WORKFLOW-AM directory
2) Copy driverAM form DRIVER directory (It is already there for darter) 
3) Copy helloiexe and helloifexe from i-serial directory (They are there for darter)
4) Change the project ID in pbssubAM to the right one
5) Type "qsub pbssubAM"
6) Check for output : type "grep IEL-Module pbssubAM.oxxxx"

USECASE Directory: Running WORKFLOW-MM examples --------------------->

This runs hello user modules, perl, R, python, sh, java, scripts.

1) cd USECASE/WORKFLOW-MM directory
2) Copy driverMM form DRIVER directory (It is already there for darter) 
3) Copy helloiexe and helloifexe from i-serial directory (They are there for darter)
4) Change the project ID in pbssubMM to the right one
5) Type "qsub pbssubMM"
6) Check for output : type "grep IEL-Module pbssubMM.oxxxx"

