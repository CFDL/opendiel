/*
 * Copyright (c) 2015 University of Tennessee
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include"modrscript.h"
#include"IEL_exec_info.h"
#include<unistd.h>
#include<stdlib.h>
#include <sys/wait.h>
#include <stdio.h>

extern int R_running_as_main_program;   /* in ../unix/system.c */

extern IEL_exec_info_t* exec_info;

int modrscript(IEL_exec_info_t *exec_info) {

  int PID, status;
  int ret;

  if((PID = fork()) == 0) {
    //child
    printf("Hello from child\n");
    int argc = exec_info->modules[exec_info->module_num].mod_argc;
    const char **argv = (const char**)exec_info->modules[exec_info->module_num].mod_argv;

    char ** new_argv = (char**) malloc(sizeof(char*) * 3);

    new_argv[0] = "R";
    new_argv[1] = "--slave";
    new_argv[2] = "--file=INPUTFILE.R";

    SEXP aleph, beth;
    R_running_as_main_program = 0;              //May do nothing?
    Rf_initialize_R(3, new_argv);              //R --slave --file=(filename)
    Rf_mainloop();


  } else {
    //parent
    printf("Hello from parent\n");
    while(wait(&status) != PID);
    if(status == 0)
       printf(" !!! ===> SUCCESSFUL R Analysis  RUN,  Exit Code = %d, IN RANK ID: DIR RCOMP - (%d)\n  ",
           status, exec_info->module_rank);

    if(status != 0)
       printf(" !!! ===> FAILED R Analysis  RUN,  Exit Code = %d, IN RANK ID: DIR RCOMP - (%d)\n  ",
           status, exec_info->module_rank);

    fflush(stdout);
  }
}

