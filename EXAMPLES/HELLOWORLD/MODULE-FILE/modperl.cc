/*
 * Copyright (c) 2015 University of Tennessee
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include"modperl.h"
#include"IEL_exec_info.h"
#include<unistd.h>
#include<stdlib.h>
#include <sys/wait.h>
#include <stdio.h>

extern "C" {
IEL_exec_info_t* exec_info_perl;

int modperl(IEL_exec_info_t *exec_info_arg) {

  int PID, status;
  int ret;

  exec_info_perl = exec_info_arg;
  if((PID = fork()) == 0) {
    //child
    printf("Hello from child\n");
    int argc = exec_info_perl ->modules[exec_info_perl->module_num].mod_argc;
    const char **argv = (const char**)exec_info_perl->modules[exec_info_perl->module_num].mod_argv;

    char ** new_argv = (char**) malloc(sizeof(char*) * 5);

//    For darter ----
//    new_argv[0] = "/sw/xc30/perl/5.20.2/cle5.2_gnu4.9.2/bin/perl";
//    For wrangler ----
    new_argv[0] = "/usr/bin/perl";
    new_argv[1] = "combine_csv.pl";
    new_argv[2] = "Sim_Outputs";
    new_argv[3] = "13_Stand-aloneRetail_inputs_matrix.csv";
    new_argv[4] = NULL;

    execv(new_argv[0], new_argv);
    perror("execv");
    exit(1);

  } else {
    //parent
    printf("Hello from parent\n");
    while(wait(&status) != PID);
    if(status == 0)
       printf(" SUCCESSFUL PERL  RUN,  Exit Code = %d, IN RANK ID: DIR PERL - (%d)\n  ",
           status, exec_info_perl->module_rank);

    if(status != 0)
       printf(" !!! ===> FAILED PERL RUN,  Exit Code = %d, IN RANK ID: DIR PERL - (%d)\n  ",
           status, exec_info_perl->module_rank);

    fflush(stdout);
  }
}
}//extern "C"

