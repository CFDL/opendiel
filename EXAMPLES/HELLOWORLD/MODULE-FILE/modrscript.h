/*
 * Copyright (c) 2015 University of Tennessee
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

///#ifndef MAINMOD_modrscript_H
///#define MAINMOD_modrscript_H

#include "R.h"
#include "Rinternals.h"
#include "Rembedded.h"
#include "Rinterface.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "IEL_exec_info.h"

///#ifdef __cplusplus
///extern "C" {
///#endif

extern IEL_exec_info_t* exec_info;
//int modrscript(IEL_exec_info_t* exec_info_arg);
int modrscript(IEL_exec_info_t* exec_info);

///#ifdef __cplusplus
///}
///#endif

///#endif

