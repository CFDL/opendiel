/*
 * Copyright (c) 2015 University of Tennessee
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include "helloi.h"
#include <stdio.h>
#include <stdlib.h>

int helloi(IEL_exec_info_t *exec_info) {

  FILE * fp;
  fp = fopen ("file.txt", "w+");
  printf("i\n");
  fflush(stdout);

  fprintf(fp, "%s %s %s ", "HELLO-", "FROM-", "I-" );
   
  fclose(fp);

  return IEL_SUCCESS;

}
