# openDIEL CPP Managed Mode Example

Creates an openDIEL workflow in which calls a cpp function. The cpp function must be compiled with a cpp compiler, and the final linking stage is done with a cpp compiler
