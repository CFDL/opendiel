#include "IEL_exec_info.h"

#ifndef CPP_PROGRAM
#define CPP_PROGRAM

/*place extern "C"{} around the c++ prototypes */
#ifdef __cplusplus
extern "C" {
#endif

int cpp_program(IEL_exec_info_t *);

#ifdef __cplusplus
}
#endif

#endif // CPP_PROGRAM