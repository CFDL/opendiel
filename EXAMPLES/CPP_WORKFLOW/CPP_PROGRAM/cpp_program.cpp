#include "cpp_program.h"
#include <iostream>

int cpp_program(IEL_exec_info_t *execInfo) {
  std::cout << "hello from process number " << execInfo->module_copy_rank
            << " of copy " << execInfo->module_copy_num << std::endl;
  return IEL_SUCCESS;
}