# openDIEL Fortran Managed Mode Example

Creates an openDIEL workflow in which calls a fortran function and a c function. Both functions multiply two matrices using mpi and output the results. A split directory is used to distribute the results
