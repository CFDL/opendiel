#include "helloall.h"
#include "unistd.h"

int helloall(IEL_exec_info_t *exec_info) {
  
  usleep(50000);
  printf("hello, all\n");
  fflush(stdout);

  return IEL_SUCCESS;
}
