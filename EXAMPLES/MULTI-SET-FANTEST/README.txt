Welcome to the MULTI-SET-FANTEST example for using the openDIEL workflow
system. This example features the use of the openDIEL's workflow set
functionality.

To build the example, simply use "make" in this directory
(openDIEL/APPLICATION/MULTI-SET-FANTEST), and to rebuild the entire
example, use "make cleanall" in this directory and then type "make" again.
This compiles all non-automatically loaded modules involved in the example
(first, second, third, fourth, fifth, and last) and then compiles the 
driver, which is used to run the example in the openDIEL.

To run the example, enter the DRIVER directory relative to this one, and
then you can utilize your machine's runscript if it exists, and if not,
you can simply run the driver under your machine's MPI environment with 16
processes.

This example runs two sets of groups at once comprised of examples from
both the DEPEND-BASIC and FANTEST simulations. See the annotated
workflow.cfg file for more details.
