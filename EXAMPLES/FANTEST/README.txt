Welcome to the FANTEST example for using the openDIEL workflow system.
This example features the use of the openDIEL's num_runs setting as well
as a more advanced example its dependency features than DEPEND-BASIC.

To build the example, simply use "make" in this directory
(openDIEL-V2/APPLICATION/FANTEST), and to rebuild the entire example,
use "make cleanall" in this directory and then type "make" again.
This compiles all modules involved in the example (first, second, third,
fourth, fifth, and last) and then compiles the driver, which is used
to run the example in the openDIEL. Note that all modules used here are
loaded manually rather than automatically.

To run the example, enter the DRIVER directory relative to this one, and
then you can utilize your machine's runscript if it exists, and if not,
you can simply run the driver under your machine's MPI environment with 17
processes. The runscript rebuilds the driver again (useful for if you have
only made changes to the openDIEL source code and not the example itself)
and then runs it. The script should take just over a minute to run.

The FANTEST example runs 17 processes via settings described in the
DRIVER/workflow.cfg file; this file provides a working example of how to
set up an openDIEL configuration file for running other tests.

The runscript places the output of the example in a file called fantest.out.
Inside of fantest.out, you should see the output of the openDIEL (several
initialization and closure messages) and the output of all modules that
have been run. Each module's output should be surrounded with -----, so it
is easy to differentiate the modules' specific outputs from the openDIEL's
outputs. Each module utilizes the sleep() function to help ensure that the
order of outputs matches more closely to the dependencies outlined in
workflow.cfg. Again, see the annotated workflow.cfg to better understand
the output of the modules. It is also important to note that any parallel
modules to be included in a run must be added into the driver via 
IELAddModule(), and any serial modules must be included in workflow.cfg
via the proper automatic module syntax (see HELLOWORLD for more details
on this).
