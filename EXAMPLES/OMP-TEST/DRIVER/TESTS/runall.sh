#!/bin/bash

#This script will run and do a basic check of the output
#All output will be put into the directory OUTPUT

PBSSUBDIR="./PBSSUB"
OUTPUTDIR="./OUTPUT"
WORKFLOWDIR="./WORKFLOWS"

for i in {1..9}
do
	sbatch $PBSSUBDIR/pbssub-comet-0$i
done

echo "Finished submitting. Once the output comes, run test.sh to test it"
