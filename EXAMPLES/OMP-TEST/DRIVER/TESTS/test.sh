testfailed=0

# Simply count the number of lines of output that comes from 
# the omp threads
for i in {1..9}
do
	testfail=0
	numthreads=$(cat OD-hellompi-test-0$i* | grep Team | wc | awk '{print $1;}')
	correctnumthreads=$(cat ./CORRECT-OUTPUT/test-$i-correct | head -n 1)

	numhosts=$(cat OD-hellompi-test-0$i* | grep master | wc | awk '{print $1;}')
	correctnumhosts=$(cat ./CORRECT-OUTPUT/test-$i-correct | head -n 2 | tail -n 1)

	host1=$(cat OD-hellompi-test-0$i* | grep master | tail -n 2 | tail -n 1 | awk '{print $7;}')
	host2=$(cat OD-hellompi-test-0$i* | grep master | tail -n 2 | head -n 1 | awk '{print $7;}')

	if [ $host1 = $host2 ] 
	then 
		numhosts="1"
	fi

	if [ $numhosts != $correctnumhosts ] 
	then 
		echo "Test $i failed: incorrect number of hosts. Got $numhosts hosts, expected $correctnumhosts"
		testfailed=$(($testfailed + 1))
		testfail=1
	elif [ $host1 = $host2 ] && [ $numhosts != "1" ]
	then 
		echo "Test $i failed: Hosts are the same"
		testfailed=$(($testfailed + 1))
		testfail=1
	elif [ $numthreads != $correctnumthreads ] 
	then
		echo "Test $i failed: $numthreads threads were created, expected $correctnumthreads"
		testfailed=$(($testfailed + 1))
		testfail=1
	fi
	
	if [ $testfailed -eq 0 ] 
	then
		echo "Test $i passed"
	fi

	if [ $testfail -gt 0 ] 
	then 
		echo "Host 1 = $host1"
		echo "Host 2 = $host2"
	fi
done

# Print whether or not any tests failed
if [ $testfailed -gt 0 ]
then 
	echo "Test failure, $testfailed tests failed"
else 
	echo "All tests successfully passed"
fi
