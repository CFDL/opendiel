  /**********************************************************/
 /* Example provided by Cornell University *****************/
/**********************************************************/

#include <stdio.h>
#include <sched.h>
#include "hello.h"
#include "omp.h"
int hello(IEL_exec_info_t *exec_info){
  int processor_name_len; 
  char processor_name[MPI_MAX_PROCESSOR_NAME]; 

  MPI_Get_processor_name(processor_name, &processor_name_len);
  printf("Hello from master thread. Running on %s, CPU %d\n", processor_name, sched_getcpu() );

#pragma omp parallel
  {
     MPI_Get_processor_name(processor_name, &processor_name_len);
     printf("Team member %d reporting from team of %d on process %d. Running on %s, CPU %d\n",
            omp_get_thread_num(),omp_get_num_threads(), exec_info->IEL_rank, processor_name, sched_getcpu() );
  }

  printf("Master thread finished, goodbye.\n");

  return IEL_SUCCESS;
}
