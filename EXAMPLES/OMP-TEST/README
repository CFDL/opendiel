CONTENTS
===============================================================================
DIRECTORY STRUCTURE...List of directories that can be found in this example and
                      a summary of their contents
INTRODUCTION..........Introduction into this example
KEYWORDS..............List and description of important keywords used by this 
                      example
COMPILING.............Instructions on how to build this example
RUNNING...............Instructions on how to run this example
TODO..................List of things that need to be done for this example

DIRECTORY STRUCTURE
===============================================================================
DRIVER                          - Contains source code for the driver and 
                                  workflow.cfg's needed to run examples
HELLO-OMP                       - Contains the source code for the modules that 
                                  run code containing openMP threads
MODULE-FILE                     - Contains header files and modexec source code
OMP-TEST/DRIVER/TESTS           - Contains batch submission scripts to run 
                                  tests on comet
OMP-TEST/DRIVER/TESTS/WORKFLOWS - Contains annotated workflows that give 
                                  various examples of how the OMP threads can 
                                  be set for modules

INTRODUCTION
===============================================================================
This example contains files to test the functionality of setting
the number of openMP threads per process with the openDIEL keyword 
'threads_per_process'

KEYWORDS
===============================================================================
The following keywords are important to this example: 

threads_per_process
-------------------
This keyword is used to set the number of openMP threads are be created for 
OMP parallel sections. It works by setting the environment variable 
'OMP_NUM_THREADS=threads_per_process' and calling 
'omp_set_num_threads(threads_per_process)'. This keyword is specified on a 
per-module basis.

Note that running code with that contains calls to omp_set_num_threads() with a 
number that differs from that which is specified by the 'threads_per_process'
variable in the Workflow Configuration File will result in the 
'threads_per_process' value being overridden by the one set by 
omp_set_num_threads(). For example, if one sets 'threads_per_process=12', but 
the code in the executable you're running calls 'omp_set_num_threads(2)', the 
number of omp threads used for parallel sections will be 2.

COMPILING
===============================================================================
Running 'make' in the toplevel OMP-TEST directory will result in the following 
steps being performed: 

1. Build hello_exe and hello managed module
-------------------------------------------
In HELLO-OMP, an executable 'hello_exe' and a library that is the same as the 
executable 'hello_exe', only it is to be called by the openDIEL driver. 

The executable will be placed in DRIVER, and the library will be places in 
MODULE-FILE.

2. Build modexec
----------------
In MODULE-FILE, modexec.a will be compiled

3. Build driver
---------------
Two versions of the driver will be build, one that it meant to run the
automatic module, and one that is meant to build the managed module

RUNNING
===============================================================================
After compiling, the driver in DRIVER can be run with 2 MPI processes: 

mpirun -np 2 ./driver workflow.cfg

To start the automatic version, run with 2 MPI processes: 

mpirun -np 2 ./driver_exe workflow_AM.cfg

TODO
===============================================================================
Create additional workflow.cfg files to test functionality and combinations of 
settings[] 
-Particularly needed is on automatic modules
