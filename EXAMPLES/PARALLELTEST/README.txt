Welcome to the PARALLELTEST module for using the openDIEL.  This example
uses a simple parallel module to show how the openDIEL can integrate
parallel code as a module as well as how the openDIEL can use multithreading.

To build the example, simply use "make" in this directory
(openDIEL-V2/APPLICATION/PARALLELTEST), and to rebuild the entire
example, use "make cleanall" in this directory and then type "make" again.
This compiles the "alone" module (which asynchronously sends an MPI
message to the process below within the module and then receives a
message from the process above within the module) and then compiles 
the driver, which is used to run the example in the openDIEL.

To run the example, enter the DRIVER directory relative to this one, and
then you can utilize your machine's runscript if it exists, and if not,
you can simply run the driver under your machine's MPI environment with 6
processes.

The annotated configuration file, DRIVER/workflowMM.cfg, showcases various
settings of modules that allow more precise control of how modules are run.
