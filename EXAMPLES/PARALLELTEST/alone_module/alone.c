/*
 * Copyright (c) 2015 University of Tennessee
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include "alone.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <mpi.h>

int alone(IEL_exec_info_t *exec_info) {
  // Exchange process numbers within the module
  int data[2];
  data[0] = exec_info->IEL_rank;
  int source, dest;

  // Send this module's overall MPI rank down one rank
  if(exec_info->module_copy_rank == 0)
    dest = exec_info->module_processes[exec_info->module_num] - 1;
  else
    dest = exec_info->module_copy_rank - 1;
  
  if(exec_info->module_copy_rank == exec_info->module_processes[exec_info->module_num] - 1)
    source = 0;
  else
    source = exec_info->module_copy_rank + 1;

  MPI_Request request0, request1;
  MPI_Status status;
  MPI_Isend(&data[0], 1, MPI_INT, dest, 0, exec_info->module_copy_comm, &request0);
  MPI_Irecv(&data[1], 1, MPI_INT, source, 0, exec_info->module_copy_comm, &request1);
  MPI_Wait(&request1, &status);

  printf("process %d has received from process %d\n",
      data[0], data[1]);
  fflush(stdout);
  
  FILE * fp;
  char filename[10];
  sprintf(filename, "file%d.txt", data[0]); 
  
  fp = fopen (filename, "w+");
  fprintf(fp, "process %d has received from process %d\n",
      data[0], data[1]);
  fclose(fp);

  return IEL_SUCCESS;

}
