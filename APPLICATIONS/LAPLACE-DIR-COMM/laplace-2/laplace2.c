/******************************************************************/
/* Laplace Equation                                               */
/*  T is initially 0.0 everywhere except at the boundaries where  */
/*  T=100.                                                        */
/*                                                                */
/*          T=100                                                 */ 
/*         ________           |_____|_____|_____|_____|           */
/*        |        |          |     |     |     |     |           */
/*        |        |          |     |     |     |     |           */
/*  T=100 | T=0.0  | T=100    |  1  |  2  |  3  |  4  |           */
/*        |        |          |     |     |     |     |           */ 
/*        |________|          |_____|_____|_____|_____|           */
/*          T=100             |     |     |     |     |           */
/*                                                                */
/*                                                                */ 
/*  Use Central Differencing Method                               */
/*  Each process only has subgrid                                 */
/*  Each process works on a subgrid and then sends its boundaries */
/*    to nearest neighbors                                        */
/*  THANKS TO PSC FOR USE OF THIS PROGRAM                         */
/******************************************************************/

#define NC       10                     /* Number of Cols        */
#define NR       10                     /* Number of Rows        */
#define NITER    100                   /* Max num of Iterations */
#define MAX(x,y) ( ((x) > (y)) ? x : y )

#include <stdio.h>
#include <math.h>
#include "mpi.h"                        /* Required MPI library */
#include "IEL_exec_info.h"              /* DIEL executive info struct */
#include "laplace2.h"

void initialize(float t[NR+2][NC+2], int nr, int nc);
void set_bcs   (float t[NR+2][NC+2], int mype, int npes, int nr, int nc);

int laplace2(IEL_exec_info_t *exec_info) {

  int         npes;                      /* Number of processes */
  int         mype;                      /* My process number  */
  int         niter;                     /* iteration counter  */
  int         nc;                        /* number of columns */
  int         nr;                        /* number of rows */
  MPI_Request request;

  nr = exec_info->num_shared_bc / (exec_info->IEL_num_ranks);
  nc = exec_info->num_shared_bc / (exec_info->IEL_num_ranks);

  /* t is the current array, told is a copy of the previous iteration of t */
  float       t[nr+2][nc+2], told[nr+2][nc+2]; 

  /* Delta t, used for laplace calculation */
  float       dt; 

  /* iteration variables */
  int         i, j, iter;

  /* Give initial guess of 0. */
  initialize(t, nr, nc);                 

  /* Set npes to the number of IEL ranks and mype to the current IEL rank */
  npes = exec_info->IEL_num_ranks;
  mype = exec_info->IEL_rank;

  /* Set the boundary values */
  set_bcs(t, exec_info->IEL_rank, exec_info->IEL_num_ranks, nr, nc); 

  /* Copy the values into told */
  for( i=0; i<=nr+1; i++ )    
    for( j=0; j<=nc+1; j++ )
      told[i][j] = t[i][j];

/*----------------------------------------------------------*
 |       Do Computation on Sub-grid for Niter iterations    |
 *----------------------------------------------------------*/

  niter = NITER;

  for( iter=1; iter<=niter; iter++ ) {

    for( i=1; i<=nr; i++ ) {
      for( j=1; j<=nc; j++ ) {
        t[i][j] = 0.25 * ( told[i+1][j] + told[i-1][j] + told[i][j+1] + told[i][j-1] );
      }
      dt = 0.;
    }

    for(i = 1; i <= nr; i++) {      /* Copy for next iteration  */
      for(j = 1; j <= nc; j++) {
	     dt = MAX(abs(t[i][j]-told[i][j]), dt);
         told[i][j] = t[i][j];
      }
    }
  
    /* Set the shared_bc write equal to the top row of t.
     * shared_bc write is specified by the user in the workflow.cfg file */
    for(i = 1500; i < 2000; i++) {
      exec_info->shared_bc[i] = t[0][i%1500];
    }
    
    /* This function sends the shared_bc 'write' values to the
     * "laplace0" shared_bc 'read' values and 'reads' the shared_bc
     * 'write' values from the "laplace0" module.
     */
    IEL_bc_exchange(exec_info, "laplace1", &request);

    /* Set the top row of t equal to what is received by the IEL_bc
     * exchange from laplace 1. */
    for(i = 1500; i < 2000; i++) {
      t[0][i%1500] = exec_info->shared_bc[i];
    }

    /* Print out a desired iteration to see results */
    if(iter%100 == 0) {
      printf("Laplace 2, Iteration %d, t[30][116] = %20.8f\n", iter, t[30][116]);
    } 
  }

  return IEL_SUCCESS;
}  
