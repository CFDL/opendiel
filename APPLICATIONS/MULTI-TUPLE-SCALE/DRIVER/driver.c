/*
 * Copyright (c) 2015 University of Tennessee
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include <stdlib.h>
#include <stdio.h>
#include "IEL.h"
#include "libconfig.h"
#include "IEL_exec_info.h"
#include "modexec.h"
#include "hello1.h"
#include "hello2.h"
#include "hello3.h"
#include "hello4.h"
#include "hello5.h"
#include "hello6.h"
#include "hello7.h"
#include "hello8.h"
/*
#include "hello9.h"
#include "hello10.h"
#include "hello11.h"
#include "hello12.h"
#include "hello13.h"
#include "hello14.h"
#include "hello15.h"
#include "hello16.h"
#include "hello17.h"
#include "hello18.h"
#include "hello19.h"
#include "hello20.h"
#include "hello21.h"
#include "hello22.h"
#include "hello23.h"
#include "hello24.h"
*/
#include "tuple_server.h"

#define MOD_STRING_LENGTH 20

void ConfigFile(void);

int main(int argc, char* argv[])
{
  int rc, rank, num_modules, i, size;
  char mod_name[MOD_STRING_LENGTH];
  config_t cfg;
  config_setting_t *setting;

  // Initialize basic MPI settings
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD,&rank);
  MPI_Comm_size(MPI_COMM_WORLD,&size);

  // ----------------- Timer ---------------------
  timestamp ("Begin", "driver.c", 1);
  // ----------------- Timer ---------------------

  // Add all non-serial modules manually via IELAddModule
  IELAddModule(ielTupleServer,"ielTupleServer");
  IELAddModule(ielTupleServer, "ielTupleServer1");
  IELAddModule(ielTupleServer, "ielTupleServer2");
  IELAddModule(ielTupleServer, "ielTupleServer3");
  IELAddModule(ielTupleServer, "ielTupleServer4");
  IELAddModule(ielTupleServer, "ielTupleServer5");
  IELAddModule(ielTupleServer, "ielTupleServer6");
  IELAddModule(ielTupleServer, "ielTupleServer7");
  IELAddModule(ielTupleServer, "ielTupleServer8");
  IELAddModule(ielTupleServer, "ielTupleServer9");
  /*
  IELAddModule(ielTupleServer, "ielTupleServer10");
  IELAddModule(ielTupleServer, "ielTupleServer11");
  IELAddModule(ielTupleServer, "ielTupleServer12");
  IELAddModule(ielTupleServer, "ielTupleServer13");
  IELAddModule(ielTupleServer, "ielTupleServer14");
  IELAddModule(ielTupleServer, "ielTupleServer15");
  IELAddModule(ielTupleServer, "ielTupleServer16");
  IELAddModule(ielTupleServer, "ielTupleServer17");
  IELAddModule(ielTupleServer, "ielTupleServer18");
  IELAddModule(ielTupleServer, "ielTupleServer19");
  IELAddModule(ielTupleServer, "ielTupleServer20");
  IELAddModule(ielTupleServer, "ielTupleServer21");
  IELAddModule(ielTupleServer, "ielTupleServer22");
  IELAddModule(ielTupleServer, "ielTupleServer23");
  IELAddModule(ielTupleServer, "ielTupleServer24");
  IELAddModule(ielTupleServer, "ielTupleServer25");
  */

  /* Add user modules here! */
  IELAddModule(hello1, "hello1");
  IELAddModule(hello2, "hello2");
  IELAddModule(hello3, "hello3");
  IELAddModule(hello4, "hello4");
  IELAddModule(hello5, "hello5");
  IELAddModule(hello6, "hello6");
  IELAddModule(hello7, "hello7");
  IELAddModule(hello8, "hello8");
  /*
  IELAddModule(hello9, "hello9");
  IELAddModule(hello10, "hello10");
  IELAddModule(hello11, "hello11");
  IELAddModule(hello12, "hello12");
  IELAddModule(hello13, "hello13");
  IELAddModule(hello14, "hello14");
  IELAddModule(hello15, "hello15");
  IELAddModule(hello16, "hello16");
  IELAddModule(hello17, "hello17");
  IELAddModule(hello18, "hello18");
  IELAddModule(hello19, "hello19");
  IELAddModule(hello20, "hello20");
  IELAddModule(hello21, "hello21");
  IELAddModule(hello22, "hello22");
  IELAddModule(hello23, "hello23");
  IELAddModule(hello24, "hello24");
  */

  // Read configuration file for retrieval of serial modules;
  // find the number of modules, and transmit it to all procs
  if(rank == 0) {
    config_init(&cfg);
    if(argc < 2) {
      fprintf(stderr, "IEL ERROR: No config file specified\n");
      MPI_Abort(MPI_COMM_WORLD, 1);
    }

    if(!config_read_file(&cfg, argv[1])) {
      fprintf(stderr, "IEL ERROR: Cannot read config file [%s[\n", argv[1]);
      MPI_Abort(MPI_COMM_WORLD, 1);
    }

    if((setting = config_lookup(&cfg, "modules")) == NULL) {
      fprintf(stderr, "IEL ERROR: No modules entry in config file [%s]\n", argv[1]);
      MPI_Abort(MPI_COMM_WORLD, 1);
    }

    num_modules = config_setting_length(setting);

    MPI_Bcast(&num_modules, 1, MPI_INT, 0, MPI_COMM_WORLD);
  } else {

    MPI_Bcast(&num_modules, 1, MPI_INT, 0, MPI_COMM_WORLD);
  }

  // For all modules in the configuration file, add them to
  // the IEL as serial modules. Any actual serial modules
  // will be run under the name "MODULE-n" where n is the
  // number of the module in the configuration file.
  for(i = 0; i < num_modules; i++) {
    snprintf(mod_name, MOD_STRING_LENGTH, "MODULE-%d", i);
    // Modexec runs the executable specified by its provided argument
    IELAddModule(&modexec, mod_name);
  }

  // Run the executive
  MPI_Barrier(MPI_COMM_WORLD);
  rc = IELExecutive(MPI_COMM_WORLD,argv[1]);

  // ----------------- Timer ---------------------
  timestamp ("End", "driver.c", -1);
  timer_finalize (rank);
  // ----------------- Timer ---------------------

  MPI_Finalize();

  return rc;
}
