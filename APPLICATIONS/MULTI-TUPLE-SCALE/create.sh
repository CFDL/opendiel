#!/bin/bash

for (( i=7; i <=24; i+=2))
do 
  next=$((i+1))
  cp -r hello5/ hello$i
  cp -r hello6/ hello$next
  cd hello$i
  mv *.c hello$i.c
  mv *.h hello$i.h
  # Change the tags/mod names in the files
  sed -i -e "s/hello5/hello$i/g" hello$i.h
  sed -i -e "s/hello5/hello$i/g" hello$i.c
  sed -i -e "s/Hello5/Hello$i/g" hello$i.c
  sed -i -e "s/hello6/hello$next/g" hello$i.h
  sed -i -e "s/hello6/hello$next/g" hello$i.c
  sed -i -e "s/5/$i/g" Makefile
  cp *.h ../inc/

  cd ../hello$next
  mv *.c hello$next.c
  mv *.h hello$next.h
  sed -i -e "s/hello6/hello$next/g" hello$next.h
  sed -i -e "s/hello6/hello$next/g" hello$next.c
  sed -i -e "s/Hello6/Hello$next/g" hello$next.c
  sed -i -e "s/hello5/hello$i/g" hello$next.h
  sed -i -e "s/hello5/hello$i/g" hello$next.c
  sed -i -e "s/6/$next/g" Makefile
  cp *.h ../inc/

  cd ../

done
