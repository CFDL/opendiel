#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "hello1.h"

int hello1 (IEL_exec_info_t *exec_info)
{
  int i, rv;
  size_t sz;
  int *my_ints;
  double *my_doubles;
  double start, end;

  printf("Hello from hello1!\n");

  /* shut the compiler up */
  my_doubles = NULL;

  /* Create an array of ints, numbered 0 through NUMVARS-1 */
  my_ints = (int *) malloc(sizeof(int) * NUMVARS);
  for (i = 0; i < NUMVARS; i++) {
    my_ints[i] = i;
  }

  /* Send that array of ints to hello2 */
  /* The following is an equivalent call to what is used in the code below: 
   * rv = IEL_static_tput(sizeof(int) * NUMVARS, "hello2", "ints", (void *) my_ints);
   */
  sz = get_size(INT, NUMVARS);
  /* Get the start time */
  start = MPI_Wtime();
  rv = IEL_static_tput(sz, "hello2", "ints", (void *) my_ints);
  end = MPI_Wtime();
  fprintf(stderr, "It took %0.4lf seconds to send my array of ints!\n", end - start);

  if (rv == -1) {
    fprintf(stderr, "tput failed in hello 1\n");
    exit(1);
  }

  /* Hello2 is supposed to send me an array of doubles! Malloc space for it and
   * receive it */
  my_doubles = (double *) malloc(sizeof(double) * NUMVARS);
  rv = IEL_static_tremove(&sz, "hello1", "doubles", (void **) &my_doubles);
  if (rv == -1) {
    fprintf(stderr, "tremove failed in hello 1\n");
    exit(1);
  }

  /* Print out the beginning and end of the array I got from hello2 */
  for (i = 0; i < 10 && i < NUMVARS; i++) {
    printf("Hello1: my_doubles[%d] = %0.4lf\n", i, my_doubles[i]);
  }
  printf("\t.\n\t.\n\t.\n");
  for (i = NUMVARS-11; i < NUMVARS; i++) {
    printf("Hello1: my_doubles[%d] = %0.4lf\n", i, my_doubles[i]);
  }

  printf("Goodbye from hello1!\n");

  return 0;
}
