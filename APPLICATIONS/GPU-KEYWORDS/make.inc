include $(IEL_HOME)/Makefile.inc

INCLUDES = -I../INCLUDE
LIBS =  -L../LIB
LIBS += -lsimple_grid -lgrid_engine -lsearch_method -lgrid_search_method -ltrainee -lmagmadnn_trainee 

INCLUDES += -I$(CUDA_DIR)/include
LIBS += -L$(CUDA_DIR)/lib64
LIBS += -lcudnn -lcudart

INCLUDES += -I$(MAGMA_DIR)/include
LIBS += -L$(MAGMA_DIR)/lib
LIBS += -lmagma

INCLUDES += -I$(MAGMADNN_DIR)/include
LIBS += -L$(MAGMADNN_DIR)/lib
LIBS += -lmagmadnn

LIBS += $(MACH_LIB) -lmodexec $(LIBCONFIG_LIB) 
INCLUDES += $(MACH_INC) $(LIBCONFIG_INC) 

INCLUDES += -I$(BLAS_DIR)
