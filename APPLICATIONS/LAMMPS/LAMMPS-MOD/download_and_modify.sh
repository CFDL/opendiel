#!/bin/sh

LAMMPS_SRC=lammps-*/src

# Download lammps and untar
if ! ls ./lammps-* 1> /dev/null 2>&1; then
	echo "Downloading lammps..."
	wget http://lammps.sandia.gov/tars/lammps-stable.tar.gz
	tar -xf lammps-stable.tar.gz
	rm lammps-stable.tar.gz
fi

echo "Modifying for openDIEL"
if [ ! -d ./Archive ]; then 
	mkdir ./Archive/
fi
if [ ! -f ./Archive/main.cpp ]; then 
	cp $LAMMPS_SRC/main.cpp Archive/
fi
cp main.cpp $LAMMPS_SRC/main.cpp
cp lammps_mod.h $LAMMPS_SRC
