#!/bin/sh

LAMMPS_SRC=lammps-*/src

# Download lammps and untar
if ! ls ./lammps-* 1> /dev/null 2>&1; then
	echo "Downloading lammps..."
	wget http://lammps.sandia.gov/tars/lammps-stable.tar.gz
	tar -xf lammps-stable.tar.gz
	rm lammps-stable.tar.gz
fi
