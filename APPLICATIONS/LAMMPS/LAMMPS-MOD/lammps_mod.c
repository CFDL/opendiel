#include "lammps_mod.h"
#include "IEL_exec_info.h"

int lammps_mod (IEL_exec_info_t *exec_info) 
{
	int argc = IEL_ARGC(exec_info);
	char **argv = IEL_ARGV(exec_info);
	void *lmp;
	
	lammps_open(argc, argv, exec_info->module_copy_comm, &lmp);
	lammps_file(lmp, exec_info->modules[exec_info->module_num].stdinFile);
	lammps_close(lmp);

	return 0;
}
