#include "IEL_exec_info.h"

#ifndef _MAINMOD_lammps_mod_H
#define _MAINMOD_lammps_mod_H

int lammps_mod_gpu (IEL_exec_info_t *exec_info);

#endif
