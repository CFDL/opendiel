function [xdim,ydim,BCL,tfinal,stimTimes,dur,ectopicCells,scarCells,tl,fileName] = get_parameters()
%Sets parameters for the specified scenario. Modify the scenario variable
%with the case number of the simulation you want to run.

scenario = 1;

switch scenario
%Normal Conduction
    case 1
        xdim = 50;
        ydim = 50;

        BCL = 75;
        tfinal = 2000;
        stimTimes = [0,50];
        dur = restitution(100) + 20;

        ectopicCells = 0; %enter 1 to simulate with ectopic cells
        scarCells = 0; %enter 1 for simulate with scarred cells

        tl = 'Normal Conduction';
        fileName = 'NormalConduction';

%Normal Conduction with Scar
    case 2
        xdim = 50;
        ydim = 50;

        BCL = 75;
        tfinal = 2000;
        stimTimes = [0,50];
        dur = restitution(100) + 20;

        ectopicCells = 0; %enter 1 to simulate with ectopic cells
        scarCells = 1; %enter 1 for simulate with scarred cells

        tl = 'Normal Conduction with Scar';
        fileName = 'NormalConductionScar';

%Spiral Wave with Scar
    case 3
        xdim = 50;
        ydim = 50;

        BCL = 75;
        tfinal = 2000;
        stimTimes = [0,50];
        dur = restitution(100) + 20;

        ectopicCells = 0; %enter 1 to simulate with ectopic cells
        scarCells = 1; %enter 1 for simulate with scarred cells

        tl = 'Spiral Wave with Scar';
        fileName = 'SpiralWaveScar';

%Alternans
    case 4
        xdim = 50;
        ydim = 50;

        BCL = 54;
        tfinal = 2000;
        stimTimes = [0];
        dur = restitution(100) + 30;

        ectopicCells = 0; %enter 1 to simulate with ectopic cells
        scarCells = 0; %enter 1 for simulate with scarred cells

        tl = 'Alternans';
        fileName = 'Alternans';

%Alternans (ectopic)
    case 5
        xdim = 50;
        ydim = 50;

        BCL = 53;
        tfinal = 2000;
        stimTimes = [0];
        dur = restitution(100) + 20;

        ectopicCells = 1; %enter 1 to simulate with ectopic cells
        scarCells = 0; %enter 1 for simulate with scarred cells

        tl = 'Alternans (ectopic)';
        fileName = 'AlternansEctopic';

%Normal (ectopic)
    case 6
        xdim = 50;
        ydim = 50;

        BCL = 75;
        tfinal = 2000;
        stimTimes = [0];
        dur = restitution(100) + 20;
       
        ectopicCells = 1; %enter 1 to simulate with ectopic cells
        scarCells = 0; %enter 1 for simulate with scarred cells

        tl = 'Normal Conduction (ectopic)';
        fileName = 'NormalEctopic';

%Wave Break
    case 7
        xdim = 50;
        ydim = 50;

        BCL = 53;
        tfinal = 2000;
        stimTimes = [0];
        dur = restitution(100) + 20;

        ectopicCells = 0; %enter 1 to simulate with ectopic cells
        scarCells = 0; %enter 1 for simulate with scarred cells

        tl = 'Wave Break';
        fileName = 'WaveBreak';     
        
%Normal Conduction Fiber
    case 8
        xdim = 200;
        ydim = 3;

        BCL = 75;
        tfinal = 2000;
        stimTimes = [0];
        dur = restitution(100) + 20;

        ectopicCells = 0; %enter 1 to simulate with ectopic cells
        scarCells = 0; %enter 1 for simulate with scarred cells

        tl = 'Normal Conduction Fiber';
        fileName = 'NormalConductionFiber';
        
%Normal Conduction with Scar Fiber
    case 9
        xdim = 100;
        ydim = 3;

        BCL = 75;
        tfinal = 2000;
        stimTimes = [0,50];
        dur = restitution(100) + 20;

        ectopicCells = 0; %enter 1 to simulate with ectopic cells
        scarCells = 1; %enter 1 for simulate with scarred cells

        tl = 'Normal Conduction with Scar Fiber';
        fileName = 'NormalConductionScarFiber';      
        
%Spiral Wave with Scar
    case 10
        xdim = 50;
        ydim = 3;

        BCL = 75;
        tfinal = 2000;
        stimTimes = [0,50];
        dur = restitution(100) + 20;

        ectopicCells = 0; %enter 1 to simulate with ectopic cells
        scarCells = 1; %enter 1 for simulate with scarred cells

        tl = 'Spiral Wave with Scar Fiber';
        fileName = 'SpiralWaveScarFiber';
     
%Alternans Fiber
    case 11
        xdim = 200;
        ydim = 3;

        BCL = 54;
        tfinal = 2000;
        stimTimes = [0];
        dur = restitution(100) + 30;

        ectopicCells = 0; %enter 1 to simulate with ectopic cells
        scarCells = 0; %enter 1 for simulate with scarred cells

        tl = 'Alternans Fiber';
        fileName = 'AlternansFiber';
        
    otherwise
        return;
        
end

end

