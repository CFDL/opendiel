function[] = normal_conduction()
%This function illustrates a normal wave propagation in the heart

%Initialization of variables
propagationtime = 1;
refractThreshold = 0.1;
excitationThreshold = 0.9;

[xdim,ydim,BCL,tfinal] = get_parameters();

t_step = 1;
stimTimes = [0, 50];
for i = 3:20
    stimTimes(i) = stimTimes(i-1) + BCL;
end

tstart = 0;    
tend = 0;

voltage = zeros(xdim,ydim,tfinal+1);

for x = 1:xdim
    for y = 1:ydim
        voltage(x,y,1) = 0;
        DI(x,y) = 100;
        APD(x,y) = restitution(100);
        duration(x,y) = restitution(100) + 20;
    end
end

%Simulation
for x = 1:3
        for y = 1:3
            stimFunction(x,y,tend);
        end
end

for numOfStim = 2:length(stimTimes)
    tstart = tend + t_step;
    tend = min(stimTimes(numOfStim), tfinal);
    for t = tstart:t_step:tend
        for x = 1:xdim
            for y = 1:ydim
                propagationFunction(x,y,t);
            end
        end
    end
    
    for x = 1:3
        for y = 1:3
            stimFunction(x,y,tend);
        end
    end
    
    

end

for i = 1:tfinal+1
    time(i) = i-1;
    v1(i) = voltage(1,1,i);
    vmid(i) = voltage(round(xdim/2),round(ydim/2),i);
    vend(i) = voltage(xdim,ydim,i);
end

%Graph
%figure();
scatter(time, v1, 'r', 'filled');
hold on;
scatter(time, vmid, 'g', 'filled');
hold on;
scatter(time, vend, 'b', 'filled');
xlabel(' Time (ms) ')
ylabel(' Voltage (V) ')
title(' Normal Conduction ')
saveas(gcf, 'normal_graph.png')

%{
%Animation
figure();
v = VideoWriter('NormalConduction.avi');
open(v);
cellular_automata(voltage, xdim, ydim, tfinal, v, 'Normal Conduction');
close(v);
%}


%ECG
for t = 1:tfinal+1
    potentialA = phi(-100,20,xdim,ydim,t,voltage);
    potentialB = phi(150,40,xdim,ydim,t,voltage);
    ecg(t) = potentialB - potentialA;
end
%figure();
plot(time, ecg);
xlabel(' Time (ms) ')
ylabel(' Voltage (V) ')
title(' ECG ')

%==== Stimulation & Propagation Functions ====
    function [] = stimFunction(x,y,t)
        if(voltage(x,y,t+1) <= refractThreshold)
            depolarization(x,y,t);
        end
    end

    function [] = propagationFunction(x,y,t)
        if(voltage(x,y,t+1-t_step) <= refractThreshold)
            b = check4excitation(x,y,t);
            if(b == 1)
                depolarization(x,y,t);
            else
                evolution(x,y,t);
            end
        else
            evolution(x,y,t);
        end
    end

    function [b] = check4excitation(x,y,t)
        val = 0;
        for i=x-1:x+1
            for j=y-1:y+1
                if(i == 0 || j == 0)
                    continue;
                end
                if(i > xdim || j > ydim)
                    continue
                end
                if(i == x && j == y)
                    continue
                end
                if(voltage(i,j,t+1-propagationtime) > excitationThreshold)
                    val = val + 1;
                end
            end
        end
        
        if(val >= 3)
            b = 1;
        else
            b = 0;
        end
    end

    function [] = depolarization(x,y,t)
        DI(x,y) = duration(x,y) - APD(x,y);
        APD(x,y) = restitution(DI(x,y));
        voltage(x,y,t+1) = 1;
        duration(x,y) = 0;
    end

    function [] = evolution(x,y,t)
        duration(x,y) = duration(x,y) + t_step;
        voltage(x,y,t+1) = initial_wave_form(APD(x,y), duration(x,y));
    end

end

