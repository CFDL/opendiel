function [y] = restitution(Dn)
%This function graphs the action potential duration (APD) as a function
%of the diastolic interval.
%Amax = maximum amplitude
%A0 = initial amplitude
%tau = arbitrary constant

Amax = 60;
A0 = 50;
tau = 20;

f = @(Dn) Amax - A0.*exp(-Dn./tau);
y = f(Dn);

end

