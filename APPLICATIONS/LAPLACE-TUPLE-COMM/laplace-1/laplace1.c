/******************************************************************/
/* Laplace Equation                                               */
/*  T is initially 0.0 everywhere except at the boundaries where  */
/*  T=100.                                                        */
/*                                                                */
/*          T=100                                                 */ 
/*         ________           |_____|_____|_____|_____|           */
/*        |        |          |     |     |     |     |           */
/*        |        |          |     |     |     |     |           */
/*  T=100 | T=0.0  | T=100    |  1  |  2  |  3  |  4  |           */
/*        |        |          |     |     |     |     |           */ 
/*        |________|          |_____|_____|_____|_____|           */
/*          T=100             |     |     |     |     |           */
/*                                                                */
/*                                                                */ 
/*  Use Central Differencing Method                               */
/*  Each process only has subgrid                                 */
/*  Each process works on a subgrid and then sends its boundaries */
/*    to nearest neighbors                                        */
/*  THANKS TO PSC FOR USE OF THIS PROGRAM                         */
/******************************************************************/

#define NC       10                     /* Number of Cols        */
#define NR       10                     /* Number of Rows        */
#define NITER    10                   /* Max num of Iterations */
#define MAX(x,y) ( ((x) > (y)) ? x : y )

#include <stdio.h>
#include <math.h>
#include <unistd.h>
#include "mpi.h"                        /* Required MPI library */
#include "IEL_exec_info.h"              /* DIEL executive info struct */
#include "tuple_comm.h"                 /* Include Tuple Server */
#include "laplace1.h"

void initialize(double t[NR+2][NC+2], int nr, int nc);
void set_bcs   (double t[NR+2][NC+2], int mype, int npes, int nr, int nc);

int laplace1(IEL_exec_info_t *exec_info) {

  int         npes;                      /* Number of processes */
  int         mype;                      /* My process number  */
  int         niter;                     /* iteration counter  */
  int         nc;                        /* number of columns */
  int         nr;                        /* number of rows */
  size_t      size;                      /* Size of the sent/received row */

//  nr = exec_info->num_shared_bc / (exec_info->IEL_num_ranks);
//  nc = exec_info->num_shared_bc / (exec_info->IEL_num_ranks);

  nr = NR; 
  nc = NC;

  fprintf(stderr, "nr: %d, nc: %d\n", nr, nc);

  /* t is the current array, told is a copy of the previous iteration of t */
  double       t[nr+2][nc+2], told[nr+2][nc+2]; 

  /* Delta t, used for laplace calculation */
  double       dt; 

  /* iteration variables */
  int         i, j, iter;

  /* Give initial guess of 0. */
  initialize(t, nr, nc);                 



//  user_init(exec_info, "laplace1", NITER);
//  user_init(exec_info, "laplace1");
//sleep(2);


//  fprintf(stderr, "From laplace1: %s, %s\n", info.current_module_tags[0], info.current_module_tags[1]);
  /* Set npes to the number of IEL ranks and mype to the current IEL rank */
  npes = exec_info->IEL_num_ranks;
  mype = exec_info->IEL_rank;

  /* Set the boundary values */
  set_bcs(t, mype, npes, nr, nc); 

  /* Copy the values into told */
  for( i=0; i<=nr+1; i++ )    
    for( j=0; j<=nc+1; j++ )
      told[i][j] = t[i][j];

/*----------------------------------------------------------*
 |       Do Computation on Sub-grid for Niter iterations    |
 *----------------------------------------------------------*/

  niter = NITER;

  for( iter=1; iter<=niter; iter++ ) {

    for( i=1; i<=nr; i++ ) {
      for( j=1; j<=nc; j++ ) {
        t[i][j] = 0.25 * ( told[i+1][j] + told[i-1][j] + told[i][j+1] + told[i][j-1] );
      }
      dt = 0.;
    }

    for(i = 1; i <= nr; i++) {      /* Copy for next iteration  */
      for(j = 1; j <= nc; j++) {
	     dt = MAX(abs(t[i][j]-told[i][j]), dt);
         told[i][j] = t[i][j];
      }
    }

   /* 
  IEL_tput_simplified(sizeof(double) * nc, "laplace1", "one\0", t[0]);
  IEL_tremove_simplified(&size, "laplace1", "zero\0", (void *) &t[0]);
  IEL_tremove_simplified(&size, "laplace1", "three\0", (void *) &t[nr-1]);
//  fprintf(stderr, "--------------------\n");
  IEL_tput_simplified(sizeof(double) * nc, "laplace1", "two\0", t[nr-1]);
  */

  IEL_tput(sizeof(double) * nc, 1, 0, t[0]);
  IEL_tget(&size, 0, 0, 1, (void *) &t[0]);
  IEL_tget(&size, 3, 0, 1, (void *) &t[nr-1]);
  IEL_tput(sizeof(double) * nc, 2, 0, t[nr-1]);

      printf("Laplace 1, Iteration %d, t[%d][%d] = %20.8f\n", iter, NR, NC / 2, t[NR][NC / 2]);
    /* Print out a desired iteration to see results */
    if(iter%100 == 0) {
//      printf("Laplace 1, Iteration %d, t[25][467] = %20.8f\n", iter, t[25][467]);
    } 
    if(iter%100 == 0) {
      printf("Laplace 1, Iteration %d, t[%d][%d] = %20.8f\n", iter, NR, NC / 2, t[NR][NC / 2]);
    } 
  }

  return IEL_SUCCESS;
}  
