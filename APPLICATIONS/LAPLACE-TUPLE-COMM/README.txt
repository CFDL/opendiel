To build the example, simply use "make" in this directory
(openDIEL/APPLICATION/LAPLACE-TUPLE-COMM), and to rebuild the entire
example, use "make cleanall" in this directory and then type "make" again.
This compiles all non-automatically loaded modules involved in the example,
and then compiles the driver, which is used to run the example in the
openDIEL.

To run the example, enter the DRIVER directory relative to this one, and
then you can utilize your machine's runscript if it exists, and if not,
you can simply run the driver under your machine's MPI environment with 4
processes.


*************** UPDATE BY ZT **************
-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
                                      API:
                                    *******
  IEL_tput (size_t size, int tag, int serverRank, void *data)
    This function places the array of data given to it on the specified server
    with the user-given tag. If only a single tuple server is used, the
    serverRank is 0. The tag can be any user-given integer that will be used to
    identify the data when IEL_tget() is called. The size is the size of the
    data to be passed to the tuple server. NOTE: The data needs to be a 
    contiguous array in memory. 


  IEL_tget (size_t *size, int tag, int serverRank unsigned char del, void **data)
    This function pulls the array of data corresponding to the user-given tag
    from the specified server. The size variable will be set by the function
    indicating to the user the size of the data that was read. This function
    will fill out an array, so simply declear (but do not in
-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

        Laplace-0
     --------------
    |              |
    |              |
     --------------   <- Boundary 0
           ^
           |
           v
        Laplace-1
     --------------   <- Boundary 1
    |              |
    |              |
     --------------   <- Boundary 2
           ^
           |
           v
        Laplace2-
     --------------   <- Boundary 3
    |              |
    |              |
     --------------  

From the picture above, Laplace-0 will use tput to share Boundary 0 then will
call tget to obtain Boundary 1 from Laplace-1. Once it receives the relevant
array, it will update its array with the new contents and print. The same will
happen for Laplace-1. It will also use tput/get to share these boundaries with
Laplce-0 and will additionally share it's Boundary 2 with Laplace-2 while
getting Laplace-2's boundary. Likewise, Laplace-2 will use tput/get to update
its boundary with the bottom boundary of Laplace-1. 
