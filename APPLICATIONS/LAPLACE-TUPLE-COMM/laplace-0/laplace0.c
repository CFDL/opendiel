/******************************************************************/
/* Laplace Equation                                               */
/*  T is initially 0.0 everywhere except at the boundaries where  */
/*  T=100.                                                        */
/*                                                                */
/*          T=100                                                 */ 
/*         ________           |_____|_____|_____|_____|           */
/*        |        |          |     |     |     |     |           */
/*        |        |          |     |     |     |     |           */
/*  T=100 | T=0.0  | T=100    |  1  |  2  |  3  |  4  |           */
/*        |        |          |     |     |     |     |           */ 
/*        |________|          |_____|_____|_____|_____|           */
/*          T=100             |     |     |     |     |           */
/*                                                                */
/*                                                                */ 
/*  Use Central Differencing Method                               */
/*  Each process only has subgrid                                 */
/*  Each process works on a subgrid and then sends its boundaries */
/*    to nearest neighbors                                        */
/*  THANKS TO PSC FOR USE OF THIS PROGRAM                         */
/******************************************************************/

#define NC       10                     /* Number of Cols        */
#define NR       10                     /* Number of Rows        */
#define NITER    10                   /* Max num of Iterations */
#define MAX(x,y) ( ((x) > (y)) ? x : y )

#include <stdio.h>
#include <math.h>
#include "mpi.h"                        /* Required MPI library */
#include "IEL_exec_info.h"              /* DIEL executive info struct */
#include "tuple_comm.h"                 /* Include Tuple Server */
#include "laplace0.h"

void initialize(double t[NR+2][NC+2], int nr, int nc);
void set_bcs   (double t[NR+2][NC+2], int mype, int npes, int nr, int nc);

int laplace0(IEL_exec_info_t *exec_info) {

  int         npes;                      /* Number of processes */
  int         mype;                      /* My process number  */
  int         niter;                     /* iteration counter  */
  int         nc;                        /* number of columns */
  int         nr;                        /* number of rows */
  size_t      size;                      /* size of the sent/received row */

  nr = NR;
  nc = NC;
  printf("nr: %d, nc: %d\n", nr, nc);

  /* t is the current array, told is a copy of the previous iteration of t */
  double       t[nr+2][nc+2], told[nr+2][nc+2]; 

  /* Delta t, used for laplace calculation */
  double       dt; 

  /* iteration variables */
  int         i, j, iter;

  /* Give initial guess of 0. */
  initialize(t, NR, NC);                 

//  user_init(exec_info, "laplace0");

  /* Set npes to the number of IEL ranks and mype to the current IEL rank */
  npes = exec_info->IEL_num_ranks;
  mype = exec_info->IEL_rank;

  /* Set the boundary values */
  set_bcs(t, mype, npes, nr, nc); 

  /* Copy the values into told */
  for( i=0; i<=nr+1; i++ )    
    for( j=0; j<=nc+1; j++ )
      told[i][j] = t[i][j];

/*----------------------------------------------------------*
 |       Do Computation on Sub-grid for Niter iterations    |
 *----------------------------------------------------------*/

  niter = NITER;

  for( iter=1; iter<=niter; iter++ ) {

    for( i=1; i<=nr; i++ ) {
      for( j=1; j<=nc; j++ ) {
        t[i][j] = 0.25 * ( told[i+1][j] + told[i-1][j] + told[i][j+1] + told[i][j-1] );
      }
      dt = 0.;
    }

    for(i = 1; i <= nr; i++) {      /* Copy for next iteration  */
      for(j = 1; j <= nc; j++) {
	     dt = MAX(abs(t[i][j]-told[i][j]), dt);
         told[i][j] = t[i][j];
      }
    }
  
    
//  IEL_tput_simplified(80, "laplace0", "zero\0", t[nr-1]);
//  IEL_tremove_simplified(&size, "laplace0", "one\0", (void *) &t[nr]);

    /* This function sends the shared_bc 'write' values to the
     * "laplace1" shared_bc 'read' values and 'reads' the shared_bc
     * 'write' values from the "laplace1" module.
     */
    // ZT -- Replace this direct communication with tuple space comm.
//  IEL_tput(size_t size, int tag, int serverRank, void *data)
  IEL_tput(sizeof(double) * nc, 0, 0, t[nr-1]);
  IEL_tget(&size, 1, 0, 1, (void *) &t[nr]); 

    /* Print out a desired iteration to see results */
      printf("Laplace 0, Iteration %d, t[%d][%d] = %20.8f\n", iter, 1, 1, t[1][1]);
    if(iter%100 == 0) {
      printf("Laplace 0, Iteration %d, t[241][10] = %20.8f\n", iter, t[241][10]);
    } 
  }

  return IEL_SUCCESS;
}  

/********************************************************************
 *								    *
 * Initialize all the values to 0. as a starting value              *
 *								    *
 ********************************************************************/

void initialize( double t[NR+2][NC+2], int nr, int nc ){

  int        i, j;
	  
  for( i=0; i<=nr+1; i++ )        /* Initialize */
    for ( j=0; j<=nc+1; j++ )
      t[i][j] = 0.0;
}

/********************************************************************
 *								    *
 * Set the values at the boundary.  Values at the boundary do not   *
 * Change through out the execution of the program		    *
 *								    *
 ********************************************************************/

void set_bcs(double t[NR+2][NC+2], int mype, int npes, int nr, int nc){

  int i, j;

  for(i=0; i <= nr + 1; i++) {      /* Set Left and Right boundary */
    t[i][0]       = 100.0;
    t[i][nc+1] = 100.0;
  }

  if(mype == 1) {                    /* Top boundary */
    for(j=0; j <= nc+1; j++) {
      t[0][j] = 100.0;
    }
  }

  if(mype == npes-1) {              /* Bottom boundary */
    for(j=0; j <= nc + 1; j++) {
      t[nr+1][j] = 100.0;
    }
  }
}
