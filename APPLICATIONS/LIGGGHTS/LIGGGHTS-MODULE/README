DESCRIPTION
===========
This directory contains everything needed to build a LIGGGHTS library that can
be linked with an openDIEL driver. 

BUILDING
========
Run 'make', and the library will be downloaded, modified, build, and placed in 
LIGGGHTS-MODULE/MODULE-FILE. 

The build process is as follows when running 'make': 
1) LIGGGHTS-PUBLIC is downloaded
2) In LIGGGHTS-PUBLIC/src, several files are replaced with modified versions 
   found in the LIGGGHTS-MODIFIED-CODE subdirectory
3) The static library is build
4) The library is then copied to LIGGGHTS-MODULE/MODULE-FILE

MODIFICATIONS PERFORMED
=======================
On certain VTK dump styles, it appears that the VTK library is not being 
initialized properly by LIGGGHTS to run with a communicator other than 
MPI_COMM_WORLD. Files that have the improper initialization have been modified 
to work when passing a communicator other than MPI_COMM_WORLD for the 
initialization of lammps within LIGGGHTS.

All modified files (along with some header files) can be found in 
LIGGGHTS-MODIFIED-CODE, and will replace files found in the LIGGGHTS-PUBLIC/src 
when running 'make'

DIRECTORIES
===========
LIGGGHTS-MODIFIED-CODE - Contains modified code that will replace certain code
                         in LIGGGHTS-PUBLIC/src prior to compilation
LIGGGHTS-PUBLIC        - This will appear once 'make' is run. This is the 
                         repository LIGGGHTS
