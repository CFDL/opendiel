#include <stdlib.h>
#include <stdio.h>
#include "library.h"
#include "IEL_exec_info.h"
#include "liggghts_call.h"


int liggghts_mod (IEL_exec_info_t *exec_info) {
	int argc = IEL_ARGC(exec_info); 
	char **argv = IEL_ARGV(exec_info);
	void *ptr;

	lammps_open(argc, argv, exec_info->module_copy_comm, &ptr);
	lammps_file(ptr, exec_info->modules[exec_info->module_num].stdinFile);
	lammps_close(ptr);

	return 0;
}
