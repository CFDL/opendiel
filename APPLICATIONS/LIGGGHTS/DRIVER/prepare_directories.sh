#!/bin/sh
echo "####################################################################"
echo "# prepare_directories.sh: Creating directories for liggghts inputs #"
echo "####################################################################"
echo

mkdir liggghts_test_dir-0
mkdir liggghts_test_dir-0/post
mkdir liggghts_test_dir-0/meshes

mkdir liggghts_test_dir-1
mkdir liggghts_test_dir-1/post
mkdir liggghts_test_dir-1/meshes

echo "########################################################################"
echo "# prepare_directories.sh: Copying files into liggghts test directories #"
echo "########################################################################"
echo
cp ../LIGGGHTS-MODULE/LIGGGHTS-PUBLIC/examples/LIGGGHTS/Tutorials_public/chute_wear/in.chute_wear liggghts_test_dir-0
cp ../LIGGGHTS-MODULE/LIGGGHTS-PUBLIC/examples/LIGGGHTS/Tutorials_public/chute_wear/in.chute_wear liggghts_test_dir-1
cp ../LIGGGHTS-MODULE/LIGGGHTS-PUBLIC/examples/LIGGGHTS/Tutorials_public/chute_wear/meshes/* liggghts_test_dir-0/meshes
cp ../LIGGGHTS-MODULE/LIGGGHTS-PUBLIC/examples/LIGGGHTS/Tutorials_public/chute_wear/meshes/* liggghts_test_dir-1/meshes
