#!/bin/sh
echo "##################################################################################"
echo "# collect_output.sh: make directory 'results' to place results from liggghts run #"
echo "##################################################################################"
echo
mkdir results 
mkdir results/post_0
mkdir results/post_1

echo "#######################################################################"
echo "# collect_output.sh: Collecting output from liggghts test directories #"
echo "#######################################################################"
echo
cp liggghts_test_dir-0/post/* results/post_0
cp liggghts_test_dir-1/post/* results/post_1

echo "#########################################################"
echo "# collect_output.sh: Removing liggghts test directories #"
echo "#########################################################"
echo 
rm -rf liggghts_test_dir-0
rm -rf liggghts_test_dir-1
