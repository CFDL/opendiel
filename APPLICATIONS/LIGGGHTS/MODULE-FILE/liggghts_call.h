#include "IEL_exec_info.h"

#ifndef _MAINMOD_lammps_mod_H
#define _MAINMOD_lammps_mod_H

#ifdef __cplusplus
extern "C" {
#endif

int liggghts_mod (IEL_exec_info_t *exec_info);

#ifdef __cplusplus
}
#endif

#endif
