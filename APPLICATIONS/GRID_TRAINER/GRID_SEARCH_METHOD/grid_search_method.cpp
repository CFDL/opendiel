#include "grid_search_method.h"
#include <float.h>

using namespace std;

bool exists(const std::string &name) {
  struct stat buffer;
  return (stat(name.c_str(), &buffer) == 0);
}

grid_search_method::grid_search_method() {}

grid_search_method::~grid_search_method() {

  for (size_t i = 0; i < param_step_list.size(); i++)
    if (param_step_list[i] != NULL)
      delete param_step_list[i];

  for (size_t i = 0; i < param_sets.size(); i++)
    if (param_sets[i] != NULL)
      delete param_sets[i];
}

/*initial setup for the trainer*/
void grid_search_method::setup() {
  /* send the file information to the trainees*/
  for (size_t i = 0; i < n_trainees; i++) {
    send_file_data(i, file_data);
  }

  /*create a list of each step for each parameter */
  create_param_steps();

  /*create a list of all combinations of each parameter */
  create_param_sets();

  n_total_params = param_sets.size();
  n_params_done = 0;
  n_trainees_avail = n_trainees;

  if (exists(output_filename))
    new_file = false;
  else
    new_file = true;

  if (new_file) {

    save_header();
  } else {
    resume();
  }

  /*uncomment the following line to see the permutations of the indices */
  // print_param_sets();
}

/*The trainer_loop() function is required for the search method. It is called
 * once by the process with IEL module_rank 0.*/
void grid_search_method::trainer_loop() {
  size_t current_trainee = 0;
  double percent_done;

  /*read in the original layers from the base class*/
  vector<grid_layer> layers = get_original_layers();
  vector<grid_param> *current_param_set;
  vector<thread> threads;
  int current_param_index = 0;
  printf("I'm the trainer!\n");

  setup();

  printf("Training %li trainees with %i different parameters.\n", n_trainees,
         n_total_params);

  /*send parameters to trainees*/
  while (unfinished_param_sets.empty() == false) {

    /*get the next param set and remove it from the list */
    current_param_index = unfinished_param_sets.back();
    unfinished_param_sets.pop_back();

    /*send the hyperparameters to the trainee */
    send_hyperparameters(current_trainee, param_sets.at(current_param_index),
                         &layers);

    /*create a thread for each trainee to wait for the metrics */
    thread t(&grid_search_method::wait_for_metrics, this, current_trainee,
             current_param_index);
    threads.push_back(move(t));

    /*increment the trainee */
    current_trainee++;
    n_params_done++;

    /* once all of the trainees have been sent parameters
     *save the results to a file */
    if (current_trainee >= n_trainees_avail) {
      for (auto it = threads.begin(); it != threads.end(); it++) {
        it->join();
      }

      threads.clear();

      percent_done = ((double)n_params_done / n_total_params) * 100.0;
      printf("progress: %.2lf%%\n", percent_done);

      /*free any extra trainees*/
      free_unused_trainees();

      /*start at the beginning*/
      current_trainee = 0;
    }
  }

  /* send the remaining trainee's the done signal */
  for (size_t i = 0; i < n_trainees_avail; i++) {
    send_hyperparameters(i, NULL, NULL);
  }

  printf("Trainer finished!\n");
}

void grid_search_method::wait_for_metrics(int trainee, int param_index) {
  trainee_metric *m = new trainee_metric;
  recv_mutex.lock();
  /*wait for the metrics */
  recv_metrics(trainee, m);
  recv_mutex.unlock();

  save_mutex.lock();
  /*save the results */
  save_metric(m, param_index);
  save_mutex.unlock();
  delete m;
}

/*The trainee_loop() function is required for search methods. It is called by
 * every process of IEL module_rank 1 or greater. Trainees can be sent data by
 * using any send function in the trainer loop using a number form 0 to
 * n_trainees as the id */
void grid_search_method::trainee_loop() {
  printf("I'm a trainee!\n");
  // return;
  /*receive the file data */
  t->recv_file_data();

  /*read the training data*/
  if (t->read_file_data() != 0) {
    fprintf(stderr, "ERROR: reading file\n");
  }

  /* get the first hyperparamters */
  int finished = t->recv_hyperparameters();

  while (finished == 0) {
    t->train();
    t->send_metrics();

    /*get the next hyperparamters if available*/
    finished = t->recv_hyperparameters();
  }

  printf("Trainee %i finished!\n", t->get_id().id);
}

/*sends unused trainees the done signal */
void grid_search_method::free_unused_trainees() {
  if (n_trainees_avail <= unfinished_param_sets.size())
    return;

  size_t unused = n_trainees_avail - unfinished_param_sets.size();

  if (unused > 1)
    printf("%li trainees are unused\n", unused);
  else
    printf("%li trainee is unused\n", unused);

  /* send the trainee's the done signal */
  for (size_t i = unfinished_param_sets.size(); i < n_trainees_avail; i++) {
    send_hyperparameters(i, NULL, NULL);
  }

  n_trainees_avail -= unused;
}

/*returns true if a and b are within 0.0001 of eachother*/
bool floatcmp(float a, float b) {
  float x = a - b;
  if (x < 0)
    x = -x;
  if (x < 0.0001)
    return true;
  return false;
}

/*create a list of each step for each parameter */
void grid_search_method::create_param_steps() {
  /*get the parameters and layers that were read in from the file*/
  vector<grid_param> params = get_original_params();

  vector<grid_param>::iterator p_iter;

  for (p_iter = params.begin(); p_iter != params.end(); p_iter++) {
    vector<grid_param> *param_steps = new vector<grid_param>;
    for (float v = p_iter->start; v <= p_iter->end; v += p_iter->step_size) {
      grid_param param_step = *p_iter;
      param_step.cur_val = v;
      param_steps->push_back(param_step);

      if (!floatcmp(v, p_iter->end)) {
        if (v + p_iter->step_size > p_iter->end) {
          param_step.cur_val = p_iter->end;
          param_steps->push_back(param_step);
        }
      }
    }

    /*uncomment the following line to see the parameters which are generated */
    // grid_engine::print_params(param_steps);

    param_step_list.push_back(param_steps);
  }
}

/*print the current param sets*/
void grid_search_method::print_param_sets() {
  if (param_sets.size() == 0)
    printf("param set list is empty\n");

  for (size_t i = 0; i < param_sets.size(); i++) {
    printf("size: %li\n indicies:", param_sets.at(i)->size());
    grid_engine::print_params(param_sets.at(i));
    printf("\n");
  }
}

/*create a list of all combinations of each parameter */
void grid_search_method::create_param_sets() {
  if (param_sets.empty() == true) {
    vector<grid_param> *list = new vector<grid_param>;
    param_sets.push_back(list);
  }

  /*we have filled a param set with a combination, so add it to the list */
  if (param_sets.back()->size() == param_step_list.size()) {
    vector<grid_param> *list = new vector<grid_param>;
    *list = *param_sets.back();
    unfinished_param_sets.push_back(param_sets.size() - 1);
    param_sets.push_back(list);
    return;
  }

  int index = param_sets.back()->size();

  for (size_t i = 0; i < param_step_list.at(index)->size(); i++) {
    param_sets.back()->push_back(param_step_list.at(index)->at(i));
    create_param_sets();
    param_sets.back()->pop_back();
  }

  /* there is an extra one we need to remove in the end? */
  if (param_sets.back()->size() == 0)
    param_sets.pop_back();
}

void grid_search_method::resume() {

  ifstream input_file(output_filename, ios::in);

  vector<string> param_types = {"EPOCHS", "BATCH_SIZE", "LEARNING_RATE",
                                "WEIGHT_DECAY"};
  map<int, param_t> param_strucure;
  vector<grid_param> read_in_params;
  char buffer[256];
  string p;
  string line;
  int n_line = 0;
  int word = 0;
  int n_results_loaded = 0;

  while (getline(input_file, line)) {
    stringstream check1(line);
    if (n_line == 0) {
      word = 0;
      while (getline(check1, p, ' ')) {
        for (size_t i = 0; i < param_types.size(); i++) {
          if (param_types[i] == p) {
            param_strucure.insert(make_pair(word, (param_t)i));
          }
        }
        word++;
      }
    } else {
      word = 0;
      while (getline(check1, p, ' ')) {
        auto it = param_strucure.find(word);
        if (it != param_strucure.end()) {
          grid_param g;
          g.type = it->second;
          g.cur_val = stof(p);
          read_in_params.push_back(g);
        }
        word++;
      }

      for (auto it = unfinished_param_sets.begin();
           it != unfinished_param_sets.end(); it++) {
        bool equal = true;
        int matched_types = 0;
        for (auto iit = param_sets[*it]->begin(); iit != param_sets[*it]->end();
             iit++) {
          auto cmpit = read_in_params.begin();
          while (equal && cmpit != read_in_params.end()) {
            if (iit->type == cmpit->type)
              if (!floatcmp(iit->cur_val, cmpit->cur_val))
                equal = false;
              else
                matched_types++;
            cmpit++;
          }

          if (equal == false) {
            break;
          }
        }
        if (matched_types == param_sets[*it]->size()) {
          if (equal == true) {
            n_results_loaded++;
            unfinished_param_sets.erase(it);
            n_params_done++;
            break;
          }
        }
      }

      read_in_params.clear();
    }
    n_line++;
  }

  if (param_strucure.size() == 0)
    new_file = true;
  else
    new_file = false;

  cout << n_results_loaded << " results loaded from " << output_filename.c_str()
       << ", " << unfinished_param_sets.size() << " remain" << endl;

  input_file.close();
}

void grid_search_method::save_header() {
  ofstream output_file(output_filename, ios::out);
  /*get the parameters and layers that were read in from the file*/
  vector<grid_param> params = get_original_params();

  output_file << "ACCURACY";
  output_file << " LOSS";
  output_file << " TRAINING_TIME";
  for (size_t i = 0; i < params.size(); i++) {
    switch (params.at(i).type) {
    case param_t::EPOCHS:
      output_file << " EPOCHS";
      break;
    case param_t::BATCH_SIZE:
      output_file << " BATCH_SIZE";
      break;
    case param_t::LEARNING_RATE:
      output_file << " LEARNING_RATE";
      break;
    case param_t::WEIGHT_DECAY:
      output_file << " WEIGHT_DECAY";
      break;
    default:
      output_file << " INVALID";
      break;
    }
  }
  output_file << "\n";

  if (output_file.bad())
    cout << "Error Writing to file\n";

  output_file.close();
}

void grid_search_method::save_metric(trainee_metric *m, int param_index) {

  ofstream output_file(output_filename, ios::app);
  output_file << m->accuracy;
  output_file << " " << m->loss;
  output_file << " " << m->training_time;
  for (size_t j = 0; j < param_sets.at(param_index)->size(); j++) {
    output_file << " " << param_sets.at(param_index)->at(j).cur_val;
  }
  output_file << "\n";

  output_file.close();
}

void grid_search_method::print_grid() {
  vector<grid_param> *parameters;
  vector<int> *current_param_set;

  for (size_t i = 0; i < param_sets.size(); i++) {
    printf("%lf", 0.0f);
    printf(" %lf", 0.0f);
    printf(" %lf", 0.0f);
    for (size_t j = 0; j < param_sets.at(i)->size(); j++) {
      printf(" %lf", param_sets.at(i)->at(j).cur_val);
    }
    printf("\n");
  }
}
