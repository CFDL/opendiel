#pragma once

#include "IEL_exec_info.h"
#include "grid_engine.h"
#include "grid_search_method.h"
#include "magmadnn_trainee.h"

int simple_grid(IEL_exec_info_t *exec_info);
