#pragma once
#include <string>

enum class layer_t { INPUT, FULLYCONNECTED, ACTIVATION, OUTPUT };
enum class function_t { SIGMOID, TANH, RELU, SOFTMAX };

typedef struct {
  layer_t type;
  function_t func_type;
  int n_hidden_units;
} grid_layer;