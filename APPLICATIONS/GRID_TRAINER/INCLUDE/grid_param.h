#pragma once
#include <string>

enum class param_t { EPOCHS, BATCH_SIZE, LEARNING_RATE, WEIGHT_DECAY };
enum class value_t { CONTINUOUS, DISCRETE };

typedef struct {
  param_t type;
  value_t value_type;
  float start;
  float end;
  float step_size;
  float cur_val;
} grid_param;