#pragma once

#include "search_method.h"
#include <bits/stdc++.h>
#include <fstream>
#include <iostream>
#include <map>
#include <mutex>
#include <sys/stat.h>
#include <thread>

class grid_search_method : public search_method
{
private:
  std::vector< std::vector< grid_param > * > param_step_list;
  std::vector< std::vector< grid_param > * > param_sets;
  std::vector< size_t > unfinished_param_sets;
  // FILE *output_file;
  std::mutex save_mutex;
  std::mutex recv_mutex;
  bool new_file;

  int n_total_params;
  int n_params_done;
  size_t n_trainees_avail;

  void create_param_steps();
  void print_param_sets();
  void create_param_sets();
  void free_unused_trainees();
  void setup();
  void resume();
  void save_metric(trainee_metric *m, int param_index);
  void save_header();
  void print_grid();
  void wait_for_metrics(int trainee, int param_index);

public:
  grid_search_method();
  ~grid_search_method();
  void trainer_loop();
  void trainee_loop();
};