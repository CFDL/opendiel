#pragma once

#include "grid_engine.h"

class grid_engine;

class trainee
{
private:
  friend class grid_engine;
  grid_id *id;
  int init(IEL_exec_info_t *exec_info);

protected:
  std::vector< grid_param > *params;
  std::vector< grid_layer > *layers;
  trainee_metric *metrics;
  grid_file_data *file_data;

public:
  trainee();
  ~trainee();
  grid_id get_id();
  trainee_metric get_metrics();
  void send_metrics();
  int recv_hyperparameters();
  int recv_file_data();
  void print_layers();
  void print_params();
  void print_metrics();
  void print_id();
  void print_file_data();
  virtual void train() = 0;
  virtual int read_file_data() = 0;
};
