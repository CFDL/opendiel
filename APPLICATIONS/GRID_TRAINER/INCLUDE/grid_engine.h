#pragma once

#include "IEL.h"
#include "IEL_exec_info.h"
#include "grid_file_data.h"
#include "grid_id.h"
#include "grid_layer.h"
#include "grid_param.h"
#include "libconfig.h"
#include "magmadnn.h"
#include "modexec.h"
#include "mpi.h"
#include "search_method.h"
#include "trainee.h"
#include "tuple_comm.h"
#include "tuple_server.h"
#include <cstdint>
#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <libconfig.h++>
#include <math.h>
#include <stdint.h>
#include <unistd.h>
#include <vector>

class trainee;
class search_method;

class grid_engine {
private:
  IEL_exec_info_t *exec_info;
  std::vector<grid_param> *_params;
  std::vector<grid_layer> *_layers;
  std::string results_filename;
  unsigned int cur_param_incrementing = 0;
  int n_trainers;
  char *params_file;
  char *results_file;
  trainee *_trainee;
  search_method *_search_method;
  grid_file_data *data_info;
  std::vector<grid_id *> trainee_ids;
  std::vector<trainee_metric> trainee_metrics;

  friend class trainee;
  friend class search_method;
  void create_ids();
  void send_ids();

  /*communication functions*/
  static int recv_hyperparameters(grid_id *id, std::vector<grid_param> *params,
                                  std::vector<grid_layer> *layers);
  static int send_hyperparameters(grid_id *id, std::vector<grid_param> *params,
                                  std::vector<grid_layer> *layers);
  static int recv_metrics(grid_id *id, trainee_metric *m);
  static int send_metrics(grid_id *id, trainee_metric *m);
  static int recv_file_data(grid_id *id, grid_file_data *data);
  static int send_file_data(grid_id *id, grid_file_data *data);

protected:
public:
  ~grid_engine();
  grid_engine();
  void set_trainee(trainee *t);
  void set_search_method(search_method *s);
  void runUniformGrid();
  void set_results_filename(std::string filename);
  int read_cfg(const char *filename);
  int run(IEL_exec_info_t *exec_info);
  static void print_layers(std::vector<grid_layer> *layers);
  static void print_params(std::vector<grid_param> *params);
  static void print_metrics(trainee_metric *metric);
  static void print_id(grid_id *id);
  static void print_file_data(grid_file_data *data);
};
