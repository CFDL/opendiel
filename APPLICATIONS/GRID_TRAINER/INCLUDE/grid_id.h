#pragma once
/*The grid_id struct holds a unique id for each trainee and each trainee's data
 * type for tuple communication */
typedef struct {
  int id;
  int data_id;
  int training_data_id;
  int param_id;
  int layer_id;
  int metric_id;
} grid_id;