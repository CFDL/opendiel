#pragma once
#include <string>

#define MAX_FILENAME_LENGTH 200

typedef struct {
  char labels_filename[MAX_FILENAME_LENGTH];
  char features_filename[MAX_FILENAME_LENGTH];
  int n_output_classes;
  int n_samples;
  int n_features;
  int is_example;
} grid_file_data;

typedef struct {
  int finished;
  int n_params;
  int n_layers;
} grid_training_data;

typedef struct {
  double accuracy;      /**<accuracy the training accuracy of the model */
  double loss;          /**<loss the final loss from the models loss function */
  double training_time; /**<training_time the training duration in seconds */
} trainee_metric;