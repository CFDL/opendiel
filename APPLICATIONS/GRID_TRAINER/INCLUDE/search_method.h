#pragma once

#include "grid_engine.h"

class grid_engine;
class trainee;

class search_method
{
private:
  friend class grid_engine;
  void init(std::vector< grid_id * > *t_ids, std::vector< grid_param > *p,
            std::vector< grid_layer > *l, grid_file_data *f_d,
            std::string out_filename);
  std::vector< grid_id * > *_trainee_ids;
  std::vector< grid_param > *_params;
  std::vector< grid_layer > *_layers;

protected:
  trainee *t;
  size_t n_trainees;
  grid_file_data *file_data;
  std::string output_filename;

  std::vector< grid_param > get_original_params();
  std::vector< grid_layer > get_original_layers();

  int recv_hyperparameters(int rank, std::vector< grid_param > *params,
                           std::vector< grid_layer > *layers);
  int send_hyperparameters(int rank, std::vector< grid_param > *params,
                           std::vector< grid_layer > *layers);

  int recv_metrics(int rank, trainee_metric *m);

  int send_file_data(int rank, grid_file_data *data);

public:
  search_method();
  ~search_method();
  virtual void trainer_loop() = 0;
  virtual void trainee_loop() = 0;
};
