#pragma once
#include "IEL_exec_info.h"
#include "magmadnn.h"
#include "trainee.h"

class magmadnn_trainee : public trainee
{
public:
  magmadnn_trainee();
  ~magmadnn_trainee();
  void train();
  int read_file_data();

private:
  magmadnn::model::nn_params_t prms;
  magmadnn::Tensor< float > *data;
  magmadnn::Tensor< float > *labels;
  uint32_t n_features;

  /* these are used for reading in the MNIST data set -- found at
   * http://yann.lecun.com/exdb/mnist/ */
  magmadnn::Tensor< float > *read_mnist_images(const char *file_name,
                                               uint32_t &n_images,
                                               uint32_t &n_rows,
                                               uint32_t &n_cols);

  magmadnn::Tensor< float > *read_mnist_labels(const char *file_name,
                                               uint32_t &n_labels,
                                               uint32_t n_classes);
};
