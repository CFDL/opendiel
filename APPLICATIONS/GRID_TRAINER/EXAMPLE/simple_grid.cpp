#include "simple_grid.h"

int simple_grid(IEL_exec_info_t *exec_info)
{
  int rv;
  /*create the grid engine*/
  grid_engine *ge = new grid_engine();

  magmadnn_trainee *t = new magmadnn_trainee();
  grid_search_method *s = new grid_search_method();

  /*supply it with a trainee and search method*/
  ge->set_trainee(t);
  ge->set_search_method(s);

  /*run training */
  rv = ge->run(exec_info);

  delete ge;

  return rv;
}
