# This import registers the 3D projection, but is otherwise unused.
from mpl_toolkits.mplot3d import Axes3D  # noqa: F401 unused import
import csv
import matplotlib.pyplot as plt
import numpy as np
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib.ticker import FormatStrFormatter
import time
import re

# Fixing random state for reproducibility
np.random.seed(19680801)


def follow(thefile, plt):
    thefile.seek(0, 2)
    while True:
        line = thefile.readline()
        if not line:
            time.sleep(1)
            plt.pause(0.05)
            continue
        yield line


def adjust_values(v):

    value_range_convert = 1

    lowest = min(v)
    highest = max(v)
    if((highest - lowest) != 0):
        value_range_convert = 1.0 / (highest - lowest)

    for value in v:
        value = (value-lowest) * value_range_convert


fig = plt.figure()
ax = fig.add_subplot(111)

n = 100

points = ()
line_count = 0
x = ()
y = ()
z = ()
v = ()

with open('results.csv') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=' ')
    for row in csv_reader:
        if(line_count == 0):
            line_count += 1
            continue
        if float(row[4]) != 5:
            continue
        x += float(row[3]),
        y += float(row[4]),
        z += float(row[5]),
        v += float(row[0]),

        line_count += 1
    print(f'Processed {len(points)} lines.')


im = ax.scatter(x, z, c=v, cmap=plt.cm.Oranges, marker='o')

ax.set_xlabel('Learning Rate')
# ax.set_proj_type('persp')
ax.xaxis.set_major_formatter(FormatStrFormatter('%.2f'))
ax.set_ylabel('Epochs')
#ax.set_zlabel('Batch Size')
fig.colorbar(im, ax=ax)

im.set_clim(min(v), max(v))

print("Highest accuracy: ", max(v), " with Epochs: ")

plt.show()
# plt.pause(0.05)

# logfile = open("results.csv", "r")
# loglines = follow(logfile, plt)
# for line in loglines:
#     parts = line.split()
#     x += float(parts[3]),
#     y += float(parts[4]),
#     z += float(parts[5]),
#     v += float(parts[0]),
#     adjust_values(v)
#     im = ax.scatter(x, y, z, c=v, cmap=plt.cm.jet, s=1, marker='d')
#     im.set_clim(min(v), max(v))
