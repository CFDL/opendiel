#include "grid_engine.h"

using namespace std;

grid_engine::grid_engine()
{
  _params = new vector< grid_param >;
  _layers = new vector< grid_layer >;
  data_info = new grid_file_data;
}

grid_engine::~grid_engine()
{
  delete data_info;
  delete _params;
  delete _layers;

  if (_trainee != NULL)
    delete _trainee;

  if (_search_method != NULL)
    delete _search_method;

  for (size_t i = 0; i < trainee_ids.size(); i++) {
    if (trainee_ids[i] != NULL)
      delete trainee_ids[i];
  }
}

/*sets the trainee */
void grid_engine::set_trainee(trainee *t) { _trainee = t; }

/*sets the search method */
void grid_engine::set_search_method(search_method *s) { _search_method = s; }

/*initializes the trainees and search method */
int grid_engine::run(IEL_exec_info_t *exec_info)
{
  char *params_file;
  char *results_file;

  params_file = exec_info->modules[exec_info->module_num].mod_argv[0];
  results_file = exec_info->modules[exec_info->module_num].mod_argv[1];
  n_trainers = exec_info->module_sizes[exec_info->module_num] - 1;

  if (exec_info->module_rank == 0) {
    /*read in the layers and parameters */
    read_cfg(params_file);

    /*create the grid_ids */
    create_ids();

    /*send the grid_ids. after this all communication through the tuple space
     * should use the grid ids as data tags */
    send_ids();

    _search_method->init(&trainee_ids, _params, _layers, data_info,
                         results_file);

    _search_method->trainer_loop();

  } else if (exec_info->module_rank > 0) {

    _trainee->init(exec_info);

    _search_method->t = _trainee;

    _search_method->trainee_loop();
  }

  return IEL_SUCCESS;
}

/*create grid_ids for each trainee */
void grid_engine::create_ids()
{
  int id = n_trainers + 1; /* start the data ids at n_trainers +1
                            * to avoid collision with ranks*/

  /*create a unique id for each trainee's data type*/
  for (int i = 0; i < n_trainers; i++) {
    grid_id *g = new grid_id;
    g->id = i;
    g->training_data_id = id++;
    g->layer_id = id++;
    g->param_id = id++;
    g->metric_id = id++;
    trainee_ids.push_back(g);
  }
}

/* send the grid_ids to each trainee based off of their IEL module_rank.*/
void grid_engine::send_ids()
{
  for (int i = 0; i < n_trainers; i++) {
    IEL_tput(sizeof(grid_id), i, 0, (void *)trainee_ids[i]);
  }
}

/*
 * Uses a grid_id to look in the tuple space for grid_training_data, list of
 * parameters, and then a list of layers. The vectors will be cleared and
 * resized. Use after a send_hyperparamters to the same grid_id
 * @param[in] id -- the grid_id used to look in the tuple space
 * @param[out] params -- a vector of parameters
 * @param[out] layers -- a vector of layers
 */
int grid_engine::recv_hyperparameters(grid_id *id,
                                      std::vector< grid_param > *params,
                                      std::vector< grid_layer > *layers)
{
  int size;
  grid_training_data meta_data;
  params->clear();
  layers->clear();

  /* receive the meta data from the tuple space */
  IEL_tget_malloced(&size, id->training_data_id, 0, 1, (void **)&meta_data);

  if (meta_data.finished == 1) {
    return 1;
  }

  params->resize(meta_data.n_params);
  layers->resize(meta_data.n_layers);

  /* receive the params from the tuple space */
  IEL_tget_malloced(&size, id->param_id, 0, 1, (void *)params->data());

  /* receive the layers from the tuple space */
  IEL_tget_malloced(&size, id->layer_id, 0, 1, (void *)layers->data());

  return 0;
}

/*
 * Uses a grid_id to send hyperparameters to the tuple space.
 * Use before a recv_hyperparamters to the same grid_id
 * @param[in] id -- the grid_id used to look in the tuple space
 * @param[in] params -- a vector of parameters
 * @param[in] layers -- a vector of layers
 */
int grid_engine::send_hyperparameters(grid_id *id,
                                      std::vector< grid_param > *params,
                                      std::vector< grid_layer > *layers)
{
  grid_training_data meta_data;

  if (params == NULL || layers == NULL) {
    meta_data.finished = 1;
  } else {
    meta_data.n_layers = layers->size();
    meta_data.n_params = params->size();
    meta_data.finished = 0;
  }

  /* put the number of params and layers in the tuple space */
  IEL_tput(sizeof(grid_training_data), id->training_data_id, 0,
           (void *)&meta_data);

  if (meta_data.finished == 1)
    return 0;

  // /* put the parameters in the tuple space */
  IEL_tput(sizeof(grid_param) * params->size(), id->param_id, 0,
           (void *)params->data());

  // /* put the layers in the tuple space */
  IEL_tput(sizeof(grid_layer) * layers->size(), id->layer_id, 0,
           (void *)layers->data());

  return 0;
}

/*
 * Uses a grid_id to receive metrics from the tuple space.
 * Use after a send_metrics to the same grid_id
 * @param[in] id -- the grid_id used to look in the tuple space
 * @param[out] m -- trainee_metric
 */
int grid_engine::recv_metrics(grid_id *id, trainee_metric *m)
{
  int size;
  /* receive the metrics from the tuple space */
  IEL_tget_malloced(&size, id->metric_id, 0, 1, (void *)m);

  return 0;
}

/*
 * Uses a grid_id to send metrics to the tuple space.
 * Use before a recv_metrics to the same grid_id
 * @param[in] id -- the grid_id used to look in the tuple space
 * @param[in] m -- trainee_metric
 */
int grid_engine::send_metrics(grid_id *id, trainee_metric *m)
{
  /* put the metrics in the tuple space */
  IEL_tput(sizeof(trainee_metric), id->metric_id, 0, (void *)m);

  return 0;
}

/*
 * Uses a grid_id to receive file information from the tuple space.
 * Use after a send_file_data() to the same grid_id
 * @param[in] id -- the grid_id used to look in the tuple space
 * @param[out] data -- grid_file_data
 */
int grid_engine::recv_file_data(grid_id *id, grid_file_data *data)
{
  int size;
  /* receive the metrics from the tuple space */
  IEL_tget_malloced(&size, id->data_id, 0, 1, (void *)data);

  return 0;
}

/*
 * Uses a grid_id to send file information from the tuple space.
 * Use before a recv_file_data() to the same grid_id
 * @param[in] id -- the grid_id used to look in the tuple space
 * @param[in] data -- grid_file_data
 */
int grid_engine::send_file_data(grid_id *id, grid_file_data *data)
{
  /* put the metrics in the tuple space */
  IEL_tput(sizeof(grid_file_data), id->data_id, 0, (void *)data);

  return 0;
}

/*prints a vector of layers*/
void grid_engine::print_layers(std::vector< grid_layer > *layers)
{
  vector< grid_layer >::iterator it;
  int idex = 0;

  if (layers->size() == 0)
    printf("NO LAYERS\n");

  for (it = layers->begin(); it != layers->end(); it++) {
    printf("layer %i type: ", idex++);
    switch (it->type) {
      case layer_t::INPUT:
        printf("INPUT\n");
        break;
      case layer_t::FULLYCONNECTED:
        printf("FULLYCONNECTED\n");
        break;
      case layer_t::ACTIVATION:
        printf("ACTIVATION\n");
        printf("\tfunction: ");
        switch (it->func_type) {
          case function_t::SIGMOID:
            printf("SIGMOID\n");
            break;
          case function_t::TANH:
            printf("TANH\n");
            break;
          case function_t::RELU:
            printf("RELU\n");
            break;
          case function_t::SOFTMAX:
            printf("SOFTMAX\n");
            break;
          default:
            printf("INVALID\n");
            break;
        }
        break;
      case layer_t::OUTPUT:
        printf("OUTPUT\n");
        break;
      default:
        printf("INVALID\n");
        break;
    }
  }
}

/*prints a vector of params*/
void grid_engine::print_params(std::vector< grid_param > *params)
{
  vector< grid_param >::iterator it;
  int idex = 0;

  if (params->size() == 0)
    printf("NO PARAMS\n");

  for (it = params->begin(); it != params->end(); it++) {
    printf("param %i type: ", idex++);
    switch (it->type) {
      case param_t::EPOCHS:
        printf("EPOCHS\n");
        break;
      case param_t::BATCH_SIZE:
        printf("BATCH_SIZE\n");
        break;
      case param_t::LEARNING_RATE:
        printf("LEARNING_RATE\n");
        break;
      case param_t::WEIGHT_DECAY:
        printf("WEIGHT_DECAY\n");
        break;
      default:
        printf("INVALID\n");
        break;
    }
    printf("\tcur val: %f\n", it->cur_val);
  }
}

/*prints a trainee metric*/
void grid_engine::print_metrics(trainee_metric *metric)
{
  printf("Accuracy: %lf\n", metric->accuracy);
  printf("Loss: %lf\n", metric->loss);
  printf("Training Time: %lf\n", metric->training_time);
}

/*prints a grid_id */
void grid_engine::print_id(grid_id *id)
{
  printf("rank: %i\n", id->id);
  printf("training_data_id: %i\n", id->training_data_id);
  printf("param_id: %i\n", id->param_id);
  printf("layer_id: %i\n", id->layer_id);
  printf("metric_id: %i\n", id->metric_id);
}

/*prints a file data struct */
void grid_engine::print_file_data(grid_file_data *data)
{
  printf("labels filename: %s\n", data->labels_filename);
  printf("features filename: %s\n", data->features_filename);
  printf("output classes: %i\n", data->n_output_classes);
}

// read_cfg takes the name of the cfg file that you want to read, and returns
// a vector of the parameters stored in the file
int grid_engine::read_cfg(const char *filename)
{
  int i, success;
  std::string name;
  libconfig::Config cfg;
  string labels_file;
  string images_file;
  string param_type;
  string value_type;
  string layer_type;
  string function;
  string labels_filename;
  string features_filename;

  // Read the configuration file
  try {
    cfg.readFile(filename);
  } catch (const libconfig::FileIOException &fioex) {
    std::cerr << "I/O error while reading file: " << filename << std::endl;
    exit(EXIT_FAILURE);
  } catch (const libconfig::ParseException &pex) {
    std::cerr << "Parse error at " << pex.getFile() << ":" << pex.getLine()
              << " - " << pex.getError() << std::endl;
    exit(EXIT_FAILURE);
  }
  const libconfig::Setting &root = cfg.getRoot();

  // Load MNIST data if it has been specified
  try {
    cout << "Looking for data specification...";
    const libconfig::Setting &data_settings = root["data_info"];
    cout << "Found\n";
    data_settings.lookupValue("labels_data_file", labels_filename);
    data_settings.lookupValue("features_data_file", features_filename);
    data_settings.lookupValue("n_samples", data_info->n_samples);
    data_settings.lookupValue("n_features", data_info->n_features);
    data_settings.lookupValue("n_output_classes", data_info->n_output_classes);
    data_settings.lookupValue("is_example", data_info->is_example);
  } catch (libconfig::SettingNotFoundException &nf) {
    cerr << "Error: Data specification not Found\n";
  }

  // TODO: figure out a better way to avoid non contiguous data (memcpy?)
  for (size_t j = 0; j < labels_filename.length(); j++) {
    if (j == MAX_FILENAME_LENGTH) {
      printf("FILENAME TRUNCATED\n");
    }
    data_info->labels_filename[j] = labels_filename[j];
  }

  for (size_t j = 0; j < features_filename.length(); j++) {
    if (j == MAX_FILENAME_LENGTH) {
      printf("FILENAME TRUNCATED\n");
    }
    data_info->features_filename[j] = features_filename[j];
  }

  data_info->labels_filename[labels_filename.length()] = '\0';
  data_info->features_filename[features_filename.length()] = '\0';

  // Get parameters from config file
  try {
    const libconfig::Setting &params_list = root["parameters"];
    for (i = 0; i < params_list.getLength(); i++) {
      const libconfig::Setting &par = params_list[i];
      grid_param cur_params;
      par.lookupValue("name", param_type);
      if (param_type == "learning_rate")
        cur_params.type = param_t::LEARNING_RATE;
      else if (param_type == "weight_decay")
        cur_params.type = param_t::WEIGHT_DECAY;
      else if (param_type == "epochs")
        cur_params.type = param_t::EPOCHS;
      else if (param_type == "batch_size")
        cur_params.type = param_t::BATCH_SIZE;
      else
        fprintf(stderr, "Parameter Type: %s Unknown\n", param_type.c_str());

      par.lookupValue("type", value_type);
      if (value_type == "continuous")
        cur_params.value_type = value_t::CONTINUOUS;
      else if (value_type == "discrete")
        cur_params.value_type = value_t::DISCRETE;
      else
        fprintf(stderr, "Value Type: %s Unknown\n", value_type.c_str());

      par.lookupValue("start", cur_params.start);
      par.lookupValue("start", cur_params.cur_val);
      par.lookupValue("end", cur_params.end);
      par.lookupValue("step_size", cur_params.step_size);
      if (cur_params.start > cur_params.end) {
        cur_params.step_size *= -1;
      }
      _params->push_back(cur_params);
    }
  } catch (libconfig::SettingNotFoundException &nf) {
    cerr << "Error reading config file: No parameters specified\n";
    return 1;
  }

  // Get layers from config file
  try {
    const libconfig::Setting &cfg_layers = root["layers"];
    for (i = 0; i < cfg_layers.getLength(); i++) {
      const libconfig::Setting &lay = cfg_layers[i];
      grid_layer cur_layer;
      success = lay.lookupValue("type", layer_type);
      if (!success) {
        cerr << "error reading in layer, skipping\n";
        continue;
      }
      if (layer_type == "input")
        cur_layer.type = layer_t::INPUT;
      else if (layer_type == "fully connected")
        cur_layer.type = layer_t::FULLYCONNECTED;
      else if (layer_type == "activation") {
        cur_layer.type = layer_t::ACTIVATION;
        lay.lookupValue("function", function);
        if (function == "SIGMOID")
          cur_layer.func_type = function_t::SIGMOID;
        else if (function == "TANH")
          cur_layer.func_type = function_t::TANH;
        else if (function == "RELU")
          cur_layer.func_type = function_t::RELU;
        else if (function == "SOFTMAX")
          cur_layer.func_type = function_t::SOFTMAX;
        else {
          fprintf(stderr, "Function Type: %s Unknown\n", function.c_str());
        }
      } else if (layer_type == "output")
        cur_layer.type = layer_t::OUTPUT;
      else {
        fprintf(stderr, "Layer Type: %s Unknown\n", layer_type.c_str());
      }

      lay.lookupValue("n_hidden_units", cur_layer.n_hidden_units);
      _layers->push_back(cur_layer);
    }
  } catch (libconfig::SettingNotFoundException &nf) {
    cerr << "Error reading config file: No layers specified\n";
    return 1;
  }
  return 0;
}