#include "trainee.h"

using namespace std;

trainee::trainee()
{
  params = new vector< grid_param >;
  layers = new vector< grid_layer >;
  metrics = new trainee_metric;
  id = new grid_id;
  file_data = new grid_file_data;
}

trainee::~trainee()
{
  if (params != NULL)
    delete params;
  if (layers != NULL)
    delete layers;
  if (metrics != NULL)
    delete metrics;
  if (id != NULL)
    delete id;
  if (file_data != NULL)
    delete file_data;
}

/*Receive id information from grid engine*/
int trainee::init(IEL_exec_info_t *exec_info)
{
  int size;

  IEL_tget_malloced(&size, exec_info->module_rank - 1, 0, 1, (void **)id);

  return 0;
}

grid_id trainee::get_id() { return *id; }
trainee_metric trainee::get_metrics() { return *metrics; }

void trainee::send_metrics() { grid_engine::send_metrics(id, metrics); }

int trainee::recv_hyperparameters()
{
  return grid_engine::recv_hyperparameters(id, params, layers);
}

int trainee::recv_file_data()
{
  return grid_engine::recv_file_data(id, file_data);
}

void trainee::print_layers()
{
  printf("layers for id: %i\n", id->id);
  grid_engine::print_layers(layers);
}

void trainee::print_params()
{
  printf("params for id: %i\n", id->id);
  grid_engine::print_params(params);
}

void trainee::print_metrics()
{
  printf("metrics for id: %i\n", id->id);
  grid_engine::print_metrics(metrics);
}

void trainee::print_id() { grid_engine::print_id(id); }

void trainee::print_file_data()
{
  printf("file data for id: %i\n", id->id);
  grid_engine::print_file_data(file_data);
}