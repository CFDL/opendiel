#include "magmadnn_trainee.h"

using namespace magmadnn;
using namespace std;

magmadnn_trainee::magmadnn_trainee()
{
  magmadnn_init();
  data = NULL;
  labels = NULL;
}

magmadnn_trainee::~magmadnn_trainee()
{
  magmadnn_finalize();
  if (data != NULL)
    delete data;
  if (labels != NULL)
    delete labels;
}

/*The train function is required in order for the trainee to work. It creates a
 * network from the stored parameters that were set using recv_hyperparamters()
 * function. Do no call train in the trainee loop unless the parameters have been
 * set using send_hyperparamters() in the trainer loop followed by
 * recv_hyperparamters() in the trainee loop */
void magmadnn_trainee::train()
{
  vector< grid_param * > parameter_space;

  size_t i;
  vector< layer::Layer< float > * > mdnn_layers;
  layer::Layer< float > *new_layer;

  /*initialize the parameters from grid_param type to magmadnn nn_params_t*/
  for (i = 0; i < params->size(); i++) {
    switch (params->at(i).type) {
      case param_t::EPOCHS:
        prms.n_epochs = params->at(i).cur_val;
        break;
      case param_t::BATCH_SIZE:
        prms.batch_size = params->at(i).cur_val;
        break;
      case param_t::LEARNING_RATE:
        prms.learning_rate = params->at(i).cur_val;
        break;
      case param_t::WEIGHT_DECAY:
        fprintf(stderr, "WEIGHT_DECAY not implemented yet\n");
        break;
      default:
        break;
    }
  }

  /* INITIALIZING THE NETWORK */
  /* create a variable (of type float) with size  (batch_size x n_features)
      This will serve as the input to our network. */
  auto x_batch = op::var< float >("x_batch", {prms.batch_size, n_features},
                                  {NONE, {}}, DEVICE);

  /* create the layers from grid_layer type to magmadnn layers*/
  for (i = 0; i < layers->size(); i++) {
    switch (layers->at(i).type) {
      case layer_t::INPUT:
        new_layer = layer::input(x_batch);
        break;
      case layer_t::FULLYCONNECTED:
        new_layer = layer::fullyconnected(mdnn_layers.back()->out(),
                                          layers->at(i).n_hidden_units);
        break;
      case layer_t::ACTIVATION:
        new_layer =
            layer::activation(mdnn_layers.back()->out(),
                              (layer::activation_t)layers->at(i).func_type);
        break;
      case layer_t::OUTPUT:
        new_layer = layer::output(mdnn_layers.back()->out());
        break;
      default:
        break;
    }
    mdnn_layers.push_back(new_layer);
  }

  auto model = model::NeuralNetwork< float >(
      mdnn_layers, optimizer::CROSS_ENTROPY, optimizer::SGD, prms);

  /* metric_t records the model metrics such as accuracy, loss, and training
   * time */
  model::metric_t m;
  model.fit(data, labels, m, false);

  // save the metrics to the trainee class in order to be received by using
  // send_metrics() in the trainee loop followed by recv_metrics() in the trainer
  // loop
  metrics->accuracy = m.accuracy;
  metrics->loss = m.loss;
  metrics->training_time = m.training_time;

  /*cleanup magmadnn training data */
  // for (size_t i = 0; i < mdnn_layers.size(); i++) {
  //   if (mdnn_layers[i] != NULL)
  //     delete mdnn_layers[i];
  // }
  // delete mdnn_layers.back()->out();
}

/*The read_file_data() function is required by a trainee in order to read in a
 * file supplied by grid_file_data in the base class. Grid_file_data is set using
 * send_file_data() in the trainer loop followed by recv_file_data() in the
 * trainee loop */
int magmadnn_trainee::read_file_data()
{
  uint32_t n_images, n_rows, n_cols, n_labels, n_classes = 10;
  /*data is known to be mnist but a general read function can be used */
  data = read_mnist_images(file_data->labels_filename, n_images, n_rows, n_cols);
  labels = read_mnist_labels(file_data->features_filename, n_labels, n_classes);

  if (data == NULL || labels == NULL) {
    fprintf(stderr, "FILE READ ERROR\n");
    return 1;
  }

  n_features = n_rows * n_cols;

  return 0;
}

/*The following functions */
#define FREAD_CHECK(res, nmemb)       \
  if ((res) != (nmemb)) {             \
    fprintf(stderr, "fread fail.\n"); \
    return NULL;                      \
  }

inline void endian_swap(uint32_t &val)
{
  /* taken from
   * https://stackoverflow.com/q
   * uestions/13001183/how-to-read-little-endian-integers-from-file-in-c
   */
  val = (val >> 24) | ((val << 8) & 0xff0000) | ((val >> 8) & 0xff00) |
        (val << 24);
}

Tensor< float > *magmadnn_trainee::read_mnist_images(const char *file_name,
                                                     uint32_t &n_images,
                                                     uint32_t &n_rows,
                                                     uint32_t &n_cols)
{
  FILE *file;
  unsigned char magic[4];
  Tensor< float > *data;
  uint8_t val;

  file = std::fopen(file_name, "r");

  if (file == NULL) {
    std::fprintf(stderr, "Could not open %s for reading.\n", file_name);
    return NULL;
  }

  FREAD_CHECK(fread(magic, sizeof(char), 4, file), 4);
  if (magic[2] != 0x08 || magic[3] != 0x03) {
    std::fprintf(stderr, "Bad file magic.\n");
    return NULL;
  }

  FREAD_CHECK(fread(&n_images, sizeof(uint32_t), 1, file), 1);
  endian_swap(n_images);

  FREAD_CHECK(fread(&n_rows, sizeof(uint32_t), 1, file), 1);
  endian_swap(n_rows);

  FREAD_CHECK(fread(&n_cols, sizeof(uint32_t), 1, file), 1);
  endian_swap(n_cols);

  char bytes[n_rows * n_cols];

  /* allocate tensor */
  data = new Tensor< float >({n_images, n_rows, n_cols}, {NONE, {}}, HOST);

  for (uint32_t i = 0; i < n_images; i++) {
    FREAD_CHECK(fread(bytes, sizeof(char), n_rows * n_cols, file),
                n_rows * n_cols);

    for (uint32_t r = 0; r < n_rows; r++) {
      for (uint32_t c = 0; c < n_cols; c++) {
        val = bytes[r * n_cols + c];

        data->set(i * n_rows * n_cols + r * n_cols + c, (val / 128.0f) - 1.0f);
      }
    }
  }

  fclose(file);

  return data;
}

Tensor< float > *magmadnn_trainee::read_mnist_labels(const char *file_name,
                                                     uint32_t &n_labels,
                                                     uint32_t n_classes)
{
  FILE *file;
  unsigned char magic[4];
  Tensor< float > *labels;
  uint8_t val;

  file = std::fopen(file_name, "r");

  if (file == NULL) {
    std::fprintf(stderr, "Could not open %s for reading.\n", file_name);
    return NULL;
  }

  FREAD_CHECK(fread(magic, sizeof(char), 4, file), 4);

  if (magic[2] != 0x08 || magic[3] != 0x01) {
    std::fprintf(stderr, "Bad file magic.\n");
    return NULL;
  }

  FREAD_CHECK(fread(&n_labels, sizeof(uint32_t), 1, file), 1);
  endian_swap(n_labels);

  /* allocate tensor */
  labels = new Tensor< float >({n_labels, n_classes}, {ZERO, {}}, HOST);

  for (unsigned int i = 0; i < n_labels; i++) {
    FREAD_CHECK(fread(&val, sizeof(char), 1, file), 1);

    labels->set(i * n_classes + val, 1.0f);
  }

  fclose(file);

  return labels;
}