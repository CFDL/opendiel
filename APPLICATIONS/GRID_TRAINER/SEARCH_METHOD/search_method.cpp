#include "search_method.h"

using namespace std;

search_method::search_method() {}

search_method::~search_method() {}

/*Receive information from grid engine*/
void search_method::init(vector< grid_id * > *t_ids, vector< grid_param > *p,
                         vector< grid_layer > *l, grid_file_data *f_d,
                         string out_filename)
{
  _trainee_ids = t_ids;
  _params = p;
  _layers = l;
  file_data = f_d;
  n_trainees = _trainee_ids->size();
  output_filename = out_filename;
}

std::vector< grid_param > search_method::get_original_params()
{
  return *_params;
}
std::vector< grid_layer > search_method::get_original_layers()
{
  return *_layers;
}

int search_method::recv_hyperparameters(int id,
                                        std::vector< grid_param > *params,
                                        std::vector< grid_layer > *layers)
{
  return grid_engine::recv_hyperparameters((*_trainee_ids)[id], params, layers);
}

int search_method::send_hyperparameters(int id,
                                        std::vector< grid_param > *params,
                                        std::vector< grid_layer > *layers)
{
  return grid_engine::send_hyperparameters((*_trainee_ids)[id], params, layers);
}

int search_method::recv_metrics(int id, trainee_metric *m)
{
  return grid_engine::recv_metrics((*_trainee_ids)[id], m);
}

int search_method::send_file_data(int id, grid_file_data *data)
{
  return grid_engine::send_file_data((*_trainee_ids)[id], data);
}