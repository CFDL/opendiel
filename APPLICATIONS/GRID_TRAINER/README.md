# CONTENTS

1. <u>DIRECTORY STRUCTURE</u>- Listing of what's in this directory
1. <u>INTRODUCTION</u>- Introduction to this example
1. <u>BUILDING</u>- Instructions on how to build this example
1. <u>RUNNING</u>- How to run this example after building
1. <u>TODO</u>- List of work that still needs to be done

# 1. DIRECTORY STRUCTURE

| DIRECTORY NAME     | DESCRIPTION                                                    |
| ------------------ | -------------------------------------------------------------- |
| DRIVER             | This directory contains the main openDIEL driver.              |
| EXAMPLE            | An example function which is run by the driver                 |
| GRID_ENGINE        | The main class which contains DIEL functions for communication |
| GRID_SEARCH_METHOD | A search method which searches a grid space uniformly          |
| INCLUDE            | Header Directory                                               |
| MAGMADNN_TRAINEE   | A trainee that uses magmadnn                                   |
| SEARCH_METHOD      | The base search method class                                   |
| TEST_DATA          | MNIST Example data                                             |
| TRAINEE            | The base trainee class                                         |

# INTRODUCTION

The Example allows you to specify a parameter space and DNN model, and then
perform an exhaustive grid search over the parameter space in parallel. The
DNN is created with MagmaDNN.

In `simple_grid.cpp` a grid engine is created and supplied with a magmadnn trainee and a grid search method.

In order to add another search method create a class similar to the one found in `GRID_SEARCH_METHOD`.

In order to add another trainee type create a class similar to the one found in `MAGMADNN_TRAINEE`.

# BUILDING

1. You will need to add paths to your CUDA, MAGMA, and MagmaDNN installations
   in the make.inc file
2. Then, type 'make', and the application will be built.

# RUNNING

To run the example, you will need to run the following in DRIVER:

    mpirun -np 13 ./driver workflow.cfg

Or simply use one of the run scripts:

    ./small-run-example.sh
    ./run-example.sh

There are a few important files:
parameters.cfg - This is what the grid engine will read in to figure out what
architectures should look like, and what parameter space
should end up being searched.

# TODO

- Add the more trainees to using different DNN backends like Tensorflow.
- Add the ability to create more diverse parameter types
- Add a communication function for arbitrary data types(void\*)
