To build the example, simply use "make" in this directory
(openDIEL/APPLICATION/LAPLACE-TUPLE-COMM), and to rebuild the entire
example, use "make cleanall" in this directory and then type "make" again.
This compiles all non-automatically loaded modules involved in the example,
and then compiles the driver, which is used to run the example in the
openDIEL.

To run the example, enter the DRIVER directory relative to this one, and
then you can utilize your machine's runscript if it exists, and if not,
you can simply run the driver under your machine's MPI environment with 6
processes.

The MULTI-TUPLE example runs 6 processes via the settings described in the
DRIVER/workflow.cfg file; this file provides a working example of how to set up
an openDIEL configuration file for running other tests. 

In this example, hello1 creates an array of integers and sends that array to
hello2 using a tuple server. Simultaneously, hello2 creates an array of doubles
and sends that array to hello1. Once both functions receive their respective
arrays, they print out the first and last ten elements of the array. 

This example uses statically mapped tuple servers which is a paradigm that
assigns each process its own tuple server that it will receive from. To send an
array to another process, the user simply needs to call 'IEL_static_tput' with
the intended receiving module's name (see the API below for more details). 


The illustration below shows the flow of data in this example. M1,
corresponding to hello1, sends its data to the server mapped to M2,
corresponding to hello2, and likewise, M2 sends its data to the server mapped
to M1. Since each module is mapped to a unique server, only the module name and
tag needs to be given for reliable data transfer.

 ____              ____
|    |            |    |
| TS | *          | TS |
|____|   *      / |____|
  |        *  /      |
  |        / *       |
  V      /     *     V
 ____  /         * ____
|    |            |    |
| M1 |            | M2 |
|____|            |____|



-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
                                      API:
                                    *******
   IEL_static_tput(size_t size, char *recving_mod, char *str_tag, void *data)
    * A wrapper for IEL_tput; this function places the array of data on the
    * server that is statically mapped to the module denoted by the variable 
    * 'recving_mod'. This function also requires that the size of the data 
    * and a tag for the data be given. 

   IEL_static_tget(size_t *size, char *requesting_mod, char *str_tag, void **data)
    * A wrapper for IEL_tget; this function requests the array of data placed
    * on the server mapped to itself. In order to determine which server is
    * mapped to itself, the name of the module calling this function must be
    * provided to the 'requesting_mod' variable. This function also requires
    * the tag given to the data and a handle to an UNALLOCATED buffer to place
    * the data in, as well as a handle to a variable to return the size of the
    * buffer to the user. This function leaves the data on the server (copies).

   IEL_static_tremove(size_t *size, char *requesting_mod, char *str_tag, void **data)
    * A wrapper for IEL_tget; this function requests the array of data placed
    * on the server mapped to itself. In order to determine which server is
    * mapped to itself, the name of the module calling this function must be
    * provided to the 'requesting_mod' variable. This function also requires
    * the tag given to the data and a handle to an UNALLOCATED buffer to place
    * the data in, as well as a handle to a variable to return the size of the
    * buffer to the user. This function removes the data from the server (deletes) 

-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
