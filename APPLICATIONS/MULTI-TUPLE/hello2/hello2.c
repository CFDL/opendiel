#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "hello2.h"

int hello2 (IEL_exec_info_t *exec_info)
{
  int i, rv;
  size_t sz; 
  int *my_ints;
  double *my_doubles;
  double start, end;

  printf("Hello from hello2!\n");

  /* shut compiler up */
  my_ints = NULL;

  /* Create an array of doubles numbered 0 through NUMVARS-1 */
  my_doubles = (double *) malloc(sizeof(double) * NUMVARS);
  for (i = 0; i < NUMVARS; i++) {
    my_doubles[i] = (double) i;
  }

  start = MPI_Wtime();
  /* Send that array of doubles to hello1 */
  rv = IEL_static_tput(sizeof(double) * NUMVARS, "hello1", "doubles", (void *) my_doubles);
  end = MPI_Wtime();
  fprintf(stderr, "It took %0.4lf seconds to send my array of doubles!\n", end - start);
  if (rv == -1) {
    fprintf(stderr, "tput failed in hello2\n");
    exit(1);
  }

  /* Hello1 is supposed to send me an array of ints! Malloc space for it and
   * request the array */
  my_ints = (int *) malloc(sizeof(int) * NUMVARS);
  rv = IEL_static_tremove(&sz, "hello2", "ints", (void **) &my_ints);
  if (rv == -1) {
    fprintf(stderr, "tremove failed in hello2\n");
    exit(1);
  }

  /* Print out the array I got from hello1 */
  for (i = 0; i < 10 && i < NUMVARS; i++) {
    printf("my_ints[%d] = %d\n", i, my_ints[i]);
  }
  printf("\t.\n\t.\n\t.\n"); 
  for (i = NUMVARS-11; i < NUMVARS; i++) {
    printf("my_ints[%d] = %d\n", i, my_ints[i]);
  } 

  printf("Goodbye from hello2!\n");

  return 0;
}
