  /**********************************************************/
 /* Example provided by Cornell University *****************/
/**********************************************************/
#include <stdio.h>
#include "hello.h"
#include "omp.h"

int hello (IEL_exec_info_t *exec_info) {
  int rank, size, max_processor_len;
  char processor_name[MPI_MAX_PROCESSOR_NAME]; 
  printf("Hello from master thread.\n");

//  MPI_Init();

  MPI_Comm_rank(exec_info->module_copy_comm, &rank);
  MPI_Comm_size(exec_info->module_copy_comm, &size);

  printf("Hello from process of rank %d in communicator size %d\n", rank, size);

#pragma omp parallel
  {
     MPI_Get_processor_name(processor_name, &max_processor_len);
     printf("Team member %d reporting from team of %d threads on process %d from processor %s\n",
            omp_get_thread_num(),omp_get_num_threads(), exec_info->IEL_rank, processor_name );
  }

  printf("Master thread finished, goodbye.\n");
//  MPI_Finalize();

  return 0;
}
