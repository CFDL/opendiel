This module provides a simple example for using the openDIEL to run a
Fortran module.

To do so, the module must have a simple C wrapper; an example of such a
wrapper is provided in fort-module. The wrapper simply reduces the
executive info structure to only info needed by a fortran module (done by
an openDIEL library function that is provided), then passes the reduced
structure to the Fortran subroutine.

The Fortran code must bind the C structure to a Fortran derived data type,
and also, rather than the main program, must only contain a subroutine
called with the derived data type as its argument. This subroutine is called
by the C wrapper and passed the reduced executive info for the Fortran
module to utilize.

Additionally, any instances of MPI_COMM_WORLD within the Fortran program to
be used as a module must be changed to finfo%module_copy_comm to keep any
message passing within the specific copy of the module. Any instances of
the use of the argument list must also be replaced; a detailed 
description of how to do so can be found inside of fort-module/fmod.F90,
which is as follows. Any instances of the use of IARGC must also be replaced
with finfo%mod_argc.

fmod.F90 annotated file:
      module fmod
      use iso_c_binding
      implicit none

      ! Binding of reduced executive info structure from C; necessary
      ! for all Fortran openDIEL modules
      type, bind(c) :: IEL_fort_info_t
        integer(c_int) :: num_modules
        integer IEL_comm
        integer module_comm
        integer module_copy_comm
        integer(c_int) :: IEL_rank
        integer(c_int) :: module_rank
        integer(c_int) :: module_copy_rank
        integer(c_int) :: IEL_num_ranks
        integer(c_int) :: module_num_ranks
        integer(c_int) :: module_copy_num_ranks
        integer(c_int) :: module_num
        integer(c_int) :: module_copy_num
        integer(c_int) :: tuple_size
        integer(c_int) :: mod_argc
        type(c_ptr) :: mod_argv_len
        type(c_ptr) :: mod_argv
      end type IEL_fort_info_t

      contains

      ! Replace fortran program block with subroutine
      subroutine fortran_routine(finfo) bind(c,name='fortran_routine')
      use iso_c_binding
      implicit none
      type(IEL_fort_info_t) :: finfo

      ! Where the argv and its information are placed; necessary to
      ! initialize
      integer, pointer, dimension(:) :: f_string_len
      integer :: i
      type(c_ptr), dimension(:), pointer :: cptr
      character(c_char), pointer, dimension(:) :: arg_string
      
      ! Set up placement of argv element inside of f_string; also
      ! necessary to initialize
      call c_f_pointer( finfo%mod_argv_len, f_string_len, &
        (/ finfo%mod_argc /) )
      call c_f_pointer( finfo%mod_argv, cptr, (/ finfo%mod_argc /) )

      ! The following block of code represents where a user's code
      ! (or a call to a subroutine that contains the user's code) would
      ! go.
      ! Example code to place each element of argv inside f_string and
      ! print it; to use in module, replace i with the element of argv
      ! to use. Must replace any instances of the use of GETARG.
      do i=lbound(cptr, 1),ubound(cptr,1)
        call c_f_pointer(cptr(i), arg_string, (/ f_string_len(i) + 1 /))
        print*,'Element ', i, ' of argv is ', &
          arg_string(1:f_string_len(i))
      enddo

      ! Important note: all instances of MPI_COMM_WORLD must be replaced
      ! with finfo%module_copy_comm, and all instances of IARGC must be
      ! replaced with finfo%mod_argc

      return
      end subroutine
      end module fmod
