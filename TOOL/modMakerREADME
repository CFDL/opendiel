modMaker README

syntax:
    modMaker [option list] input_file

    The input_file is the source file which contains the primary entry point ("main")
    for your code. It should be given as the relative path from the directory specified
    by the -s option.

options:
    Most options have default values, which are listed below. If the default
    value is acceptable, the option does not need to be specified at the
    command line. But all options must be correctly set either way for 
    the script to work. No parameters should be in quotes!

    -l LANGUAGE or --language LANGUAGE
        LANGUAGE is either "C", "Fortran" or "C++" (no quotes). Default 
        is C.

    -n NAME or --name NAME
        The name refers to the name given to the DIEL module you want to
        create. This name will be used for the primary entry point (function) for the
        module, replacing the name of the function specified with -f. You 
        probably will want to change this. Default is "module0".

    -f FUNCTION_NAME or --function FUNCTION_NAME
        This is the current, non-DIEL name for the primary entry point of your
        code. For C, this will usually be "main". Default is "main". 
        
        Note that for Fortran, if it is "PROGRAM XYZ" this option needs to be 
        specified as "XYZ". Pattern matching is done with case ignored, so 
        if your primary entry point is "PROGRAM MAIN", the default value will 
        still work. 

    -s DIRECTORY or --src_dir DIRECTORY
        The top-level directory of the source code you want to integrate with 
        DIEL. This means it is the lowest directory that contains all the source
        files within itself or within subdirectories. This has no default value.

    -p or --parallel
        This specifies that the code is a parallel (MPI) code, which changes
        modMaker's behavior somewhat. The default behavior is for a serial
        code.
