# This is the worker program that uses python to do work
# It can read in a c file and return a "package" used under the IEL model

import sys
from array import *
import os

#Global Flags

#initRemoved = False
#finRemoved = False
#mainRemoved = False

#Global Buffer

status = ""
lineCounter = 1
lines = []

################# Display Functions #################

# Clears the screen for displaying messages
def clearScreen ():
	print("\033c")

# Shows debug messages without being cleared (not exactly by not clearing)
def debug (s):
	showProgress (s)

# Shows the archived "more-important" messages that would "not be cleared"
def displayArchive ():
	clearScreen ()
	print status

# Display a one-time message
def displayMsg (s):
	displayArchive ()
	print s

# Shows progress and archive the progress
def showProgress (s):
	global status
	status = status + s + "\n"
	displayArchive ()

# Displays a line with its highlight beneath certain region
def displayWithHighlight (lineNumber, line, start, end):
	print "%d)	%s" % (lineNumber, line[:len(line) - 1])
	count = line.count ("\t")

	tempString = "\t" * (count + 1)
	tempString = tempString + " " * (start - count)
	tempString = tempString + "-" * (end - start + 1)
	tempString = tempString + "\n"

	print tempString

################# Worker Functions #################

# Description:		Validate user-input path
# Usage:			pathName = validatePath (pathName, True)

# Variables:		[str]	pathName:	user-input pathname
#					[bool]	existing:	whether an existing or non-existing pathName is needed
#					[bool]	directory:	whether a directory or file pathName is needed, true refers to directory

# Returns			[str]	a valid pathName as-per required

def validatePath (pathName, existing, directory):

	while existing and pathName == "":
		if directory:
			pathName = raw_input ("Please specify a directory for transformation: ").strip()
		else:
			pathName = raw_input ("Please specify a file for transformation: ").strip()

	if existing and directory:
		while not os.path.exists (pathName):
			print "\"%s\" doesn't exists" % (pathName)
			pathName = raw_input ("Please enter an existing dirname: ").strip()
		while not os.path.isdir (pathName):
			print "\"%s\" is not a directory" % (pathName)
			pathName = raw_input ("Please enter an existing dirname: ").strip()

	if existing and not directory:
		while not os.path.exists (pathName):
			print "\"%s\" doesn't exists" % (pathName)
			pathName = raw_input ("Please enter an existing filename: ").strip()
		while not os.path.isfile (pathName):
			print "\"%s\" is not a file" % (pathName)
			pathName = raw_input ("Please enter an existing filename: ").strip()


	if not existing and directory:
		while os.path.exists (pathName):
			print "\"%s\" exists already" % (pathName)
			pathName = raw_input ("Please enter a non-existing dirname or leave blank for auto-generated directory: ").strip()

	if not existing and not directory:
		while os.path.exists (pathName):
			print "\"%s\" exists already" % (pathName)
			pathName = raw_input ("Please enter a non-existing filename or leave blank for auto-generated filename: ").strip()


	return pathName

# Description:		Find the previous occurance of string s in line with respect of startLocation
# Usage:			void function (); int main ();
#					This function can locate the first ";"

# Variables:		[str]	line:			the line in question
#					[int]	startLocation:	the token must be before this position
#					[str]	s:				token in question
# Returns			[int]	location of token. -1 if not found.

def findLastSeperator (line, startLocation, s):

	location = line.find (s)
	prev = location

	if location == -1 or location > startLocation:
		return -1

	while (location < startLocation):
		prev = location
		location = line.find (s, location + 1)
		if location == -1:
			return prev

	return prev



# Description:		Determines if a given character is alpha-numeric. (10 numbers, 26 characters of upper- and lowercases and underscore)
# Usage:			if isAlphanumeric (c): ...

# Variables:		[char]	c:	character in question
# Returns			[bool]	True if c is alphanumeric. False if c is not

def isAlphanumeric (c):

	valid = False
	if (ord (c) >= 48 and ord (c) <= 57):
		valid = True
	if (ord (c) >= 65 and ord (c) <= 90):
		valid = True
	if (ord (c) >= 97 and ord (c) <= 122):
		valid = True
	if (ord (c) == 95):
		valid = True

	return valid


# Description:		Checks if a sub-line contains only spaces (or space-equivalent, ie. tab)
# Usage:			if spaceOnly (line, startLocation, endLocation): {do sth}

# Variables:		[str]	line:			the line in question
#					[int]	startLocation:	start of substring
#					[int]	endLocation:	end of substring

# Example:			0 1 2 3 4 5 6 7 8 9 
#					w o r d     w o r d
#					Pass in (line, 4, 6)
# Returns			[bool]	True if there's only spaces. False otherwise.

def spaceOnly (line, startLocation, endLocation):

	if startLocation >= endLocation:
		return False

	valid = True

	for counter in xrange (endLocation - startLocation):
		if line [startLocation + counter] != ' ' and line [startLocation + counter] != '\t':
			valid = False

	return valid

# Description:		To prevent overwriting existing files by adding numbers at the back
# Usage:			outputFileName = sequenced (outputFileName)

# Variables:		[str]	filename:	the original filename
# Returns			[str]	filename:	the "safe", "updated" filename	

def sequenced (filename):

	temp = filename
	counter = 1

	while os.path.exists (temp):
		temp = filename + "_" + str(counter)
		counter += 1

	return temp



################# User Communication #################

# Description:		Let the user decide whether or not to make a change
# Usage:			if promptChange (line, ln, start, end, reason): {do sth}

# Variables:		[str]	line:			the line in question
#					[int]	linenumber:		linenumber to be displayed
#					[int]	start:			start of questionable segment
#					[int]	end:			end of questionable segment
#					[str]	reason:			reason for change
# Returns			[bool]	True if user accepts the change. False if not.

def promptChange (line, lineNumber, start, end, reason):

	if start == end:
		return line

	print "We have found something that may need to be changed in your code for the following reason:\n"
	print "\t%s\n" % (reason)

	end = end - 1

	while (True):

		displayWithHighlight (lineNumber, line, start, end)

		option = raw_input ("If you agree on this change press [Enter] or key in [y/Y]. If it is incorrect, key in [n/N] ").strip()
	
		if option == "" or option == "y" or option == "Y":
			return True
		elif option == "n" or option == "N":
			return False
		else:
			print "Unrecognized input. Please try again."


# Description:		Receive file related inputs and options from user
# Usage:			(inputfile, outputfile, mode, directory, moduleName) = receiveArguments (argv)

# Variables:		[list]	argv:	System arguments vector
# Returns			[tuple]	tuple of (input_filename, output_filename, mode, directory, module_name)
# Representations:	modes: 	A will replace original files which will then be put into an archive
#							R replaces without archiving
#							C duplicates before changing. Original copy is not affected
#					direc:	D means that modMaker makes changess on a directory
#							F means that modMaker makes changes on a file

def receiveArguments (argv):
	clearScreen ()
	mode = "A"
	directory = "F"
	modeSpec = False
	dirSpec = False 

	li = sorted (sys.argv[1:])
	counter = 0
	
	while (counter < len(li) and li[counter][0] == "-"):

		if len(li[counter]) == 1:
			print "Please check on input format and spacing. Unrecognized Input!"
			sys.exit (1)

		elif li[counter][1] == "r" or li[counter][1] == "R":
			if not modeSpec:
				mode = "R"
				modeSpec = True
			else:
				print "Overlapped Flags!! %s, %s" % ("-" + mode, "-R")
				sys.exit (1)

		elif li[counter][1] == "c" or li[counter][1] == "C":
			if not modeSpec:
				mode = "C"
				modeSpec = True
			else:
				print "Overlapped Flags!! %s, %s" % ("-" + mode, "-C")
				sys.exit (1)

		elif li[counter][1] == "a" or li[counter][1] == "A":
			if not modeSpec:
				mode = "A"
				modeSpec = True
			else:
				print "Overlapped Flags!! %s, %s" % ("-" + mode, "-A")
				sys.exit (1)

		elif li[counter][1] == "d" or li[counter][1] == "D":
			if not dirSpec:
				directory = "D"
				dirSpec = True
			else:
				print "Overlapped Flags!! %s, %s" % ("-" + directory, "-D")
				sys.exit (1)

		elif li[counter][1] == "f" or li[counter][1] == "F":
			if not dirSpec:
				directory = "F"
				dirSpec = True
			else:
				print "Overlapped Flags!! %s, %s" % ("-" + directory, "-F")
				sys.exit (1)

		elif li[counter][1] != "o" and li[counter][1] != "O":
			print "Unrecognized Flag %s" % (li[counter])

		counter += 1

	counter = 1
	inputFileName = ""
	outputFileName = ""
	moduleFlag = False
	moduleName = ""

	while (counter < len (sys.argv)):
		if moduleFlag and moduleName == "":
			moduleName = sys.argv [counter]

		elif sys.argv [counter][0] != "-":
			if inputFileName == "":
				inputFileName = sys.argv [counter]
			elif outputFileName == "":
				outputFileName = sys.argv [counter]

		if sys.argv [counter] == "-o" or sys.argv [counter] == "-O":
			if not moduleFlag:
				moduleFlag = True
			else:
				print "Overlapped Flags!! %s, %s" % (sys.argv [counter], "-O")
				sys.exit (1)

		counter += 1

	if mode == "C": #--------------------------------
		if directory == "F":
			inputFileName = validatePath (inputFileName, True, False)
			outputFileName = validatePath (outputFileName, False, False)
		else:
			inputFileName = validatePath (inputFileName, True, True)
			outputFileName = validatePath (outputFileName, False, True)

	elif mode == "R": #--------------------------------
		if directory == "F":
			inputFileName = validatePath (inputFileName, True, False)
		else:
			inputFileName = validatePath (inputFileName, True, True)

		if outputFileName != "":
			displayMsg ("You have chosen to use the replace option. Changes will be made directly to the original file and %s doesn't matter\n" % (outputFileName))
			
			warning = raw_input ("Type \"exit\" to end program or [Enter] to continue: ")
			while warning != "" and warning != "exit":
				warning = raw_input ("Type \"exit\" to end program or [Enter] to continue: ")

			if warning == "exit":
				sys.exit (1)

	elif mode == "A": #--------------------------------
		if directory == "F":
			inputFileName = validatePath (inputFileName, True, False)
		else:
			inputFileName = validatePath (inputFileName, True, True)

	if directory == "D":
		dirD = "directory"
	else:
		dirD = "file"

	clearScreen()
	if moduleName == "":
		moduleName = "first"

	if mode == "A":

		if outputFileName == "":
			outputFileName = "Archive_modMaker"

		(head, tail) = os.path.split (inputFileName)

		if head:
			outputFileName = os.path.join (head, outputFileName)

		showProgress ("Transforming %s \"%s\" into module \"%s\"" % (dirD, inputFileName, moduleName))
		showProgress ("Old files will be put into directory %s" % (outputFileName))

	elif mode == "R":
		showProgress ("Transforming %s \"%s\" into module \"%s\" which will replace the original %s" % (dirD, inputFileName, moduleName, dirD))
		outputFileName = ""

	elif mode == "C":

		if outputFileName == "" and directory == "F":
			(head, tail) = os.path.split (inputFileName)
			outputFileName = "module_" + tail

			if head:
				outputFileName = os.path.join (head, outputFileName)

			outputFileName = sequenced (outputFileName)

		if outputFileName == "" and directory == "D":

			(head, tail) = os.path.split (inputFileName)
			outputFileName = "Module_" + tail

			if head:
				outputFileName = os.path.join (head, outputFileName)
				
			outputFileName = sequenced (outputFileName)

		showProgress ("Duplicating %s \"%s\" into a module under the name \"%s\" and filename \"%s\"" % (dirD, inputFileName, moduleName, outputFileName))
		showProgress ("Original files will not be lost")

	return (inputFileName, outputFileName, mode, directory, moduleName)



# Description:		Receive consent to speed up replacement process
# Usage:			consents = (inputFileName, strings)

# Variables:		[str]		inputFileName:	the file to undergo the process
#					list[str]	stringList:		the strings to look for
# Returns			list[bool]	consents:		the consents [True, True, False, ...] compiled
#					[bool]		continue:		whether or not the file should continue transformation

def receiveConsent (inputFileName, stringList):
	
	if len (stringList) == 0:
		return None

	if not os.path.exists (inputFileName):
		debug ("File doesn't exists")
		sys.exit (1)

	consent = []

	option = "s"
	while (option != "" and option != "N" and option != "n"):
		option = raw_input ("We are about to conduct transformation on %s.\nPress [Enter] to begin or key-in \"n/N\" to skip this file.  " % (inputFileName))

	if option == "":

		inputFile = open (inputFileName, 'r')
		lines = inputFile.readlines ()
		inputFile.close ()

		for string in stringList:

			displayArchive()
			lineNumber = 1
			counter = 0

			for line in lines:
				start = line.find (string)
				if start != -1:
					displayWithHighlight (lineNumber, line, start, start + len(string))
					counter += 1
				lineNumber += 1

			if counter != 0:
				option = "s"
				while (option != "" and option != "N" and option != "n"):
					option = raw_input ("We have found %d occurances of %s.\nPress [Enter] to authorize every change or key-in \"n/N\" to authorize one-by-one." % (counter, string))
				
				if option == "":
					consent.append (True)

					if counter == 1:
						showProgress ("%d %s has been replaced." % (counter, string))
					else:
						showProgress ("%d %s have been replaced." % (counter, string))
				else:
					consent.append (False)
					
					if counter == 1:
						showProgress ("Suspected %s remains unchanged." % (string))
					else:
						showProgress ("%d suspected %ss remain unchanged." % (counter, string))

			else:
				consent.append (False)

		return (consent, True)

	else:

		for string in stringList:
			consent.append (False)

		return (consent, False)
	

################# Checking Functions #################

#	Wrapper function for checking all function titles
def checkFunctionTitle (line, moduleName):
	line = checkMain (line, moduleName)
	return line

#	Wrapper function for checking all in-line statements
def checkStatement (line):
	line = checkInit (line)
	line = checkFinalize (line)
	return line

#	Wrapper function for checking all in-line variables
def checkVariable (line, consents):
	if consents [0]:
		line = line.replace ("MPI_COMM_WORLD", "exec_info->module_copy_comm")
	else:
		line = checkMPICommWorld (line)
	return line

#	Defines parameters to check the main
def checkMain (line, moduleName):
	msgB = []
	msgB.append ("Only one Main can exist and it belongs in the driver.c")
	msgB.append ("Main has been replaced")
	msgB.append ("Suspected Main remained unchanged")
	replacement = "int " + moduleName + " (IEL_exec_info_t *exec_info)"
	return baseCheck (line, ["int", "main"], "}", ")", msgB, replacement, "")

#	Defiens parameters to check MPI_Init
def checkInit (line):
	msgB = []
	msgB.append ("Only one Init can exist and it belongs in the driver.c")
	msgB.append ("MPI_Init has been removed")
	msgB.append ("Suspected MPI_Init remained unchanged")
	return baseCheck (line, ["MPI_Init"], ";", ";", msgB, "", "int")

#	Defines parameters to check MPI_Finalize
def checkFinalize (line):
	msgB = []
	msgB.append ("Only one Finalize can exist and it belongs in the driver.c")
	msgB.append ("MPI_Finalize has been removed")
	msgB.append ("Suspected MPI_Finalize remained unchanged")
	return baseCheck (line, ["MPI_Finalize"], ";", ";", msgB, "", "int")

#	Defines parameters to check MPI_COMM_WORLD
def checkMPICommWorld (line):
	msgB = []
	msgB.append ("MPI_COMM_WORLD has been divided into subcommunicators")
	msgB.append ("MPI_COMM_WORLD has been replaced")
	msgB.append ("Suspected MPI_COMM_WORLD remained unchanged")
	return baseCheck (line, ["MPI_COMM_WORLD"], "", "", msgB, "exec_info->module_copy_comm", "")






# Description:		Base checking function which is versatile and powerful
# Usage:			line = baseCheck (...)

# Variables:		[str]	line:			the line in question
#					[list]	tokenList:		list of tokens to match to
#					[str]	prevSep:		Separator that is likely to lead the segment in question. Replacement starts right afterwards. Starts from start of token match if omitted.
#					[str]	nextSep:		Separator that is likely to follow the segment. Replacements continues to this point. Ends at end of token matach if omitted.
#					[list]	msgBundle:		0: contains "reasons", why the segment needs to be replaced / removed
#											1: contains "successful msg", showing that the segment was properly dealt with
#											2: contains "unchanged msg", showing that the segment was not affected
#					[str]	replacement:	if omittied, substring in concern is commented out. Otherwise, provides replacement for substring.
# 					[str]	returnsValue:	if set, the replacement concerns a function which returns value type specified. This only affects "commenting out"
# Returns:			line that can be altered or unchanged

def baseCheck (line, tokenList, prevSep, nextSep, msgBundle, replacement, returnsValue):

	firstPos = []
	for token in tokenList:
		pos = line.find (token)
		if pos == -1:
			return line
		else:
			firstPos.append (pos)

	candidates = findCandidates (line, tokenList, firstPos)

	if replacement == "":
		replace = False
	else:
		replace = True

	for (begin, end) in reversed (candidates):
		valid = True
		if begin != 0:
			if isAlphanumeric (line [begin - 1]):
				valid = False

		if isAlphanumeric (line [end]):
			valid = False

		if valid:

			typePos = -1

			if nextSep != "":
				end = line.find (nextSep, end + 1) + 1

			if returnsValue != "":
				typePos = findLastSeperator (line, begin, returnsValue)

			if prevSep != "":
				temp = findLastSeperator (line, begin, prevSep)
				if temp != -1:
					begin = temp + 1
				else:
					begin = 0

			if promptChange (line, lineCounter, begin, end, msgBundle [0]):
				if replace:
					output = ""
					output = output + line [:begin]
					output = output + replacement
					output = output + line [end:]
					showProgress (msgBundle [1])
					line = output
				else:
					output = ""

					if typePos != -1 and typePos >= begin and typePos < end:
						equals = line.find ("=", typePos)
						if equals != -1 and equals > typePos:
							if line[equals - 1] == " ":
								output = output + line [:equals - 1]
							else:
								output = output + line [:equals]

							output = output + ";\n"
					elif begin != 0:
						output = output + line [:begin]
						output = output + "\n"

					output = output + "//"
					output = output + line [begin : end]
					if line[end] != "\n":
						output = output + "\n"
					output = output + line [end:]
					showProgress (msgBundle [1])
					line = output
			else:
				showProgress (msgBundle [2])

	return line





# Description:		Verstaile token matching function
# Usage:			candidates = findCandidates (line, tokenList, firstPos)

# Variables:		[str]	line:			the line in question
#					[list]	tokenList:		list of tokens to match to
#					[str]	prevSep:		Separator that is likely to lead the segment in question. Replacement starts right afterwards. Starts from start of token match if omitted.
#					[str]	nextSep:		Separator that is likely to follow the segment. Replacements continues to this point. Ends at end of token matach if omitted.
#					[list]	msgBundle:		0: contains "reasons", why the segment needs to be replaced / removed
#											1: contains "successful msg", showing that the segment was properly dealt with
#											2: contains "unchanged msg", showing that the segment was not affected
#					[str]	replacement:	if omittied, substring in concern is commented out. Otherwise, provides replacement for substring.

def findCandidates (line, tokenList, firstPos):

	locationDictionary = []
	result = []
	counter = 0
	arraySize = len (tokenList)

	for token in tokenList:

		locationArray = []

		while (firstPos [counter] != -1):
			locationArray.append (firstPos [counter])
			firstPos [counter] = line.find (tokenList[counter], firstPos [counter] + 1)

		counter += 1
		locationDictionary.append (locationArray)

	accessArray = []
	for i in xrange (arraySize):
		accessArray.append (0)

	while (True):
		valid = True
		for i in xrange (arraySize - 1):
			j = accessArray [i]
			k = accessArray [i + 1]

			if locationDictionary [i][j] > locationDictionary [i+1][k]:
				valid = False
				accessArray [i + 1] += 1
				if accessArray [i + 1] >= len (locationDictionary [i + 1]):
					return result
			elif not spaceOnly (line, locationDictionary [i][j] + len (tokenList [i]), locationDictionary [i + 1][k]):
				valid = False
				for c in xrange (i + 1):
					accessArray [c] += 1
					if accessArray [c] >= len (locationDictionary [c]):
						return result
		if valid:
			begin = locationDictionary [0][accessArray [0]]
			end = locationDictionary [arraySize - 1][accessArray [arraySize - 1]] + len (tokenList [arraySize - 1])
			result.append ((begin, end))
			accessArray [i] += 1
			if accessArray [i] >= len (locationDictionary [i]):
				return result

#################
