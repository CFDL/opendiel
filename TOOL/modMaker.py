import worker
import sys
import os

# Description:		Transform a file under the copy mode
# Usage:			copyMode (inputFileName, outputFileName, moduleName)

# Variables:		[string] inputFileName:		the file to be changed
#					[string] outputFileName:	the filename of the file to be output
#					[string] moduleName:		the name of module to be created

def copyMode (inputFileName, outputFileName, moduleName):
	
	(consents, cont) = worker.receiveConsent (inputFileName, ["MPI_COMM_WORLD"])
	if not cont:
		worker.showProgress ("%s has been left untouched." % (inputFileName))
		return

	try:
		inputFile = open (inputFileName, 'r')
		outputFile = open (outputFileName, 'w')

		for line in inputFile:
			line = worker.checkFunctionTitle (line, moduleName)
			line = worker.checkStatement (line)
			line = worker.checkVariable (line, consents)
			outputFile.write (line)

			worker.lineCounter += 1

		inputFile.close ()
		outputFile.close ()
	except:
		print "Unexpected error:", sys.exc_info()[0]
		if os.path.exists (outputFileName):
			os.remove (outputFileName)
		extention = len (inputFileName)
		print "--------------------------------------------------------------------" + "-" * (extention + 1)
		print "Module building in %s have been reverted while prior changes retained." % (inputFileName)
		print "--------------------------------------------------------------------" + "-" * (extention + 1)
		raise

# Description:		Transform a file under the replace mode
# Usage:			replaceMode (inputFileName, outputFileName, moduleName)

# Variables:		[string] inputFileName:		the file to be changed
#					[string] moduleName:		the name of module to be created

def replaceMode (filename, moduleName):

	(consents, cont) = worker.receiveConsent (filename, ["MPI_COMM_WORLD"])
	if not cont:
		worker.showProgress ("%s has been left untouched." % (filename))
		return

	inputFile = open (filename, 'r')
	lines = inputFile.readlines ()
	inputFile.close ()
	
	try:
		outputFile = open (filename, 'w')

		for line in lines:
			line = worker.checkFunctionTitle (line, moduleName)
			line = worker.checkStatement (line)
			line = worker.checkVariable (line, consents)
			outputFile.write (line)

			worker.lineCounter += 1

		outputFile.close ()
	except:
		print "Unexpected error:", sys.exc_info()[0]
		outputFile = open (filename, 'w')
		for line in lines:
			outputFile.write (line)
		outputFile.close ()
		extention = len (filename)
		print "------------------------------------------------------------" + "-" * (extention + 1)
		print "Changes in %s have been reverted while prior changes retained." % (filename)
		print "------------------------------------------------------------" + "-" * (extention + 1)

# Description:		Transform a file under the archive mode
# Usage:			archiveMode (inputFileName, outputFileName, moduleName)

# Variables:		[string] inputFileName:		the file to be changed
#					[string] outputFileName:	the filename of the archived file
#					[string] moduleName:		the name of module to be created

def archiveMode (inputFileName, outputFileName, moduleName):

	outputFileName = worker.sequenced (outputFileName)	

	(consents, cont) = worker.receiveConsent (inputFileName, ["MPI_COMM_WORLD"])
	if not cont:
		worker.showProgress ("%s has been left untouched." % (inputFileName))
		return

	inputFile = open (inputFileName, 'r')
	lines = inputFile.readlines ()
	inputFile.close ()
	
	try:
		outputFile = open (inputFileName, 'w')
		archiveFile = open (outputFileName, 'w')

		for line in lines:
			archiveFile.write (line)
		
		wrote_include = 0
		for line in lines:
			if line.find("#include") > -1 and not wrote_include: 
				outputFile.write("#include \"" + moduleName + ".h\"\n")
				wrote_include = 1
			if line.find("main") > -1 and line.find("int argc") > -1 and line.find("char"):
				if line.find("{") > -1: 
					outputFile.write("  int argc = IEL_ARGC(exec_info);\n")
					outputFile.write("  int argv = IEL_ARGV(exec_info);\n")
				else: 
					lines.insert(lines.index(line) + 2, "int argc = IEL_ARGC(exec_info);\n")
					lines.insert(lines.index(line) + 3, "char **argv = IEL_ARGV(exec_info);\n")
			line = worker.checkFunctionTitle (line, moduleName)
			line = worker.checkStatement (line)
			line = worker.checkVariable (line, consents)
			outputFile.write (line)

			worker.lineCounter += 1

		outputFile.close ()
		archiveFile.close ()
	except:
		print "Unexpected error:", sys.exc_info()[0]
		outputFile = open (inputFileName, 'w')
		for line in lines:
			outputFile.write (line)
		outputFile.close ()
		if os.path.exists (outputFileName):
			os.remove (outputFileName)
		extention = len (inputFileName)
		print "------------------------------------------------------------" + "-" * (extention + 1)
		print "Changes in %s have been reverted while prior changes retained." % (inputFileName)
		print "------------------------------------------------------------" + "-" * (extention + 1)
		raise

status = ""
(inputFileName, outputFileName, mode, directory, moduleName) = worker.receiveArguments (sys.argv)
option = "l"
while option != "" and option != "exit":
	option = raw_input ("Press Enter to Continue or type \"exit\" to quit: ")

if option == "exit":
	sys.exit (0)

if directory == "F":
	if mode == "C": #--------------------------------

		copyMode (inputFileName, outputFileName, moduleName)

	elif mode == "R": #--------------------------------

		replaceMode (inputFileName, moduleName)

	elif mode == "A": #--------------------------------

		if not os.path.exists (outputFileName):
			try:
				os.makedirs (outputFileName)
			except OSError:
				worker.displayMsg ("We do not have the permission to create a directory OR disk space is full OR directory already exists. Please either:\n\n 1)\t create a directory;\n 2)\t remove some files; OR\n 3)\t use another mode\n")
				sys.exit (1)

		outputFileName = os.path.join (outputFileName, inputFileName)
		archiveMode (inputFileName, outputFileName, moduleName)

elif directory == "D":
	if mode == "C":

		if not os.path.exists (outputFileName):
			try:
				os.makedirs (outputFileName)
			except OSError:
				worker.displayMsg ("We do not have the permission to create a directory OR disk space is full OR directory already exists. Please either:\n\n 1)\t create a directory;\n 2)\t remove some files; OR\n 3)\t use another mode\n")
				sys.exit (1)
		
		for dirname, dirnames, filenames in os.walk(inputFileName):
		    for filename in filenames:
		    	if not filename.startswith ('.'):
				
					iName = os.path.join (dirname, filename)
					oName = os.path.join (outputFileName, filename)
					oName = worker.sequenced (oName)

					worker.showProgress ("\n=====In file %s=====\n" % (iName))
					copyMode (iName, oName, moduleName)

	elif mode == "R":
		
		for dirname, dirnames, filenames in os.walk(inputFileName):
		    for filename in filenames:
		    	if not filename.startswith ('.'):
					iName = os.path.join (dirname, filename)

					worker.showProgress ("\n=====In file %s=====\n" % (iName))
					replaceMode (iName, moduleName)

	elif mode == "A":
		
		if not os.path.exists (outputFileName):
			try:
				os.makedirs (outputFileName)
			except OSError:
				worker.displayMsg ("We do not have the permission to create a directory OR disk space is full OR directory already exists. Please either:\n\n 1)\t create a directory;\n 2)\t remove some files; OR\n 3)\t use another mode\n")
				sys.exit (1)	

		for dirname, dirnames, filenames in os.walk(inputFileName):
		    for filename in filenames:
		    	if not filename.startswith ('.'):
					iName = os.path.join (dirname, filename)
					
					worker.showProgress ("\n=====In file %s=====\n" % (iName))
					archiveMode (iName, os.path.join (outputFileName, filename), moduleName)
		
