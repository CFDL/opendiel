# CONTENTS

1. **DIRECTORY STRUCTURE** -Listing of what's in this directory
1. **INTRODUCTION** -Introduction to openDIEL
1. **BUILDING** -Instructions on how to build openDIEL
1. **TODO** - List of work that still needs to be done

# DIRECTORY STRUCTURE

Throughout this repository, directories will be in all caps, files and
executables will be lowercase.

| DIRECTORY    | DESCRIPTION                                                                                          |
| ------------ | ---------------------------------------------------------------------------------------------------- |
| APPLICATIONS | Contains applications of openDIEL                                                                    |
| EXAMPLES     | Contains examples of basic openDIEL functionality                                                    |
| BIN          | Contains openDIEL executables after building                                                         |
| DOCS         | Documentation for openDIEL                                                                           |
| EXTLIB       | This directory contains external libraries that may be automatically compiled when building openDIEL |
| GUI          | Contains files for a GUI. Currently not implemented                                                  |
| INC          | Contains header files for various machines that openDIEL                                             |
| LIB          | Contains openDIEL libraries and external libraries after building                                    |
| Makefile     | This makefile is used to build openDIEL. Requires a Makefile.inc to be present to be used            |
| MAKE.INC     | Contains Makefile.inc files for various machines                                                     |
| README       | Contains readme files for various machines with special instructions about how to compile            |
| SRC          | Contains openDIEL source code                                                                        |
| TOOL         | Contains openDIEL tools and scripts to assist in helping to build modules for openDIEL               |

# INTRODUCTION

Open Interoperable Distributive Executive Library (openDIEL) is a workflow
engine designed for usage in HPC environments. It consists of a set of C code,
combined with openMPI functions to unify many different modules of computation
under a single executable.

Examples of usage can be found in EXAMPLES. These examples will show the
various ways in which openDIEL is used, and ways to specify workflows in
Workflow Configuration Files.

# BUILDING

To build, you need to use a Makefile.inc for your machine found in MAKE.INC
directory, or create a new one for your machine based on the generic template.
This file must be placed in the same directory as the toplevel Makefile before
building.

If you do use a Makefile.inc that has been prepared for your machine, make sure
to check to the top of the file, as there are may be modules that may need to
be loaded.

Then, you will need to type 'make source' to build openDIEL. A detailed
listing of ways to build can be found by typing 'make' with no arguments.

Note that openDIEL depends on the external library 'libconfig'. As such, to
build openDIEL, it will be downloaded and installed.

## Commonly used build rules:

| BUILD TARGET             | DESRIPTION                                                                      |
| ------------------------ | ------------------------------------------------------------------------------- |
| `make all`               | Build openDIEL libraries, it's dependencies, and basic examples                 |
| `make exe`               | Build openDIEL executable, it's dependencies, and basic examples                |
| `make source`            | Build openDIEL libraries                                                        |
| `make extlib`            | Build external libraries                                                        |
| `make examples`          | Build basic examples found in EXAMPLES                                          |
| `make applications`      | Build applications found in APPLICATIONS                                        |
| `make cleanall`          | Remove openDIEL libraries and object files, external libraries and object files |
| `make cleansource`       | Remove openDIEL libraries and object files                                      |
| `make cleanallextlib`    | Remove external libraries and object files                                      |
| `make cleanapplications` | Remove libraries and object files found in APPLICATIONS                         |
| `make cleanexamples`     | Remove libraries and object files found in EXAMPLES                             |

This same list can be obtained by typing 'make'.

# TODO

Update readme.machine files in README[]

- there are readme files for each machine that need to be updated in README, as
  many of them do not reflect the proper way to build openDIEL.
