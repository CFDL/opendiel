#!/bin/sh
rm -rf test.txt

if ! test -f "driverMM"; then
    echo "Please compile before running the test by running make in the TEST directory"
    exit 1
fi

rm -rf HELLO*

mpirun -np 5  ../../../BIN/diel-auto ./workflowAM.cfg > /dev/null 2>&1
mpirun -np 6  ./driverMM ./workflowMM.cfg > /dev/null 2>&1

cat HELLO*/* > test.txt

if ! diff -q test.txt correct.txt &>/dev/null; then
    >&2 echo -e "HELLO WORLD [\e[31mFAILED\e[0m]"
else
    >&2 echo -e "HELLO WORLD [\e[32mPASSED\e[0m]"
fi

